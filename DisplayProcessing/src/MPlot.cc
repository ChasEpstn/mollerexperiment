#include "MPlot.h"
#include "TFile.h"
#include "TH1D.h"
// struct trackInfo{
//   int pid;
//   int trkid;
//   double stepx;
//   double stepy;
//   double stepz;
// };
	makePlot::makePlot(TString name){
		coreName = name;
		// coreName = "at25v1";
		// coreName = "at25v1short";
		// coreName = "epRadCheck";
		// coreName = "testrun2";
		// coreName = "at25run15to35v2_long";
		// coreName = "elasticat25v1";
		// coreName = "elasticAll";
		// coreName = "mRad25";
		// coreName = "mRad25";
		// coreName = "mRad25mCore1";
		// coreName="pScan25C20";
		// coreName = "elasticat25run15to35v2";
		// coreName = "at25run15to35_long";
		// coreName = "at25run15to35v1_short";
		//coreName = "testFib4p5Mev";
		//cout<<"corename is "<<coreName<<endl;
		infile.open("../../build/"+coreName+"_TrackData.txt");
		eventLine = new TEveLine();
		chain = new TChain("DaLiEventTree");
		chain->Add("../../build/"+coreName+".DLg4.root"); //filename relative to location of ReadTreeExample.C
		eve = new DLg4Event();
		chain->SetBranchAddress("DLg4Event",&eve);
		trackOrigins = new TEvePointSet();
		runFlag = false;
	}
	makePlot::~makePlot(){
		    infile.close();
	}

	std::vector<std::string> makePlot::split(const std::string &s, char delim = ' ')
	{

	    std::vector<std::string> elems;

	    std::stringstream ss(s);
	    std::string item;
	    while (std::getline(ss, item, delim))
	    {
	            elems.push_back(item);
	    }
	    return elems;
	}

	void makePlot::makeHistogram(){
		int nEve = (int)chain->GetEntries();
		// printf("file=%s nEve=%d\n",filename.Data(),nEve);

		TH2D *xyHIST = new TH2D("xyHIST","Hit Map",150,-75,75,50,-25,25);
		TH1D *hitEnergies = new TH1D("hitEnergies","Hit Energies",100,0,1);
		xyHIST->Sumw2();
		for (int i = 0; i < nEve; i++){
			//chain->GetEntry(i) dumps data from ith entry into eve
			//access data after getting entry by (for example), printf("%d",eve->GetEventID())
			chain->GetEntry(i);
			// eve->Print();
			//cout<<"Event number "<<i<<"  has hits in "<<endl;
			for(int j = 0; j < eve->GetNumTracks(); j++){
				// printf("Num tracks: %d\n",eve->GetNumTracks());
				int trkHtsX = 0;
				int trkHtsY = 0;
				for(int k = 0; k < eve->GetTrack(j)->GetNumHits();k++){
					// printf("Num  hits: %d\n",eve->GetTrack(j)->GetNumHits());
					// pointset->SetNextPoint(eve->GetTrack(j)->GetOrigin().X()/10.,eve->GetTrack(j)->GetOrigin().Y()/10.,eve->GetTrack(j)->GetOrigin().Z()/10.);
					// printf("Event Number: %d, Track Number: %d, Hit Number: %d\n",i,j,k);
					// printf("Energy: %g\n",eve->GetTrack(j)->Get4Momentum().E());

					hitEnergies->Fill(eve->GetTrack(j)->GetHit(k)->GetDE(),eve->GetWeight());
					double THRESHOLD = 0.0;//MeV
					if(eve->GetTrack(j)->GetHit(k)->GetDE()>THRESHOLD){
						if (eve->GetTrack(j)->GetHit(k)->GetDetectorID().BeginsWith('x')){
							trkHtsX += 1;
							//cout<<atoi(eve->GetTrack(j)->GetHit(k)->GetDetectorID()(9,3).Data())<<",";//Det num
							//cout<<atoi(eve->GetTrack(j)->GetHit(k)->GetDetectorID()(7,1).Data())<<",";//Layer num
						}
						if (eve->GetTrack(j)->GetHit(k)->GetDetectorID().BeginsWith('y')){
							trkHtsY += 1;
						}
					//	xyHIST->Fill(eve->GetTrack(j)->GetHit(k)->GetLocalPosition().Z(),eve->GetTrack(j)->GetHit(k)->GetLocalPosition().X(),eve->GetWeight());
					}
				cout<<"Event,"<<i<<",track,"<<k<<",det,"<<eve->GetTrack(j)->GetHit(k)->GetDetectorID()<<",dE,"<<eve->GetTrack(j)->GetHit(k)->GetDE()<<endl;	
				}
				// printf("Num hits: %g",eve->GetTrack(j)->GetNumHits());
			}
			//cout<<endl;
		}
		TString HistogramName = coreName + "_SpecHistogram.root";
		TFile f(HistogramName,"recreate");
		TH1D *momentumHist = xyHIST->ProjectionX();
		hitEnergies->Write();
		xyHIST->Write();
		momentumHist->Write();
		f.Close();
		// momentumHist->Draw();
		//xyHIST->Draw("colz");
		//hitEnergies->Draw();

	}
	void makePlot::EventDisplay(){
		// gInterpreter->GenerateDictionary("std::map<int,int,double,double,double>","map;map"); 
		// eventLine->SetLineColor(kMagenta);

		// gSystem->Load("libGeom");
		// gSystem->Load("libEve");
		// gSystem->Load("/home/cepstein/.darklight/x86_64/lib/libDLg4Event.so");
		// gSystem->Load("/home/cepstein/.darklight/x86_64/lib/libDLmadEvent.so");
		// TGeoManager::Import("RootMoller1.gdml");
		// TEveManager *tem = new TEveManager(1000,1000);
		// // TGLViewer *tgl = tem->GetDefaultGLViewer();
		// // tem->GetGeometry("RootMoller1.gdml");
		// // // gGeoManager->GetTopVolume()->Draw("ogl");

		// // TGLViewer * v = tem->GetDefaultGLViewer();
		// TEveManager::Create(kFALSE);
		// // TEveViewer *eve_v = new TEveViewer("Eve Viewer");
		// // eve_v->SpawnGLViewer();
		// // // TGLViewer *tv = eve_v->GetGLViewer();
		// // // tv->Show();
		// // // eve_v->SetGLViewer(tv, tv->GetFrame());

		// // eve_v->IncDenyDestroy();
		// // eve_v->AddScene(gEve->GetEventScene());
		// // gEve->GetViewers()->AddElement(eve_v);
		// gEve->SpawnNewViewer("Event Viewer");
		// // gEve->SpawnNewScene();
		// // gGeoManager=gEve->GetGeometry("RootMoller1.gdml");
		// gGeoManager->DefaultColors();
		// // gGeoManager->CloseGeometry();
		// etn = new TEveGeoTopNode(gGeoManager,gGeoManager->GetTopNode(),1,3,200);
		// // etn->Draw("ogl");
		// gEve->AddGlobalElement(etn); 
		// gEve->Redraw3D();

		// chain->Print();

		TGMainFrame * tgmf = new TGMainFrame(gClient->GetRoot(),1000,480);
		TGVerticalFrame *tgvf = new TGVerticalFrame(tgmf,1000,480);
		TGHorizontalFrame *tghf = new TGHorizontalFrame(tgvf,100,10);

		TGTextButton *button0 = new TGTextButton(tghf,"Previous",1);
		TGTextButton *button1 = new TGTextButton(tghf,"Next",1);

		tghf->AddFrame(button0,new TGLayoutHints(kLHintsLeft,0,0,0,0));
		tghf->AddFrame(button1,new TGLayoutHints(kLHintsLeft,0,0,0,0));

		button0->Connect("Clicked()","makePlot",this,"PlotPrev()");
		button1->Connect("Clicked()","makePlot",this,"PlotNext()");


		
		tghf->MapSubwindows();
		tghf->MapWindow();

		TGLEmbeddedViewer * V = new TGLEmbeddedViewer(tgvf);
		tgvf->AddFrame(V->GetFrame(),new TGLayoutHints(kLHintsExpandY|kLHintsExpandX,0,0,0,0));
		tgvf->AddFrame(tghf,new TGLayoutHints(kLHintsExpandX,0,0,0,0));
		tgmf->AddFrame(tgvf,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY,0,0,0,0));

		tgvf->MapSubwindows();
		tgvf->MapWindow();


		TEveManager::Create(kFALSE);
				
		TEveViewer *eve_v = new TEveViewer("Eve Viewer");
		eve_v->SetGLViewer(V, new TGFrame);
		eve_v->IncDenyDestroy();
		eve_v->AddScene(gEve->GetEventScene());
		gEve->GetViewers()->AddElement(eve_v);

		gGeoManager=gEve->GetGeometry("../../GDML_Generator/gdml_files/RootMoller.gdml");
		gGeoManager->DefaultColors();
		gGeoManager->CloseGeometry();

		TEveGeoTopNode *etn = new TEveGeoTopNode(gGeoManager,gGeoManager->GetTopNode(),0,150);
		gEve->AddElement(etn);

		tgmf->MapSubwindows();
		tgmf->MapWindow();
		tgmf->MapSubwindows();
		tgmf->MapWindow();

		// TEvePointSet *pointset = new TEvePointSet();
		// TEveLine *trackLine = new TEveLine();
		// std::vector<TEveLine> trackLines;

		// pointset->SetNextPoint(-210.456/10, -15.0686/10, 364.642/10);

		// pointset->SetMarkerColor(kCyan);
		// trackLine->SetLineColor(kMagenta);

		// std::vector<trackInfo> alltracks;
		// std::map<int,int,double,double,double> allTracks;
		std::string line;

		trkCount=0;
		int lastCountTrack=0;
	    while (getline(infile, line)){
	        std::vector<std::string> columns = split(line,' ');
	        int trkid     = std::atoi(columns[2].c_str());
	        if(trkid != lastCountTrack){
	        	trkCount ++;
	        	lastCountTrack = trkid;
	        }
	    }
	    infile.clear();
		infile.seekg(0, infile.beg);

	 //    trackLines = new TEveLine*[trkCount];
	 //    trackOrigins = new TEvePointSet*[trkCount];

	    int trkNo = 0;
	 //    gEve->AddElement(trackLines[trkNo]);
		// // gEve->AddElement(trackOrigins[trkNo]);
		// gEve->GetEventScene()->Changed();
		// gEve->FullRedraw3D(kFALSE); 
	    FindEvent(1);
		// gStyle->SetOptStat("");

		return;
	}

	void makePlot::FindEvent(int eveNo){
		if(runFlag) return;
		runFlag = true;

		printf("Showing Track Number %d\n",eveNo);
	    // int nTracks = 0;
	    // int lastTrack = 0;
		gEve->ClearOrphanage();
	    eventLine->Reset();
	    for (int k = 0; k<eventLines.size(); k++){
		   	delete eventLines[k];
	    }
	    delete trackOrigins;
	    trackOrigins = new TEvePointSet();
	    trackOrigins->SetMarkerColor(kCyan);
	    int eid; 
		int pid;     
		int trkid;     
		double stepx;   
		double stepy; 
		double stepz; 
	    eventLines.clear();
	  	
	  	std::string line;
	  	int nEve = 0;
	  	int lastEve = -1;
	  	int nTracks = 0;
	  	int lastTrack = -1;
	    while (getline(infile, line))
	    {
	        std::vector<std::string> columns = split(line,' ');
	        // // trackInfo tInfo;
	        // cout<<"-"<<columns[0]<<"-"<<endl;
	        // cout<<"-"<<columns[1]<<"-"<<endl;
	        // cout<<"-"<<columns[2]<<"-"<<endl;
	        // cout<<"-"<<columns[3]<<"-"<<endl;
	        // cout<<"-"<<columns[4]<<"-"<<endl;
	        // cout<<"-"<<columns[5]<<"-"<<endl;
	        eid = std::atoi(columns[0].c_str());
	        pid     = std::atoi(columns[1].c_str());
	        trkid     = std::atoi(columns[2].c_str());
	        stepx   = std::atof(columns[3].c_str());
	        stepy = std::atof(columns[4].c_str());
	        stepz = std::atof(columns[5].c_str());
	        
	        if (eid != lastEve){
	        	lastEve = eid;
	        	nEve++;
	        	// trackLines[nTracks]=new TEveLine();
	        	// trackLines[nTracks]->SetLineColor(kMagenta);
	        	// trackOrigins[nTracks] = new TEvePointSet();
	        	// trackOrigins[nTracks]->SetMarkerColor(kCyan);
	        }

	        if (nEve==eveNo){
	        		        // printf("nEve %d, eveNo %d\n",nEve,eveNo);

	        	
		        	
		        	if(trkid != lastTrack){
		        		nTracks++;
		        		lastTrack = trkid;
		        		eventLines.push_back(new TEveLine());
		        		// printf("eid: %d\n",eid);
						chain->GetEntry(eid);
								    	if (pid ==1){//Electron
										   	eventLines[nTracks-1]->SetLineColor(kMagenta);
								    	}
								    	else if (pid ==2){//Positron
										   	eventLines[nTracks-1]->SetLineColor(kAzure);
								    	}
								    	else if (pid ==3){//Photon
										   	eventLines[nTracks-1]->SetLineColor(kOrange);
								    	}
								    	else{
										   	eventLines[nTracks-1]->SetLineColor(kBlue);
								    	}
			        	double xOrigin = eve->GetTrack(trkid-1)->GetOrigin().X()/10.;
			        	double yOrigin = eve->GetTrack(trkid-1)->GetOrigin().Y()/10.;
			        	double zOrigin = eve->GetTrack(trkid-1)->GetOrigin().Z()/10.;
			        	// printf("Track origin: %g %g %g\n",xOrigin,yOrigin,zOrigin);
   			        	trackOrigins->SetNextPoint(xOrigin,yOrigin,zOrigin);
		        	}
		        	eventLines[nTracks-1]->SetNextPoint(stepx,stepy,stepz);
		        vis = eveNo;
		    }
	        // trackLines[nTracks]->SetNextPoint(stepx,stepy,stepz);
			// trackOrigins[nTracks]->SetNextPoint(eve->GetTrack(trkid)->GetOrigin().X()/10.,eve->GetTrack(trkid)->GetOrigin().Y()/10.,eve->GetTrack(trkid)->GetOrigin().Z()/10.);

	        // if (trkid ==1){
	        // 	trackLine->SetNextPoint(stepx,stepy,stepz);
	        // }
	       	// alltracks.push_back(tInfo);
	       	// allTracks.insert(std::pair<int,int,double,double,double>(pid,trkid,stepx,stepy,stepz));
	    }
	    gEve->ClearOrphanage();
   	    gEve->AddElement(trackOrigins);
	    // printf("NEVE: %d\n",nEve);
	    for (int k = 0; k<eventLines.size(); k++){
		    gEve->AddElement(eventLines[k]);
	    }
	    gEve->GetEventScene()->Changed();
		gEve->FullRedraw3D(kFALSE); 
	    infile.clear();
		infile.seekg(0, infile.beg);
		numEve = nEve;
		runFlag = false;
		// printf("Number of Events: %d\n",nEve);

	}

	void makePlot::PlotNo(int trkNo){
		vis = trkNo;
		// show = (TEveLine) *trackLines[trkNo];
	    // gEve->AddElement(trackLines[trkNo]);
		// gEve->AddElement(trackOrigins[trkNo]);
		gEve->GetEventScene()->Changed();
		gEve->FullRedraw3D(kFALSE); 

	}

	void makePlot::PlotInc(int prev){
		// cout<<"Prev: "<<prev<<endl;
		gEve->ClearOrphanage();
		// gEve->RemoveElement(trackLines[prev],(TEveElement*)gEve->GetCurrentEvent());
		// // show = (TEveLine) *trackLines[vis];
		// // gEve->AddElement(show);
		// gEve->AddElement(trackOrigins[vis]);
		gEve->GetEventScene()->Changed();
		gEve->FullRedraw3D(kFALSE); 

	}

	void makePlot::PlotPrev(){
		if (runFlag) return;
		// printf("Helloooooooooooo\n");
		if(vis>1){
			vis -= 1;
			FindEvent(vis);
		}
	}

	void makePlot::PlotNext(){
		if (runFlag) return;
		if(vis<numEve){
			vis += 1;
			// printf("Vis: %d\n",vis);
			FindEvent(vis);
		}
	}

	void makePlot::CloseWindow(){
		printf("Exiting\n");
		gApplication->Terminate(0);
		// printf("Still exiting\n");
		// exit(0);

	}

ClassImp(makePlot);
