#ifndef Mplot_h
#define Mplot_h

#include "TGFrame.h"
#include <vector>
#include <map>
#include <cstdlib>
#include <string>
#include <sstream>

#include "TROOT.h"
#include "TSystem.h"
#include "Riostream.h"
#include "TEveScene.h"
#include "TEveLine.h"
#include "TEvePointSet.h"
#include "TEveManager.h"
#include "TEveViewer.h"
#include "TGButton.h"
#include "TH2D.h"
#include "TEveGeoNode.h"
#include "TGLEmbeddedViewer.h"
#include "TChain.h"
#include "TGeoManager.h"
#include "DLg4Event.h"
#include "TGLayout.h"
#include "TGClient.h"
#include "TApplication.h"
// #include "TEve.h"
#include "TGeoManager.h"
// #include "TStyle.h"
#include "TString.h"
class makePlot: public TGMainFrame{

public:

	makePlot(TString);
	~makePlot();

	// TEveLine **trackLines;
	std::vector<TEveLine*> eventLines;
	// TEvePointSet **trackOrigins;
	int vis;
	int trkCount;
	int numEve;
	// TEveLine *show;
	TEveLine *eventLine;
	ifstream infile;
	TString coreName;
	TEvePointSet *trackOrigins;
	TChain* chain;
	DLg4Event* eve;
	std::vector<std::string> split(const std::string &s, char);

	void EventDisplay();
	void makeHistogram();

	void FindEvent(int);

	void PlotNo(int trkNo);

	void PlotInc(int prev);

	void PlotPrev();

	void PlotNext();
    virtual void CloseWindow();
	ClassDef(makePlot,1);
private:
	bool runFlag;
};

#endif
