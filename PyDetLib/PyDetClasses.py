#!/usr/bin/python
import numpy as np
from itertools import groupby
from operator import itemgetter
from numba import jit, jitclass, int64, float64, void, boolean, autojit

# @jit
def findRanges(data):
    ranges = []
    for k, g in groupby(enumerate(data), lambda (i, x): i-x):
        ranges.append(map(itemgetter(1), g))
    return ranges

# class DetectorPlane:
#     def __init__ (self):
#         self.fiberHits = {}
#     def AddHit(self,fiberNumber,dE):
#         self.fiberHits.setdefault(fiberNumber,0)
#         self.fiberHits[fiberNumber] += dE
#     def FibersAboveThreshold(self,thresh):
#         fibers = []
#         for fib,dE in self.fiberHits.iteritems():
#             if dE>thresh:
#                 fibers.append(fib)
#         return fibers
#     def GetNumHits(self,thresh):
#         return len(self.FibersAboveThreshold(thresh))
#     def GetHitLocations(self,thresh):
#         #For each set of hit fibers, return the mean loc
#         #Sets are separated by at least 1 non-hit fiber
#         fibs = self.FibersAboveThreshold(thresh)
#         fibs.sort()#make sure in numerical order
#         ranges = findRanges(fibs)
#         locs = [np.mean(l) for l in ranges]
#         return locs
#     def Reset(self):
#         self.fiberhits = {}

@jitclass([
    ('nFib',int64),
    ('fiberHits',float64[:])
])
class DetectorPlane(object):
    def __init__ (self,nFib):
        self.nFib = nFib
        self.fiberHits = np.zeros(nFib,dtype=float64)
    def AddHit(self,fiberNumber,dE):
        # self.fiberHits.setdefault(fiberNumber,0)
        self.fiberHits[fiberNumber] += dE
    def FibersAboveThreshold(self,thresh):
        # fibers = []
        # for fib,dE in self.fiberHits.iteritems():
        #     if dE>thresh:
        #         fibers.append(fib)
        # return fibers
        return np.where(self.fiberHits>thresh)[0]
    def GetNumHits(self,thresh):
        return self.FibersAboveThreshold(thresh).shape[0]
    def GetHitLocations(self,thresh):
        #For each set of hit fibers, return the mean loc
        #Sets are separated by at least 1 non-hit fiber
        # fibs = self.FibersAboveThreshold(thresh)
        # fibs.sort()#make sure in numerical order
        # ranges = findRanges(fibs)
        # locs = [np.mean(l) for l in ranges]
        # return locs
        return np.where(self.fiberHits>thresh)[0]
    def Reset(self):
        self.fiberhits = np.zeros(self.nFib,dtype=float64)

#test = DetectorPlane()
#test.AddHit(5,0.2)
#test.AddHit(6,0.2)
#test.AddHit(8,0.2)
#print test.FibersAboveThreshold(0.1)
#print test.GetHitLocations(0.1)

#Tracker = [DetectorPlane(),DetectorPlane(),DetectorPlane(),DetectorPlane()]
# class Tracker:
#     def __init__ (self,layers,fibsPerLayer):
#         self.layers = layers
#         self.fibsPerLayer = fibsPerLayer
#         self.planes = [DetectorPlane(fibsPerLayer[i]) for i in range(layers)]
#     def AddHitInLayer(self,layer,fiberNumber,dE):
#         self.planes[layer].AddHit(fiberNumber,dE)
#     def Reset(self):
#         # for i in range(len(self.planes)):
#         #     self.planes[i].Reset()
#         # self.planes = [DetectorPlane() for i in range(self.layers)]
#         self.planes = [DetectorPlane(self.fibsPerLayer[i]) for i in range(self.layers)]
#     def GetHitLayers(self,thresh):
#         nHits = [l.GetNumHits(thresh) for l in self.planes]
#         l = [1 if x>0 else 0 for x in nHits]
#         #return np.where(np.array(l) == 1)[0]
#         return l
#     def PrintStuff(self,thresh):
#         for det in self.planes:
#             print det.GetHitLocations(thresh)
#     def HasXHit(self,thresh): #for 4-layer
#         hits = self.GetHitLayers(thresh)
#         if (hits[0] or hits[2]):
#             return True
#         else:
#             return False
#     def HasYHit(self,thresh): #for 4-layer
#         hits = self.GetHitLayers(thresh)
#         if (hits[1] or hits[3]):
#             return True
#         else:
#             return False
#     def Has4Hits(self,thresh): #for 4-layer
#         hits = self.GetHitLayers(thresh)
#         if (hits[0] and hits[1] and hits[2] and hits[3]):
#             return True
#         else:
#             return False
#     def Has2Hits(self,thresh): #for 2-layer
#         hits = self.GetHitLayers(thresh)
#         if (hits[0] and hits[1]):
#             return True
#         else:
#             return False
#     def GetHitLocs2L(self,thresh):
#         xLocs = self.planes[0].GetHitLocations(thresh)
#         yLocs = self.planes[1].GetHitLocations(thresh)
#         return [xLocs,yLocs]


@jitclass([
    ('layers',int64),
    ('fibsX',int64),
    ('fibsY',int64),
    ('fiberHitsX',float64[:]),
    ('fiberHitsY',float64[:])
])
class Layer2Tracker(object):
    def __init__ (self,fibsX,fibsY):
        self.layers = 2
        self.fibsX = fibsX
        self.fibsY = fibsY
        self.fiberHitsX = np.zeros(fibsX,dtype=float64)
            # np.zeros(fibsY,dtype=float64)])
        self.fiberHitsY = np.zeros(fibsY,dtype=float64)
    def AddHitInLayer(self,layer,fiberNumber,dE):
        if layer == 0:
            self.fiberHitsX[fiberNumber] += dE
        elif layer ==1:
            self.fiberHitsY[fiberNumber] += dE
        else:
            pass
    def FibersAboveThreshold(self,layer,thresh):
        if layer == 0:
            return np.where(self.fiberHitsX>thresh)[0]
        else:
            return np.where(self.fiberHitsY>thresh)[0]
    def GetNumHits(self,layer,thresh):
        return self.FibersAboveThreshold(layer,thresh).shape[0]
    def Reset(self):
        self.fiberHitsX = np.zeros(self.fibsX,dtype=float64)
        self.fiberHitsY = np.zeros(self.fibsY,dtype=float64)
    def Has2Hits(self,thresh): #for 2-layer
        nHitsX = self.GetNumHits(0,thresh)
        nHitsY = self.GetNumHits(1,thresh)
        if (nHitsX > 0) and (nHitsY > 0):
            return True
        else:
            return False
    def GetHitLocs2L(self,thresh):
        xLocs = self.FibersAboveThreshold(0,thresh)
        yLocs = self.FibersAboveThreshold(1,thresh)
        return xLocs,yLocs
