##########
# Script written by C. Epstein, 9/2015 to execute 'n' sets of jobs on 'm' cores, 
# Uses python functionality to run the compiled DarkLight release code
# the proper number of times and to hadd the output together and remove the
# intermediates. 
#########

#!/usr/bin/python
from __future__ import division
import os
import multiprocessing
import subprocess
import shlex
from math import pi
# import time
# ./bin/daliSimG4 -n 10000000 --geoName Moller  --vertexGen xyz_0_0_0 --physGen RadMoller --magField zero --physParam elth_0.416332312_0.4563323129_phi_3.1415_3.1416 --coreName mRad25 -b

coreName = "pScan25C20_Long"
batch = True
nEve = 100000

nCores = multiprocessing.cpu_count() - 2
nPerCore = 100

# gen = "RadMoller"
gen = "pScan"


centerAngle = 25
deltaAngle = 2.5

def degToRad(angle):
	return angle*pi/180.

thetaMin = degToRad(centerAngle-deltaAngle)#0.416332312
thetaMax = degToRad(centerAngle+deltaAngle)#0.4563323129
phiMin = 3.1415
phiMax = 3.1416

execPath = "./bin/daliSimG4"
geom = "Moller"
vert = "xyz_0_0_0"
mField = "zero"

b = ""
if batch:
	b = " -b"
if gen == "RadMoller":
	angRange = "elth_"+str(thetaMin)+"_"+str(thetaMax)+"_phi_"+str(phiMin)+"_"+str(phiMax)
	physPars = " --physParam " + angRange
if gen == "pScan":
	physPars = ' --physParam ele_momentum_5.0_th_'+str(degToRad(centerAngle))+'_delt_0.0_pct_20_phi_'+str(phiMin)+'_'+str(phiMax)+' '

def cmd(name):
	command = execPath + " -n " + str(nEve) + " --geoName " + geom + " --physGen " + gen + " --magField " + mField + " --vertexGen " + vert + physPars + " --coreName " + name + b
	return shlex.split(command)
# print cmd(coreName)


# def calculate(value):
#     return value * 10

# def pr(txt):
# 	# time.sleep(5)
# 	# p = subprocess.Popen(["echo",str(txt)],stdout=subprocess.PIPE)
# 	p = subprocess.call(["echo",str(txt)])
# 	p.wait()
# 	return 'Done'#p.stdout.readlines()[0]

nSets = nCores * nPerCore
jobNames = [coreName+"_job_"+str(i+1) for i in range(nSets)]

# for j in jobNames:
# 	print cmd(j)

# print jobNames

fNameExt = ['.DLg4.root','.step.hist.root']

print "Old Files"
for j in jobNames:
	print '\t',j+fNameExt[0],j+fNameExt[1]
print "New Files"
print '\t',coreName+fNameExt[0],coreName+fNameExt[1]

def haddCMD(coreName,jobNames,fNameExt):
	c1 = 'hadd -f '+coreName+fNameExt[0]+' '
	for j in jobNames:
		c1 = c1 + j+fNameExt[0]+' '
	c2 = 'hadd -f '+coreName+fNameExt[1]+' '
	for j in jobNames:
		c2 = c2 + j+fNameExt[1]+' '
	return shlex.split(c1),shlex.split(c2)


def rmCMD(c1i,c2i):
	c1 = c1i[::]
	c2 = c2i[::]
	for i in range(3):
		c1.pop(0)
		c2.pop(0)
	c1.insert(0,'rm')
	c2.insert(0,'rm')
	return c1,c2

c1,c2 = haddCMD(coreName,jobNames,fNameExt)
r1,r2 = rmCMD(c1,c2)

# print r1
# print r2

# pool = multiprocessing.Pool(nCores)
# tasks = range(nCores*nPerCore)
# results = []
# # r = pool.map_async(calculate, tasks, callback=results.append)
# r = pool.map_async(pr,tasks,callback=results.append)
# r.wait() # Wait on the results
# print results

def launch(name):
	subprocess.call(cmd(name))

pool = multiprocessing.Pool(nCores)
results = []

r = pool.map_async(launch,jobNames,callback=results.append)
r.wait() # Wait on the results

hadd1,hadd2 = haddCMD(coreName,jobNames,fNameExt)
rm1,rm2 = rmCMD(hadd1,hadd2)

subprocess.call(hadd1)
subprocess.call(hadd2)
subprocess.call(rm1)
subprocess.call(rm2)

makeTrkDir = shlex.split('mkdir -p '+coreName+'TrackData')
subprocess.call(makeTrkDir)

trackFiles = [jobName+'_TrackData.txt' for jobName in jobNames]

cpTrkZero = shlex.split('cp '+jobNames[0]+'_TrackData.txt '+coreName+'_TrackData.txt')
subprocess.call(cpTrkZero)

mvTrkFiles = ['mv']+trackFiles+shlex.split('./'+coreName+'TrackData')
subprocess.call(mvTrkFiles)
