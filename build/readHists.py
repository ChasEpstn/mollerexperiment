#!/usr/bin/python

from pyhep import *

# sh = ReadStore('mapTestVIS_HR_VAC_pScan_25_1dth_1dphi_Ne3.0_HISTS.h5')
sh = ReadStore('radRun1_HR_RadMoller_25_1dth_1dphi_Ne5.0_HISTS.h5')


h2 = ReadHistFromFile(sh,'xyHISTgnu')
# h1.ReadData(sh,'pMapHist1D')

# h2.ReadData(sh,'xyHISTgnu')
# print h2.H.binVals[h2.H.binVals>0]
# print sh['xyHISTgnu']['binVals']
# a = np.ones([3,3])
# c = ['ac','bc','cc']
# r = ['ar','br','cr']

# d = pd.DataFrame(a,index=r,columns=c)
# print d
#
# print d.index.values
# print d.columns.values
# h2.SetLogZ()
h2.SetTitle('Detector Hit Map')
h2.SetXLabel('X Fiber')
h2.SetYLabel('Y Fiber')
h2.SetEqAspect()

h2.Draw('test2Dload2')
