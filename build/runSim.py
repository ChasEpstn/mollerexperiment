#!/usr/bin/python
from __future__ import division
import os
import multiprocessing
import subprocess
import shlex
from math import pi, log10
# import time
# ./bin/daliSimG4 -n 10000000 --geoName Moller  --vertexGen xyz_0_0_0 --physGen RadMoller --magField zero --physParam elth_0.416332312_0.4563323129_phi_3.1415_3.1416 --coreName mRad25 -b

coreName = "cScanRun1_R_HR1"
# coreName = "fieldTestLR_VAC_RAD"
batch = True
nEve = 1000000
useHighResField = True

nCores = multiprocessing.cpu_count() - 2
nPerCore = 1

# gen = "RadMoller"
gen = "pScan"
# gen = "Moller"

coreName += "_"+gen

centerAngle = 25
deltaAngle = 2.5
deltaPhi = 2.5#.25

coreName += "_"+str(centerAngle)
coreName += "_"+str(deltaAngle)+"dth"
coreName += "_"+str(deltaPhi)+"dphi"
coreName += "_Ne"+str(round(10*log10(nEve))/10.)
def degToRad(angle):
	return angle*pi/180.

thetaMin = degToRad(centerAngle-deltaAngle)#0.416332312
thetaMax = degToRad(centerAngle+deltaAngle)#0.4563323129
phiMin = pi - degToRad(deltaPhi)#3.1415
phiMax = pi + degToRad(deltaPhi)#3.1416

execPath = "daliSimG4"
geom = "Moller_"+str(centerAngle)
vert = "xyz_0_0_0"
fieldScale = "1"

fieldDir = "/Users/cepstein/MollerExperiment/modules/G4/geom/externalGeom/"
highResFile = "Moller1B5Fnew.fld"
lowResFile = "Moller1B5FnewCoarse.fld"

availableFieldFiles = 0
fieldFile = ""

if lowResFile in os.listdir(fieldDir):
	availableFieldFiles += 1
if highResFile in os.listdir(fieldDir):
	availableFieldFiles += 2

assert(availableFieldFiles>0)

if useHighResField:
	if availableFieldFiles & 2:
		fieldFile = highResFile
	else:
		fieldFile = lowResFile
else:
	fieldFile = lowResFile


mField = fieldDir+fieldFile+"+"+fieldScale # +1 is scaling
#mField = "/home/cepstein/MollerExperiment/modules/G4/geom/externalGeom/Moller1BFieldProcOld.dat+1" # +1 is scaling

b = ""
if batch:
	b = " -b"
if gen == "RadMoller":
	angRange = "elth_"+str(thetaMin)+"_"+str(thetaMax)+"_phi_"+str(phiMin)+"_"+str(phiMax)
	physPars = " --physParam " + angRange
if gen == "pScan":
	physPars = ' --physParam ele_mom_5.2_th_'+str(degToRad(centerAngle))+'_delt_0.0_pct_16_phi_'+str(phiMin)+'_'+str(phiMax)+' '
if gen == "Moller":
	angRange = "elth_"+str(thetaMin)+"_"+str(thetaMax)+"_phi_"+str(phiMin)+"_"+str(phiMax)
	physPars = " --physParam " + angRange
def cmd(name):
	command = execPath + " -n " + str(nEve) + " --geoName " + geom + " --physGen " + gen + " --magField " + mField + " --vertexGen " + vert + physPars + " --coreName " + name + b
	return shlex.split(command)
# print cmd(coreName)


# def calculate(value):
#     return value * 10

# def pr(txt):
# 	# time.sleep(5)
# 	# p = subprocess.Popen(["echo",str(txt)],stdout=subprocess.PIPE)
# 	p = subprocess.call(["echo",str(txt)])
# 	p.wait()
# 	return 'Done'#p.stdout.readlines()[0]

nSets = nCores * nPerCore
jobNames = [coreName+"_job_"+str(i+1) for i in range(nSets)]

# for j in jobNames:
# 	print cmd(j)

# print jobNames

fNameExt = ['.DLg4.root','.step.hist.root']

print "Old Files"
for j in jobNames:
	print '\t',j+fNameExt[0],j+fNameExt[1]
print "New Files"
print '\t',coreName+fNameExt[0],coreName+fNameExt[1]

def haddCMD(coreName,jobNames,fNameExt):
	c1 = 'hadd -f '+coreName+fNameExt[0]+' '
	for j in jobNames:
		c1 = c1 + j+fNameExt[0]+' '
	c2 = 'hadd -f '+coreName+fNameExt[1]+' '
	for j in jobNames:
		c2 = c2 + j+fNameExt[1]+' '
	return shlex.split(c1),shlex.split(c2)


def rmCMD(c1i,c2i):
	c1 = c1i[::]
	c2 = c2i[::]
	for i in range(3):
		c1.pop(0)
		c2.pop(0)
	c1.insert(0,'rm')
	c2.insert(0,'rm')
	return c1,c2

c1,c2 = haddCMD(coreName,jobNames,fNameExt)
r1,r2 = rmCMD(c1,c2)

# print r1
# print r2

# pool = multiprocessing.Pool(nCores)
# tasks = range(nCores*nPerCore)
# results = []
# # r = pool.map_async(calculate, tasks, callback=results.append)
# r = pool.map_async(pr,tasks,callback=results.append)
# r.wait() # Wait on the results
# print results

def launch(name):
	subprocess.call(cmd(name))
        #if not batch:
        #    subprocess.call(shlex.split("wmctrl -r daliSimG4 -e '0,0,0,1920,2160'"))

pool = multiprocessing.Pool(nCores)
results = []

r = pool.map_async(launch,jobNames,callback=results.append)
r.wait() # Wait on the results


hadd1,hadd2 = haddCMD(coreName,jobNames,fNameExt)
rm1,rm2 = rmCMD(hadd1,hadd2)

subprocess.call(hadd1)
subprocess.call(hadd2)
subprocess.call(rm1)
subprocess.call(rm2)

makeTrkDir = shlex.split('mkdir -p '+coreName+'TrackData')
subprocess.call(makeTrkDir)

trackFiles = [jobName+'_TrackData.txt' for jobName in jobNames]

cpTrkZero = shlex.split('cp '+jobNames[0]+'_TrackData.txt '+coreName+'_TrackData.txt')
subprocess.call(cpTrkZero)

mvTrkFiles = ['mv']+trackFiles+shlex.split('./'+coreName+'TrackData')
subprocess.call(mvTrkFiles)
