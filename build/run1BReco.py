from __future__ import division
import ROOT as r
import math
import numpy as np
from PyDetClasses import *
from pyhep import *
import pickle as pk
from numba import jit
import pandas as pd

r.gROOT.Reset()

r.gSystem.Load("/home/cepstein/.darklight/x86_64/lib/libDLg4Event.so")
r.gSystem.Load("/home/cepstein/.darklight/x86_64/lib/libDLmadEvent.so")

# w = 1200
# h = 400
thresh = 0.05

# coreName = 'cScanRun1_HR_pScan_25_2.5dth_2.5dphi_Ne5.0'
# coreName = 'cScanRun1_R_HR_pScan_25_2.5dth_2.5dphi_Ne5.0'
coreName = 'cScanRun1_R_HR1_pScan_25_2.5dth_2.5dphi_Ne6.0'

# coreName = 'mapTestVIS_HR_VAC_pScan_25_1dth_1dphi_Ne3.0'
# coreName = 'radRun1_HR_RadMoller_25_1dth_1dphi_Ne5.0'
# f = r.TFile('mapTestVIS_HR_VAC_pScan_25_1dth_1dphi_Ne3.0.DLg4.root','read')
f = r.TFile(coreName+'.DLg4.root','read')
# f = r.TFile('treeRun1_HR_Moller_25_2.5dth_2.5dphi_Ne5.0.DLg4.root','read')
# f = r.TFile('radRun1_HR_RadMoller_25_1dth_1dphi_Ne5.0.DLg4.root','read')


DLg4 = f.Get('DaLiEventTree')

# maxEve = 1e9
i=0

# xyHIST = r.TH2D("xyHIST","Hit Map",60,0,60,20,0,20)
# xyHIST.Sumw2()

detHits = []

xyHISTgnu = H2D(60,0,60,20,0,20)
pMapHist = H2D(250,4.37,5.2,60,0,60)
# pMapHist1D = H1d(60,0,60)

# @jit
def getTrkNumHits(track):
	num =0
	try:
		num = track.GetNumHits()
	except ReferenceError:
		pass
	except AttributeError:
		pass
	return num
# @jit
def getEveNumTrks(event):
	num = 0
	try:
		num = event.GetNumTracks()
	except ReferenceError:
		pass
	except AttributeError:
		pass
	return num

# @jit
# def layerNoF(s):
# 	if s == 'x':
# 		return 0
# 	else:
# 		return 1
layers = {'x':0, 'y':1}
# SciFi = Tracker(2,[60,20]) #Create a 2-layer Tracker
SciFi = Layer2Tracker(60,20)
# raw_input("Press enter to continue")
for event in DLg4:
	# hitLocs = [0,0] #x,y bitwise locs
	# if i>=maxEve:
	# 	break
	i+=1
	# print event.DLg4Event.GetWeight()
	numTrks = getEveNumTrks(event.DLg4Event)
	# print numTrks
	for j in range(numTrks):
		track= event.DLg4Event.GetTrack(j)
		numHits = getTrkNumHits(track)
		# if numHits > 1:
		# 	pass
			# print numHits
		for k in range(numHits):
			# print "Angle:",track.GetMomentum().Theta()*180./math.pi
			# print "Origin: %g, %g, %g" %(track.GetOrigin().X(),track.GetOrigin().Y(),track.GetOrigin().Z())
			# print "Energy: %g"%(track.Get4Momentum().E())
			# print ""
			hit = track.GetHit(k)
			detInfo = hit.GetDetectorID().Data()
			# print det
			# det = det.split("PaddleN")
			# det = [detInfo[0],detInfo[-2:]]
			layerNo = layers[detInfo[0]]
			try:
				fiberNo = int(detInfo[-2:])
			except(ValueError):
				fiberNo = int(detInfo[-1:])
			# layerNo = 0.5
			# if det[0] == 'x':
			# 	layerNo = 0
			# elif det[0] == 'y':
			# 	layerNo = 1
			# else:
			# 	print 'WTF'
			# 	layerNo = 1.5#should throw an error
			# layerNo = layers[det[0]]
			# try:
			# 	fiberNo = int(det[1])
			# 	# print fiberNo
			# 	SciFi.AddHitInLayer(layerNo,fiberNo,hit.GetDE())
			# except(ValueError):
			# 	pass
			SciFi.AddHitInLayer(layerNo,fiberNo,hit.GetDE())
			if (hit.GetDE()>thresh) and layerNo == 0 and track.GetOrigin().Mag()<0.001:
				pMapHist.Fill(track.Get4Momentum().P(),fiberNo,event.DLg4Event.GetWeight())
	# #this thing is a factor of 2
	# raw_input("Press enter to continue2")

	if SciFi.Has2Hits(thresh):
		# pass
		# raw_input("Press enter to continue3")
		locs = SciFi.GetHitLocs2L(thresh)
		# raw_input("Press enter to continue4")

		# # print locs
		# # print event.DLg4Event.GetWeight()
		# for locX in locs[0]:
		# 	for locY in locs[1]:
		# 		# xyHIST.Fill(locX,locY,event.DLg4Event.GetWeight())
		# 		xyHISTgnu.Fill(locX,locY,event.DLg4Event.GetWeight())
		# 		pass
		xyHISTgnu.FillMany(locs[0],locs[1],event.DLg4Event.GetWeight())
	SciFi.Reset()
	# print "Reset:",SciFi.GetHitLocs2L(0.0)

	# 		if hit.GetDE() > 0.15:
	# 			det = hit.GetDetectorID().Data()
	# 			det = det.split("PaddleN")
	# 			if det[0] == 'x':
	# 				hitLocs[0] |= 1 << int(det[1])
	# 			elif det[0] == 'y':
	# 				hitLocs[1] |= 1 << int(det[1])
	# detHits.append(hitLocs)
			# localPos = hit.GetLocalPosition()
			# # print localPos.x(),localPos.y(),localPos.z()
			# xyHIST.Fill(localPos.x(),localPos.z(),event.DLg4Event.GetWeight())
#
# r.gStyle.SetOptStat("")
# c1 = r.TCanvas("c1","c1",w,h)
# c1.SetWindowSize(w+(w-c1.GetWw()),h+(h-c1.GetWh()))
# c1.SetFixedAspectRatio()
# c1.cd()
# c1.SetLogz()
# xyHIST.Draw("colz")
#
# c2 = r.TCanvas("c2")
# c2.cd()
# xHist = xyHIST.ProjectionX()
# c2.SetLogy()
# xHist.Draw()


store = MakeStore(coreName+'_HISTS.h5')

#Uncomment this!
xyHISTgnu.SetLogZ()
xyHISTgnu.SetTitle('Detector Hit Map')
xyHISTgnu.SetXLabel('X Fiber')
xyHISTgnu.SetYLabel('Y Fiber')
xyHISTgnu.SetEqAspect()
xyHISTgnu.Scale(1.0/i)
xyHISTgnu.Draw(coreName)#'testGnu.pdf')
xyHISTgnu.SaveData(store,'xyHISTgnu')

xHISTgnu = xyHISTgnu.ProjectX()
xHISTgnu.SetXLabel('X Fiber')
xHISTgnu.SetLogY()
# xHISTgnu.Scale(1.0/i)
xHISTgnu.Draw(coreName+'X')
xHISTgnu.SaveData(store,'xHISTgnu')

yHISTgnu = xyHISTgnu.ProjectY()
yHISTgnu.SetXLabel('Y Fiber')
yHISTgnu.SetLogY()
yHISTgnu.Draw(coreName+'Y')
yHISTgnu.SaveData(store,'yHISTgnu')

pMapHist.SetXLabel('Track Momentum [MeV/c]')
pMapHist.SetYLabel('Fiber Number')
pMapHist.Draw(coreName+'M')
pMapHist.SaveData(store,'pMapHist')

pMapHist1D = pMapHist.GetSliceY()
pMapHist1D.SetYRange(4.35,5.25)
pMapHist1D.SetYLabel('Track Momentum [MeV/c]')
pMapHist1D.SetXLabel('Fiber Number')
pMapHist1D.Draw(coreName+'M1')
pMapHist1D.SaveData(store,'pMapHist1D')

# file = open('Mapping.pk','w')
# mapping = np.array([pMapHist1D.H.binCenters,pMapHist1D.H.binVals,pMapHist1D.H.binSumW2]).transpose()
# pk.dump(mapping,file)
# file.close()


# file = open(coreName+'.pk','w')
# pk.dump(xyHISTgnu,file)
# pk.dump(xHISTgnu,file)
# file.close()
# raw_input("Press enter to exit")
# detHits = np.array(detHits)
# print detHits
