// \class  LepDetMapBase
// Generic Wrapper for translation between channel ID and physical location
// \author Ross Corliss

#ifndef DLdetMap_h
#define DLdetMap_h
//#include "DLUnits.h"
//using namespace dali;  //uses DLUnits without needing dali::___
#include <TVector3.h>


enum DLdetType { NODETECTOR=-1,PLANE=1,CYLINDER=2};
enum StripType { NOSTRIP=-1,ISTRIP=0,CSTRIP=1};

class DLdetMap_item{
 public:
  DLdetMap_item() { detectorShape=NODETECTOR; stripType=NOSTRIP; }
  DLdetMap_item(int idetType, int iStripType, int iStripId, int iPlaneId, int iZringId, int iPhiPatchId, int iUniPatchId, TVector3 iCenter, double iZlengthH, double iPhiWidthH) { set( idetType, iStripType, iStripId, iPlaneId, iZringId,  iPhiPatchId,  iUniPatchId, iCenter, iZlengthH, iPhiWidthH);}
  int detectorShape;
  int stripType;
  int stripId, detectorId, zRingId, phiPatchId, uniPatchId;
  TVector3 center;
  double zLengthH, phiWidthH; // half widths
  bool isIstrip() { return  !isVoid() && stripType==ISTRIP;}
  bool isCstrip() { return  !isVoid() && stripType==CSTRIP;}
  bool isVoid() { return detectorShape<=NODETECTOR;}
  void set(int idetType, int iStripType, int iStripId, int iPlaneId, int iZringId, int iPhiPatchId, int iUniPatchId, TVector3 iCenter, double iZlengthH, double iPhiWidthH){
    detectorShape=idetType;  stripType=iStripType; stripId=iStripId; detectorId=iPlaneId;
    zRingId=iZringId; phiPatchId=iPhiPatchId; uniPatchId=iUniPatchId;
    center=iCenter;   zLengthH=iZlengthH;  phiWidthH=iPhiWidthH;
  }

  virtual void print();

  ClassDef(DLdetMap_item,1);
};


class DLdetMap {
 protected:
  int version;
 public:
  DLdetMap(int ver=0){version=ver;}
  void set(int a) {version=a;}
  virtual void print(int level)=0;
  virtual int LocalPositionToStripId(TString detectorName, TVector3 localPosition)=0;
  virtual DLdetMap_item *LocalPositionToDetMapItem(TString detectorName, TVector3 localPosition)=0;
 
  virtual DLdetMap_item *StripIdToDetMapItem(int stripId)=0;//returns an object containing all spatial extents of the strip along with its indexing directions etc.
  ClassDef(DLdetMap,1);
};
 #endif
