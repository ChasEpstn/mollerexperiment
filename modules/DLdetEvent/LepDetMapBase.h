// \class  LepDetMapBase
// Generic Wrapper for translation between channel ID and physical location
// \author Ross Corliss

#ifndef LepDetMapBase_h
#define LepDetMapBase_h

enum LepDetType { NODETECTOR=-1,PLANE=1,CYLINDER=2};
enum StripType { NOSTRIP=-1,ISTRIP=0,CSTRIP=1};

class DetMap_item{
 public:
  DetMap_item() { type=NODETECTOR; stripType=NOSTRIP; }
  DetMap_item(int iType, int iStripType, int iStripId, int iPlaneId, int iZringId, int iPhiPatchId, int iUniPatchId, TVector3 iCenter, double iZlengthH, double iPhiWidthH) { set( iType, iStripType, iStripId, iPlaneId, iZringId,  iPhiPatchId,  iUniPatchId, iCenter, iZlengthH, iPhiWidthH);}
  int type;
  int stripType;
  int stripId, planeId, zRingId, phiPatchId, uniPatchId;
  TVector3 center;
  double zLengthH, phiWidthH; // half widths
  bool isIstrip() { return stripType==ISTRIP;}
  bool isCstrip() { return stripType==CSTRIP;}
  bool isVoid() { return stripType!=NOSTRIP;}
  void set(int iType, int iStripType, int iStripId, int iPlaneId, int iZringId, int iPhiPatchId, int iUniPatchId, TVector3 iCenter, double iZlengthH, double iPhiWidthH){
    type=iType;  stripType=iStripType; stripId=iStripId; planeId=iPlaneId;
    zRingId=iZringId; phiPatchId=iPhiPatchId; uniPatchId=iUniPatchId;
    center=iCenter;   zLengthH=iZlengthH;  phiWidthH=iPhiWidthH;
  }

  virtual void print(){
    printf("stripId=%d: planeId=%d,zRingId=%d,phiPatchId=%d,uniPatchId=%d; type=%d,stripType=%d,zLength/2=%2.2fmm,width/2=%2.2fmm\n",stripId,planeId,zRingId,phiPatchId,uniPatchId,type,stripType,zLengthH,phiWidthH);
    return;
  }
};


class LepDetMapBase {
 public:
  LepDetMapBase(){return;};
  virtual void print(int level)=0;
  //eventually the following two may need to be modified to not need strip, but that's very advanced:
  // virtual int GlobalPositionToStripId(TVector3 position)=0;
  virtual int LocalPositionToStripID(TString detectorName, TVector3 position)=0;
  //  virtual TVector3 StripIdToGlobalPosition(int stripId)=0;
  virtual DetMap_item *LocalPositionToDetMapItem(TString detectorName, TVector3 position)=0;
  virtual DetMap_item *StripIdToDetMapItem(int stripID)=0;//returns an object containing all spatial extents of the strip along with its indexing directions etc.
};
 #endif
