// \class  LepDetMap
// strips mapping  for Lepton Detector for Darklight
// \author Ross Corliss
#ifndef LepDetMapPhase1_h
#define LepDetMapPhase1_h
//#include "DLUnits.h"
#include <TVector3.h>
#include <TH1I.h>
#include "DLdetMap.h"
#include <vector>

//using namespace dali;  //uses DLUnits without needing dali::___
using namespace std;


class LepDetMapPhase1_tiling{
 private:
  int debug;
  static const int maxZdivs=10;
  TH1I *patchPerId;//internal stripId --> patchId table
  TH1I *zRingPerZ;//internal z-coord --> zRing table
  int localStripId[240][410];//alas for hardcoded values.
 public: //being overly light-weight by allowing direct access:
  double cPitch,iPitch;

  int nZdivs;
  int *nPhiDivs;//[maxZdivs];
  int nPatches;//sum of all nPhiDivs 

  //dimensions measured in pitches:
  int totZsize;
  int phiSize;//phi size measured in iPitch
  int *zSize;//[maxZdivs];//z size of each z segment in cPitch

  //numbering scheme conventions:
  int iOffset, cOffset;
  int offsetPerZ;
  int offsetPerPlane;
  
  void BuildLookupLocalStrip();
  bool lookupTableReady;

 public:
  LepDetMapPhase1_tiling(int ver=1, int d=0);
  int LookupLocalPatch(int stripId);
  int LookupLocalStripId(float xfrac, float zfrac);
  int CalculateLocalStripId(float xfrac, float zfrac);
  TH1I *GetPatchLookupHist(){return patchPerId;}
  TH1I *GetZringLookupHist(){return zRingPerZ;}

  ClassDef(LepDetMapPhase1_tiling,1);
};

//=========================================
//=========================================
class LepDetMapPhase1_planePatch{
 private:
  int debug;
  //various parameters used to tile the strips into the patch:
  double cHalfSize,iHalfSize;
  double cPitch,iPitch;
  double cHalfWidth, iHalfWidth;
  TVector3 cHat, iHat;
  TVector3 center, localSize;

  
  //by default, iStrips are continuous, while cStrips are pads.
  vector<DLdetMap_item*> cStrip;
  vector<DLdetMap_item*> iStrip;
 public:
  LepDetMapPhase1_planePatch(TVector3 patchCenter,
			     TVector3 patchSize,
			     int nC, int nI,
			     int cOffset, int iOffset,
			     int planeId, int zRingId,
			     int phiPatchId, int uniPatchId,
			     int d=0);

  TVector3 GetCenter(){return center;}
  TVector3 GetSize(){return localSize;}
  vector<DLdetMap_item*> GetStrips(int stripType){
    if (stripType==ISTRIP) return iStrip;
    return cStrip;
  }
  
  DLdetMap_item *GetStrip(int stripId){
    DLdetMap_item *ret=NULL;
    if (debug>1) printf("LepDetMapPhase1_planePatch::GetStrip(%d) (%lui,%luc) (ilow=%d,clow=%d)\n",stripId,iStrip.size(),cStrip.size(),iStrip[0]->stripId,cStrip[0]->stripId);

    //this assumes that stripId is always higher for 'c' than for 'i'.
    int cRelativeId=stripId-cStrip[0]->stripId;
    if (debug>1) printf("LepDetMapPhase1_planePatch::GetStrip(%d): cRelativeId=%d\n",stripId,cRelativeId);
    if (cRelativeId>=0) {
      ret=cStrip[cRelativeId];
    }else{
      int iRelativeId=stripId-iStrip[0]->stripId;
      if (debug>1) printf("                          ::GetStrip(%d)  iRelativeId=%d\n",stripId,iRelativeId);
      ret=iStrip[iRelativeId];
    }
    return ret;

  }

  ClassDef(LepDetMapPhase1_planePatch,1);
};


//=========================================
//=========================================
class LepDetMapPhase1_plane{
 private:
  int debug;
  TString name;
  int planeId;
  TVector3 center;
  TVector3 localSize;
  double angle;

  LepDetMapPhase1_tiling *tiling;
  int stripOffset;
  int uniPatchOffset;

  vector<LepDetMapPhase1_planePatch*> patch;
 public:

  LepDetMapPhase1_plane(TString name="blank",int iPlaneId=-1,
			TVector3 iCenter=TVector3(0,0,0),
			TVector3 iSize=TVector3(0,0,0),
			double iAngle=0,
			int iStripOffset=0,
			int iUniPatchOffset=0, LepDetMapPhase1_tiling *t=NULL, int d=0);

  vector<LepDetMapPhase1_planePatch*> GetPatches(){return patch;}
  LepDetMapPhase1_planePatch* GetPatch(int i){return patch[i];}
  TString GetName(){return name;}
  void print(int level){return;}

  TVector3 GetCenter(){return center;}
  TVector3 GetSize(){return localSize;}
  double GetAngle(){return angle;}
  int LocalPositionToStripId(TVector3 position){
    if ((TMath::Abs(position.X())-localSize.X()/2)>0 || ((TMath::Abs(position.Z())-localSize.Z()/2)>0)){
      if (debug>1) printf("LepDetMapPhase1_plane(%s)::LocalPositionToStripId(%2.2f,%2.2f,%2.2f) --> out of bounds (must be within +/-(%2.2f,ANY,%2.2f).\n",name.Data(),position.X(),position.Y(),position.Z(),localSize.X()/2,localSize.Z()/2);
      return -1;
    }
    return tiling->LookupLocalStripId(position.X()/localSize.X()+0.5,position.Z()/localSize.Z()+0.5); //+0.5 because this uses fractional distance from CORNER of detector (-xmax/2,0,-zmax/2), not center (0,0,0).
  }
  DLdetMap_item *LocalPositionToDetMapItem(TVector3 position){
    if (debug>1) printf("LepDetMapPhase1_plane(%s)::LocalPositionToDetMapItem(%2.2f,%2.2f,%2.2f)\n",name.Data(),position.X(),position.Y(),position.Z());
    int localStripId=LocalPositionToStripId(position);
    if (localStripId<0) return NULL;
    int localPatchId=tiling->LookupLocalPatch(localStripId);
    return patch[localPatchId]->GetStrip(localStripId+stripOffset);//GetStrip uses global, not localId.
  }
  DLdetMap_item *StripIdToDetMapItem(int stripId){
    if (debug>1) printf("LepDetMapPhase1_plane(%s)::StripIdToDetMapItem(%d): tiling=%lu\n",name.Data(),stripId,(unsigned long)tiling);
    int p=tiling->LookupLocalPatch(stripId-stripOffset);
    if (p<0 || p>patch.size()) return NULL;
    return patch[p]->GetStrip(stripId);
  }
  TVector3 StripIdToGlobalPosition(int stripId){
    TVector3 localPos=StripIdToDetMapItem(stripId)->center;
    TVector3 globalPos=localPos;
    globalPos.RotateZ(angle);
    return globalPos+localPos;
  }

  ClassDef(LepDetMapPhase1_plane,1);
};


//=========================================
//=========================================
class LepDetMapPhase1 : public DLdetMap {
 public:
  int debug;
  vector<LepDetMapPhase1_plane*> plane;
  DLdetMap_item *myMap;
  TString geomName;
  LepDetMapPhase1_tiling *tiling;
  
  public:
  LepDetMapPhase1(int ver=0, int d=0);
  void print(int level);
  void verify();

  vector<LepDetMapPhase1_plane*> GetPlanes(){return plane;}
  LepDetMapPhase1_plane* GetPlane(int p){return plane[p];}
  
  int LocalPositionToStripId(TString detectorName, TVector3 localPosition){
    if (debug>1) printf("LetpDetMapPhase1::LocalPositionToStripId(%s,(%2.2f,%2.2f,%2.2f))\n",detectorName.Data(),localPosition.X(),localPosition.Y(),localPosition.Z());
    DLdetMap_item *strip=LocalPositionToDetMapItem(detectorName,localPosition);
    if (strip==NULL) return -1;
    return strip->stripId;}
  DLdetMap_item *LocalPositionToDetMapItem(TString detectorName, TVector3 localPosition);
  DLdetMap_item *StripIdToDetMapItem(int stripId);
  TVector3 StripIdToGlobalPosition(int stripId);
  

  ClassDef(LepDetMapPhase1,1);
};

#endif
