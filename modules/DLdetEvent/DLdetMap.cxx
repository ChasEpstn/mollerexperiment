#include "DLdetMap.h"
#include "DLUnits.h"

using namespace dali;

ClassImp(DLdetMap_item);
void  DLdetMap_item::print() {
    printf("DLdetMap item:");
    if( isVoid())
      printf("  stripId=%d is void\n", stripId);
    else {
      if ( isIstrip()) printf(" i-");
      if ( isCstrip()) printf(" c-");
      printf("stripId=%d detId=%d  zRingId=%d phiPatchId=%d uniPatchId=%d center: z/mm=%.1f Rxy/mm=%.1f phi/rad=%.4f delZ/mm=%.1f delPhi/rad=%.4f range=[%.4f,%.4f]\n", stripId, detectorId,zRingId,phiPatchId,uniPatchId, center.Z()/mm, center.Pt()/mm, center.Phi(),zLengthH/mm, phiWidthH, center.Phi()- phiWidthH, center.Phi()+ phiWidthH);
    }
}

