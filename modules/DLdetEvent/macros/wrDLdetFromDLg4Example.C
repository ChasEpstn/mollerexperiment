void wrDLdetFromDLg4Example(TString core="abc",  int mapVer=201502) {
  
  // Load shared library to read MadGraph-TTree
  gSystem->Load("~/.darklight/x86_64/lib/libDLdetEvent");
  gSystem->Load("~/.darklight/x86_64/lib/libDLg4Event");

  DLdetEvent *detEve=new DLdetEvent();
  DLg4Event *g4Eve;


  int maxEve = 100;
  printf("produce %d DLdet events, mapVer=%d, ....\n",maxEve,mapVer);

  //Get pointers to branches used to create the det hits
  TFile *inFile=TFile::Open(core+".DLg4.root","READ");
  assert(inFile->IsOpen());
  TTree *inTree=(TTree*)(inFile->Get("DaLiEventTree"));
  inTree->SetBranchAddress("DLg4Event",&g4Eve);

  
  // Get pointers to branches used to store the det hits
  TFile *outfile=new TFile(core+".DLdet.root","RECREATE"," tree with DL det-events");
  assert(outfile->IsOpen());
  TTree *outTree = new TTree("DLdetEventTree","plain tree with DarkLight det-events");
  outTree->Branch("DLdetEvent",&detEve);
  outTree->Branch("DLg4Event",&g4Eve);

  //get detector mapping to do the translation:
  int detMapVer=1;//generic -- only one version is implemented at the moment
  int detMapDebug=99;//all debug statements.  Set this to zero to see none of them, or ~1 to see some.
  LepDetMapPhase1 *detMap=new LepDetMapPhase1(detMapVer,detMapDebug);


  int nEntries=inTree->GetEntries();
  printf("tree has %d events\n",nEntries);
  for(int i=0;i<maxEve;i++) {
    inTree->GetEntry(i);
    printf("clearing.\n");
    detEve->clear(); // empty old event before use
    printf("getting event header.\n");
    detEve->id=g4Eve->GetEventID();
    detEve->weight=g4Eve->GetWeight();
    detEve->mapVer=mapVer;

    printf("getting hit data.\n");
    int nTracks=g4Eve->GetNumTracks();
    for (int t=0;t<nTracks;t++){
      printf(" getting track %d of %d\n",t,nTracks);
      DLg4Track *track=g4Eve->GetTrack(t);
      int nHits=track->GetNumHits();
      for (int h=0;h<nHits;h++){
      printf(" getting hit %d of %d\n",h,nHits);
	DLg4Hit *hit=track->GetHit(h);
	TVector3 pos=hit->GetLocalPosition();
	TString detName=hit->GetDetectorID();
	printf( "getting stripId for %s, (%2.2f,%2.2f,%2.2f)\n",detName.Data(),pos.X(),pos.Y(),pos.Z());
	int stripId=detMap->LocalPositionToStripId(detName,pos);
	printf( "stripId=%d\n",stripId);
	if (stripId>-1)
	  detEve->addLepDetHit(stripId,hit->GetDE());
      }
    }
    
    //if(i<2)
      detEve->print();
    outTree->Fill();
  } // end of loop over events
  
  //  mTree->Show(1);// event id startting from 0
  // mTree->Print();
  
  outTree->Write();

  printf("\ntotal, nInp=%d,  outFile=%s \n",nEntries, outfile->GetName());
}


