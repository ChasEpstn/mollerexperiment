#ifndef LepDetMapPhase1Util_h
#define LepDetMapPhase1Util_h
#include <TMath.h>
#include <TBox.h>
#include <TEllipse.h>
#include <TLine.h>

#include "LepDetMapPhase1.h"
// \class  LepDetMapPhase1Util
// utility functions for strips mapping  for Lepton Detector for Darklight
// \author Ross Corliss


//-------------------------------------
class LepDetMapPhase1Util {
 private:
  LepDetMapPhase1 *ldMap;
 public:
  LepDetMapPhase1Util(LepDetMapPhase1 *x) { ldMap=x; }
  void test() {
    printf(" LepDetMapPhase1Util::test:\n");
    ldMap->print(1);
  }


//-------------------------------------
//-------------------------------------
  TBox * stripId2TBox(int stripId, int iCol=1) { // for R*phi x Z view
    if( iCol==5) iCol=44; // yellow is bad
    DLdetMap_item *x= ldMap->StripIdToDetMapItem(stripId);
    if (x==NULL) return NULL;
    // printf(" stripId2TBox: "); x->print();
    double z1=x->center.Z() - x->zLengthH;
    double z2=x->center.Z() + x->zLengthH;
    double x1=x->center.X() - x->phiWidthH;
    double x2=x->center.X() + x->phiWidthH;
    TBox *tb=new TBox(z1,x1,z2,x2);
    tb->SetFillColor(iCol);
    return tb;
  }

  TLine *StripIdToTLinePhiZ(int stripId, int iCol=1){
    if( iCol==5) iCol=44; // yellow is bad
    DLdetMap_item *strip= ldMap->StripIdToDetMapItem(stripId);
    if (strip==NULL) return NULL;
    int detId=strip->detectorId;
    LepDetMapPhase1_plane *plane=ldMap->GetPlane(detId);
    TVector3 pCenter=plane->GetCenter();
    float angle=plane->GetAngle();
    TVector3 sCenter=strip->center;
    TVector3 center=sCenter+pCenter;

      
    //no allowance for angles in z:
    double z1,z2,x1,x2,width;
    if (strip->isIstrip()){
      z1=center.Z()-strip->zLengthH;
      z2=center.Z()+strip->zLengthH;
      x1=x2=center.X();
      width=strip->phiWidthH*2;
    }else{
      z1=z2=center.Z();
      x1=center.X()-strip->phiWidthH;
      x2=center.X()+strip->phiWidthH;
      width=strip->zLengthH*2;
    }
    TLine *line=new TLine(z1,x1,z2,x2);
    line->SetLineColor(iCol);
    line->SetLineWidth(width);
    return line;
  }

  TLine *StripIdToTLineXY(int stripId, int iCol=1){
    if( iCol==5) iCol=44; // yellow is bad
    DLdetMap_item *strip= ldMap->StripIdToDetMapItem(stripId);
    if (strip==NULL) return NULL;
    int detId=strip->detectorId;
    LepDetMapPhase1_plane *plane=ldMap->GetPlane(detId);
    TVector3 pCenter=plane->GetCenter();
    double angle=plane->GetAngle();
    TVector3 sCenter=strip->center;
    TVector3 center=sCenter+pCenter;
    TVector3 phiHat(1,0,0);
    phiHat.RotateZ(angle);
      
    //no allowance for angles in z:
    double width;
    TVector3 edge1,edge2;
    if (strip->isIstrip()){
      edge1=center+strip->phiWidthH*phiHat;
      edge2=center-strip->phiWidthH*phiHat;
      width=strip->phiWidthH*2;
    }else{
      edge1=center+strip->phiWidthH*phiHat;
      edge2=center-strip->phiWidthH*phiHat;
      width=strip->zLengthH*2;
    }
    TLine *line=new TLine(edge1.X(),edge1.Y(),edge2.X(),edge2.Y());
    line->SetLineColor(iCol);
    line->SetLineWidth(width);
    return line;
  }

  ClassDef(LepDetMapPhase1Util,1)
};

#endif
