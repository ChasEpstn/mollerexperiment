// \class  LepDetMap
// strips mapping  for Lepton Detector for Darklight
// \author Jan Balewski
#ifndef LepDetMap_h
#define LepDetMap_h
#include "DLUnits.h"
#include <TVector3.h>

using namespace dali;  //uses DLUnits without needing dali::___


enum LepDetDim{mxCyl=5, mxRing=7,mxStripIndex=150000,c_stripOffset=2000, cyl_stripOffset=30000};

class LepDetMap_item { //used for inversed maping of id to physical properties
 public:
  LepDetMap_item() { type=-1; }
  int type;
  int stripId, cylId, zRingId, phiPatchId, uniPatchId;
  TVector3 center;
  double zWidthH, phiWidthH; // half widths
  bool isIstrip() { return type==0;}
  bool isCstrip() { return type==1;}
  bool isVoid() { return type<0;}
  void set(int a, int b, int c, int d,  TVector3 e, double f, double g, int h, int i) {
    type=a;  stripId=b; cylId=c; zRingId=d;
    center=e;   zWidthH=f;  phiWidthH=g;  phiPatchId=h; uniPatchId=i;
  }

  void print();
}; 

//====================================================
class LepDetMap_ring { // all info about lepton detector ring
 public:
  LepDetMap_ring(){};
  void init( int idr, int idc, int stripOff, double rc);
  int myId;
  double z1,z2; // z-coverage
  double myR; // radii of sensitive cylinder
  int nPhiPatch;
  double phiPatchWidth;// computed based on nPhiPatch
  int phiStripIdPhase; // computed based on Z-len &  stripPitch
  int stripIdOffset;
  double stripPitchLen; // note I-strips width is 50% of its pitch
  double stripWidthAngle; // computed based on Pitch & R

  void print(int level=0);
  int position2stripId( TVector3 r, int *isC=0,  int *phiPatchId=0); 
  void fill(LepDetMap_item *map, int cylId);
};

//.............................................
class LepDetMap_cyl { // all info about lepton detector cylinder
 public:
  LepDetMap_cyl(){};
  void init(int idc);
  int myId;
  double cylR, cylReps;
  LepDetMap_ring *ring;
  int stripIdOffset;

  void print(int level=99);
  int Z2ringId(double z); // returns negative on error
  LepDetMap_ring *Z2ring(double z); // returns NULL on error
  void fill(LepDetMap_item *map);
};

//----------------------------------------------
class LepDetMap {
 private:
  LepDetMap_cyl *myCyl;
  LepDetMap_item *myMap;
  int  myVersion; 
  void fillInverseMap();

  public:
  LepDetMap();
  void print(int level); 
  int Rxy2cylId(double rxy); // returns negative on error
  LepDetMap_cyl * Rxy2cyl(double rxy); // returns NULL  on error
  double getRcyl(int cylId) { return myCyl[cylId].cylR; }
  void drawMap();
  int  position2stripId( TVector3 pos, int *isC=0, int *cylId=0, int *ringId=0, int *phiPatchId=0);
  LepDetMap_item * stripId2item( int stripId );
  int version() { return myVersion; }
  int numPhiPatches(int cylId, int ringId);
  int phasePhiPatches(int cylId, int ringId);
  static int maxUnifiedPatchId() { return 500; } // should match with the method below
  static int minUnifiedPatchId() { return 90; } 
  static  int  unifiedPatchId(int cylId, int ringId, int phiPatchId);
};

#endif
