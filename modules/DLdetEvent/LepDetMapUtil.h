#include <TMath.h>
#include <TBox.h>
#include <TEllipse.h>

#include "LepDetMap.h"
// \class  LepDetMapUtil
// utility functions for strips mapping  for Lepton Detector for Darklight
// \author Jan Balewski

class  LepDetMap;
//-------------------------------------
class LepDetMapUtil {
 private:
  LepDetMap *ldMap;
 public:
  LepDetMapUtil(LepDetMap *x) { ldMap=x; }
  void test() {
    printf(" LepDetMapUtil::test:\n");
    ldMap->print(1);
  }


//-------------------------------------
//-------------------------------------
  TBox * stripId2TBox(int stripId, int iCol=1) { // for R*phi x Z view
    if( iCol==5) iCol=44; // yellow is bad
    LepDetMap_item *x= ldMap->stripId2item(stripId);
    if (x==NULL) return NULL;
    // printf(" stripId2TBox: "); x->print();
    double z1=x->center.Z() - x->zWidthH;
    double z2=x->center.Z() + x->zWidthH;
    double p1=x->center.Phi() - x->phiWidthH;
    double p2=x->center.Phi() + x->phiWidthH;
    double r=x->center.Pt();
    TBox *tb=new TBox(z1,p1*r,z2,p2*r);
    tb->SetFillColor(iCol);
    return tb;
}


//-------------------------------------
//-------------------------------------
  TEllipse * 
    stripId2TEllipse(int stripId, int iCol=1) {// for X x Y  end-few
    if( iCol==5) iCol=44; // yellow is bad
    LepDetMap_item *x= ldMap->stripId2item(stripId);
    if (x==NULL) return NULL;
    // printf(" stripId2TEllipse: "); x->print();

    /*
      TArc(Double_t x1, Double_t y1, Double_t radius, Double_t phimin = 0, Double_t phimax = 360)
      TEllipse(Double_t x1, Double_t y1, Double_t r1, Double_t r2 = 0, Double_t phimin = 0, Double_t phimax = 360, Double_t theta = 0)
    */

    if (x->isIstrip() ) { // return small cicrcle at strip center
      float circR=1;
      TEllipse *elip=new  TEllipse( x->center.X(), x->center.Y(),circR,circR);
      elip->SetLineColor(iCol);
      elip->SetFillStyle(0);
      return elip;
    }
    if (x->isCstrip() ) { // return small arc at strip center
      float rad2deg=360/TMath::TwoPi();
      float circR= x->center.Pt();
      double p1=x->center.Phi() - x->phiWidthH;
      double p2=x->center.Phi() + x->phiWidthH;
      TEllipse *elip=new  TEllipse( 0,0,circR,circR,p1*rad2deg,p2*rad2deg);
      elip->SetLineColor(iCol);
      elip->SetFillStyle(0);
      return elip;
    }

    
    return NULL;
}


};
