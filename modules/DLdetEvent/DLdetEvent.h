//
//*-- Author : Jan Balewski, MIT
#ifndef DALI_DET_EVE__HH
#define DALI_DET_EVE__HH

#include <TObject.h>
//---------------
class DLdetHit  : public TObject{ // curated hit container
 public:
  Int_t chanId;
  Int_t value;
  DLdetHit() { clear();}
  void clear() { chanId=-1; value=0; }  
  void print( int flag=0){
    printf(" DLdetHit ch=%d val=%d\n",chanId,value);
    if(flag){}	
  }  

  ClassDef(DLdetHit,1);
};


//---------------
class DLdetEvent : public TObject {
 public:
  Double_t weight; // measured in picobarns for events set
  Int_t id; // event ID
  Int_t mapVer; // mapping of channels to physical coordinates
  std::vector <DLdetHit> lepDet; // list of hits in lepton detector

  // .... methods ....
  DLdetEvent(){};
  Int_t sizeLepDet(){ return (int) lepDet.size();} 

  void addLepDetHit( int chan, int val) {
    DLdetHit hit;
    hit.chanId=chan;
    hit.value=val;
    lepDet.push_back(hit);
  }

  DLdetHit *getLepDetHit(int i) {
    if(i<0 ||i >=  sizeLepDet()) return NULL;
    return &(lepDet[i]);
  }

  void clear() { 
    id=0;weight=0;  mapVer=0;
    lepDet.clear();
  }
  
  void print(int flag=0) {
    printf("\nDLdetEvent:  ID=%d  weight=%g  mapVer=%d sizeLepDet=%d\n",id, weight,mapVer,sizeLepDet());
    for(unsigned int i=0;i< sizeLepDet();i++) {
      printf(" it=%d ",i); lepDet[i].print(flag);    
    }
  }// end of PRINT

  ClassDef(DLdetEvent,1)

};


#endif
