// \class  LepDetMap
// strips mapping  for Lepton Detector for Darklight
// \author Jan Balewski

#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <TMath.h>

#include "DLUnits.h"
using namespace dali;

#include "LepDetMap.h"

void  LepDetMap_item::print() {
    printf("LepDetMap item:");
    if( isVoid()) 
      printf("  stripId=%d is void\n", stripId);
    else {
      if ( isIstrip()) printf(" i-");
      if ( isCstrip()) printf(" c-");
      printf("stripId=%d cylId=%d  zRingId=%d phiPatchId=%d uniPatchId=%d center: z/mm=%.1f Rxy/mm=%.1f phi/rad=%.4f delZ/mm=%.1f delPhi/rad=%.4f range=[%.4f,%.4f]\n", stripId, cylId,zRingId,phiPatchId,uniPatchId, center.Z()/mm, center.Pt()/mm, center.Phi(),zWidthH/mm, phiWidthH, center.Phi()- phiWidthH, center.Phi()+ phiWidthH);
    }
}

//=========================================
//=========================================
void 
LepDetMap_ring::init(int idr, int idc, int stripOff, double rc){

  // tmp dims, 'idc' is indexing cylinders 0,...,4
  double Za[5]={14*cm, 16*cm, 16*cm, 18*cm, 20*cm}; // scale Z of all cylinders with R
  double Zb[5]={12*cm, 15*cm, 18*cm, 21*cm, 24*cm}; 

  //printf(" idr=%d idc=%d\n",idr,idc);
  myId=idr;
  stripIdOffset= stripOff+ 2*c_stripOffset*myId;
  stripPitchLen=1*mm; 
  myR=rc;
  stripWidthAngle=  stripPitchLen/myR/2.;

  switch (myId) {
  case 0: // fixed Z range
    z1=-32*cm; z2=-16*cm; 
    nPhiPatch=6;
    break;

  case 1:
    z1=-16*cm; z2=-0*cm; 
    nPhiPatch=6;
    break;

  case 2:
    z1=0*cm; z2=Za[idc]/2.;
    nPhiPatch=12;
    break;
 
  case 3:
    z1=Za[idc]/2.; z2=Za[idc]; 
    nPhiPatch=12;
    break;
 
  case 4:
    z1=Za[idc]; z2=Za[idc]+ 1./3* Zb[idc]; 
    nPhiPatch=18;
    break;
 
  case 5:
    z1=Za[idc]+ 1./3* Zb[idc]; z2=Za[idc]+ 2./3* Zb[idc]; 
    nPhiPatch=18;
    break;
 
   case 6:
    z1=Za[idc]+ 2./3* Zb[idc]; z2=Za[idc]+ Zb[idc]; 
    nPhiPatch=18;
    break;
 
  default:
    assert(171==191);
  }

  // derived dimensions
  phiPatchWidth=TMath::TwoPi()/ nPhiPatch;
   // compute how many C-strips fit along Z and round it up to nearest 10.
  int nz10=(int) ( (z2-z1)/stripPitchLen/10.);
  phiStripIdPhase=10*(nz10+1);
  //printf(" nz20=%d, phiStripIdPhase=%d\n",nz10,phiStripIdPhase);

}

//=========================================
//=========================================
void 
LepDetMap_ring::print(int level){
  // printf("LepDetMap_ring::print(%d)\n",level);
  printf("  ringId=%d  z1,z2 /cm=%.2f, %.2f nPhiPatch=%d stripIdOffset=%d stripPitchLen/mm=%.1f  r/mm=%.1f phiStripIdPhase=%d\n", myId, z1/cm, z2/cm,  nPhiPatch,stripIdOffset,  stripPitchLen/mm, myR/mm,phiStripIdPhase);

  if(level<=0) return;
}

//=========================================
//=========================================
int 
LepDetMap_ring::position2stripId( TVector3 r, int *isC, int *paId) {
  // printf("LepDetMap_ring::position2stripId (m) x,y,z=%.1f %.1f %.1f Rxy=%.1f  ringId=%d\n",r.X()/mm,r.Y()/mm,r.Z()/mm,r.Pt()/mm,myId);
  
  int stripId=-1;
  double phi=r.Phi();
  if (phi<0) phi+=TMath::TwoPi();
  // printf(" ppp %f %f %d ii %p %p\n",  phi,phiPatchWidth,(int) ( phi/phiPatchWidth),isC,paId);
  int phiPatchId=(int) ( phi/phiPatchWidth);
  if(paId) *paId=phiPatchId;
  int genIndex=(int) (phi/ stripWidthAngle);
  bool isCstrip= genIndex%2;
  // printf("aa  phi/rad=%.4f, genIndex=%d iCstrip=%d  z/mm=%f\n",phi,genIndex, isCstrip,r.Z()/mm);
  if (! isCstrip) 
    stripId= genIndex/2;
  else { // c-strip indexing depends also on Z
    int zIndex=(int)( (r.Z() - z1) / stripPitchLen);
    int localCstripId=phiStripIdPhase*phiPatchId + zIndex;
    //printf( "  phi=%f , /phiPatchWidth=%f zIndex=%d\n", phi,phiPatchWidth,zIndex);
    //printf("   localCstripId=%d phiPatchId=%d zIndex=%d\n", localCstripId,phiPatchId, zIndex);
    assert( localCstripId>=0);
    assert( localCstripId<c_stripOffset);    
    stripId=c_stripOffset+ localCstripId;
  }
  //printf("  phi/rad=%.4f, genIndex=%d iCstrip=%d stripId=%d\n",phi,genIndex, isCstrip,stripId);
  assert(genIndex>=0);
  assert(genIndex<2*c_stripOffset);

  if(isC) {
    // printf("provided isC\n");
    *isC=isCstrip;
  }
  return stripId+stripIdOffset;

}


//=========================================
//=========================================
void 
LepDetMap_ring::fill(LepDetMap_item *map, int cylId) {
  printf("    fill inv. map for ringID=%d\n",myId);
  int isC,phiPatchId;

  //........................................... populate i-strips
  double z=(z1+z2)/2.;
  double hdelZ=(z2-z1)/2.;
  double hdelPhi=stripWidthAngle/2.;
  double phi= hdelPhi; // assumes i-strip is closest to +X axis
  for( ; phi < TMath::TwoPi() ; phi+=2*stripWidthAngle ){ // loop over i-strips from 0 to 2Pi 
    // the c-strips use 50% of the surface
    TVector3 pos; pos.SetPtEtaPhi(myR,0,phi); pos.SetZ(z); // this order works 
    // if(phi<0.1) printf(" z=%f %f  phi=%f %f  r=%f %f\n",z/mm, pos.Z()/mm,phi,pos.Phi(),pos.R/mm,pos.Pt()/mm);
    int stripId= position2stripId( pos, &isC, &phiPatchId);
   
    assert(stripId>=0);
    assert(stripId<mxStripIndex);
    LepDetMap_item *item=map+stripId;
    assert(item->isVoid());
    int uniPatchId=LepDetMap::unifiedPatchId(cylId, myId, phiPatchId);
    item-> set(0, stripId,cylId, myId, pos, hdelZ, hdelPhi, phiPatchId ,  uniPatchId);
    // if(phi<0.1) { printf(" stripId=%d ",stripId); item->print();} // pos.Print();
  }

  //........................................... populate c-strips
  hdelZ= stripPitchLen/2;
  hdelPhi=phiPatchWidth/2.;
  phi= hdelPhi; // 1st patch is closest to +X axis
  for ( ; phi < TMath::TwoPi(); phi+=phiPatchWidth ){ // loop over patches for c-strips
    //printf(" new patch phi=%f\n", phi);
    for( z=z1+hdelZ; z <z2 ; z+= stripPitchLen) { // stepping form z1 to z2
      // 1st c-strip in a patch  has most negative z
      TVector3 pos; pos.SetPtEtaPhi(myR,0,phi); pos.SetZ(z); // this order works       
      int stripId= position2stripId( pos, &isC, &phiPatchId);
      if( isC==0 ) { phi+=stripWidthAngle;   z-= stripPitchLen;  /* printf(" corrected patch phi-avr=%f by delPhi=%f phiPatchId=%d\n", phi,stripWidthAngle,phiPatchId); */ continue; } // add small offset to move to c-strip encodcing
      assert(stripId>=0);
      assert(stripId<mxStripIndex);
      LepDetMap_item *item=map+stripId;
      assert(item->isVoid());
      int uniPatchId=LepDetMap::unifiedPatchId(cylId, myId, phiPatchId);
      item-> set(1, stripId,cylId, myId, pos, hdelZ, hdelPhi, phiPatchId , uniPatchId);
      //  if(stripId==32000) { printf(" set stripId=%d ",stripId); item->print();} // pos.Print();
    }
    
  }    

}


//=========================================
//=========================================

//=========================================
//=========================================
void 
LepDetMap_cyl::init(int idc){
  myId=idc;
  cylReps=5*mm;
  stripIdOffset=cyl_stripOffset*myId;
  ring=new  LepDetMap_ring[mxRing];
  
  switch (myId) {
  case 0:
    cylR=9*cm;
    break;

  case 1:
    cylR=11*cm;
    break;

  case 2:
    cylR=13*cm;
    break;

  case 3:
    cylR=17*cm;
    break;

  case 4:
    cylR=25*cm;
    break;

  default:
    assert(17==19);
  }

  // define segmentation of each ring
  for(int ir=0;ir<mxRing;ir++)
    ring[ir].init(ir,myId, stripIdOffset,cylR);
}

//=========================================
//=========================================
void 
LepDetMap_cyl::print(int level){
  printf("LepDetMap_cyl::print(%d), mxRing=%d  stripIdOffset=%d\n",level,mxRing,stripIdOffset);
  printf("  cylId=%d  R/cm=%.1f, epsR/mm=%.1f\n", myId, cylR/cm, cylReps/mm);

  if(level<=0) return;

  for(int ir=0;ir<mxRing;ir++)
    ring[ir].print(level-1);
}

//=========================================
//=========================================
int 
LepDetMap_cyl::Z2ringId(double z){ 
  //printf("_cyl::Z2ringId cylID=%d z1/m=%f z/mm=%f\n",myId,ring[0].z1,z);
   if ( z > ring[mxRing-1].z2 ) return -1;
  for(int ir=mxRing-1;ir>=0;ir--)
    if ( z >  ring[ir].z1 ) return ir;
  return -2;
}

//=========================================
//========================================= 
LepDetMap_ring * 
LepDetMap_cyl::Z2ring(double z){ 
   int ringId=Z2ringId(z);
   // printf("aa ringId=%d\n",ringId);
   if ( ringId<0 ) return NULL;
   return ring+ringId;
}

//=========================================
//=========================================
void 
LepDetMap_cyl::fill(LepDetMap_item *map) {
  printf(" fill inv. map for cylID=%d\n",myId);
  for(int ir=0;ir<mxRing;ir++)
    ring[ir].fill(map,myId);
 
}


//=========================================
//=========================================

//=========================================
//=========================================
LepDetMap::LepDetMap() {
  myVersion=201502; // unique ID of current mapping implementation
  printf("cnstr LepDetMap version=%d\n",myVersion);
  myCyl= new  LepDetMap_cyl [mxCyl];

  // define segmentation of each cylinder
  for(int ic=0;ic<mxCyl;ic++) 
    myCyl[ic].init(ic);

  myMap=NULL;

}

//=========================================
//=========================================
void 
LepDetMap::print(int level){
  printf("LepDetMap::print(%d), version=%d mxCyl=%d\n",level,myVersion,mxCyl);
  if(level<=0) return;
  
  for(int ic=1;ic<mxCyl;ic++) // for now skip cyl #0 (replacing proton det)
    myCyl[ic].print(level-1);
 
}

//=========================================
//=========================================
int 
LepDetMap:: Rxy2cylId(double rxy){
  for(int ic=0;ic<mxCyl;ic++) 
    if (fabs(rxy-myCyl[ic].cylR ) < myCyl[ic].cylReps) return ic;
  return -1;
}

//=========================================
//=========================================
int 
LepDetMap::position2stripId( TVector3 pos, int *isC, int *cylId, int *ringId, int *paId){
   LepDetMap_cyl *geoCyl=Rxy2cyl(pos.Perp());
   if(geoCyl==NULL) return -1 ; //  it was not lepton det hit
   // geoCyl->print(0);
   // printf(" mmm search for z/mm=%f\n", pos.Z());
   if(cylId) *cylId=geoCyl->myId;
   LepDetMap_ring * geoRing=geoCyl->Z2ring(pos.Z());  
   if(geoRing==NULL) return -2 ; //   z was outside the converage of cylinder
   // geoRing->print(0);
   if(ringId) *ringId=geoRing->myId; 
   int stripId=geoRing->position2stripId(pos,isC,paId);
   if (stripId<0) return -3 ; // internal mapping error
   return  stripId; 
}

//=========================================
//=========================================
LepDetMap_cyl * 
LepDetMap::Rxy2cyl(double rxy){
   int cylId= Rxy2cylId(rxy);
   if ( cylId<0 ) return NULL;
   return myCyl+cylId;
}

//=========================================
//=========================================
int
LepDetMap::numPhiPatches(int cylId, int ringId){
   if ( cylId<0 ) return -1;
   if ( cylId>=mxCyl ) return -2;
   if ( ringId<0 ) return -3;
   if ( ringId>=mxRing ) return -4;
   return myCyl[cylId].ring[ringId].nPhiPatch;
}

//=========================================
//=========================================
int
LepDetMap::phasePhiPatches(int cylId, int ringId){ 
   if ( cylId<0 ) return -1;
   if ( cylId>=mxCyl ) return -2;
   if ( ringId<0 ) return -3;
   if ( ringId>=mxRing ) return -4;
   return myCyl[cylId].ring[ringId].phiStripIdPhase;
}

//=========================================
//=========================================
void 
LepDetMap::fillInverseMap(){
  for(int ic=1;ic<mxCyl;ic++) {// for now skip cyl #0 (replacing proton det)
    myCyl[ic].fill(myMap);
    //printf("\n WARN: tmp filling lep-det-map only for cyl=1 in LepDetMap::fillInverseMap() - it takes too long\n\n");    break;
  }
}

//=========================================
//=========================================
int 
LepDetMap::unifiedPatchId(int cylId, int ringId, int phiPatchId){
 if (cylId<0) return -10;
    if (cylId>=mxCyl) return -11;

    if ( phiPatchId<0) return -2;
    if ( ringId<0) return -1;
    if ( ringId<2) {
      if ( phiPatchId>=6) return -3;
      return phiPatchId+ 6*ringId + 100*cylId;
    }
    if ( ringId<4){
      if ( phiPatchId>=12) return -4;
      return 12+ phiPatchId+ 12*(ringId-2) + 100*cylId;
    } 
    if ( ringId<7){
      if ( phiPatchId>=18) return -5;
      return 36+ phiPatchId+ 18*(ringId-4) + 100*cylId;
    }
    return -6;
}

//=========================================
//=========================================
LepDetMap_item *  
LepDetMap::stripId2item( int stripId ) {
  if ( myMap==NULL) {
     myMap= new  LepDetMap_item[mxStripIndex];
     printf(" LepDetMap generating inverse mapping , once ...\n");
     fillInverseMap();
  }

  if ( stripId <0 ) return NULL;  
  if ( stripId >= mxStripIndex ) return NULL;
  if ( myMap[stripId].isVoid() )  return NULL;
  return  myMap+stripId;
}




#include "TH1.h"
//=========================================
//=========================================
void 
LepDetMap::drawMap(){
  TH1F *h1=new TH1F("drM","drawMap; stripIndex; center phi (rad)", mxStripIndex, -0.5,mxStripIndex-0.5);
  int nok=0;
  for(int is=0;is<mxStripIndex;is++) {
     LepDetMap_item *item=myMap+is;
     if( item->isVoid()) continue;
     // if( item->isCstrip()) continue;
     nok++;
     double phi=item->center.Phi();
     if(phi<0) phi+=TMath::TwoPi();
     h1->Fill(is, phi);
  }  
  printf(" nok=%d\n",nok);
  h1->Draw();
}
