// \class  LepDetGeom
// static class for loading the geometry of the leptop detector for LepDetMap
// \author Ross Corliss
#ifndef LepDetGeom_h
#define LepDetGeom_h
#include "DLUnits.h"
#include <TVector3.h>
//#include "the_portable_detector_geometry_file" 

using namespace dali;


class LepDetGeom{
  int GetNplanes(TString geomID);
  int GetNcylinders(TString geomID);
  void GetPlane(TString geomID, int planeID, TString &name,
		double &radius, double &angle, double &zcenter,
		double &zlength, double &width);
  void GetCylinder(TString geomID, int planeID, TString &name,
		double &radius, double &zcenter,
		double &zlength);
};

//move this to a .cxx?
int LepDetGeom::GetNplanes(TString geomID){
  switch (geomID)
    {
    case "geomMRI_verA":
    case "geomMRI_verB":
    case "geomMRI_verC":
    case "geomMRI_verD":
      return 8;
      break;
    default:
      return 0;
    }
}

int LepDetGeom::GetNcylinders(TString geomID){
  //in the future, build in phase2?
  switch (geomID)
    {
    case "geomMRI_verA":
    case "geomMRI_verB":
    case "geomMRI_verC":
    case "geomMRI_verD":
      return 0;
      break;
    default:
      return 0;
    }
}


void LepDetGeom::GetPlane(TString geomID, int planeID, TString &name,
			  double &radius, double &angle, double &zcenter,
			  double &zlength, double &width){
  if (planeID>=GetNplanes(geomID){
      name=0;radius=0;angle=0;zcenter=0;zlength=0;width=0;
      printf("Out of bounds: geomID=%s has no plane #%d.\n",geomID.Data(),planeID);
    }

  double *flatIRplane; //array of the inner radius of each plane. dist. from x=y=0 at point of closest approach
  double *flatLplane; //full length of plane in z
  double *flatHplane; //full height of plane in y
  double *flatHoffset; //y position of center of plane vertical offset of plane from y=0.
  double *flatLoffset;
  double *flatRot;
  const G4String *flatName;
  bool useDetailedGEMs=false; //whether to use the 4-layered approximation, or a single-layer approximation.

    switch (geomID){
      case "geomMRI_verA":
	const int numPlanesVerA=8;
	double irVerA[]={145*mm, 170*mm, 195*mm, 220*mm,
			 145*mm, 170*mm, 195*mm, 220*mm};
	double lVerA[]={ 600*mm, 600*mm, 600*mm, 600*mm,
			 600*mm, 600*mm, 600*mm, 600*mm};
	double hVerA[]={ 280*mm, 280*mm, 280*mm, 280*mm,
			 280*mm, 280*mm, 280*mm, 280*mm};
	double offVerA[]={ 0*mm,   0*mm,   0*mm,   0*mm,
			   0*mm,   0*mm,   0*mm,   0*mm};
	double rotVerA[]={90*degree,90*degree,90*degree,90*degree,
			  270*degree,270*degree,270*degree,270*degree};
	static const TString nameVerA[]={"lepCy1R","lepCy2R","lepCy3R","lepCy4R",
					  "lepCy1L","lepCy2L","lepCy3L","lepCy4RL"};
	flatIRplane=irVerA;
	flatLplane=lVerA;
	flatHplane=hVerA;
	flatHoffset=offVerA;
	flatLoffset=offVerA;
	flatRot=rotVerA;
	flatName=nameVerA;
	nPlanes=numPlanesVerA;
	useDetailedGEMs=false;
	break;
      case "geomMRI_verB":
	const int numPlanesVerB=8;
	double irVerB[]={100*mm, 120*mm, 140*mm, 160*mm,
			 100*mm, 120*mm, 140*mm, 160*mm};
	double lVerB[]={ 600*mm, 600*mm, 600*mm, 600*mm,
			 600*mm, 600*mm, 600*mm, 600*mm};
	double hVerB[]={ 140*mm, 150*mm, 160*mm, 170*mm,
			 140*mm, 150*mm, 160*mm, 170*mm};
	double offVerB[]={ 0*mm,   0*mm,   0*mm,   0*mm,
			   0*mm,   0*mm,   0*mm,   0*mm};
	double rotVerB[]={90*degree,90*degree,90*degree,90*degree,
			  270*degree,270*degree,270*degree,270*degree};
	static const TString nameVerB[]={"lepCy1R","lepCy2R","lepCy3R","lepCy4R",
					  "lepCy1L","lepCy2L","lepCy3L","lepCy4RL"};
	flatIRplane=irVerB;
	flatLplane=lVerB;
	flatHplane=hVerB;
	flatHoffset=offVerB;
	flatLoffset=offVerB; //caution -- using the zeros twice!
	flatRot=rotVerB;
	flatName=nameVerB;
	nPlanes=numPlanesVerB;
	useDetailedGEMs=false;
	break;
      case "geomMRI_verC":
	const int numPlanesVerC=8;
	//thickness=2cm, spacing=1cm, OD of pipe=7.6cm ==> start =9.6cm
	double irVerC[]={96*mm, 126*mm, 156*mm, 186*mm,
			 96*mm, 126*mm, 156*mm, 186*mm};//centerpoint, as written
	double lVerC[]={ 410*mm, 410*mm, 410*mm, 410*mm,
			 410*mm, 410*mm, 410*mm, 410*mm};
	double hVerC[]={ 240*mm, 240*mm, 240*mm, 240*mm,
			 240*mm, 240*mm, 240*mm, 240*mm};
	double offVerC[]={ 0*mm,   0*mm,   0*mm,   0*mm,
			   0*mm,   0*mm,   0*mm,   0*mm};
	double offLVerC[]={ 0*mm,   0*mm,   0*mm,   0*mm,
			    0*mm,   0*mm,   0*mm,   0*mm};
	double rotVerC[]={0*degree,0*degree,0*degree,0*degree,
			  180*degree,180*degree,180*degree,180*degree};
	static const TString nameVerC[]={"lepA1","lepA2","lepA3","lepA4",
					  "lepB1", "lepB2", "lepB3", "lepB4"};
	flatIRplane=irVerC;
	flatLplane=lVerC;
	flatHplane=hVerC;
	flatHoffset=offVerC;
	flatLoffset=offLVerC;
	flatRot=rotVerC;
	flatName=nameVerC;
	nPlanes=numPlanesVerC;
	useDetailedGEMs=true;
	break;
      case "geomMRI_verD":
	const int numPlanesVerD=8;
	//thickness=2cm, spacing=1cm, OD of pipe=7.6cm ==> start =9.6cm
	double irVerD[]={96*mm, 126*mm, 156*mm, 186*mm,
			 96*mm, 126*mm, 156*mm, 186*mm};//centerpoint, as written
	double lVerD[]={ 410*mm, 410*mm, 410*mm, 410*mm,
			 410*mm, 410*mm, 410*mm, 410*mm};
	double hVerD[]={ 240*mm, 240*mm, 240*mm, 240*mm,
			 240*mm, 240*mm, 240*mm, 240*mm};
	double offVerD[]={ 0*mm,   0*mm,   0*mm,   0*mm,
			   0*mm,   0*mm,   0*mm,   0*mm};
	double offLVerD[]={ 0*mm,   0*mm,   0*mm,   0*mm,
			    230*mm,   230*mm,   360*mm,   360*mm};
	double rotVerD[]={0*degree,0*degree,0*degree,0*degree,
			  180*degree,180*degree,180*degree,180*degree};
	static const TString nameVerD[]={"lepA1","lepA2","lepA3","lepA4",
					  "lepB1", "lepB2", "lepB3", "lepB4"};
	flatIRplane=irVerD;
	flatLplane=lVerD;
	flatHplane=hVerD;
	flatHoffset=offVerD;
	flatLoffset=offLVerD;
	flatRot=rotVerD;
	flatName=nameVerD;
	nPlanes=numPlanesVerD;
	useDetailedGEMs=true;
	break;
      default:
	return;
    }
    

  return;
}
void LepDetGeom::GetCylinder(TString geomID, int cylID, TString &name,
			     double &radius, double &zcenter,
			     double &zlength){
  if (cylID>=GetNcylinders(geomID){
      name=0;radius=0;zcenter=0;zlength=0;
      printf("Out of bounds: geomID=%s has no cylinder #%d.\n",geomID.Data(),cylID);

  return;
}
