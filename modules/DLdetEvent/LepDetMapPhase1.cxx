// \class  LepDetMapPhase1
// Patch and Strip mapping  for Lepton Detector for Phase 1 Darklight
// \author Ross Corliss

#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <TMath.h>

#include "DLUnits.h"
using namespace dali;

#include "LepDetMapPhase1.h"
#include "DLdetMap.h"

ClassImp(LepDetMapPhase1_tiling);
ClassImp(LepDetMapPhase1);
ClassImp(LepDetMapPhase1_plane);
ClassImp(LepDetMapPhase1_planePatch);
//TILING:
//=========================================
//=========================================
LepDetMapPhase1_tiling::LepDetMapPhase1_tiling(int ver, int d){ //someday, this will take more params...
  if (debug>0) printf("_tiling: staring constructor\n");
  debug=d;
  int nPatches;
  if (ver==1){
    cPitch=1*mm;
    iPitch=1*mm;
    nZdivs=5;
    //nPhiDivs=new int[nZdivs];
    //zSize=new int[nZdivs];

    //old, wide pattern:
    //phiSize=240;
    //static int tzSize[]={150,80,80,50,50};
    //static int tnPhiDivs[]={2,4,4,6,6};
    //nPatches=22;
    //offsetPerZ=1000;
    //iOffset=0;
    //cOffset=500;
    //offsetPerPlane=8000;
    phiSize=120;
    static int tzSize[]={140,80,80,50,50};
    static int tnPhiDivs[]={1,2,2,3,3};
    zSize=tzSize;
    nPhiDivs=tnPhiDivs;
    offsetPerZ=400;
    offsetPerPlane=2000;
    iOffset=0;
    cOffset=200;
    lookupTableReady=false;
  }
  
  nPatches=0;
  for (int i=0;i<nZdivs;i++){
    nPatches+=nPhiDivs[i];
  }

  //create the lookup table that takes stripId --> patch
  if (debug>1) printf("_tiling:  constructing patch boundaries\n");


  
  int nPatchBoundaries=(nPatches+nZdivs)*2;
  float patchIdBoundary[nPatchBoundaries];
  int patchId[nPatchBoundaries-1];
  int p=0;
  int pActive=0;
  int edge=0;
  int lowestPatch=0;
  for (int i=0;i<nZdivs;i++){
    if(i>0) {
      patchId[p]=-1;//space between Z's, but not before the first one or after the last
      lowestPatch+=nPhiDivs[i-1]; //lowest patch is upped by the number of divs of the previous ring.
    }
    patchIdBoundary[p]=i*offsetPerZ;
    if (debug>2) printf("_tiling:   patchIdBoundary[%d]=%.1f (start of i)\n",p,patchIdBoundary[p]);
    p++;
    int patchPhiSize=phiSize/nPhiDivs[i];
    int patchZsize=zSize[i];

    //all the i's come first, then all the c's.
    for (int j=0;j<nPhiDivs[i];j++){
      patchId[p]=lowestPatch+j;
      patchIdBoundary[p]=patchIdBoundary[p-1]+patchPhiSize;//high edge.  We already did low.
      if (debug>2) printf("_tiling:   patchIdBoundary[%d]=%.1f (high edge of i)\n",p,patchIdBoundary[p]);
      p++;
    }
    //space between i and c:
    patchId[p]=-1;
    patchIdBoundary[p]=i*offsetPerZ+cOffset;
    if (debug>2) printf("_tiling:   patchIdBoundary[%d]=%.1f (start of c)\n",p,patchIdBoundary[p]);
    p++;
    
   //all the c's:
    for (int j=0;j<nPhiDivs[i];j++){
      patchId[p]=lowestPatch+j;
      patchIdBoundary[p]=patchIdBoundary[p-1]+patchZsize;//high edge.  We already did low.
      if (debug>2) printf("_tiling:   patchIdBoundary[%d]=%.1f (high edge of c)\n",p,patchIdBoundary[p]);
      p++;
    }
  }


  

  patchPerId=new TH1I("patchPerId","patchPerId;stripId;patchId",nPatchBoundaries-1,patchIdBoundary);
  for (int i=0;i<nPatchBoundaries-1;i++){
    //if (i>0 && patchIdBoundary[i-1]>=patchIdBoundary[i])
    if (debug>2) printf("_tiling:   %.1f<stripId<%.1f: patchId=%d\n",patchIdBoundary[i],patchIdBoundary[i+1],patchId[i+1]);
    patchPerId->Fill( (patchIdBoundary[i]+patchIdBoundary[i+1]) /2,patchId[i+1]);
  }
  //debug only:  patchPerId->Draw();




  
  //create the lookup table that takes z --> zring
  if (debug>1) printf("_tiling:  constructing zring boundaries\n");
  float zBoundary[nZdivs+1];
  zBoundary[0]=0;
  for (int i=0;i<nZdivs;i++){
    zBoundary[i+1]=zBoundary[i]+zSize[i];
    if (debug>2) printf("_tiling:   zBoundary[%d]=%f\n",i,zBoundary[i+1]);
  }
  totZsize=zBoundary[nZdivs];
  zRingPerZ=new TH1I("zRingPerZ","zRingPerZ;z fractional position;zRing",nZdivs,zBoundary);
  for (int i=0;i<nZdivs;i++){
    zRingPerZ->Fill((zBoundary[i]+zBoundary[i+1])/2,i);
  }

  if (debug>0) printf("_tiling: finished constructor\n");
  
  return;
}


int LepDetMapPhase1_tiling::LookupLocalPatch(int stripId){
  if (debug>1) printf("LepDetMapPhase1_tiling::LookupLocalPatch(%d)",stripId);
  if (stripId<0) {
    if (debug>1) printf("aborting -- out of range\n");
    return -1;
  }
  if (debug>1) printf("\n");
  float floatSafeStripId=stripId+0.5;//stripId is an integer, and might fall on boundaries.  Add 0.5 to center it in a bin.
  int binId=patchPerId->FindBin(floatSafeStripId);
  if (binId<1 || binId>patchPerId->GetNbinsX()) return -1;
  int patchId= patchPerId->GetBinContent(binId);
  if (debug>2) printf("LepDetMapPhase1_tiling::LookupLocalPatch(%d):  binId=%d==>patchId=%d\n",stripId,binId,patchId);
  
  return patchId;
}

int LepDetMapPhase1_tiling::LookupLocalStripId(float xfrac, float zfrac){
  if (debug>1) printf("LepDetMapPhase1_tiling::LookupLocalStripId(%1.4f,%1.4f)\n",xfrac,zfrac);
  //caution that floating points fractions very close to boundaries may return the adjacent strip instead
  if (xfrac>1 || xfrac <0 || zfrac>1 || zfrac<0) return -1;
  //x and z are in position relative to CORNER divided by total length
  if (!lookupTableReady){
    BuildLookupLocalStrip();
  }
  int x=(xfrac)*phiSize;
  int z=(zfrac)*totZsize;
  int stripId=localStripId[x][z];
  if (debug>2) printf("LepDetMapPhase1_tiling::LookupLocalStripId:  localStripId[%d][%d]=%d\n",x,z,stripId);
  return stripId;
}

void LepDetMapPhase1_tiling::BuildLookupLocalStrip(){
  if (debug>10) printf("LepDetMapPhase1_tiling::BuildLookupLocalStrip()\n");
  for (int x=0;x<phiSize;x++){
    float xfrac=(x+0.5)*1.0/phiSize;
    for (int z=0;z<totZsize;z++){
      float zfrac=(z+0.5)*1.0/totZsize;
      localStripId[x][z]=CalculateLocalStripId(xfrac,zfrac);
    }
  }
  lookupTableReady=true;
  return;
}

  
int LepDetMapPhase1_tiling::CalculateLocalStripId(float xfrac, float zfrac){
  //x and z are in position divided by total length -- should revise to not use fractions but stripwidths
  //caution that floating points fractions very close to boundaries may return the adjacent strip instead
  
  //get Z ring:
  int z=zfrac*totZsize;
  int zRingBin=zRingPerZ->FindBin(z);
  int zRing=zRingPerZ->GetBinContent(zRingBin);

  int x=xfrac*phiSize;
  int stripType=x%2;//

  //get patch:
  int index=-1;
  if (stripType==ISTRIP){//I
    //x position gives index:
    index=x+iOffset;
  }
  else if (stripType==CSTRIP){
    //fractional x position gives the patch:
    int patchInRing=xfrac*nPhiDivs[zRing];
    //relative z position gives index:
    int zMin=zRingPerZ->GetBinLowEdge(zRingBin);
    index=z-zMin+patchInRing*zSize[zRing]+cOffset;
  }
  int fullIndex=index+zRing*offsetPerZ;
  if (debug>12) printf("LepDetMapPhase1_tiling::CalculateLocalStrip(%1.4f,%1.4f): x=%d,z=%d, stripType=%d, zRing=%d,indexInRing=%d, index=%d\n",xfrac,zfrac,x,z,stripType,zRing,index,fullIndex);
  if (index<0) return index;
  
  return fullIndex;
}












//PLANE_PATCH

//=========================================
//=========================================
LepDetMapPhase1_planePatch::LepDetMapPhase1_planePatch(TVector3 patchCenter, TVector3 patchSize, int nC, int nI, int cOffset, int iOffset, int planeId, int zRingId, int phiPatchId, int uniPatchId, int d){
  debug=d;
  if (debug>0) printf("LepDetMapPhase1_planePatch: starting constructor\n");

  //strip dimensions:
  double stripPadding=0*um; //set to nonzero to have dead space between strips
  //by default, i-Strips are continuous, while c-Strips are pads:
  // I C I C I C I <-- The strip closest to the lower edge is always I.
  // | - | - | - | ...
  // | - | - | - | ...
  // | - | - | - | ...
  // | - | - | - | ...
  // | - | - | - | ...
  // | - | - | - | ...
 
  if (debug>1) printf("LepDetMapPhase1_planePatch:  loading variables for strips\n");

  center=patchCenter;
  localSize=patchSize;
  
  double cHalfSize=patchSize.X()/2;
  double iHalfSize=patchSize.Z()/2;

  double cPitch=patchSize.Z()/nC;
  double iPitch=patchSize.X()/nI;

  double cHalfWidth=cPitch/2-stripPadding;//half the spacing
  double iHalfWidth=iPitch/4-stripPadding;//1/4 the spacing -- have to accommodate the c strip pads between each i strip!
  
  if (debug>1) printf("LepDetMapPhase1_planePatch:  clearing strip vectors\n");

  cStrip.clear();//=new DLdetMap_item[nC];
  iStrip.clear();//=new DLdetMap_item[nI];

  TVector3 iHat(iPitch,0,0);//indexing direction * spacing for 'i' in local coords
  TVector3 cHat(0,0,cPitch);//indexing direction * spacing for 'c' in local coords

  //TVector3 shiftToCenter(iPitch/2,0,cPitch/2);//move from the lower edge of a pitch x pitch block to the center.
  
  if (debug>1) printf("LepDetMapPhase1_planePatch:  constructing C strips\n");
  for (int i=0;i<nC;i++){
    TVector3 stripCenter=center+(i-nC/2.0+0.5)*cHat;//patch center, move left half the total number of strips (because half are to the left of center), then move right the current strip number from the left edge, then move half a pitch in to the center of the strip.
    if (debug>2) printf("LepDetMapPhase1_planePatch:  constructing C strip %d (stripId=%d, patchId=%d) centered at (%2.2f,%2.2f,%2.2f)\n",
			i,i+cOffset,uniPatchId,stripCenter.X(),stripCenter.Y(),stripCenter.Z());
    cStrip.push_back( new DLdetMap_item(PLANE,CSTRIP,i+cOffset,planeId,zRingId,phiPatchId, uniPatchId,stripCenter,cHalfWidth,cHalfSize));
  }

  if (debug>1) printf("LepDetMapPhase1_planePatch:  constructing I strips\n");
  for (int i=0;i<nI;i++){
    TVector3 stripCenter=center+(i-nI/2.0+0.5)*iHat; //-(nI/2.0)*iHat+iHalfWidth+i*iHat;
    if (debug>2) printf("LepDetMapPhase1_planePatch:  constructing I strip %d (stripId=%d, patchId=%d) centered at (%2.2f,%2.2f,%2.2f)\n",i,i+iOffset,uniPatchId,stripCenter.X(),stripCenter.Y(),stripCenter.Z());
    iStrip.push_back( new DLdetMap_item(PLANE,ISTRIP,i+iOffset,planeId,zRingId,phiPatchId, uniPatchId,stripCenter,iHalfSize,iHalfWidth));
    //non-vector version:  iStrip[i].set(LepDetType::PLANE,LepStripType::I,i+iOffset,planeId,zRingId,phiPatchId, uniPatchId,stripCenter,iSize,pitch);
  }
    if (debug>0) printf("LepDetMapPhase1_planePatch: finishing constructor\n");
    return;
}







//PLANE

//=========================================
//=========================================
LepDetMapPhase1_plane::LepDetMapPhase1_plane(TString iName,int iPlaneId, TVector3 iCenter, TVector3 iSize, double iAngle, int iStripOffset, int iUniPatchOffset, LepDetMapPhase1_tiling *t, int d){
  debug=d;
  if (debug>0) printf("LepDetMapPhase1_plane: starting constructor\n");
  name=iName;
  planeId=iPlaneId;
  center=iCenter;
  localSize=iSize;
  angle=iAngle;
  stripOffset=iStripOffset;
  uniPatchOffset=iUniPatchOffset;
  tiling=t;
  if (tiling==NULL){
    if (debug>1) printf("LepDetMapPhase1_plane:  tiling is NULL\n");
  
    patch.push_back(new LepDetMapPhase1_planePatch(iCenter,iSize,1,1,iStripOffset,iStripOffset+1,iPlaneId,0,iPlaneId,iPlaneId,debug-1));
    return;
  }
  
  if (debug>1) printf("LepDetMapPhase1_plane:  tiling is not null.  Loading tiling params\n");
  if (debug>4) printf("LepDetMapPhase1_plane:  tiling=%lx\n",(unsigned long)tiling);
  
  //load tiling scheme in from the tiling scheme:
  int stripOffsetForI=tiling->iOffset;
  int stripOffsetForC=tiling->cOffset;
  int stripOffsetPerZ=tiling->offsetPerZ;
  int nZsteps=tiling->nZdivs;
  double pitch=1*mm*localSize.X()/tiling->phiSize;
  double zSize[nZsteps];
  int nPhiDivs[nZsteps];
  for (int i=0;i<nZsteps;i++){
    zSize[i]=1*mm*tiling->zSize[i];
    nPhiDivs[i]=tiling->nPhiDivs[i];
    if (debug>10) printf("LepDetMapPhase1_plane:   zSize[%d]=%f,nPhiDivs[%d]=%d\n",i,zSize[i],i,nPhiDivs[i]);
  }
  
  TVector3 zHat(0,0,1);
  double zMin=-localSize.Z()/2.0;
  double xMin=-localSize.X()/2.0;
  double zCenter=zMin;//start at zmin and go from there
  int totPatches=0;
  
  if (debug>1) printf("LepDetMapPhase1_plane:  constructing patches\n");
  for (int z=0;z<nZsteps;z++){
    if (debug>2) printf("LepDetMapPhase1_plane:   constructing zring %d\n",z);
    zCenter+=zSize[z]/2.0;//shift from upper edge of previous to center of this one.
    double xPatchSize=localSize.X()/nPhiDivs[z];
    int nC=tiling->zSize[z];
    int nI=tiling->phiSize/nPhiDivs[z];
    for (int i=0;i<nPhiDivs[z];i++){
      if (debug>2) printf("LepDetMapPhase1_plane:    constructing patch %d of %d\n",i, nPhiDivs[z]);
      double xCenter=xMin+xPatchSize*i+xPatchSize/2.0;
      TVector3 tCenter(xCenter,0,zCenter);
      TVector3 pSize(xPatchSize,iSize.Y(),zSize[z]);
      int cOffset=stripOffset+z*stripOffsetPerZ+stripOffsetForC+nC*i;
      int iOffset=stripOffset+z*stripOffsetPerZ+stripOffsetForI+nI*i;
      patch.push_back(new LepDetMapPhase1_planePatch(tCenter,pSize,nC,nI,cOffset, iOffset, planeId,z,i,planeId*22+totPatches,debug-1));
      totPatches++;
    }
    zCenter+=zSize[z]/2.0;//shift from center of this one to upper edge
  }
  
  if (debug>0) printf("LepDetMapPhase1_plane: finishing constructor\n");
 
  return;
}
  


//=========================================
//=========================================
LepDetMapPhase1::LepDetMapPhase1(int ver, int d) :  DLdetMap(ver){
  debug=d;
  if (debug>0) printf("LepDetMapPhase1: starting constructor\n");

  //parse version number:
  //version=ver;
  int leptonVersion=(version/1)%100;
  int protonVersion=(version/100)%100;
  int photonVersion=(version/10000)%100;
  //etc

  //etc:  if (leptonVersion==1){
    geomName="geomMRI_verC";
    //}
  
   
   
   if (debug>1) printf("LepDetMapPhase1:  geom=%s\n",geomName.Data());

  //at some point, this should pull these from the geometry:
  //int nPlanes=LepDetGeom::GetNplanes(geomName);

  //TString *detName=LepDetGeom::GetDetName(geomName);

  //double *length=LepDetGeom::GetLength(geomName);
  //double *width=LepDetGeom::GetWidth(geomName);
  //double activeLayerDepth=LepDetGeom::GetActiveLayerDepth(geomName);

  //double *tangentialOffset=LepDetGeom::GetTangential(geomName);
  //double *zOffset=LepDetGeom::GetZ(geomName);
  //double *radius=LepDetGeom::GetRadius(geomName);
  //double *angle=LepDetGeom::GetAngle(geomName);
  //double activeLayerOffset=LepDetGeom::Get(geomName);

  //for now, force using 'VerC":
  //set geom:
  const int nPlanes=8;
  //go down to the base layer, then up past the empty dead layer, past the air gap, and halfway through the active layer:
  double activeLayerOffset=-10*mm+5*mm+1*mm+1.5*mm;
  double irVerC[]={96*mm, 126*mm, 156*mm, 186*mm,
		   96*mm, 126*mm, 156*mm, 186*mm};//centerpoint, as written
  double lVerC[]={ 400*mm, 400*mm, 400*mm, 400*mm,
		   400*mm, 400*mm, 400*mm, 400*mm};
  double hVerC[]={ 120*mm, 120*mm, 120*mm, 120*mm,
		   120*mm, 120*mm, 120*mm, 120*mm};
  double offVerC[]={ 0*mm,   0*mm,   0*mm,   0*mm,
		     0*mm,   0*mm,   0*mm,   0*mm};
  double offLVerC[]={ 0*mm,   0*mm,   0*mm,   0*mm,
		      0*mm,   0*mm,   0*mm,   0*mm};
  double rotVerC[]={0*degree,0*degree,0*degree,0*degree,
		    180*degree,180*degree,180*degree,180*degree};
  static const TString nameVerC[]={"lepA1","lepA2","lepA3","lepA4",
				    "lepB1", "lepB2", "lepB3", "lepB4"};
  
  if (debug>1) printf("LepDetMapPhase1:  making new tiling\n");
  tiling=new LepDetMapPhase1_tiling(1,debug-1);
  if (debug>4) printf("LepDetMapPhase1:  tiling=%lx\n",(unsigned long)tiling);
  int stripOffsetPerPlane=tiling->offsetPerPlane;
  int patchesPerPlane=tiling->nPatches;

  //build layers using that geom:
  if (debug>1) printf("LepDetMapPhase1:  building planes\n");
  plane.clear();
  for (int i=0;i<nPlanes;i++){
    if (debug>2) printf("LepDetMapPhase1:   getting params for plane %d\n",i);
    double phi=rotVerC[i]*radian;
    double r=irVerC[i]+activeLayerOffset;
    TVector3 center(r*TMath::Sin(phi),r*TMath::Cos(phi),offLVerC[i]);
    TVector3 size(hVerC[i],3*mm,lVerC[i]);
    if (debug>2) printf("LepDetMapPhase1:   pushing plane %d\n",i);
    plane.push_back(new LepDetMapPhase1_plane(nameVerC[i],i,
					      center,size,phi,
					      (i)*stripOffsetPerPlane,i*patchesPerPlane,tiling,debug-1));//final digit is for indexing scheme just in case.
  }
  
  if (debug>0) printf("LepDetMapPhase1: finished with constructor\n");
  
}

//=========================================
//=========================================
void 
LepDetMapPhase1::print(int level){
  printf("LepDetMapPhase1::print(%d), geom=%s nPlanes=%lu\n",level,geomName.Data(),plane.size());
  if(level>0){
    for (int i=0;i<plane.size();i++){
      plane[i]->print(level-1);
    }
  }

  return;
}

//=========================================
//=========================================
void 
LepDetMapPhase1::verify(){
  printf("LepDetMapPhase1::verify(), geom=%s nPlanes=%lu\n",geomName.Data(),plane.size());

  
  const int nStrips=64000;//wow.  That's big.
  DLdetMap_item *strip[nStrips];
  for (int i=0;i<nStrips;i++){
    strip[i]=NULL;
  }

  //check uniqueness:
  int nStripsUsed=0;
  for (int i=0;i<nStrips;i++){
    if (debug>0) printf("Seeking stripId=%d\n",i);
    DLdetMap_item *s=StripIdToDetMapItem(i);
    strip[nStripsUsed]=s;
    if (s!=NULL){
      for (int j=0;j<nStripsUsed;j++){
	if (s==strip[j]) assert ("strip used twice!"=="oops");
      }
      nStripsUsed++;
    }

  }
  printf("found %d unique, non-null strips.  no stripId found more than once.\n",nStripsUsed);


  int nErr=0;
  for (int i=0;i<plane.size();i++){
    LepDetMapPhase1_plane *pl=plane[i];
    TVector3 c=pl->GetCenter();
    TVector3 s=pl->GetSize();
    printf("plane %d (\"%s\") centered at (%f,%f,%f) has %lu patches\n",i,pl->GetName().Data(),c.X(),c.Y(),c.Z(), pl->GetPatches().size());
    for(int j=0;j<pl->GetPatches().size();j++){
      LepDetMapPhase1_planePatch *pa=pl->GetPatch(j);
      if (pa==NULL) {
	//printf("  patch %d doesn't exist\n",j);
	//nErr++;
	continue;//break;
      }
      c=pa->GetCenter();
      s=pa->GetSize();
      printf("  patch %d centered at (%f,%f,%f), size= (%f,%f,%f)\n",j,c.X(),c.Y(),c.Z(),s.X(),s.Y(),s.Z());
      for (int k=0;k<2;k++){
	vector<DLdetMap_item*>st=pa->GetStrips(CSTRIP);
	if (k>0) st=pa->GetStrips(ISTRIP);
	for (int m=0;m<st.size();m++){
	  //detailed check to see if the strips are self-consistent in lookup tables:
	  int id=st[m]->stripId;
	  TVector3 sc=st[m]->center;
	  //check size:
	  if (TMath::Abs(sc.X()-c.X())>s.X() || TMath::Abs(sc.Z()-c.Z())>s.Z()){
	    printf("  strip %d centered at (%f,%f,%f), is outside of patch centered at (%f,%f,%f), size= (%f,%f,%f)\n",id,sc.X(),sc.Y(),sc.Z(),c.X(),c.Y(),c.Z(),s.X(),s.Y(),s.Z());
	    nErr++;
	  }
	  int idByPos=LocalPositionToStripId(pl->GetName(),sc);
	  int idById=StripIdToDetMapItem(id)->stripId;
	  if (id!=idByPos) {
	    printf("  strip %d at (%2.2f,%2.2f,%2.2f) is recovered by LocalPositionToStripId(%s) as id=%d\n",id,sc.X(),sc.Y(),sc.Z(),pl->GetName().Data(),idByPos);
	    nErr++;
	  }
	  if (id!=idById) {
	    printf("  strip %d is recovered by StripIdToDetMapItem as id=%d\n",id,idById);
	    nErr++;
	  }
	}
      }
    }
  }
  printf("Found %d consistency errors.\n",nErr);
  return;
}



TVector3 
LepDetMapPhase1::StripIdToGlobalPosition(int stripId){
  //dead-reckon the correct plane:
  int p=stripId/tiling->offsetPerPlane;
  return plane[p]->StripIdToGlobalPosition(stripId);
}

DLdetMap_item *
LepDetMapPhase1::StripIdToDetMapItem(int stripId){
  if (debug>1) printf("LepDetMapPhase1::StripIdToDetMapItem(%d)\n",stripId);

  DLdetMap_item *ret=NULL;
  //dead-reckon the correct plane:
  int p=stripId/tiling->offsetPerPlane;
  if (debug>2) printf("LepDetMapPhase1::StripIdToDetMapItem(%d): plane=%d\n",stripId,p);
  if (p<0 || p>=plane.size()) return NULL;
  ret=plane[p]->StripIdToDetMapItem(stripId);
  if (debug>2) printf("LepDetMapPhase1::StripIdToDetMapItem(%d) finished\n",stripId);

  return ret;
}

DLdetMap_item *
LepDetMapPhase1::LocalPositionToDetMapItem(TString detectorName, TVector3 position){
  if (debug>1) printf("LepDetMapPhase1::LocalPositionToDetMapItem(%s,(%2.2f,%2.2f,%2.2f))\n",detectorName.Data(),position.X(),position.Y(),position.Z());
  for (int i=0;i<plane.size();i++){
    if (plane[i]->GetName()==detectorName){
      return plane[i]->LocalPositionToDetMapItem(position);
    }
  }
  return NULL;
}








