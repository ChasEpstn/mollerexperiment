// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
//*--  Author: Jan Balewski, MIT, January 2012
//*--  Updated  October 2014
//
// $Id: JFitMaster.cc,v 1.7 2011/12/15 18:10:14 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#include <assert.h>
#include <math.h>
#include <TRandom3.h>
#include <TMinuit.h>
#include <TH1.h>
#include <TFile.h> // 4Ttree write

#include <TRandom3.h> 
#include <TLine.h>

#include "DLmadEvent.h"
#include "DLg4Event.h"
#include "JFitMaster.h"
#include "JFsimUtil.h"
#include "JFtrackModelHPT.h"
#include "JFmonitor.h"

// G4E -related
#include "JFg4eUtil.h"
#include "G4ErrorFreeTrajState.hh"
#include "JFglobalHack.h"  // one global to rule them all

#include "DLUnits.h"
//The following works, unless one also inherits a 'using namespace CLHEP', which can appear in G4 headers
//when using both G4 and dali units, one must specify (even though they should be identical)

//using namespace dali;  //uses DLUnits without needing dali::___


//=====================================
JFitMaster::JFitMaster(){

  // those are owned
  treeChain=0;
  fileTout=0;
  g4Eve=new  DLg4Event ; // input & output
  madEve=new  DLmadEvent;  // input & output, only to pass-by info

  fitMon= new JFmonitor;
  simUtil=new JFsimUtil( );

  GH-> fitMon= fitMon; // for chi2 vs. nfunc

  setMinRecoTrack(1); // drops fit if less seed is found
  par_nSeedNode=4; // minimum  of hits for seed calculation
  par_maxIterFcn=800; // max no. of calls of the fit-cuntion (aka G4E full track swim)
  par_avrTrackDist=2.*dali::mm; // this is QA cut on the track on a average distance fit-hit

  sequentialMinuit  = new TMinuit(JFglobalHack::nSequentialFitPar);

  sequentialMinuit->SetPrintLevel(1); // -1=quiet
  double arglist[10];
  int error_flag = 0;
  arglist[0] = 1;
  //Minuit defines parameter errors as the change  in parameter value required to change the function value
  sequentialMinuit->mnexcm("SET ERR",arglist,1,error_flag);
  //Interprets a command and takes appropriate action
  //void mnexcm(const char* comand, Double_t* plist, Int_t llist, Int_t& ierflg)

 
}

//=====================================
void JFitMaster::setLeptonHitSigSR(float x, float y) { 
  simUtil->setLeptonHitSigSim(x); 
  simUtil->setLeptonHitSigReco(y); 
}


//=====================================
void JFitMaster::inputEventFile(TString filename){
  filename+=".DLg4.root";
  printf("JFitMaster::inputEventFile=%s\n",filename.Data());
  treeChain= new TChain("DaLiEventTree");
  treeChain->Add(filename); 
  treeChain->SetBranchAddress("DLg4Event",&g4Eve);
  treeChain->SetBranchAddress("DLmadEvent",&madEve);// input & output, only to pass-by info
  int nEve = (int)treeChain->GetEntries();
  printf("Opened DLg4Event file(s)=%s nEve=%d \n",filename.Data(),nEve );
  assert(nEve>0);
  printf("Use JFitMaster: minRecoTrack=%d maxIterFcn=%d par_avrTrackDist/mm=%.1f\n",par_minRecoTrack, par_maxIterFcn,par_avrTrackDist/dali::mm);

 // auxiliary initializations of other  used classes:
  fitMon->Init();
  TList *Lx=fitMon->hSG[3]->GetListOfFunctions(); assert(Lx);
  TLine *ln=new TLine(par_maxIterFcn,0,par_maxIterFcn,1e6);  ln->SetLineColor(kRed);  ln->SetLineStyle(2);  Lx->Add(ln);

  Lx=fitMon->hSG[5]->GetListOfFunctions(); assert(Lx);
  ln=new TLine(par_avrTrackDist/dali::mm,0,par_avrTrackDist/dali::mm,1e6);  ln->SetLineColor(kRed);  ln->SetLineStyle(2);  Lx->Add(ln);
}



//=====================================
void JFitMaster::outputEventFile(TString filename){
  filename+=".DLg4e.root";
  printf("JFitMaster::outputEventFile=%s\n",filename.Data());
  fileTout = new TFile(filename, "RECREATE", "G4E DarkLight events");
  if (fileTout->IsZombie()){
    printf("Unable to open file \"%s\" for writing.  Aborting.\n",fileTout->GetName());
    exit(131);
  } else {
    printf("Open file \"%s\" for writing, OK.\n",fileTout->GetName());
 }
  treeDL= new TTree("DaLiEventTree","Combined Tree Mad+G4+G4E",99);
  // output geant4-event container 
  treeDL->Branch("DLmadEvent",&madEve);
  treeDL->Branch("DLg4Event",&g4Eve); 
  treeDL->Branch("DLjfitEvent",&(GH->recEve)); 

}


//===============================================
//===============================================
bool JFitMaster::readOneEvent(int  ieve){
  GH->inpEve->clear();
  GH->recEve->clear();
  GH->fitAbortCode=0;

  treeChain->GetEntry(ieve);
  GH->inpEve->id=g4Eve->GetEventID();
  GH->inpEve->run=555;
  GH->inpEve->weight=g4Eve->GetWeight();
  if(ieve%1==0 ) { 
    printf("\n\n==================  readOneEvent(%d)======= inpEve ID=%d g4trks=%d\n",ieve,GH->inpEve->id, g4Eve->GetNumTracks());
    madEve->print(1);
  }

   fitMon->IncrementCase("inp",GH->inpEve);  
  
  if( g4Eve->GetNumTracks() <par_minRecoTrack) {
    return false;
  }
  g4Eve->Print();
  fitMon->IncrementCase("aG4tr",GH->inpEve);  

  int nUsable=0;
  for(int itr=0; itr<g4Eve->GetNumTracks(); itr++) {
    DLg4Track* g4Trk=g4Eve->GetTrack(itr);
    // if(g4Trk->GetParticleName()!="e-")  continue; // tmp1 for debugging 
    if(simUtil->digitizeCylHits(g4Trk)) {
      nUsable++;
      fitMon->hSG[2]->Fill("in "+g4Trk->GetParticleName(),1.);
    } else {
      continue; // ?? why did I wanted to store truth for tracks/ w/o hits?
    }
    //.... store truth for residua calcualation
    JPhysParticle particle;
    particle.name=g4Trk->GetParticleName().Data();
   
    double mass=0.51; 
    if(particle.name=="e+")  particle.charge=1; 
    else if(particle.name=="e-")  particle.charge=-1; 
    else if(particle.name=="proton") {
      particle.charge=1; mass=938; }

    particle.mom4.SetVectM(g4Trk->GetMomentum(),mass);
    GH->inpEve->addParticle(particle);
  }

  printf("JFitMaster::readOneEvent nUsable (aka trackable) hit-sequences  =%d , required =%d\n",nUsable,par_minRecoTrack);
  if( nUsable <par_minRecoTrack) {
    return false;
  }

  printf("ieve=%d nUsable=%d >=%d\n",ieve, nUsable,par_minRecoTrack);
  fitMon->IncrementCase("mLongG4tr",GH->inpEve);  

  { // add vertex truth to inpEve
    assert(g4Eve->GetNumTracks()>0);
    //    DLg4Track* g4Trk=g4Eve->GetTrack(0); 
    GH->inpEve->vertPos=g4Eve->GetVertex()->GetPosition();
  }

  GH->inpEve->print(0);
  
  return true;
}


//===============================================
//===============================================
void JFitMaster::finish(TString name){
  fitMon->saveHisto(name);
  if(fileTout) {
    printf("    JFitMaster: output tree saved %s\n",fileTout->GetName());
    treeDL->Print();
    fileTout->Write();
    fileTout->Close();
  }

}

//===============================================
//===============================================
//=====================================
int JFitMaster::computeSeedHelix(JFitTrack *jfTrack, TVector3 vert3D, JFtrackModelHPT &seedHelix){
 // select 3 points on the track 
  
  int nNodes=jfTrack->sizeNodes();
  if(nNodes<par_nSeedNode)  return 0; //?
 
  assert(nNodes>=3);
  TVector3 A=jfTrack->getNode(0)->hit.getV3()-vert3D; // begin
  TVector3 B=jfTrack->getNode(1)->hit.getV3()-vert3D; // middle
  TVector3 C=jfTrack->getNode(2)->hit.getV3()-vert3D; // end

  int charge=findSeedHelix(A,B,C, 0.,   seedHelix);
  printf(" end-of computeSeedHelix, model:Z.abc= %f %f %f , charge=%d\n",A.Z(),B.Z() ,C.Z(),charge);
  
  return charge;
}




//===============================================
//===============================================
//=====================================
int JFitMaster::findSeedHelix(TVector3 A, TVector3 B, TVector3 C, double Z0,  JFtrackModelHPT & seedHelix){ // returns charge or 0 on abort
  
  //jx  double par_RxyBeam_mm=90; // tmp, to abort seed computation if seed has worng offset, eg. eve 6393 in A'=30 files

  int zDir=0; // invalid

#if 0 // jx , old code, assumes z-monotonic for high pT tracks
  // assure 3 points are monotonic in Z
  if( A.Z()< B.Z() &&  B.Z() <C.Z() ) zDir=1;
  else if (A.Z()> B.Z() &&  B.Z() >C.Z())zDir=-1;
  else return 0;
#endif

  // use first pair of points to determin the charge
  zDir=1;
  if( A.Z() > B.Z() ) zDir=-1;

  assert(zDir!=0);
   
  //calculate the slopes of lines connecting A-B and B-C
  Float_t m_a = ( B.Y()-A.Y() ) / ( B.X()-A.X() );
  Float_t m_b = ( C.Y()-B.Y() ) / ( C.X()-B.X() );
  
  //calculate the center of the circle
  Float_t R_x = ( m_a*m_b*( A.Y()-C.Y() ) + m_b*( A.X()+B.X() ) - m_a*( B.X()+C.X() ) ) / 2. / ( m_b-m_a );
  Float_t R_y = -1./m_a * ( R_x - ( A.X()+B.X() )/2. ) + ( A.Y()+B.Y() )/2.;

  seedHelix.Rcen.Set(R_x,R_y);

  printf("radius of curvature = %f mm  zDir=%d\n",seedHelix.Rcen.Mod()/dali::mm,zDir);
  
  //calculate the angular speed
  TVector2 a(A.X(),A.Y());
  TVector2 b(B.X(),B.Y());
  a-=seedHelix.Rcen;
  b-=seedHelix.Rcen;
  
  Float_t deltaPhi = (b.Rotate(-a.Phi())).Phi();
  //printf("deltaPhi 1  = %f deg \n",deltaPhi/dali::deg);
  deltaPhi = b.Phi_mpi_pi(deltaPhi);
  printf("deltaPhi 2 = %f deg   B.Z()-A.Z()/mm=%.2f  \n",deltaPhi/dali::deg, ( B.Z()-A.Z())/dali::mm);
  seedHelix.alpha = deltaPhi / ( B.Z()-A.Z() );
  
  Float_t Zmid = Z0; // set it externaly, needed for phi angle absolute value
  printf(" (mm) Zmid=%.2f  Zmid-A.Z()=%.2f, A.Z()=%.2f \n",Zmid/dali::mm, (Zmid-A.Z())/dali::mm, A.Z()/dali::mm);
  printf("angle between the first point and Dmid = %f radian \n",seedHelix.alpha*( Zmid-A.Z() ));
  a=a.Rotate( seedHelix.alpha*( Zmid-A.Z() ) );
  a+=seedHelix.Rcen;
  seedHelix.Dmid.SetXYZ(a.X(),a.Y(),Zmid);
  printf("end of findSeedHelix, DmidXYZ=%.2g, %.2g, %.2g mm  a.Mod()=%f Dmid.Per=%f \n", seedHelix.Dmid.X()/dali::mm,seedHelix.Dmid.Y()/dali::mm,seedHelix.Dmid.Z()/dali::mm,a.Mod(),seedHelix.Dmid.Perp());

  int charge=1;
  //jx  change here sign convention for DL reco, 2014-04-04 
  if( zDir *seedHelix.alpha <0)  charge=-1;
  seedHelix.charge=charge;

  //  printf("zDir=%d alpha=%f charge=%d\n", zDir,  seedHelix.alpha, charge);

  return charge;
}




// $Log: JFitMaster.cc,v $
    
