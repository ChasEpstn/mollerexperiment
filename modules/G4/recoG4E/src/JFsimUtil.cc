// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
//*--  Author: Jan Balewski, MIT, January 2012
//*--  Author: Len K. Eun, LBL, March, 2012
//
// $Id: JFsimUtil.cc,v 1.7 2011/12/15 18:10:14 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#include <assert.h>
#include <math.h>
#include <TRandom3.h>

#include "DLg4Event.h"
#include "JFglobalHack.h" // one global to rule them all

#include "DLUnits.h"
using namespace dali;  //uses DLUnits without needing dali::___
#include "JFsimUtil.h"

//=====================================
JFsimUtil::JFsimUtil(){
  par_LepHitSigSim=0.5*mm; //(mm) gaussian, added noise
  par_LepHitSigReco=0.5*mm; //(mm) gaussian, used for chi2 in reco
  printf("JFsimUtil:  use lepton hit simu/mm=%f  reco/mm=%f\n",par_LepHitSigSim/mm,par_LepHitSigReco/mm);
  assert( par_LepHitSigSim>9*um);
}


//===============================================
//===============================================
int JFsimUtil::digitizeCylHits(DLg4Track * g4Trk){ //RETURN 1 if track is usable for fit
  printf(" JFsimUtil::digitizeCylHits() copy & digitilaze hits for selected track g4tr_id=%d\n",g4Trk->GetTrackID());
  
  //a1 double par_maxCrossAngle=60./180.*3.1416;// for accepted hits
  int par_minHitLeptonTrack=4; // for leptons

  JFitTrack jfTrack; // output container for hits
  
  TString lastDetName="bhla"; double lastR=0;
  int lastDir=1; //(inside -->outside )
  for(int i=0; i<g4Trk->GetNumHits(); i++){
    DLg4Hit* hit=g4Trk->GetHit(i);
    TString detName=hit->GetDetectorID();
    if(!detName.Contains("lepCy")) continue; // skip non-sensitive volumes
    if(detName==lastDetName)  continue; // skip duplicated hits in the same cyl

    TVector3 pos=hit->GetPosition(); 
    int moveDir= lastR <= pos.Perp();
    lastR= pos.Perp();
    double angle=pos.Angle(hit->GetLocalMomentum()); 
    //a1 if(angle>par_maxCrossAngle) continue;
    // printf("   moveDir=%d lastDir=%d lastR=%f\n",moveDir,lastDir,lastR);
    if(moveDir<=0) break; // do not accept returning tracks (2014-06-02)
    //    if(moveDir>0 && lastDir<=0) break; // do not accept 2nd spiral
    lastDetName=detName;
    lastDir=moveDir;

    // add noise:
    double err1D=par_LepHitSigSim;
    //jx if(detName.Contains("SiSens"))err1D=par_SilHitSig_mm;
    double errPhi=gRandom->Gaus(0, err1D)/pos.Pt();
    double newZ=gRandom->Gaus(pos.Z(),err1D);

    // here noise is added to the G4 hit
    pos.RotateZ(errPhi);
    pos.SetZ(newZ); 
    angle=pos.Angle(hit->GetLocalMomentum()); // WARN - never tested chang form glob to local P

    printf(" Harvest hit/mm: x=%.2f y=%.2f z=%.2f  Rxy=%.2f cyl=%s err1D/mm=%.3f angle/deg=%.1f lastDir=%1d\n",pos.X(),pos.Y(),pos.Z(),pos.Perp(),detName.Data(),err1D,angle/3.1416*180., lastDir);  

    JFcylTrackNode node(detName,pos.Perp(),pos.Phi(),pos.Z(),par_LepHitSigReco);

    jfTrack.addNode(node);

  } // end of loop over hits for this track

printf("... done  extractRawHits(), harvested nHit=%d \n", jfTrack.sizeNodes());



#if 0 // old bad logic to pick proton track, fix it later 
  if (jfTrack.sizeNodes()> 1)jfTrack.isLepton=1;// only proton is stopped in Si0
  int usable=0;
  if (  jfTrack.isLepton==1 ) {
    if (jfTrack.sizeNodes()>=par_minHitLeptonTrack) usable=1;
  } else {
    if (jfTrack.sizeNodes()>=1) usable=1;
  }
#endif
  
  jfTrack.isLepton=1; 
  jfTrack.g4TrackID=g4Trk->GetTrackID();
  int usable=0;
  if( jfTrack.sizeNodes()>=par_minHitLeptonTrack) usable=1;
 
  
  if(usable) {
    GH->inpEve->addTrack(jfTrack);
    printf("digitizeCylHits-ok, G4 truth: trk:%s pT=%.2f MeV/c pZ=%.2f MeV/c, zOrigin=%.1fcm used nNodes=%d\n",g4Trk->GetParticleName().Data(),g4Trk->GetMomentum().Perp(),g4Trk->GetMomentum().Z(),g4Trk->GetOrigin().Z()/10., jfTrack.sizeNodes());
  } else {
    printf(" skip track in digitization\n");
  }
  return usable;

}



// $Log: JFsimUtil.cc,v $
    
