// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
// Global utility pointer to hide all Minuit global-pointers mess
//
//*--  Author: Jan Balewski, MIT, March 2013
//
// $Id:  $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#include "JFitEvent.h"
#include "JFglobalHack.h"
#include "globals.hh" // contains G4-unit system
#include "G4SystemOfUnits.hh" // to fix CLHEP issue


//==========================
JFglobalHack::JFglobalHack(){
  inpEve= new JFitEvent; 
  recEve= new JFitEvent; 
  fitMon=0;
  useVertex=0; // 0=no
  vertX0=0*cm; vertY0=-0*cm; //(mm) vertex average location in transverse plane
  printf("JFglobalHack() vert offset x/mm=%.1f, y/mm=%.1f \n",vertX0/cm,vertY0/cm);
}
