// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
//
//*--  Author: Jan Balewski, MIT, January 2013
//
// $Id: JFmonitor.cc,v 1.6 2011/12/15 21:02:48 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
#include <assert.h>

#include <TMath.h>
#include <TFile.h>
#include <TObjArray.h>
#include <TH2.h>
#include <TProfile.h>
#include <TLine.h>

#include "DLg4Event.h"  //for : fillE1E2TH12_g4Eve(..)
#include "JFitEvent.h"
#include "JFmonitor.h"
#include "JFsimUtil.h"

#include "JFtrackModelHPT.h" // only to convert PT-->R

#define deg2rad (TMath::DegToRad())
#include "JFitMaster.h"

#include "DLUnits.h"
using namespace dali;  //uses DLUnits without needing dali::___

//=====================================
//=====================================
void JFmonitor::Init(){
  HList=new TObjArray;
  Init_seq_glob();
  for(int i=0;i<mxSG;i++)  if(hSG[i])HList->Add(hSG[i]);
 
}

//=====================================
//=====================================
void JFmonitor::Init_seq_glob(){
  memset(hSG,0,sizeof(hSG)); 
  TH1* h; //TLine *ln;  TList *Lx; 
  double mxPt=90*MeV; // MeV - larg PT range
   
  hSG[0]=h=new TH1F("aEveCase","Events yields ;case",11,0,11);  h->GetXaxis()->SetTitleOffset(0.4);  h->GetXaxis()->SetLabelSize(0.06);  h->GetXaxis()->SetTitleSize(0.05); h->SetMinimum(0.);  h->SetLineColor(kBlue); h->SetFillColor(42);h->SetLineWidth(2);  h->SetMarkerSize(2);//<-- large text

  //predefine order of bins
  char const *nameA[]={"inp","aG4tr","eeAnyG4tr","mLongG4tr","eeLongG4tr","zVseed","mTrSeed","mTrFit","IMcut2","-"};
  for(unsigned int i=0;i<sizeof(nameA)/sizeof(char*);i++)  hSG[0]->Fill(nameA[i],0.0);

  hSG[1]=h=(TH1F*) hSG[0]->Clone();
  h->SetNameTitle("aEveWCase","Events weight sums");
  h->Sumw2();

  hSG[2]=h=new TH1F("aSeqCase","Tracks , sequntial fit;case",15,0,15);  h->GetXaxis()->SetTitleOffset(0.4);  h->GetXaxis()->SetLabelSize(0.06);  h->GetXaxis()->SetTitleSize(0.05); h->SetMinimum(0.);  h->SetLineColor(kBlue); h->SetFillColor(42);h->SetLineWidth(2);  h->SetMarkerSize(2);//<-- large text
 
  //predefine order of bins
  char const *nameB[]={"in proton","in e-","se e-","re e-","0","in e+","se e+","re e+","se bad", "fitCnvrg","goodChi2", "W:missCyl","-"};
  for(unsigned int i=0;i<sizeof(nameB)/sizeof(char*);i++)  hSG[2]->Fill(nameB[i],0.0);
  hSG[3]=new TH1F("anFcnCnt","# fit func/track, sequential; number of calls",200,0,1500);
 
  hSG[4]=h=new TH1F("ach2dof","CHI2/DOF per track, sequential; #chi^2/DOF; # of tracks", 4000,0,400.);
  h->SetFillColor(27);

  hSG[5]=h=new TH1F("amm2dof","avr helix2hits dist, per track, sequential; #sqrt{chi^2/DOF}*sigReco (mm); # of tracks", 4000,0,20.);
  h->SetFillColor(27);
  
  hSG[6]=h=new TH2F("ach2iter","total CHI2 per track during fit, sequential; nFunc call; total  #chi^2 ~~",200,0,800, 400,0,400);

  //free 7-9
  //new ....
  //..... reconstructed tracks
  hSG[10]=h=new TH1F("arecPtpos","reco PT Q=e+, seq.glob.; reco  PT (MeV/c); # tracks",500, 0.,mxPt);
  hSG[11]=h=new TH1F("arecPtneg","reco PT Q=e-, seq.glob. ; reco  PT (MeV/c); # tracks",500, 0.,mxPt);
  hSG[12]=h=new TH1F("arecEtapos","reco #eta Q=e+, seq.glob. ; reco  #eta ; # tracks",100, -3,3);
  hSG[13]=h=new TH1F("arecEtaneg","reco #eta Q=e-, seq.glob.; reco  #eta ; # tracks",100, -3,3);

  //free 14-19

  //..... reconstructed track errors
  hSG[20]=h=new TH2F("arelDPT_PT"," #Delta PT/PT  vs. PT, seq.glob.; true PT (MeV);  100 * #Delta PT / PT; #tracks",30,0.5,mxPt+0.5,50, -mxPt/5,mxPt/5);

  hSG[21]=h=new TH2F("anFitP_Rcu","# of 2D hits used per track, seq.glob.; true R curv (cm);  nFitPoints",35,0.5,65,20, -0.5,19.5);

  hSG[22]=h=new TH2F("arelAng_Eta"," angle(trueP-recoP)   vs. Eta,  seq.glob.; true  Eta;  #Delta angle (mrad); #tracks",30,-2,2,50,0,190);
  
  hSG[23]=h=new TH2F("arelDPT_Eta"," true-reco PT  vs. true Eta, seq.glob.; true Eta ;  100 * #Delta PT/ PT ; #tracks",30,-2,2,50, -mxPt/3,mxPt/3);

  hSG[24]=h=new TH2F("aHitErr_RT","norm.  3D hit vs. R;  hit R (cm); 3D #delta position/#sigma reco",50,0,50,50,0,5);

}



//=====================================
//=====================================
void  JFmonitor::IncrementCase(TString sCase, JFitEvent * inpEve){
  hSG[0]->Fill(sCase,1.);
  hSG[1]->Fill(sCase,inpEve->weight);
}


//-------------------------------------------
//-------------------------------------------
void JFmonitor::saveHisto(TString fname) {
  fname+=".fitMon.root";
  TFile f(fname.Data(),"recreate");
  assert(f.IsOpen());
  printf(" histos are written  to '%s'  \n" ,fname.Data());
  HList->Write();
  
  f.ls();//list saved histos
  f.Close();
  assert(!f.IsOpen());

  printf("          , save Ok %d events fitMonitor'ed\n",NtotEve);
}



//=====================================
//=====================================

void JFmonitor::EvalTrackResidua(JFitEvent * inpEve,JFitEvent * recEve, int itrk){

   JPhysParticle *trueParticle= inpEve->getParticle(itrk);
   JPhysParticle *recoParticle= recEve->getParticle(itrk);

   JFitTrack *recoTrack = inpEve->getTrack(itrk);

   double truePT=trueParticle->mom4.Pt();
   double recoPT=recoParticle->mom4.Pt();
   double relPtErr=(truePT-recoPT)/truePT;

   double trueEta=trueParticle->mom4.Eta();
   double recoEta=recoParticle->mom4.Eta();

   TVector3 trueP3=trueParticle->mom4.Vect();
   TVector3 recoP3=recoParticle->mom4.Vect();
   double angErr=recoP3.Angle(trueP3);

   JFtrackModelHPT helModel;// tmp, bad solution
   double trueRcur=helModel.PtMeV_to_Rmm(truePT); // (mm) this is confusing hack
   // printf("xx truePT=%f  trueRcur=%f\n", trueRcur, truePT);

   // only reco track info
   assert(recoParticle->isValid());
   int iQoff=0;
   if(recoParticle->charge==-1) iQoff=1;

   hSG[10+iQoff]->Fill(recoPT/MeV);
   hSG[12+iQoff]->Fill(recoEta);

   hSG[20]->Fill(truePT/MeV,100*relPtErr);
   hSG[21]->Fill(trueRcur/cm,recoTrack->sizeNodes());
   hSG[22]->Fill(trueEta,angErr/mrad);
   hSG[23]->Fill(trueEta,100*relPtErr);



   //.......  grab  some pointers...
   std::vector<JFcylTrackNode>  &recNodes=recEve->getTrack(itrk)->nodes;
  
  // JFtrackModelHPT &rec=recEve->trkModel;
  //  double recPt=rec.getPtMeV();
  //double delRcur=trueRcur-rec.Rcen.Mod();
  // double rel100PtErr=100*delRcur/rec.Rcen.Mod();
  
  for(int it=0;it<(int)recNodes.size(); it++) {// loop over nodes in  real event
    JFcylTrackNode *rNode=&recNodes[it]; // real 
    if(!rNode->hit.inFit) continue; // skip nodes not qulified for use in fitting
    assert(rNode->other);
    JFcylTrackNode *iNode=rNode->other;
    TVector3 posIn=iNode->hit.getV3();
    TVector3 posRe=rNode->hit.getV3();
    TVector3 delV3=posIn-posRe; // 3D difference between hit recorded and reco
    //printf("it=%d  zz=%f %f   dd=%f\n",it,posIn.Z(), posRe.Z(), delV3.Mag());
    hSG[24]->Fill(posIn.Perp()/cm,  delV3.Mag()/iNode->hit.sig1D);    
  }// end of loop over hist
}


// $Log: JFmonitor.cc,v $
