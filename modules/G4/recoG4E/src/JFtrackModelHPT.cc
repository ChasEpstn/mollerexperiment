// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
//
//*--  Author: Jan Balewski, MIT, January 2012
//
// $Id: JFtrackModelHPT.cc,v 1.6 2011/12/15 21:02:48 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#include <TMath.h>

#include "JFtrackModelHPT.h"

#define deg2rad (TMath::DegToRad())

//=====================================

JFtrackModelHPT::JFtrackModelHPT(TVector3 xDmid,  TVector2 xRcen,Double_t xalpha, int q){
  clear();
  Dmid=xDmid;
  Rcen=xRcen;
  alpha=xalpha;
  charge=q;  
}


//=====================================

JFtrackModelHPT::~JFtrackModelHPT(){
  clear();
}


//=====================================

void JFtrackModelHPT::print(){
  
  printf("\ttrkModelHPT ");
  if(charge) 
    printf(" Dmid XYZ=%.2f, %.2f, %.2f,  Rcen X,Y=%.2f,%.2f Rcurv=%.2f  alpha(rad/m)=%.2f  PT/MeV=%.2f, Rphi0/deg=%.1f  Px,y=%.2f , %.2f, Pz/MeV=%.2f charge=%d\n",
	   Dmid.X(),Dmid.Y(),Dmid.Z(),  Rcen.X(), Rcen.Y(), getRcurv(), alpha*1000., getPtMeV(),getRphi0()/3.1416*180, getPxMeV(), getPyMeV(), getPzMeV(),charge);
  else
    printf("is not defined, charge=%d\n",charge);
    
}

//=====================================

void JFtrackModelHPT::clear(){
  Dmid=TVector3(-999,999,8888);
  Rcen=TVector2(-9999999,0);
  alpha=0;
  charge=0;
}


//=====================================
void JFtrackModelHPT::swimTrack2Z(  TVector3 &trace) {
 // compute x,y-point of track given by model at fixed z 

 double z=trace.Z() - Dmid.Z();
 double angle= alpha *  z;

 TVector2 Dorg(Dmid.X(),Dmid.Y());
 Dorg-=Rcen;
 TVector2 R=Dorg.Rotate( angle ); 
 R+=Rcen;// new position along the helix in X-Y plane
 
 trace.SetX(R.X());
 trace.SetY(R.Y());

}


// $Log: JFtrackModelHPT.cc,v $
