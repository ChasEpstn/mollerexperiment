#include <assert.h>
#include <TObject.h>
#include <TMinuit.h>
#include <TH2.h>

#include "JFitEvent.h"
#include "JFglobalHack.h"
#include "JFmonitor.h"

// G4E:
#include "G4ErrorFreeTrajState.hh"
#include "G4ErrorSurfaceTrajState.hh"
#include "G4ErrorPropagatorManager.hh"
#include "G4ErrorGeomVolumeTarget.hh"

#include "JFg4eUtil.h"

#include "DLUnits.h"
using namespace dali;  //uses DLUnits without needing dali::___

//JB to re-direct some of the warnings to /dev/null- not sure if this is thread-safe
#include <G4strstreambuf.hh>

//===============================================
//===============================================
void g_sequentialChi2FcnB(int &npar,double grad[],double &fcnVal, double inpPar[],int iflag){
  (void) npar; (void) grad; // to make compiler happy,Jan

  double par_Rref=5*dali::mm;

  fcnVal=0;
  //printf(" g_helixChi2FcnB() cnt=%d  ------------------- iflag=%d\n",gMinuit->fNfcn,iflag);

  //.......  grab  some pointers...
  std::vector<JFcylTrackNode>  &inpNodes=GH->inpEve->getTrack(GH->itrack2fit)->nodes;
  JFitTrack *recTrack =GH->recEve->getTrack(GH->itrack2fit);

  G4String sCharge=GH->recEve->getParticle(GH->itrack2fit)->name.Data();
  assert(sCharge=="e+" || sCharge=="e-"); // to prevent uninitialized seed 

  recTrack->nodes.clear();    //..... clear reco nodes

  // build G4E seed based on helix seed
  
  G4ThreeVector inpTrkP(inpPar[0], inpPar[1], inpPar[2]);
  G4ThreeVector inpTrkR(sin(inpPar[3])* par_Rref, cos(inpPar[3])* par_Rref,inpPar[4]);
  recTrack->origin=JFcylTrackNode ("origin",inpTrkR.perp(),inpTrkR.phi(),inpTrkR.z()); 

  // printf("G4E current full (mm) R=%.2f %.2f %.2f\n",inpTrkR.x(),inpTrkR.y(),inpTrkR.z());
  // printf("G4E current full (MeV) P=%.2f %.2f %.2f  PT=%.2f charge=%s\n",inpTrkP.x(),inpTrkP.y(),inpTrkP.z(), inpTrkP.perp(),sCharge.data());

  G4ErrorTrajErr covM( 5, 0 );// assume prior errors are zero
  G4ErrorFreeTrajState myFreeTrajState = G4ErrorFreeTrajState( sCharge, inpTrkR, inpTrkP, covM );
  // JFg4eUtil::printFreeTraj4(&myFreeTrajState,"afterInitFit");
  //  JFg4eUtil::printSurfYZTraj3(myFreeTrajState,"afterInitFit");

  G4ErrorPropagatorManager* g4emgr = G4ErrorPropagatorManager::GetErrorPropagatorManager();
  G4ErrorMode propDir=G4ErrorMode_PropForwards;

  for(int it=0;it<(int)inpNodes.size(); it++) {// loop over tracks  in  real event
    JFcylTrackNode *iNode=&inpNodes[it]; // real 
      
    if(!iNode->hit.inFit) continue; // skip nodes not qulified for use in fitting
    JFcylHit &iHit=iNode->hit; 
    TVector3 rPos;
    
    G4ErrorGeomVolumeTarget targetVol(iHit.name.Data());  
    G4ErrorFreeTrajState tmpFreeTrajState=myFreeTrajState;
    // printf(" propagate -->node=%s partile=%s \n",iHit.name.Data(),myFreeTrajState.GetParticleType().data());

    // JB to re-direct some of LOG stream  to /dev/null : START
    G4strstreambuf* oldBuffer = dynamic_cast<G4strstreambuf*>(G4cout.rdbuf(0));
    G4int propRetErr=g4emgr->Propagate( &myFreeTrajState, &targetVol, propDir); //0 is good (C-style error code)
    G4cout.rdbuf(oldBuffer);// restore default log-stream

     
// add huge chi2, +5% (e+e-) pairs, 2x faster reco vs. alternative 'continue'
    if(propRetErr){
      printf("*********** Jan abort2a propagation, track missed target volume=%s, event#=%d Nfcn=%d\n", iHit.name.Data(), GH->inpEve->id,gMinuit->fNfcn);
      GH->fitAbortCode|=0x2;
      fcnVal+=1e3;  return; 
    }      

    // JFg4eUtil::printFreeTraj4(&myFreeTrajState,"afterSwim-"+iHit.name);
    // JFg4eUtil::printSurfYZTraj3(myFreeTrajState,"afterSwim-"+iHit.name);
    G4Point3D R=myFreeTrajState.GetPosition();
    rPos=TVector3(R.x(),R.y(),R.z()); 

    JFcylTrackNode rNode(iHit.name,rPos.Perp(),rPos.Phi(),rPos.Z());
    rNode.other=iNode;
    recTrack->nodes.push_back(rNode);
    
    TVector3 delPos=iHit.getV3()- rPos;
    // printf("del x,y,z=%f %f %f  name=%s it=%d\n", delPos.X(), delPos.Y(), delPos.Z(),iHit.name.Data(),it);
    double sig=iHit.sig1D;
    double del=delPos.Mag(); //  mm
    fcnVal+=del*del/sig/sig;

    if(gMinuit->fNfcn==0  || iflag==5000) printf("hit=%d del=%.3f mm  del/sig=%.2f sig=%.2f totChi2=%.2f \n",it,del,del/sig,iHit.sig1D,fcnVal);
 
  } // end of loop over nodes 
  // printf("g-G-recxT:"); recTrack->print(1); //0=short, 1=+nodes
  //  printf("g-G-recxE:"); GH->recEve->print(2); //0=short, 1=+particles, 2=+tracks
  // GH->recEve->print(); assert(1==2);
  GH->fitMon->hSG[6]->Fill(gMinuit->fNfcn, fcnVal);
  return;
}



// $Log:  $
