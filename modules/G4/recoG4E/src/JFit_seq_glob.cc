// $Id:  $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#include <assert.h>
#include <TH1.h>
#include <TMinuit.h>
#include <G4ThreeVector.hh>

#include "JFitMaster.h"
#include "JFglobalHack.h" // one global to rule them all
#include "DLg4Event.h"
#include "JFsimUtil.h"
#include "JFtrackModelHPT.h"

#include "DLUnits.h"
using namespace dali;  //uses DLUnits without needing dali::___

//===============================================
//===============================================
//=====================================
void JFitMaster::fitOneEventB(){ 
  //g_inpEve->print(1); // dumps data from ith event   

  printf("================== JFitMaster::fitOneEventB(id=%d)======= start\n",GH->inpEve->id);
  GH->recEve->id=GH->inpEve->id;
  GH->recEve->weight=GH->inpEve->weight;
  GH->recEve->run=777;// tmp
 
  sequentialMinuit->SetFCN(g_sequentialChi2FcnB);

  if (! prepareSeedsB() ) return;

 

  printf("================== JFitMaster::fitOneEventB(id=%d)======= seed OK \n",GH->inpEve->id);
  if (!  fitSequentialGlobal() ) return;    
  printf("================== fitSequentialGlobal() done =======\n"); 

  GH->recEve->print(1);

  //j0 fitMon->EvalEventResidua(  GH->inpEve, GH->recEve);
 
} 


//===============================================
//===============================================
//=====================================
bool JFitMaster::fitSequentialGlobal() {
  
  printf("MMM-G-inp0:"); GH->inpEve->print(2);//0=short, 1=+particles, 2=+tracks
  printf("MMM-G-rec0:"); GH->recEve->print(1);

  //  assert(5==9);
  int nOKfit=0;

  for( int itrk=0; itrk<GH->recEve->sizeParticles(); itrk++) {
    JPhysParticle *recParticle= GH->recEve->getParticle(itrk);
    if(!recParticle->isValid()) continue;// skip not valid seed for proton
       
    TVector3 P3=recParticle->mom4.Vect();
    double invMass=recParticle->mom4.M(); // it will be preserved as well as the charge
    TString sCharge=recParticle->name.Data();

    // build G4E seed based on helix seed & firts hit on the track
    JFitTrack *inpTrack =GH->inpEve->getTrack(GH->itrack2fit);
    TVector3 hit0_3D=inpTrack->getNode(0)->hit.getV3();
    G4ThreeVector inpTrkR(hit0_3D.X(),hit0_3D.Y(),hit0_3D.Z());
    
    G4ThreeVector inpTrkP(P3.X(),P3.Y(),P3.Z());
    
    printf("G4E seed full (MeV) P=%.2f %.2f %.2f  PT=%.2f name=%s  itrk=%d\n",inpTrkP.x(),inpTrkP.y(),inpTrkP.z(), inpTrkP.perp(),sCharge.Data(),itrk);
    
    double arglist[10];
    int error_flag = 0;
    arglist[0] = 5000.;  arglist[1] = 1.;
    double f_null = 0.0;
    
    //define:         NO. NAME         VALUE      STEP SIZE      LIMITS
    sequentialMinuit->mnparm(0, "Px", inpTrkP.x() ,0.001,f_null,f_null,error_flag);
    sequentialMinuit->mnparm(1, "Py", inpTrkP.y() ,0.001,f_null,f_null,error_flag);
    sequentialMinuit->mnparm(2, "Pz", inpTrkP.z() ,0.001,f_null,f_null,error_flag);
    sequentialMinuit->mnparm(3, "phi0", atan2(inpTrkR.y(),inpTrkR.x()) ,0.01,f_null,f_null,error_flag);
    sequentialMinuit->mnparm(4, "Z0",  inpTrkR.z()/20. ,0.01,f_null,f_null,error_flag);
    /* Note, for sequential-global fit the vertex position is constrain to be on a tube with R=10 cm
       No track-kink is fitted 
    */
     
    sequentialMinuit->SetMaxIterations(par_maxIterFcn*1.1);
    gMinuit->fNfcn=0; // reset FCN-counter
    
    GH->itrack2fit=itrk; // index of track being fit
    //pick MINUIT algorithim to use
    const char* com = "MIGRAD";
    //const char* com = "SIMPLEX";
    //const char* com = "MINOS";
    //http://root.cern.ch/root/html/src/TMinuit.cxx.html#sLGJVE
    
    sequentialMinuit->mnexcm(com,arglist,2,error_flag); // fit this one track
    
    // access fit performance info
    Double_t chi2min,edm,errdef;
    Int_t nvpar,nparx,icstat;
  
    sequentialMinuit->mnstat(chi2min,edm,errdef,nvpar,nparx,icstat);

    // check if fit was aborted due to my conditions
     if( GH->fitAbortCode & 0x2 ) fitMon->hSG[2]->Fill("W:missCyl",1.);
    
    // sequentialMinuit->mnexcm("CAL1",arglist,2,error_flag); // call fit funct 1 time , iflag=5000 to print residua in the log file

    int ndof=2*GH->inpEve->getTrack(itrk)->nodes.size()-nvpar; 
    double chi2dof=chi2min/ndof;
    printf("JFitM-end nvpar=%d ndof=%d nFitFunc=%d chi2/dof=%f\n",nvpar,ndof,gMinuit->fNfcn,chi2dof);
    fitMon->hSG[3]->Fill(gMinuit->fNfcn);
    if(gMinuit->fNfcn>par_maxIterFcn) {  
      recParticle->clear(); continue; }// too many iterations

    fitMon->hSG[2]->Fill("fitCnvrg",1.);
    fitMon->hSG[4]->Fill(chi2dof);

    GH->recEve->getTrack(GH->itrack2fit)->globChi2dof=chi2dof; 

    double avrTrkDist=sqrt(chi2dof)*   simUtil->par_LepHitSigReco;
    fitMon->hSG[5]->Fill(avrTrkDist/dali::mm);
    if( avrTrkDist >par_avrTrackDist ) {
      recParticle->clear(); continue;}  // too large chi2
    
    nOKfit++;
    fitMon->hSG[2]->Fill("goodChi2",1.);  
    fitMon->hSG[2]->Fill("re "+sCharge,1.);    

    // get OUTPUT parameters fro each track to a local array (what for?)
    double recPar[JFglobalHack::nSequentialFitPar], recParErr[JFglobalHack::nSequentialFitPar];
    double  plo[JFglobalHack::nSequentialFitPar], phi[JFglobalHack::nSequentialFitPar];
    TString pName[JFglobalHack::nSequentialFitPar];
    int istat;
    // loop over fit-params
    for(int ip=0;ip< JFglobalHack::nSequentialFitPar;ip++) 
      sequentialMinuit->mnpout(ip,pName[ip],recPar[ip],recParErr[ip],plo[ip],phi[ip],istat);

    recParticle->mom4.SetVectM(  TVector3(recPar[0], recPar[1], recPar[2]) , invMass);
    fitMon->EvalTrackResidua(GH->inpEve,GH->recEve, itrk);
    
  }// end-of-loop over fitted particles

  GH->recEve->pass=1; 
  printf("MMM-G-rec1:"); GH->recEve->print(2); //0=short, 1=+particles, 2=+tracks
  
  if( nOKfit <par_minRecoTrack) { 
    return false;
  }

  fitMon->IncrementCase("mTrFit",GH->inpEve);  
  return true;   

}

//===============================================
//===============================================
//=====================================
int JFitMaster::prepareSeedsB(){// do NOT include vertex information 
  // returns true if ok
  GH->recEve->pass=0; 
  
  int nSeedHelix=0;
  for( int itr=0; itr<GH->inpEve->sizeTracks(); itr++) {
    JFitTrack *jfTrack = GH->inpEve->getTrack(itr);
    
    JPhysParticle particle; 
    JFitTrack emptyTrack; // stub to sync the inp & rec events
    if ( jfTrack->isLepton==1) {
      if (jfTrack->sizeNodes() < par_nSeedNode)  return 0; 
      TVector3 vert3D=jfTrack->getNode(1)->hit.getV3();// hack to reuse common code
      JFtrackModelHPT  seedHelix;
      if( computeSeedHelix(jfTrack,vert3D,seedHelix )) nSeedHelix++;
      seedHelix.print();

      /*  check prediction at some z
      TVector3  trace(0,0,50*dali::mm);  seedHelix.swimTrack2Z(trace );
      printf("swim track seed to xyz/mm=%.2f ,%.2f %.2f\n",trace.x(),trace.y(),trace.z());
      */

      // store helix for fitting
      double mass=0.51; assert(jfTrack->isLepton==1);// just in case
      particle.mom4.SetVectM(seedHelix.getTrackP(),mass);
      particle.charge=seedHelix.getQ();
      if(particle.charge==1) particle.name="e+";
      if(particle.charge==-1) particle.name="e-";
      fitMon->hSG[2] ->Fill("se "+particle.name,1.);

    }
    GH->recEve->addParticle(particle);
    GH->recEve->addTrack(emptyTrack);

  }

  if( nSeedHelix <par_minRecoTrack)   return false;
  fitMon->IncrementCase("mTrSeed",GH->inpEve);  

  return true;
}
