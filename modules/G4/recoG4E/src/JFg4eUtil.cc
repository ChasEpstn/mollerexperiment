// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
//*--  Author: Jan Balewski, MIT, April 2013
//
// $Id: JFg4eUtil.cc,v 1.7 2011/12/15 18:10:14 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#include <assert.h>
#include <math.h>
#include <G4ErrorPropagatorManager.hh>
//#include "G4ErrorPropagator.hh"
#include "G4ErrorGeomVolumeTarget.hh"
#include "G4ErrorFreeTrajState.hh"
#include "G4ErrorSurfaceTrajState.hh"
#include "JFg4eUtil.h"

#include "G4SystemOfUnits.hh" // to fix CLHEP issue

//===============================================
//===============================================
void JFg4eUtil::printFreeTraj4(G4ErrorFreeTrajState *g4ErrorTrajState, TString txt){
  G4cout << "\n $$$---------------- G4E FreeTraj  " << txt<<G4endl;
  // extract current state info
  G4Point3D posEnd = g4ErrorTrajState->GetPosition();
  G4Normal3D momEnd = g4ErrorTrajState->GetMomentum();
  G4ErrorTrajErr errorEnd = g4ErrorTrajState->GetError();
   G4ErrorFreeTrajParam freeParams=g4ErrorTrajState->GetParameters();
   G4cout << " freeParams (1/MeV,rad,rad,mm,mm) charge="<< g4ErrorTrajState->GetCharge()<< freeParams<<G4endl;

  G4cout << " Position (mm): " << posEnd <<"  phi/deg="<<posEnd.phi()/deg<<"  Rxy/cm="<<posEnd.perp()/cm<< G4endl
            << " Momentum (MeV): " << momEnd <<"  phi/deg="<<momEnd.phi()/deg<<"  PT/MeV="<<momEnd.perp()/MeV<< G4endl;

    double nSigP=   sqrt(errorEnd(1,1))* (momEnd.mag()/GeV);
    double pMag=1/freeParams.GetInvP();

    printf("freeParam: |P| (MeV) %.4f  +/- %.4f  nSigP=%.3g\n",pMag,pMag*nSigP,nSigP);
    printf("freePar: lam (deg)  %.2f  +/- %.2f\n",freeParams.GetLambda()/deg,sqrt(errorEnd(2,2))*rad/deg);
    printf("freePar: phi (deg)  %.2f  +/- %.2f\n",freeParams.GetPhi()/deg,sqrt(errorEnd(3,3))*rad/deg);

    printf("freePar:    lam (rad)  %.4f  +/- %.6f\n",freeParams.GetLambda()/rad,sqrt(errorEnd(2,2)));
    printf("freePar:    phi (rad)  %.4f  +/- %.6f\n",freeParams.GetPhi()/rad,sqrt(errorEnd(3,3)));
    printf("freePar:    computed: (MeV)  errLam*P=%.2f  errPhi*P=%.2f  units: 1rad=%.3f 1deg=%f 1MeV=%.3f\n",momEnd.mag()/MeV*sqrt(errorEnd(2,2)),momEnd.mag()/MeV*sqrt(errorEnd(3,3)),rad,deg,MeV);

    printf("freePar: Yt (cm)  %.4f  +/- %.3f\n",freeParams.GetYPerp()/cm,sqrt(errorEnd(4,4)));
    printf("freePar: Zt (cm)  %.4f  +/- %.3f\n",freeParams.GetZPerp()/cm,sqrt(errorEnd(5,5)));

    // printStateCov2( errorEnd, "FreeState" );
  G4cout << " $$$---------------- G4E Free-done\n"<< G4endl;
}

//=======================================
//=======================================
//=======================================
void JFg4eUtil::printSurfYZTraj3(G4ErrorFreeTrajState &freeTrajState, TString txt){
  G4cout << " $$$---------------- SurfTraj  " << txt<<G4endl;
  // extract current momentum vector in LAB                        
  G4Normal3D momLab        =  freeTrajState.GetMomentum();

  G4Vector3D Udir=momLab;
  //G4cout << " Udir-0: " << Udir << G4endl;
  Udir=Udir.unit();
  G4cout << " Udir: " << Udir << G4endl;
 
  G4Vector3D Wdir=Udir.cross(G4Vector3D(0,0,1));
  G4Vector3D Vdir=Wdir.cross(Udir);

  G4cout << " Vdir: " << Vdir << G4endl;
  G4cout << " Wdir: " << Wdir << G4endl;

  // G4cout << " Wdir*Plab: " << Wdir.dot(momLab ) << G4endl;
  //G4cout << " Vdir*Plab: " << Vdir.dot(momLab ) << G4endl;

  // project traj to surf representation:   
  G4ErrorMatrix transfM; // hack of bug 1447
  G4ErrorSurfaceTrajState surfTrajState(freeTrajState,Vdir,Wdir,transfM);
  // surfTrajState.Dump( );
  
  // extract current state info in LAB                        
  G4Point3D posEnd         =  surfTrajState.GetPosition();
  G4Normal3D momEnd        =  surfTrajState.GetMomentum();
  G4ErrorTrajErr errorEnd2 = surfTrajState .GetError();
  G4ErrorSurfaceTrajParam surfParams=surfTrajState.GetParameters();
  G4cout << " surfParams (1/MeV,rad,rad,mm,mm): "<< surfParams<<G4endl;

  double invP_MeV=surfParams.GetInvP(); // it is correct: Params is in MeV and cov in GeV (idotic)
  double nSigP=   sqrt(errorEnd2(1,1))* (1e-3/invP_MeV); // now is relative error
  printf("surfErr:   |P| (MeV) %.2f  +/- %.3f  nSigP=%.3g  xCheck=%f\n",momEnd.mag()/MeV,nSigP*momEnd.mag()/MeV ,nSigP,1./invP_MeV);

  printf("surfPar+Err: dirPV~z+t (1?rad)  %.2f  +/- %.3g, \n",surfParams.GetPV(),sqrt(errorEnd2(2,2)));
    printf("surfPar+Err: dirPW~xy (1?)  %.2f  +/- %.3g\n",surfParams.GetPW(),sqrt(errorEnd2(3,3)));

    printf("surfPar+Err: Z+t   (mm)  %.2f  +/- %.3g\n",surfParams.GetV(),sqrt(errorEnd2(4,4)));
    printf("surfPar+Err: XY   (mm)  %.2f  +/- %.3g\n",surfParams.GetW(),sqrt(errorEnd2(5,5)));
    printf("at surf X location   (cm)  %.2f \n",posEnd.x()/cm);

    // printStateCov2(errorEnd2,txt);
  G4cout << " $$$----------------Surf-done\n"<< G4endl;
}

//=======================================
//=======================================
//=======================================
void JFg4eUtil::computeKinkError(G4ErrorFreeTrajState &freeTrajState, double &errTheta, double &errPhi){
  // G4cout << " $$$---------------- computeKinkError  " <<G4endl;
  // extract current momentum vector in LAB                        
  G4Normal3D momLab        =  freeTrajState.GetMomentum();

  G4Vector3D Udir=momLab;
  Udir=Udir.unit();
  // G4cout << " Udir: " << Udir << G4endl;
 
  G4Vector3D Wdir=Udir.cross(G4Vector3D(0,0,1));
  G4Vector3D Vdir=Wdir.cross(Udir);

  //G4cout << " Vdir: " << Vdir << G4endl;
  //G4cout << " Wdir: " << Wdir << G4endl;

  // project traj to surf representation: 
  
  G4ErrorMatrix transfM; // hack of bug 1447
  G4ErrorSurfaceTrajState surfTrajState(freeTrajState,Vdir,Wdir,transfM);
  
  G4ErrorTrajErr errorEnd2 = surfTrajState.GetError();
  
  // it is correct: Params is in MeV and cov in GeV (idotic)
  //
  // double nSigP= sqrt(errorEnd2(1,1))*(momLab.mag()/GeV); // now is relative error
  // double sigP_MeV= nSigP*(momLab.mag()/MeV);
  // printf("computeKink surfErr:   |P| (MeV) %.2f  +/- %.3f  nSigP=%.3g  \n",momLab.mag()/MeV,sigP_MeV, nSigP);
  
  errTheta=sqrt(errorEnd2(2,2));
  errPhi=sqrt(errorEnd2(3,3));
  // printf("computeKinkError error: theta=%.3g phi=%.3g\n", errTheta,  errPhi);

}

//=======================================
//=======================================
//=======================================
void JFg4eUtil::printStateCov2(G4ErrorTrajErr &covM, const char *txt){
  printf("\ndump state sig(i) & correlations for: %s\n        ",txt);
  // int K[5]={1,3,4,2,5}; //mapping
  int K[5]={1,2,3,4,5}; //mapping

  for(int j=0;j<5; j++)
    printf("k=%d      ",K[j]);
  printf("\n");

  for(int j=0;j<5; j++) {
    int k=K[j];
    printf("k=%d    ",k);
    for(int i=0;i<j; i++)    printf(". . . . ");
    printf(" %.4g  ",sqrt(covM(k,k)));
    for(int i=j+1;i<5; i++){
      int m1=K[i];
      double corr=covM(k,m)/sqrt(covM(k,k)) /sqrt(covM(m1,m1));
      printf("  %2d%c   ",  int(100.*corr),(int)37);
    }
    printf("\n");
  }
  printf("FreeCov[1..5] [1/p, lam, phi, yT, zT] (1/GeV,rad,rad,cm,cm)\n");
  printf("SurfCov[1..5] [1/p,  V',  W',  V,  W] (1/GeV,  1,  1,cm,cm)\n");

}





// $Log: JFg4eUtil.cc,v $
    
