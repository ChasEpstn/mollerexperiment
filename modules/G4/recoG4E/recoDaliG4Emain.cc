//
// ------------------------------------------------------------
// Proof of principle GEANT4E (aka G4E) tracking  for DarkLight experiment,
// It is very slow because each particle is thrown by G4 ~400x before 
// Minuit is happy.
// Benchmark  processing speed of 1-2 tracks per second.
// ------------------------------------------------------------
//
// History:
// -  Jan Balewski, MIT, 2013
// - added argument-interpreter, October 2014
//

#include <TString.h>

#include <G4ErrorPropagatorManager.hh>
#include "DaLiDetectorConstruction.h"
 
#include "JFitMaster.h"
#include "DLg4Event.h"
 
// one global variable to rule them all
#include "JFglobalHack.h"
JFglobalHack *GH=new JFglobalHack;
 
//---------------------------------------------------------------------
//---------------------------------------------------------------------
// Run-time arguments interpreter declarations
// Descripion: a swiss-army-knife for parsing run time arguments
// Author: Jan Balewski  (MIT 2013)
//---------------------------------------------------------------------
//---------------------------------------------------------------------
#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <assert.h>

void argUsage(char  *argv0 , const char *message=NULL);
void argUsageLong(const char *message=NULL);


//=======================================
int main(int argc, char *argv[]) {
  char  *userName    = getenv("USER");
  
  // switches, can be changed at runtime
  int numEve=-1; // default: all found events
  char  *geoName  = (char *)"geom12_verA";
  char  *magField  = NULL;
  asprintf(&magField,"%s/.darklight/shared/externalGeom/2013-10-25d-CDS.dat",getenv("HOME"));// crude steps grid
  char *inpPath=(char *)"./";
  char *outPath=(char *)"outReco/";
  char  *coreName = (char *)"aprime30_pt60pz0"; 
  char  *coreSufix = (char *)"_g4e"; 
  char  *baseDir = NULL;
  asprintf(&baseDir,"%s/.darklight/shared/",getenv("HOME"));
  int    debugMode  = false;

  // switches controlling G4E reco:
  //JFitMaster.h:
  int minRecoTrack=2; // minimal # of required reco tracks
  int avrTrackDist=3000; // (um) QA cut based on distance track-hits
  //JFsimUtil.h:
  int lepHitSigSim=500; // (um) added 1D gaussian smearing  to hits from G4
  int lepHitSigRec=500; // (um)  assumed 1D hit error while calculating chi2


   //============================================================
  {//========================== START argv parsing ============== for 100+ lines
    // working variables
    int    optInd   = 0;
    char   optChar  = 0;  

    static struct option optLong[] = {
    { "debug"      , 0 , &debugMode   , true},
    { "help"       , 0 , 0    , 'h' },
    { "longHelp"   , 0 , 0    , 'H' },
    { "numEve"     , 1 , 0    , 'n' }, // has one param & is equivalent to -n
    { "coreName"   , 1 , 0    , 'c' },
    { "coreSufix"  , 1 , 0    , 's' },
    { "geoName"    , 1 , 0    , 'g' },
    { "magField"   , 1 , 0    , 'm' },
    { "inpPath"    , 1 , 0    , 'i' },
    { "outPath"    , 1 , 0    , 'o' },
    { "baseDir"    , 1 , 0    , 'D' },
    { "minRecoTrack"  , 1 , 0   , 'M' }, 
    { "avrTrackDist"  , 1 , 0   , 'A' }, 
    { "lepSimSig"  , 1 , 0   , 'S' }, 
    { "lepRecSig"  , 1 , 0   , 'R' }, 
    { 0, 0, 0, 0}
  };


  // arguments and init
  char  * argv0 = argv[0];
  
  opterr = 0;
  while((optChar = getopt_long(argc,argv,"hHdn:c:s:g:m:i:o:D:M:A:S:R:",optLong,&optInd)) != EOF) {
    switch(optChar) {
    case  0  :   break;
    case 'c' : coreName = optarg;    break; 
    case 's' : coreSufix= optarg;    break; 
    case 'g' : geoName  = optarg;    break; 
    case 'i' : inpPath  = optarg;    break; 
    case 'o' : outPath  = optarg;    break; 
    case 'm' : magField  = optarg;   break; 
    case 'D' : baseDir  = optarg;   break; 
    case 'd' : debugMode= true;      break;
    case 'n' : numEve      = atoi(optarg);     break; 
    case 'M' : minRecoTrack      = atoi(optarg);     break; 
    case 'A' : avrTrackDist      = atoi(optarg);     break; 
    case 'S' : lepHitSigSim      = atoi(optarg);     break; 
    case 'R' : lepHitSigRec      = atoi(optarg);     break; 
    case 'h' : argUsage(argv0); return(0);     break;
    case 'H' : argUsage(argv0);  argUsageLong(argv0);  return(0);       break;
    case '?':   
      if (isprint (optopt))
	fprintf (stderr, "\nUnknown option `-%c'\n\n", optopt);
      else
	fprintf (stderr,"\nUnknown option character `\\x%x'.\n\n",optopt);
    default  : argUsage(argv0,"unknown option");    break;
    };
  }
  
 /* Print any remaining command line arguments (not options).   */
  if (optind < argc)    {
      printf ("\n WARN, non-options ARGV-elements: ");
      while (optind < argc) 
	printf ("%s ", argv[optind++]);
      putchar ('\n');
      return -3;
  }

  printf("\n**** Final paramater choice made by user=%s  *** \n",userName);
  printf("Executing  %s  nEve=%d  coreName='%s' sufix='%s' geoName='%s'  baseDir=%s'\n",argv0,numEve,coreName,coreSufix,geoName,baseDir);
 
  printf("magnetic filed =%s \n",magField);
  printf("inpPath=%s outPath=%s  modes:  debug=%d\n\n", inpPath, outPath,debugMode);
  printf("G4E tracking params: minTrakReco=%d, avrTrackDist/um=%d, leptHitSig/um:sim=%d, rec=%d\n",minRecoTrack,avrTrackDist,lepHitSigSim,lepHitSigRec);
  
  }//========================== END argv parsing ==============
  //============================================================
  
  printf("main %s  START... \n",coreName);

  // Initialize the G4E manager   
  G4ErrorPropagatorManager* g4emgr = G4ErrorPropagatorManager::GetErrorPropagatorManager();
  G4ErrorPropagatorData* g4edata = G4ErrorPropagatorData::GetErrorPropagatorData();
  g4edata->SetVerbose(1);
  
   TString bpath=baseDir;
   DaLiDetectorConstruction *myDetBuilder=new DaLiDetectorConstruction(geoName,magField,bpath+"/externalGeom/");
   g4emgr->SetUserInitialization(myDetBuilder); 
   g4emgr->InitGeant4e();


  JFitMaster *fitMr=new JFitMaster;
  fitMr->setMinRecoTrack(minRecoTrack); // drops fitting if less # seeds is found
  fitMr-> setAvrTrackDistCut(avrTrackDist*um);
  fitMr-> setLeptonHitSigSR( lepHitSigSim*um, lepHitSigRec*um); // added to simu and used in reco
  fitMr->inputEventFile(Form("%s/%s",inpPath, coreName));
  fitMr->outputEventFile(Form("%s/%s",outPath, coreName));

  printf("\n\n main  core=%s mxEve=%d @@@@@@@@@@@@@@@  all initialized \n",coreName,numEve ); 
  //assert(2==22);
  int t1=time(0);
  //  exit(1);
  //---------------------------  EVENT loop -------------
  int mxEve = fitMr->getNevents();
  printf("Avaliable: Nevent=%d \n",mxEve);


  int iEve ;
  for (iEve=0 ; iEve < mxEve; iEve++) {
    if(iEve>=numEve && numEve>0) break;
    if(!fitMr->readOneEvent(iEve)) continue;
    // Warn, proceed only if event is serconstructable
    GH->inpEve->print();
    assert( fitMr->g4Eve->matchBaseGeometry(geoName));// do it after read...
    if(iEve%5==0) 
      printf(".............readOneEvent(ieve=%d), inpNumTracks=%d elaps min=%.1f  \n",iEve,fitMr->Getg4Eve()-> GetNumTracks(),(time(0)-t1)/60.);

    fitMr->fitOneEventB();// sequential, global, no vertex constrain, MORE-efficient for A'-reco
    //Note 1, algoB does not propaget tracks to vertex and A' precision is worse
    // Note 2, the vertex finder is not incorporated to G4E to neither algo A, nor B
    printf(".............WriteOneReconstructedEvent(ieve=%d)\n",iEve);
    fitMr->treeDL->Fill();
    
  }
  //-------------------------------------------- 
  int t2=time(0);  if(t2==t1) t2=t1+1;
  float rate=1.*iEve/(t2-t1);
  printf("sorting done %d of mxEve=%d, totMin=%.1f, CPU rate=%.1f Hz\n",iEve,mxEve,(t2-t1)/60.,rate);

  TString outFname=Form("%s%s", coreName,coreSufix);
  fitMr->finish(outPath+outFname);

  printf("done!  MAIN\n");
  printf("      for fitMonioring plots  exec:   root -l ./macros/plFitMon.C\'(1,\"%s\",\"%s\")\'   pages:1, 2,3\n",outFname.Data(),outPath);


}



//----------------------------------------
void argUsage(char  *argv0 , const char *message){
  if (message) fprintf(stderr,"%s: %s\n",argv0,message);
  fprintf(stderr,"usage: %s  [OPTIONS]\n",argv0);
  fprintf(stderr," -i | --inpPath <./>          : full path for input events \n");
  fprintf(stderr," -o | --outPath <outRec>      : full path for all outputs \n");
  fprintf(stderr," -g | --geoName <geom?>   : identical to used in simulation \n");
  fprintf(stderr," -m | --magFiled <2013-10-24-CDS.dat>  : as above\n");
  fprintf(stderr," -c | --coreName <abc>       : input=abc.DLg4.root\n");
  fprintf(stderr," -s | --coreSufix <_g4e>     : output: 'abc_g4e'  for all outputs from this job\n");
  fprintf(stderr," -n | --numEve <all>         : max number of reco events \n");
  fprintf(stderr," -D | --baseDir '../'        : path to /externalGeom/ \n");
  fprintf(stderr," -d | --debug        : set debug mode on\n");
  fprintf(stderr," -M | --minRecoTrack <2> :  minimal # of required reco tracks\n");
  fprintf(stderr," -A | --avrTrackDist <3000> : (um) QA cut based on distance track-hits\n");
  fprintf(stderr," -S | --lepSimSig <500> : (um) added 1D gaussian smearing  to hits from G4\n");
  fprintf(stderr," -R | --lepRecSig <500> :  (um)  assumed 1D hit error while calculating chi2\n");
  fprintf(stderr," -h | --help         : this short help\n");
  fprintf(stderr," -H | --longHelp     : detailed explanation of switches\n");
  if(message) exit(-1);
  return;
}



//----------------------------------------
void argUsageLong(const char *message){
  fprintf(stderr,"\n -------------------------------\nDetailed explanation of switches for %s\n",message); 

  fprintf(stderr,"\n*)------ geometry must match to that used by daliSimG4 --------\n"
	  "format  --geoName  geom12_verX_on_[x]_[y]..._off_[z]... \n"
	  "\n");

  fprintf(stderr,"\n*)------ magnetic filed  model  must match to that used by daliSimG4  --------\n"
	  "format:  --magFiled [name][_dump] \n"
	  "\n");

}



