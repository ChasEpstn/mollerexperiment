// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
//   G4E-track fit,  utility methods, no member variables
//
//*--  Author: Jan Balewski, MIT, April 2013
//
// $Id: JFg4eUtil.h,v 1.1 2011/12/15 18:10:15 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#ifndef JFg4eUtil_H
#define JFg4eUtil_H

#include <TObject.h>
#include <TVector3.h>
#include <TString.h>
#include <TMath.h>
#include <TChain.h>



//#include "JFitEvent.h"
#include <G4String.hh>
#include <G4ErrorTrajErr.hh>
class G4ErrorFreeTrajState;

class JFg4eUtil  { 
 public:
  JFg4eUtil();
  ~JFg4eUtil(){};
  void static printFreeTraj4(G4ErrorFreeTrajState *g4ErrorTrajState, TString txt);
  void static printSurfYZTraj3(G4ErrorFreeTrajState &freeTrajState, TString txt);
  void static printStateCov2(G4ErrorTrajErr &covM, const char *txt);
  void static computeKinkError(G4ErrorFreeTrajState &freeTrajState, double &errTheta, double &errPhi);
};

#endif
// $Log: JFg4eUtil.h,v $
    
