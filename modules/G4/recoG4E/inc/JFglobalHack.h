// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
// Global utility pointer to hide all Minuit global-pointers mess
//
//*--  Author: Jan Balewski, MIT, March 2013
//
// $Id:  $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
//using namespace CLHEP;

#ifndef JFglobalHack_h
#define JFglobalHack_h
class JFitEvent;
class JFmonitor;

class JFglobalHack{
 public:
  enum {nSequentialFitPar=5, nParalelFitPar=20};
  JFitEvent    *inpEve;
  JFitEvent    *recEve;
  JFmonitor    *fitMon; // for chi2 vs. nfunc
  int  itrack2fit; // index of track being fit
  JFglobalHack();
  int useVertex; // 0=no
  float vertX0, vertY0; // (mm)vertex average location in transverse plane
  unsigned int fitAbortCode;
};

// used by MINUIT, must reamin global - I do not know how to solve it, Jan

extern JFglobalHack *GH;
void g_sequentialChi2FcnA(int &npar,double grad[],double &fcnVal, double inpPar[],int iflag);
void g_sequentialChi2FcnB(int &npar,double grad[],double &fcnVal, double inpPar[],int iflag);

#endif


// $Log: $
