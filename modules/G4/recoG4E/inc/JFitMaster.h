// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
//  fit-master for  helix fit,  utility methods, no member variables
//
//*--  Author: Jan Balewski, MIT, January 2012
//*--  Author: Len K. Eun, LBL, March, 2012
//
// $Id: JFitMaster.h,v 1.1 2011/12/15 18:10:15 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#ifndef JFitMaster_H
#define JFitMaster_H

#include <TVector3.h>
#include <TString.h>
#include <TMath.h>
#include <TChain.h>
#include <TVector3.h>

class DLg4Event;
class TMinuit;
class JFsimUtil;
class TTree ;
class  DLmadEvent;

#include "JFitEvent.h"
#include "JFmonitor.h"
class JFtrackModelHPT;

class JFitMaster  {
 public:
  JFitMaster();
  ~JFitMaster(){};
  TMinuit   *sequentialMinuit;
  TChain    *treeChain; // input events
  TFile *fileTout ; // output TTree w/ events
  DLg4Event *g4Eve; // input raw DL G4-event, contains hits & G4 tracks
  DLmadEvent *madEve; // input event , contains physical particles
  TTree     *treeDL;   // Output,

  JFmonitor *fitMon; // monitor of G4E fitter
  JFsimUtil *simUtil;
  int par_minRecoTrack; // set minimal # of required reco tracks
  int par_nSeedNode; // minimum of hits for seed calculation
  int par_maxIterFcn; // max no. of calls of the fit-cuntion (aka G4E full track swim)
  float par_avrTrackDist; // QA cut based on distance track-hits

  void  inputEventFile(TString filename);
  void  outputEventFile(TString filename);
  int   getNevents(){ return (int)treeChain->GetEntries();}
  DLg4Event   *Getg4Eve(){return g4Eve;}
  bool  readOneEvent(int ieve);
 
  int   prepareSeedsA(); // include vertex for each track seed
  int   computeSeedHelix(JFitTrack *jfTrack, TVector3 vert3D, JFtrackModelHPT &seedHelix);
  int   findSeedHelix(TVector3 A, TVector3 B, TVector3 C, double Z0, JFtrackModelHPT & seedHelix);
  TVector3  computeSeedVertex();

  void   finish(TString name);
  void   setMinRecoTrack(int x) {par_minRecoTrack=x;}
  void   setAvrTrackDistCut(int x){ par_avrTrackDist=x;}
  void   setLeptonHitSigSR(float x, float y);

  void  fitOneEventB(); // sequential, global, no vertex constrain
  int   prepareSeedsB(); // do NOT include vertex information
  bool  fitSequentialGlobal(); 
 
};

#endif

// $Log: JFitMaster.h,v $
    
