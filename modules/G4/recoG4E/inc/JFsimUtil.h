// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
//   helix fit,  utility methods
//
//*--  Author: Jan Balewski, MIT, January 2012
//
// $Id: JFsimUtil.h,v 1.1 2011/12/15 18:10:15 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#ifndef JFsimUtil_H
#define JFsimUtil_H

#include <TObject.h>
#include <TVector3.h>
#include <TString.h>
#include <TMath.h>
#include <TChain.h>

#include "JFitEvent.h"
class  TRandom3;

class JFsimUtil  { 
 public:
  JFsimUtil();
  ~JFsimUtil(){};
  double par_LepHitSigSim; // added 1D gaussian smearing to hits from G4
  double par_LepHitSigReco; //assumed 1D hit error  while calculating chi2
  int    digitizeCylHits(DLg4Track * g4Trk); 
  void   setLeptonHitSigSim(float x) { par_LepHitSigSim=x;}
  void   setLeptonHitSigReco(float x) { par_LepHitSigReco=x;}

};

#endif
// $Log: JFsimUtil.h,v $
    
#if 0 


#if 0 
  //............ auxiliary info about G4-event
  // those below refere to two leptons forming A' 
  struct {
    //    double lepE1E2, lepCos12;
    double trueM, lepPT1PT2, lepCos12s1s2;
    DLg4Track * eplusTr, * eminusTr;
    int valid;
  } g4ap;
  void clear_g4ap() {
    memset(&g4ap,0,sizeof(g4ap)); 
  }
  void find_g4ap(double goalMA, int parentId, DLg4Event *g4Eve);
#endif


fitMon->hSG[2]

working code:

 // .... check if the seed tracks include the e+e- from A'
  if(simUtil->g4ap.valid) { // found invM pair:
    fitMon->IncrementCase("eeAnyG4tr",GH->inpEve); 
    // vet if both true_ee are on the list of seeds to be fitted
    int nMatch=0;
    for( int itr=0; itr<GH->inpEve-> sizeTracks(); itr++) {
      JFitTrack *inpTrack =GH->inpEve->getTrack(itr);
      int g4TrackID=inpTrack->g4TrackID;
      printf("inpEve itr=%d g4TrackID=%d\n",itr,g4TrackID);
      if(simUtil->g4ap.eplusTr->GetTrackID()==g4TrackID ||  simUtil->g4ap.eminusTr->GetTrackID()==g4TrackID ) nMatch++;
      

    }      

    TLorentzVector posP4=simUtil->g4ap.eplusTr->Get4Momentum();
    TLorentzVector eleP4=simUtil->g4ap.eminusTr->Get4Momentum();
    TLorentzVector sumP4=eleP4+posP4;
    double invM=sumP4.M(); // this should be A'
    printf("invM/MeV  e+: itr=%d m=%f;  e-: itr=%d m=%f  sum=%f nMatch=%d\n",simUtil->g4ap.eplusTr->GetTrackID(), posP4.M(), simUtil->g4ap.eminusTr->GetTrackID(),eleP4.M(),invM,nMatch);
    if(nMatch==2 ){ 
      fitMon->IncrementCase("eeLongG4tr",GH->inpEve); 
      //j0 helRsd->hA[5]->Fill( posP4.Pz()/MeV, posP4.Pt()/MeV);
      //j0 helRsd->hA[6]->Fill(eleP4.Pz()/MeV, eleP4.Pt()/MeV);      
    }

  }

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


  simUtil-> clear_g4ap();

 // pick the mass of A' you want
  // use: 1 for G4-Aprime events;  0 for MadGraph  events
  simUtil->find_g4ap(30*MeV, 1, g4Eve);
 
  //j0 helRsd->hA[12]->Fill(simUtil->g4ap.lepPT1PT2/MeV/MeV, simUtil->g4ap.lepCos12s1s2);

  //j0 helRsd->hA[13]->Fill(simUtil->g4ap.lepPT1PT2/MeV/MeV, simUtil->g4ap.lepCos12s1s2);

  //j0 helRsd->hA[14]->Fill(simUtil->g4ap.lepPT1PT2/MeV/MeV, simUtil->g4ap.lepCos12s1s2);

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

//=====================================
void JFsimUtil::find_g4ap(double goalMA, int parentId, DLg4Event *g4Eve){
  clear_g4ap(); // just in case
  //  double goalMA=30*MeV; // pick the mass of A' you want
  //  int parentId=1; // use: 1 for G4-Aprime events;  0 for MadGraph  events
  assert(goalMA>1* MeV);
  int ret=g4Eve->findAprimDaughters(goalMA, 1*MeV, parentId, & g4ap.eplusTr, & g4ap.eminusTr); // target A mass and tolerance

  // printf("JFsimUtil::find_g4ap: ret=%d  %p %p \n",ret,  g4ap.eplusTr,  g4ap.eminusTr);
  if(ret) return; // not found invM pair:
  //g4Eve->print(1);
  TLorentzVector posP4= g4ap.eplusTr->Get4Momentum();
  TLorentzVector eleP4= g4ap.eminusTr->Get4Momentum();
  TLorentzVector sumP4=eleP4+posP4;
  printf("AP4 (MeV) ee sum:  E=%f P=%f M=%f  \n",sumP4.E()/MeV,sumP4.P()/MeV,sumP4.M()/MeV);

  //  double g4ap_lepE1E2 =posP4.E() * eleP4.E();
  TVector3 U1=posP4.Vect().Unit();
  TVector3 U2=eleP4.Vect().Unit();
  double g4ap_lepCos12=U1*U2;
  g4ap.trueM=sumP4.M(); // this should be A'

  // another set of orthogonal(?) variables:
  double sth1sth2=U1.Perp() * U2.Perp();
  g4ap.lepPT1PT2=posP4.Pt() * eleP4.Pt();
  g4ap.lepCos12s1s2= g4ap_lepCos12/sth1sth2;
  g4ap.valid=true;
  // printf("\ninvM  e+: itr=%d m=%f;  e-: itr=%d  invM=%f  PP=%f/MeV^2 AA=%f valid=%d\n",
  //	 g4ap.eplusTr->GetTrackID(), posP4.M(),  g4ap.eminusTr->GetTrackID(),
  //	 g4ap.trueM,g4ap.lepPT1PT2/MeV/MeV, g4ap.lepCos12s1s2,g4ap.valid);

  //old
  //  double th1m2=posP4.Vect().Theta()-eleP4.Vect().Theta();
  // printf("\ninvM  e+: itr=%d m=%f;  e-: itr=%d  invM=%f  EE=%f th12=%f E1/MeV=%f E2/MeV=%f th1/deg=%.1f th2/deg=%.1f  delTH/deg=%.1f\n", g4ap_eplusTr->GetTrackID(), posP4.M(),  g4ap_eminusTr->GetTrackID(),g4ap_trueM,g4ap_lepE1E2/MeV/MeV, acos(g4ap_lepCos12)/deg, posP4.E()/MeV , eleP4.E()/MeV,posP4.Vect().Theta()/deg,eleP4.Vect().Theta()/deg,fabs(th1m2)/deg);
  // printf("P1 %f %f %f  \n", posP4.Vect().X(), posP4.Vect().Y(), posP4.Vect().Z());
  //printf("P2 %f %f %f  \n", eleP4.Vect().X(), eleP4.Vect().Y(), eleP4.Vect().Z());
 
}


#endif
