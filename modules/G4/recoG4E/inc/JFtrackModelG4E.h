// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
//  G4E-tracker model
//
//*--  Author: Jan Balewski, MIT, April, 2013
//
// $Id: JFtrackModelHPT.h,v 1.2 2011/12/15 21:02:48 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****


#ifndef JFtrackModelG4E_H
#define JFtrackModelG4E_H

//#include <TObject.h>
#include <TVector3.h>
#include <TString.h>
#include <TMath.h>
// this global B-filed bad - not sure how to fix it
extern  double par_Bfiled;

class JFtrackModelG4E  {
 public:
  JFtrackModelG4E(){clear(); }
  JFtrackModelG4E(TVector3 R, TVector3 P , int q){ trackR=R; trackP=P; charge=q;}
 
  ~JFtrackModelG4E(){};
          
    void print(){
      printf("\ttrkModelG4E ");
      if(charge)
	printf(" R XYZ=%.2f, %.2f, %.2f,  Rcurv=%.2f  PT/MeV=%.2f,  Px,y=%.2f , %.2f, Pz/MeV=%.2f \n",
	       trackR.X(),trackR.Y(),trackR.Z(), getRcurv(),  getPtMeV(),  trackP.X(),trackP.Y(),trackP.Z());
      else
	printf("is not defined, charge=%d\n",charge);
    }


    void clear(){ trackR=TVector3(500,600,700); trackP=TVector3(0,0,0); charge=0;}
     
    // Track  parameterization using 2 3D vectors
    TVector3 trackR, trackP; // identaical to G4E paramterization in LAB
    int charge; // 0 if track is not defined


    double getRcurv() { return getPtMeV()/0.3/par_Bfiled;}
    double getPtMeV(){ return  trackP.Pt();}
   
    // this two return physical momentum vector at the current helix definition point
    TVector3 getTrackP() {return  trackP;}
    TVector3 getTrackR() {return  trackR;}

    // ...  auxiliary     
    double PtMeV_to_Rmm(double pt){ return pt/0.3/par_Bfiled;}
    double Rmm_to_PtMeV(double r){ return r*0.3*par_Bfiled;}



};

#endif

// $Log: JFtrackModelG4E.h,v $
