// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
//  Helix tracker
//
//*--  Author: Jan Balewski, MIT, January 2012
//
// $Id: JFtrackModelHPT.h,v 1.2 2011/12/15 21:02:48 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****


#ifndef JFtrackModelHPT_H
#define JFtrackModelHPT_H

#include <TObject.h>
#include <TVector3.h>
#include <TString.h>
#include <TMath.h>

// this global B-filed bad - not sure how to fix it
//jx for STAR the sign was +, now is - @ , 2014-04-04 
static  double par_avrBfiled=-0.5; // bfiled in tesla, needed for R<-->PT conversion

class JFtrackModelHPT  {
 public:
  JFtrackModelHPT(){clear(); }
  JFtrackModelHPT(TVector3 Dmid,  TVector2 Rcen,Double_t alpha, int q);
    ~JFtrackModelHPT();
          
    void print(); 
    void clear();
     
    // helix parameterization, z of track middle is the 6th not-fitted param
    TVector3 Dmid; //(cm) point belonging to helix, preferably at avrg Z of the helix
    TVector2 Rcen; //(cm) vector pointing form orygin to center of the helix sym axis
    Double_t alpha; // (rad/cm) - angular rotation speed 
    int charge; // 0 if track is not defined

    void   swimTrack2Z( TVector3 & trace);
    double getRcurv() { return (Rcen-Dmid.XYvector()).Mod();}
    double getPtMeV(){ return getRcurv()*0.3*par_avrBfiled;} // '-' because of the B-filed sign legacy convention
    double getPzMeV(){ return -0.3*par_avrBfiled/alpha*charge;}
    double getRphi0() {return (Rcen-Dmid.XYvector()).Phi();}
    double getPxMeV() { return -getPtMeV()*sin(getRphi0())*charge;}
    double getPyMeV() { return getPtMeV()*cos(getRphi0())*charge;}
    int    getQ(){ return charge;}

    // this two return physical momentum vector at the current helix definition point
    TVector3 getTrackP() {return TVector3( getPxMeV(),getPyMeV(),getPzMeV());}
    TVector3 getTrackR() {return Dmid;}

    // ...  auxiliary     
    double PtMeV_to_Rmm(double pt){ return fabs(pt/0.3/par_avrBfiled);}
    double Rmm_to_PtMeV(double r){ return fabs(r*0.3*par_avrBfiled);}



};

#endif

// $Log: JFtrackModelHPT.h,v $
