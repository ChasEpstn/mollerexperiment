// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
//   monitors G4E fitting process
//
//*--  Author: Jan Balewski, MIT, October 2014
//
// $Id: JFmonitor.h,v 1.2 2011/12/15 21:02:48 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****


#ifndef JFmonitor_H
#define JFmonitor_H

#include <TObject.h>
#include <TString.h>
class TH1;
class TH2F;
class TFile;
class JFtrackModelHPT;
class JFitEvent;
class DLg4Event;
class TObjArray;
class JFsimUtil;

class JFmonitor  {
 public: 
  JFmonitor(){ NtotEve=0;}
  ~JFmonitor(){};

  int NtotEve; 
  void Init();

  void Init_seq_glob();
  void EvalTrackResidua(JFitEvent * inpEve,JFitEvent * recEve, int itrk);

  TObjArray* HList;
  void  saveHisto(TString fname);
  void IncrementCase(TString sCase, JFitEvent * inpEve);

  enum { mxSG=32};  TH1 *hSG[mxSG]; 
};

#endif

// $Log: JFmonitor.h,v $
