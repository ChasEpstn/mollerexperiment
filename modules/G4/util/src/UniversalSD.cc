// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
//  Processes hits from SD associated with it
//
//*--  Author: Jan Balewski, MIT, February 2013
//
// $Id: UniversalSD.cc,v 1.3 2011/12/16 21:22:32 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****


#include "UniversalSD.h"

#include "G4VSensitiveDetector.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4TouchableHistory.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"

//DL-specific libraries
#include "DLg4Event.h"

//===================================================

UniversalSD::UniversalSD( G4String name ) : G4VSensitiveDetector( name ) {

  hitDLg4=new DLg4Hit;
  setCuts(0,5000,false); // ElosMin, maxHits/track, onlyPrimary
}


//===================================================
G4bool UniversalSD::ProcessHits( G4Step * aStep, G4TouchableHistory *  ) {
  G4double edep = aStep->GetTotalEnergyDeposit();
  const G4Track *track=aStep->GetTrack () ;
 
  if( edep < par_hitElosMin ) return false; 
  if( trackDLg4->GetNumHits() >par_maxStoreHits)  return false;  
  if(par_onlyPrimTracks && track->GetParentID()!=0) return false; //skip daughter tracks
  
  G4StepPoint *stpp=aStep->GetPreStepPoint();
  G4ThreeVector momDir= stpp->	GetMomentumDirection ();
  
  G4VPhysicalVolume* phyVol=stpp->GetPhysicalVolume();
  
  //  G4LogicalVolume *  mothVolName=phyVol->GetMotherLogical ();
  
  G4ThreeVector worldpos = aStep->GetPreStepPoint()->GetPosition();
  
  G4TouchableHistory * history =
    (G4TouchableHistory *) aStep->GetPreStepPoint()->GetTouchable();
  
  G4int replicano = history->GetReplicaNumber(0);
  
  //G4int replicano2 = -111;
  //int nDepth=history->GetHistoryDepth  ();
  //if(nDepth>1) replicano2 =history->GetReplicaNumber(1);
  
  G4int replicano2 = -111;
  int nDepth=history->GetHistoryDepth	();
  if(nDepth>1) replicano2 =history->GetReplicaNumber(1);

  const G4ThreeVector 	p3=track->GetMomentum ();  
  G4ThreeVector localpos =
    history->GetHistory()->GetTopTransform().TransformPoint( worldpos );
    //  G4ThreeVector localDir =   history->GetHistory()->GetTopTransform().TransformAxis( momDir);   
  G4ThreeVector locP3 =   history->GetHistory()->GetTopTransform().TransformAxis( p3);   

  G4double trkEkin=track->GetKineticEnergy (); 
  G4double stepLen = aStep->GetStepLength();
  G4double globalTime = stpp->GetGlobalTime();

#if 0  
 printf("::ProcessHits  g4trkId=%d parentId=%d min=%f\n", track->GetTrackID(),track->GetParentID(),par_hitElosMin/eV);
  const char * phyVolName=phyVol->GetName().data();
  //const G4ThreeVector &r=aStep->GetPreStepPoint()->GetPosition();
  G4double trkQ=track->GetDefinition()->GetPDGCharge();
  G4double trkPt=p3.perp();
  
  double rxy=worldpos.perp();//sqrt(r.x()*r.x() + r.y()*r.y());
  printf(" HIT edep= %.2g keV,  Ekin/MeV=%.3g stepLen/mm=%.1f Q=%.0f, xyz/cm=%.2f,%.2f,%.2f, Rxy/cm=%.2f vol=%s, copyNo=%d, trackID=%d trackPt/MeV=%.4g\n",
	 edep/keV,trkEkin/MeV,stepLen/mm, trkQ, worldpos.x()/cm,worldpos.y()/cm,worldpos.z()/cm,rxy/cm,phyVol->GetName().data(), 
	 stpp->GetTouchableHandle() ->GetCopyNumber(),track->GetTrackID(), trkPt/MeV);

  if(strstr(phyVolName,"Gem")) printf("Gem trk pt/pz=%.3f  pZ/MeV=%.2f  pT/MeV=%.2f  |P|/MeV=%.2f histDepth=%d  rep2=%d\n", momDir.perp()/momDir.z(),momDir.z()/MeV,momDir.perp()/MeV,momDir.mag()/MeV,nDepth,replicano2  );
  if(strstr(phyVolName,"SiR")) printf("Si trk pt/pz=%.3f  pZ/MeV=%.2f  pT/MeV=%.2f  |P|/MeV=%.2f histDepth=%d\n", momDir.perp()/momDir.z(),momDir.z()/MeV,momDir.perp()/MeV,momDir.mag()/MeV,nDepth);

  printf(" HITa world xyz/cm=%.2f,%.2f,%.2f vol=%s, copyNo=%d, \n",  worldpos.x()/cm,worldpos.y()/cm,worldpos.z()/cm,phyVol->GetName().data(), 	 stpp->GetTouchableHandle() ->GetCopyNumber());

  printf("     local xyz/cm=%.2f,%.2f,%.2f stepLen=%.2f/mm\n",  localpos.x()/cm,localpos.y()/cm,localpos.z()/cm,stepLen/mm);
  printf("     glob  Pxyz/cm=%.4f,%.4f,%.4f /MeV\n",  p3.x()/MeV,p3.y()/MeV,p3.z()/MeV);
  printf("     local Pxyz/cm=%.4f,%.4f,%.4f /MeV\n\n",  locp3.x()/MeV,locp3.y()/MeV,locp3.z()/MeV);
#endif 


  
#ifdef DO_EVE_DISP
  eDisp->addTrackPvec( worldpos, momDir);
#endif
  
  
  
  // this code exports hits to a custom ttree
  hitDLg4->clear();
  hitDLg4->SetPosition(worldpos.x(),worldpos.y(), worldpos.z());
  hitDLg4->SetLocalPosition(localpos.x(),localpos.y(), localpos.z());
  hitDLg4->SetStepLen(stepLen);
  hitDLg4->SetLocalMomentum(locP3.x(),locP3.y(),locP3.z());
  hitDLg4->SetGlobalTime(globalTime);

  // printf("     local xyz/cm=%.2f,%.2f,%.2f\n\n",  localpos.x()/cm,localpos.y()/cm,localpos.z()/cm);  
  //G4ThreeVector

  hitDLg4->SetDE(edep);  // skip units
  hitDLg4->SetKinE(trkEkin); // skip units
  TString name1=phyVol->GetName().data();
  if( replicano ) { name1+="."; name1+=replicano; } // attache replica # if more than 1 copy
  hitDLg4->SetDetectorID(name1);
  trackDLg4->AddHit(hitDLg4);
  
  
  return true;
}


//===================================================

void UniversalSD::EndOfEvent( G4HCofThisEvent *  ){
  if (verboseLevel>0) { 
    printf("UniversalSD::EndOfEvent verbLev=%d\n", verboseLevel);
  } 

}

 
//===================================================

void UniversalSD::printCuts() {
  printf(" UniversalSD::PrintCuts  Elos/hit>%.1f eV , hits/track<%d , onlyPrimaryTracks=%d\n", par_hitElosMin/eV,par_maxStoreHits, par_onlyPrimTracks);
} 


// $Log: UniversalSD.cc,v $
// Revision 1.3  2011/12/16 21:22:32  balewski
// rnd seed is now fixed
