// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
//  Processes hits from SD associated with it
//
//*--  Author: Jan Balewski, MIT, February 2013
//
// $Id: UniversalSD.h,v 1.2 2011/12/15 21:02:49 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****


#ifndef UniversalSD_H
#define UniversalSD_H

#include "G4VSensitiveDetector.hh"
using namespace CLHEP;

class DLg4Event;
class DLg4Track;
class DLg4Hit;
class G4Step;
class G4HCofThisEvent;
class G4TouchableHistory;

class UniversalSD : public G4VSensitiveDetector {

public:
  UniversalSD( G4String name );
  ~UniversalSD(){};
  DLg4Hit   *hitDLg4;
  DLg4Event *eventDLg4;
  DLg4Track *trackDLg4;
  
  G4bool ProcessHits( G4Step * aStep, G4TouchableHistory * ROhist );
  void EndOfEvent( G4HCofThisEvent * HCE );
  void printCuts();
  void setCuts(double x, int m, int k) {par_hitElosMin=x; par_maxStoreHits=m; par_onlyPrimTracks=k;}
      
private:
     G4double par_hitElosMin;
     G4int par_maxStoreHits, par_onlyPrimTracks;

};

#endif

// $Log: UniversalSD.h,v $
// Revision 1.2  2011/12/15 21:02:49  balewski
// ready to export hits in ttree
//
// Revision 1.1  2011/12/15 18:09:13  balewski
// start
//
