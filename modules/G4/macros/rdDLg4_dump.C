class DLg4Event ;
class  DLg4Track ;
#include "/dali/u/balewski/.darklight/include/DLUnits.h"
using namespace dali;  //uses DLUnits without needing dali::___

void rdDLg4_dump(TString coreName="norm", int nEve=1, int nSkip=0){
          
  TString inpPath="./";
  
  assert(gSystem->Load("/dali/u/balewski/.darklight/x86_64/lib/libDLg4Event.so")==0); 
  assert(gSystem->Load("/dali/u/balewski/.darklight/x86_64/lib/libDLmadEvent.so")==0); 
 printf("Jan's privaye  libs loaded\n");
 // exit(1);

  TString fullTN1=coreName+".DLg4.root"; // pick it locally
  TFile *myFile = new TFile(fullTN1,"READ"); assert(myFile->IsOpen());
  myFile->ls();
  TTree *treeG4 = (TTree *) myFile->Get("DaLiEventTree");  assert(treeG4);

  printf("added %s  totEve: G4sim=%d\n", fullTN1.Data(), treeG4->GetEntries());
  //treeG4->Print();

  Int_t maxEve = treeG4->GetEntries(); 
  assert(maxEve>0);
 
  DLmadEvent *madEve=new DLmadEvent ;
  treeG4->SetBranchAddress("DLmadEvent",&madEve);
 
  DLg4Event *g4Eve=new DLg4Event ;
  treeG4->SetBranchAddress("DLg4Event",&g4Eve);
  
  if(nSkip>0) printf("SKIP first %d events\n", nSkip);
  for(int ieve=0;ieve<maxEve;ieve++) {
    if(ieve>=nEve) break;
    if(ieve<nSkip) continue;
    treeG4->GetEntry(ieve);
    madEve->print();  
    g4Eve->print(1);  // dump whole event
    examineG4event(ieve, g4Eve);
  
  }

}

//------------- detailed examination of geant4 events
void   examineG4event(int ieve,  DLg4Event *g4Eve){
  printf("\n\n==================  examineG4event(%d)=======  ID=%d g4trks=%d\n",ieve,=g4Eve->GetEventID(), g4Eve->GetNumTracks());

  for(int itr=0; itr<g4Eve->GetNumTracks(); itr++) {
    DLg4Track* g4Trk=g4Eve->GetTrack(itr);
    // g4Trk->Print(0);
  }

  double goalMA=70*MeV; // pick the mass of A' you want
  int parentId=0; //use:  1 for G4-Aprime events, 0 for MadGraph events
  DLg4Track * eplusTr=0, * eminusTr=0;
  
  int ret=g4Eve->findAprimDaughters(goalMA, 1*MeV, parentId, &eplusTr, &eminusTr); // target A mass and tolerance
  
  printf("main: ret=%d  %p %p \n",ret, eplusTr, eminusTr);
  if(ret==0) { // found invM pair:
    TLorentzVector posP4=eplusTr->Get4Momentum();
    TLorentzVector eleP4=eminusTr->Get4Momentum();
    TLorentzVector sumP4=eleP4+posP4;
    double invM=sumP4.M(); // this should be A'
    printf("invM/MeV  e+: itr=%d m=%f;  e-: itr=%d m=%f  sum=%f\n",eplusTr->GetTrackID(), posP4.M(), eminusTr->GetTrackID(),eleP4.M(),invM);
  }

}
