#!/bin/bash
#  Author: Jan Balewski, MIT, August 2014

set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

#
# Purpose of this job is to test DarkLight software after every commit.
# For now it is only a sequence of job of which non should fail.
# In the future we shoudl add script evaluating the output, comparing it to 
# some template.


# first define parametric functions doing certain tasks

user=`id -un`
DDD="==================== "
daliSim_exe=./daliSimG4
recoG4E_exe=./recoDaliG4E
daliGeom=geom12_verA

#============================================
#============================================
function quit1 {  
echo -n "DL nightly test finished: " ; date
exit
}  

#============================================
#============================================
function setSandbox { 
echo $DDD setSandbox for user=$1
OutDir=/tmp/$1/nightly_DarkLigh
rm -rf $OutDir
mkdir -pv  $OutDir
echo find executables
ls -l $daliSim_exe 
ls -l $recoG4E_exe
}
  
#============================================
#============================================
function run_daliSim_Aprime { 
nEve=1000
MA=$1
core=ep_ep_A${MA}_ee_jesseCuts
Log=$OutDir/daliSim_Aprime_${MA}.log
echo  $DDD run daliSim mA=$1  nEve=$nEve 
time  $daliSim_exe --physGen  madGraph --physParam ~/2013-05-madEve-jesseCuts/madAm/ep_ep_A${MA}_ee_jesseCuts --geoName $daliGeom --core $core --batch --numEve $nEve --outPath $OutDir >& $Log
}  

#============================================
#============================================
function dumpDL_event { 
nEve=5
Log=$OutDir/${core}_dump.log
echo   $DDD dumpDL_event core=$core  nEve=$nEve 
root -b <<EOF >& $Log
.x ../macros/rdDLg4_dump.C("$OutDir/$core",$nEve)
.q
EOF

}  
#============================================
#============================================
function run_recoG4E { 
nEve=500
Log=$OutDir/${core}_recoG4E.log
echo  $DDD run recoG4E core=$core  nEve=$nEve
time   $recoG4E_exe   $nEve $core  $daliGeom  $OutDir>& $Log

}  


#============================================
#============================================
function func1 { 
echo from func1 $1
AA='aa'
}  

#============================================
#============================================
#    M A I N 
#============================================
#============================================
echo; echo; echo
echo $DDD
func1 Hello-user-$user
date
w
 setSandbox $user
 run_daliSim_Aprime 40
 dumpDL_event 
 run_recoG4E 
 ls -l $OutDir/

echo " A L L    T E S T S    C O M P L I T T E D "
quit1 
echo abc 

