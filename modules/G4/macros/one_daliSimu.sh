#!/bin/bash

#
# Purpose of this job is to examin hardware configuration of a  machine running job, 
#  test output transfer 
# the trick is never  remove this 2 local files: _condor_stderr, _condor_stdout or  you will not see the stdout, stderr messages after job finishes

echo My job-oneG  inpArgString=$1=  START SSSSSSSSSSSS
pwd
hostname
whoami

coreName=`echo $1 | cut -f1 -d,`
numEve=`echo $1 | cut -f2 -d,`
outDir=`echo $1 | cut -f3 -d,`
nfsDir=`echo $1 | cut -f4 -d,`
echo one_daliSimu job input params:  core=$coreName numEve=$numEve outDir=$outDir nfsDir=$nfsDir

# get more info about the enviroment where this will run
/dali/u/balewski/examineCPU.sh 

# set geant4 libraries location
source /usr/local/darkness/templates/bashrc

# unpack STL geometry
tar xf externalGeom.tar
ls -l 

echo  fire my simulations ---------

time ./daliSimG4 --batch --magField externalGeom/2013-10-25d-CDS.dat  --numEve $numEve --coreName $coreName --setSeed 0 --baseDir ./

pwd
ls -l 
du -hs .
df -h .

# B) scp to remote machine (reuse16) 
# scp -i ./reuse16-balewskiA.pem -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  $coreName.DLg4.root balewski@reuse16.lns.mit.edu:data1

# C) cp to nfs-disc on remote machine (reuse16)
cp  $coreName.DLg4.root  $nfsDir/x-$coreName.DLg4.root

echo My OneA job   END EEEEEEEEEE
date

