#include "base.h"
#include <iostream>

// #include "CLHEP/Units/PhysicalConstants.h"
#include "TRandom.h"


typedef struct
{
  unsigned int sequence_count;
}
halton_state_t;


GeneratorBase::GeneratorBase(int num_halton_dim, unsigned int skip,unsigned long int seed)
{
  (void) skip;
  (void) seed;
  gRandom->SetSeed(seed);
  nHaltonDim=num_halton_dim;
  qrndNumbers=new double[nHaltonDim];
  updatedNums = qrndNumbers;

}

GeneratorBase::~GeneratorBase()
{

}




double GeneratorBase::getRandom()
{
  return gRandom->Uniform();
}

int GeneratorBase::generate(GeneratorEvent * eventinfo)
{

  for( int i=0;i<(nHaltonDim);i++)
    updatedNums[i]=gRandom->Uniform();
  eventinfo->lepton_prescatter.particle=(beamCharge==-1) ? "e-" : "e+";
  eventinfo->lepton_prescatter.momentum = TLorentzVector(0.,0.,sqrt(beamEnergy*beamEnergy - 0.511*0.511), beamEnergy);

  eventinfo->particles.clear(); 
  eventinfo->weight.clear();
  return generateEvent(eventinfo);
}


int GeneratorBase::generateEvent(GeneratorEvent *) //do nothing, has to be overloaded.
{
  printf("Called Base class generateEvent - no particles generated...\n");
  return 0;

}


void GeneratorBase::setBeamEnergy(double en)
{
  beamEnergy=en;
}

void GeneratorBase::setBeamCharge(int q)
{
  beamCharge = q;
}


//static!
double GeneratorBase::getMass(std::string particle)
{
  if (particle=="e+" || particle=="e-")
    return  0.511/1;
  
  if (particle=="proton" )
    return  938.0/1;
  
  if (particle=="carbon" )
    return 931.494*12.01;//6*939.0/1 + 6*938.0/1;
  
  if (particle=="neutron" )
    return  939.0/1;

  if (particle=="mu-" || particle=="mu+")
    return 105.6584;

  if (particle=="gamma" )
    return  0.0;

  std::cerr<<"Could not find Mass for "<<particle<<" please add in GeneratorBase!\n";
  exit(-1);
  
  return 0;
}
