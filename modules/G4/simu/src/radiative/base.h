#ifndef __GENBASE_H_
#define __GENBASE_H_


//forward declaration to make root happy. gsl has this as an anonymous struct (gsl_(q)rng), which can not be forward declared. so we have to do some magick(tm).


#include "generatortree.h"



// This is the definition of a Generator base class

class GeneratorBase
{
 protected:
  int nHaltonDim; // Number of unused dimensions
  int nBaseDim; // Number of dimensions used by the base
  double targetDensity;
  double *qrndNumbers; // This array holds that random numbers to be used by the generator each event.
  double *updatedNums; // This pointer points to the array of numbers that are being updated each event. Points to the same as above unless alwaysSameEvent.
  bool alwaysSameEvent;

 public:
  double beamEnergy;
  int beamCharge; //(-1 for electrons, +1 for positrons)

  GeneratorBase(int num_halton_dim, unsigned int skip,unsigned long int seed); //how many dimensions, how many events to skip (for halton), and the seed for 
  ~GeneratorBase();
    

  virtual double getRandom(); //gets the next random number from the Mersenne Twister implementation.



  virtual int generate(GeneratorEvent *eventinfo); // the "typical" function.
  virtual int generateEvent(GeneratorEvent *eventinfo); //eventinfo is supplied from the outside (to write to trees directly). Return : positive: Number of tracks. Negative: Error.  This should be overloaded by the real generators.
  virtual void setBeamEnergy(double en);
  virtual void setBeamCharge(int q);  
  static double getMass(std::string particle);
};

#endif
