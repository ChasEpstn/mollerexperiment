// $Id$
//
/// \file DaLiGen_pScan.cc
/// \brief  implementation of single track generator

#include "assert.h"

#include "TObjArray.h"
#include "TObjString.h"
#include "TRandom.h"

#include "DaLiGen_pScan.h"
#include "DLmadEvent.h"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "G4ParticleTable.hh"

using namespace CLHEP;


//=======================================
DaLiGen_pScan::DaLiGen_pScan(TString parList) {
  printf("in constr DaLiGen_pScan , parList=%s\n",parList.Data());
  madEvent=new DLmadEvent;
  myPID= 0;
  
  TObjArray * objA=parList.Tokenize("_");
  //printf("size =%d \n",objA->GetEntries());
  TIter*  iter = new TIter(objA);  
  TObject* obj = 0;
  int k=0;
  phi1=0; phi2=TMath::TwoPi();

  // decoding parameters of generator
  while( (obj=iter->Next())) {    
    k++;
    // obj->Print();
    TString ss= (( TObjString *)obj)->GetString();
    //printf("k=%d, item=%s=\n",k,ss.Data()); 
    switch (k) {
    case 1: // particle type // Geant HEP convention
      // http://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf
      //http://pdg.lbl.gov/2011/reviews/rpp2011-rev-monte-carlo-numbering.pdf 

      if     (ss=="ele") myPID=11;  
      else if(ss=="pos")  myPID=-11;  
      else if(ss=="pro")  myPID=2212;  
      else if(ss=="gam")  myPID=22;   
      else if(ss=="mum")  myPID=13;  
      else if(ss=="aprime")  myPID=43;  
      else if(ss=="lambda")  myPID=3122;  
      else assert(123==678); // undefined particle
      break;
    case 2: assert(ss=="mom"); break;
    case 3: momentum=atof(ss.Data()); break;
    case 4: assert(ss=="th"); break;
    case 5: theta=atof(ss.Data()); break;
    case 6: assert(ss=="delt"); break;
    case 7: delt=atof(ss.Data()); break;
    case 8: assert(ss=="pct"); break;
    case 9: pct=atof(ss.Data()); break;
    case 10: assert(ss=="phi"); break;			
    case 11: phi1=atof(ss.Data()); break;
    case 12: phi2=atof(ss.Data()); break;
      
   default:
      assert(345==987); // unexpected
    }
  }
  
  assert(myPID);
  assert(momentum>=0.);
  assert(phi2>phi1);

  printf("set pScan: pid=%d  P/MeV=[%.1f]  delt = %.1f pct = %.1f theta/rad=[%.1f] & phi=[%.2f %.2f] rad \n",myPID,momentum,delt,pct,theta,phi1,phi2);
}



//=====================================
DLmadEvent*  DaLiGen_pScan::NextEvent() {
  madEvent->clear();
  madEvent->id=eventCntr++;
  madEvent->weight=1;
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  double me=particleTable->FindParticle(myPID)->GetPDGMass(); //MeV, default gamma mass
    double phi = phi1+ (phi2-phi1)*gRandom->Uniform();


  // printf(" DaLiGen_pScan::NextEvent(), eveid=%d  special=%d, np=%d\n",eventCntr,special,par_nPartPerEve);

  if(delt<0.001){
      // double p_step = momentum*pow(1.0-(pct/100.),gRandom->Uniform());
      double p_step = momentum*(1.0-(pct/100.)*gRandom->Uniform());
      // printf("pid=%d M/MeV=%f  \n", myPID,me/MeV );    
      double pz = p_step*cos(theta);
      double pt = p_step*sin(theta);
      double e_step = sqrt(p_step*p_step + me*me);
      madEvent->addOutParticleMeV(myPID,pt*cos(phi),pt*sin(phi),pz,e_step);

  }

  else{
    for(int np=0; np< pct/100./delt; np++ ) { // decide on # of particles form the same vertex
      
      double p_step =momentum*(1.0-np*delt);

      // printf("pid=%d M/MeV=%f  \n", myPID,me/MeV );	  

      double pz = p_step*cos(theta);
      double pt = p_step*sin(theta);
      double e_step = sqrt(p_step*p_step + me*me);
      madEvent->addOutParticleMeV(myPID,pt*cos(phi),pt*sin(phi),pz,e_step);
    }
  }
  return madEvent; 
}
