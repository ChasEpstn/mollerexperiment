// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
// Executed one per every track
//
//*--  Author: Jan Balewski, MIT, December 2012
//
// $Id: DaLiTrackingAction.cc,v 1.2 2011/12/15 21:02:50 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****


#include "globals.hh"
#include "G4RunManager.hh"

#include "DaLiTrackingAction.h"

// needed to write hits/tracks dirrectly to the output file
#include "DLg4Event.h"
using namespace CLHEP;

int maxTrID=10;

//=====================================
DaLiTrackingAction::DaLiTrackingAction(){ 
 
  printf("DaLiTrackingAction() constructed \n");
}

//=====================================
void DaLiTrackingAction::PrintSummary(){ 
  printf("DaLiTrackingAction::PrintSummary() nothing ....\n");
}

//=====================================
DaLiTrackingAction::~DaLiTrackingAction(){ 
  delete trackDLg4;
}

//=====================================
void DaLiTrackingAction::PreUserTrackingAction(const G4Track* aTrack){

  trackDLg4->clear(); 
  G4ThreeVector momentum = aTrack->GetMomentum();
  G4ThreeVector position = aTrack->GetPosition();
  G4ThreeVector  vertex=  aTrack->GetVertexPosition ();

  //if(aTrack->GetParentID()>0) return; // skip  hist from secondary tracks

  if(myDebug>0 &&  maxTrID>aTrack->GetTrackID()) {
  G4VPhysicalVolume *  physVol=aTrack->	GetVolume();
  G4double  time= aTrack->	GetGlobalTime () ;
  printf("\nTTTT ::PreUserTrackingAction trID=%d parentID=%d start volume %s xyz/mm=%.2e,%.2e,%.2e, eneTot/MeV=%.5g  pT/MeV=%.2f, name=%s pxyz/MeV=%.2e,%.2e,%.2e\n",aTrack->GetTrackID(),aTrack->GetParentID(),physVol->GetName().data(),position.x()/mm,position.y()/mm,position.z()/mm,aTrack->GetTotalEnergy()/MeV,momentum.perp(),aTrack->GetDefinition()->GetParticleName().data(),momentum.x()/MeV,momentum.y()/MeV,momentum.z()/MeV);
  printf(" vertex/mm %.2e,%.2e,%.2e  globTime/ns=%.2e\n",vertex.x()/mm,vertex.y()/mm,vertex.z()/mm,time/ns);
  }

  //const G4Event* e = G4RunManager::GetRunManager()->GetCurrentEvent();

  trackDLg4->SetTrackID(aTrack->GetTrackID());
  trackDLg4->SetParentID(aTrack->GetParentID());
  trackDLg4->SetOrigin(TVector3(position.x()/mm,position.y()/mm,position.z()/mm));
  trackDLg4->SetParticleName(aTrack->GetDefinition()->GetParticleName());
  trackDLg4->SetMomentumPxPyPzE(momentum.x()/MeV,momentum.y()/MeV,momentum.z()/MeV,aTrack->GetTotalEnergy()/MeV);
  //  trackDLg4->Print();

}


//=====================================
void DaLiTrackingAction::PostUserTrackingAction(const G4Track* aTrack){
  //  if(0)aTrack=aTrack; // to make compiler hapy
  if(myDebug>0&&  maxTrID>aTrack->GetTrackID()) {
    G4ThreeVector position = aTrack->GetPosition();
    G4double  time= aTrack->	GetGlobalTime () ;
    G4double  velo= aTrack->GetVelocity ();
    G4double  trkLen=aTrack->GetTrackLength();
    printf("PPPP ::PostUserTrackingAction for trID=%d in physVol=%s  DLtrID=%d  nHits=%d\n",aTrack->GetTrackID(),aTrack->GetVolume()->GetName().data(), trackDLg4->GetTrackID(), trackDLg4->GetNumHits());
    printf(" position/mm %.2e,%.2e,%.2e  globTime/ns=%.2e  velo/c=%.2e  trkLen/mm=%.2e\n",position.x()/mm,position.y()/mm,position.z()/mm,time/ns,velo/(3e8*m/s),trkLen/mm);
  }
  if(trackDLg4->GetTrackID() <0) return; // skip tracks below threshold defined in pre-step
    if(trackDLg4->GetNumHits() <=0) return; // skip tracks which have not left detector hits

  eventDLg4->AddTrack(trackDLg4); 

  trackDLg4->clear(); // just in case
}

// $Log: DaLiTrackingAction.cc,v $
// Revision 1.2  2011/12/15 21:02:50  balewski
// ready to export hits in ttree
//
// Revision 1.1  2011/12/15 18:09:15  balewski
// start
//
    
