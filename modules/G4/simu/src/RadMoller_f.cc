//Function implementations for RadMoller_Gen class
#include "RadMoller.h"
#include "Riostream.h"
#include "TF1.h"
#include "Math/WrappedTF1.h"
#include "Math/BrentRootFinder.h"
#include "Math/Functor.h"
#include "Math/RootFinder.h"
#include "TSystem.h"
#include "TComplex.h"
#include "TFoamIntegrand.h"
#include "TFoam.h"
void RadMoller_Gen::setRandom(RandGen *rGen){
    random = rGen;
}


void RadMoller_Gen::SetMoller(){
    mb_flag = 1;
}

void RadMoller_Gen::SetBhabha(){
    mb_flag = 0;
}

void RadMoller_Gen::SetpRes(int res){
    pRes = res;
}

void RadMoller_Gen::SetpCells(int bins){
    pCells = bins;
}

void RadMoller_Gen::SetRadFrac(double rdf){
    radFrac = rdf;
}

void RadMoller_Gen::SetCM(){
    CM_flag = 1;
}
void RadMoller_Gen::SetLab(){
    CM_flag = 0;
}

double RadMoller_Gen::symWeight(double th, double ph){
    double symFactor;
    while(ph<0){
        ph = ph + twopi;
    }

    if((th < thetaCut1) && (th > thetaCut0) && (ph < phiCut1) && (ph > phiCut0)){
        symFactor = 1.;
    }
    else{
        symFactor = 2.;
    }
    // cout<<symFactor<<endl;
    return symFactor;
}


void RadMoller_Gen::SetTCuts(double tC0, double tC1, double pC0, double pC1){
    if(mb_flag == 1){
        cout<<"Moller Events Requested!"<<endl;
        cout<<"You Requested: "<<endl;
        cout<<"Min Electron_1 Theta = "<<tC0<<endl;
        cout<<"Max Electron_1 Theta = "<<tC1<<endl;
        cout<<"Min Electron_1 Phi = "<<pC0<<endl;
        cout<<"Max Electron_1 Phi = "<<pC1<<endl;

    }
    else{
        cout<<"Bhabha Events Requested!"<<endl;
        cout<<"You Requested: "<<endl;
        cout<<"Min Positron Theta = "<<tC0<<endl;
        cout<<"Max Positron Theta = "<<tC1<<endl;
        cout<<"Min Positron Phi = "<<pC0<<endl;
        cout<<"Max Positron Phi = "<<pC1<<endl;
    }
        // tkCut0 = 0.;//tkc0;
        thetaCut0 = tC0;
        // tkCut1 = TMath::Pi();//tkc1;
        thetaCut1 = tC1;
        phiCut0 = pC0;
        phiCut1 = pC1;




}

void RadMoller_Gen::SetECut(double dEf){
    dE_frac = dEf;
}

void RadMoller_Gen::SetLumi(double lum){
    Lumi = lum;
}

void RadMoller_Gen::SetTBeam(double tb){
    Tbeam = tb;
}

//Mandelstam T
double RadMoller_Gen::te(double x)
    {
    // return -4.*Ecmp*Ecmp*pow(sin(x/2.),2.);
      return 2*me*me - 2*Ecmp*Ecmp + 2*Pcmp*Pcmp*cos(x);
    }

//Mandelstam U
double RadMoller_Gen::ue(double x)
    {
    // return -4.*Ecmp*Ecmp*pow(cos(x/2.),2.);
      return 2*me*me - 2*Ecmp*Ecmp - 2*Pcmp*Pcmp*cos(x);
    }

double RadMoller_Gen::sqr(double x)
    {
    return x*x;
    }

double RadMoller_Gen::eps1u(TVector3 *p3, TLorentzVector *k){ //me-> units of MeV
    double Cosa = TMath::Cos(k->Angle(*p3));
    double E_k = k->E();
    return me*(-((Cosa*E_k*sqrt(-pow((2*Ecmp)/me - E_k/me,2) +
            (pow(Cosa,2)*pow(E_k,2))/pow(me,2) +
            (4*pow(Ecmp,2)*pow(Ecmp/me - E_k/me,2))/pow(me,2)))/me) +
     (2*Ecmp*(Ecmp/me - E_k/me)*((2*Ecmp)/me - E_k/me))/me)/
   (pow((2*Ecmp)/me - E_k/me,2) - (pow(Cosa,2)*pow(E_k,2))/pow(me,2));
}

double RadMoller_Gen::eps1l(TVector3 *p3, TLorentzVector *k){ //me-> units of MeV
    double Cosa = TMath::Cos(k->Angle(*p3));
    double E_k = k->E();

    return me*((Cosa*E_k*sqrt(-pow((2*Ecmp)/me - E_k/me,2) +
          (pow(Cosa,2)*pow(E_k,2))/pow(me,2) +
          (4*pow(Ecmp,2)*pow(Ecmp/me - E_k/me,2))/pow(me,2)))/me +
     (2*Ecmp*(Ecmp/me - E_k/me)*((2*Ecmp)/me - E_k/me))/me)/
   (pow((2*Ecmp)/me - E_k/me,2) - (pow(Cosa,2)*pow(E_k,2))/pow(me,2));
}

double RadMoller_Gen::p3u(TVector3 *p3, TLorentzVector *k){ //me-> units of MeV
    double Cosa = TMath::Cos(k->Angle(*p3));
    double E_k = k->E();

    return me*(sqrt(-pow((2*Ecmp)/me - E_k/me,2) + (pow(Cosa,2)*pow(E_k,2))/pow(me,2) +
        (4*pow(Ecmp,2)*pow(Ecmp/me - E_k/me,2))/pow(me,2))*
      ((2*Ecmp)/me - E_k/me) - (2*Cosa*Ecmp*E_k*(Ecmp/me - E_k/me))/pow(me,2))/
   (pow((2*Ecmp)/me - E_k/me,2) - (pow(Cosa,2)*pow(E_k,2))/pow(me,2));
}

double RadMoller_Gen::p3l(TVector3 *p3, TLorentzVector *k){ //me-> units of MeV
    double Cosa = TMath::Cos(k->Angle(*p3));
    double E_k = k->E();
    return me*(sqrt(-pow((2*Ecmp)/me - E_k/me,2) + (pow(Cosa,2)*pow(E_k,2))/pow(me,2) +
        (4*pow(Ecmp,2)*pow(Ecmp/me - E_k/me,2))/pow(me,2))*
      ((-2*Ecmp)/me + E_k/me) - (2*Cosa*Ecmp*E_k*(Ecmp/me - E_k/me))/pow(me,2))/
   (pow((2*Ecmp)/me - E_k/me,2) - (pow(Cosa,2)*pow(E_k,2))/pow(me,2));
}

//Squared Tree-Level Matrix element - does not take m->0 limit
double RadMoller_Gen::M2(double x)
    {
    return 64.0*pow(pi,2.)*pow(alpha,2.)*(pow(me,4.)/pow(te(x),2)*
        ((pow(se,2)+pow(ue(x),2))/(2.*pow(me,4))+4.*te(x)/pow(me,2)-4.0)+pow(me,4)/
        pow(ue(x),2)*((pow(se,2)+pow(te(x),2))/(2.0*pow(me,4))+4.0*ue(x)/pow(me,2)-4.)+
        pow(me,4)/(ue(x)*te(x))*(se/pow(me,2)-2.0)*(se/pow(me,2)-6.0));
    }

//Squared Tree-Level Bhabha Matrix element
double RadMoller_Gen::M2b(double x)
    {
    return 64.0*pow(pi,2.)*pow(alpha,2.)*(pow(me,4.)/pow(te(x),2)*
        ((pow(ue(x),2)+pow(se,2))/(2.*pow(me,4))+4.*te(x)/pow(me,2)-4.0)+pow(me,4)/
        pow(se,2)*((pow(ue(x),2)+pow(te(x),2))/(2.0*pow(me,4))+4.0*se/pow(me,2)-4.)+
        pow(me,4)/(se*te(x))*(ue(x)/pow(me,2)-2.0)*(ue(x)/pow(me,2)-6.0));
    }

Double_t RadMoller_Gen::SoftPhoton_Moller_Integrand(Double_t *x, Double_t *par){
    double var = x[0];

   //  return (sqrt(SD)*(-2 + TD)*log((sqrt(SD) + sqrt(-4 + SD + 4*TD*(1 - var)*var))/
   //      (sqrt(SD) - sqrt(-4 + SD + 4*TD*(1 - var)*var))))/
   //  ((1 - TD*(1 - var)*var)*sqrt(-4 + SD + 4*TD*(1 - var)*var)) +
   // (sqrt(SD)*(-2 + UD)*log((sqrt(SD) + sqrt(-4 + SD + 4*UD*(1 - var)*var))/
   //      (sqrt(SD) - sqrt(-4 + SD + 4*UD*(1 - var)*var))))/
   //  ((1 - UD*(1 - var)*var)*sqrt(-4 + SD + 4*UD*(1 - var)*var));
        return ((sqrt(SD)*(-2 + TD)*log((sqrt(SD) + sqrt(-4 + SD + 4*TD*(1 - var)*var))/
        (sqrt(SD) - sqrt(-4 + SD + 4*TD*(1 - var)*var))))/
    ((1 - TD*(1 - var)*var)*sqrt(-4 + SD + 4*TD*(1 - var)*var)) +
   (sqrt(SD)*(-2 + UD)*log((sqrt(SD) + sqrt(-4 + SD + 4*UD*(1 - var)*var))/
        (sqrt(SD) - sqrt(-4 + SD + 4*UD*(1 - var)*var))))/
    ((1 - UD*(1 - var)*var)*sqrt(-4 + SD + 4*UD*(1 - var)*var)));

}

double RadMoller_Gen::SoftPhoton_Moller_Integral(){
// gSystem->Load("libMathMore");
   // cout<<(sqrt(SD)*(-2 + TD)*log((sqrt(SD) + sqrt(-4 + SD + 4*TD*(1 - 0.2)*0.2))/
   //      (sqrt(SD) - sqrt(-4 + SD + 4*TD*(1 - 0.2)*0.2))))/
   //  ((1 - TD*(1 - 0.2)*0.2)*sqrt(-4 + SD + 4*TD*(1 - 0.2)*0.2)) +
   // (sqrt(SD)*(-2 + UD)*log((sqrt(SD) + sqrt(-4 + SD + 4*UD*(1 - 0.2)*0.2))/
   //      (sqrt(SD) - sqrt(-4 + SD + 4*UD*(1 - 0.2)*0.2))))/
   //  ((1 - UD*(1 - 0.2)*0.2)*sqrt(-4 + SD + 4*UD*(1 - 0.2)*0.2))<<endl;
   TF1 f("Integrand", this,&RadMoller_Gen::SoftPhoton_Moller_Integrand,0,0.5,0,"RadMoller_Gen","SoftPhoton_Moller_Integrand");
   ROOT::Math::WrappedTF1 wf1(f);
   // cout<<f(0.2)<<endl;
   // Create the Integrator
   ROOT::Math::GSLIntegrator ig(ROOT::Math::IntegrationOneDim::kADAPTIVE);
   // Set parameters of the integration
   ig.SetFunction(wf1);
   ig.SetRelTolerance(0.0001);
   return ig.Integral(0, 0.5);
}

double RadMoller_Gen::SoftPhoton_Moller(double x, double dE){
    SD = se/(me*me);
    UD = ue(x)/(me*me);
    TD = te(x)/(me*me);
    double III = SoftPhoton_Moller_Integral();
    // return (2*alpha*(III + (2*sqrt(SD)*log((sqrt(-4 + SD) + sqrt(SD))/2.))/sqrt(-4 + SD) +
    //    4*log(me/(2.*dE))*(0.5 + ((-2 + SD)*log((sqrt(-4 + SD) + sqrt(SD))/2.))/
    //        (sqrt(-4 + SD)*sqrt(SD)) +
    //       ((-2 + TD)*log((sqrt(4 - TD) + sqrt(-TD))/2.))/
    //        (sqrt(4 - TD)*sqrt(-TD)) +
    //       ((-2 + UD)*log((sqrt(4 - UD) + sqrt(-UD))/2.))/
    //        (sqrt(4 - UD)*sqrt(-UD))) +
    //    ((-4 + 2*SD)*(pow(pi,2)/6. +
    //         ((4 - SD)*pow(log((sqrt(-4 + SD) + sqrt(SD))/2.),2))/(-4 + SD) +
    //         log((sqrt(-4 + SD) + sqrt(SD))/2.)*log(-4 + SD) -
    //         TMath::DiLog((-2 + SD - sqrt(-4*SD + pow(SD,2)))/2.)))/
    //     sqrt(-4*SD + pow(SD,2))))/pi;
    return MollerLoop(SD,TD,UD)+((2*alpha*(III + (2*sqrt(SD)*log((sqrt(-4 + SD) + sqrt(SD))/2.))/sqrt(-4 + SD) +
       4*log(me/(2.*dE))*(0.5 + ((-2 + SD)*log((sqrt(-4 + SD) + sqrt(SD))/2.))/
           (sqrt(-4 + SD)*sqrt(SD)) +
          ((-2 + TD)*log((sqrt(4 - TD) + sqrt(-TD))/2.))/
           (sqrt(4 - TD)*sqrt(-TD)) +
          ((-2 + UD)*log((sqrt(4 - UD) + sqrt(-UD))/2.))/
           (sqrt(4 - UD)*sqrt(-UD))) +
       ((-4 + 2*SD)*(pow(pi,2)/6. +
            ((4 - SD)*pow(log((sqrt(-4 + SD) + sqrt(SD))/2.),2))/(-4 + SD) +
            log((sqrt(-4 + SD) + sqrt(SD))/2.)*log(-4 + SD) -
            TMath::DiLog((-2 + SD - sqrt(-4*SD + pow(SD,2)))/2.)))/
        sqrt(-4*SD + pow(SD,2))))/pi);

}

Double_t RadMoller_Gen::SoftPhoton_Bhabha_Integrand(Double_t *x, Double_t *par){
    double var = x[0];

    return ((sqrt(SD)*(-2 + TD)*log((sqrt(SD) + sqrt(-4 + SD + 4*TD*(1 - var)*var))/
        (sqrt(SD) - sqrt(-4 + SD + 4*TD*(1 - var)*var))))/
    ((1 - TD*(1 - var)*var)*sqrt(-4 + SD + 4*TD*(1 - var)*var)) +
   (sqrt(SD)*(2 - UD)*log((sqrt(SD) + sqrt(-4 + SD + 4*UD*(1 - var)*var))/
        (sqrt(SD) - sqrt(-4 + SD + 4*UD*(1 - var)*var))))/
    ((1 - UD*(1 - var)*var)*sqrt(-4 + SD + 4*UD*(1 - var)*var)));
}

double RadMoller_Gen::SoftPhoton_Bhabha_Integral(){
// gSystem->Load("libMathMore");
   // cout<<SoftPhoton_Bhabha_Integrand(0.2)<<endl;
   TF1 f("Integrand", this,&RadMoller_Gen::SoftPhoton_Bhabha_Integrand,0,1,0,"RadMoller_Gen","SoftPhoton_Bhabha_Integrand");
   ROOT::Math::WrappedTF1 wf1(f);
   // Create the Integrator
   ROOT::Math::GSLIntegrator ig(ROOT::Math::IntegrationOneDim::kADAPTIVE);
   // Set parameters of the integration
   ig.SetFunction(wf1);
   ig.SetRelTolerance(0.0001);
   return ig.Integral(0, 0.5);
}

double RadMoller_Gen::SoftPhoton_Bhabha(double x, double dE){
    SD = se/(me*me);
    UD = ue(x)/(me*me);
    TD = te(x)/(me*me);
    double III = SoftPhoton_Bhabha_Integral();
    // return ((2*alpha*(III + (2*sqrt(SD)*TComplex::Log((TComplex::Sqrt(-4 + SD) + TComplex::Sqrt(SD))/2.))/TComplex::Sqrt(-4 + SD) +
    //    4*log(me/(2.*dE))*(0.5 + ((2 - SD)*TComplex::Log((TComplex::Sqrt(-4 + SD) + TComplex::Sqrt(SD))/2.))/
    //        (TComplex::Sqrt(-4 + SD)*TComplex::Sqrt(SD)) +
    //       ((-2 + TD)*TComplex::Log((TComplex::Sqrt(4 - TD) + TComplex::Sqrt(-TD))/2.))/
    //        (TComplex::Sqrt(4 - TD)*TComplex::Sqrt(-TD)) +
    //       ((2 - UD)*TComplex::Log((TComplex::Sqrt(4 - UD) + TComplex::Sqrt(-UD))/2.))/(TComplex::Sqrt(4 - UD)*TComplex::Sqrt(-UD))
    //       ) + ((-4 + 2*SD)*(-pow(pi,2)/6. +
    //         TMath::Power(TComplex::Log((TComplex::Sqrt(-4 + SD) + TComplex::Sqrt(SD))/2.),2) -
    //         TComplex::Log((TComplex::Sqrt(-4 + SD) + TComplex::Sqrt(SD))/2.)*TComplex::Log(-4 + SD) +
    //         TMath::DiLog((-2 + SD - TComplex::Sqrt(-4*SD + pow(SD,2)))/2.)))/
    //     TComplex::Sqrt(-4*SD + pow(SD,2))))/pi).Rho();
    return BhabhaLoop(SD,TD,UD)+((2*alpha*(III + (2*sqrt(SD)*log((sqrt(-4 + SD) + sqrt(SD))/2.))/sqrt(-4 + SD) +
       4*log(me/(2.*dE))*(0.5 + ((2 - SD)*log((sqrt(-4 + SD) + sqrt(SD))/2.))/
           (sqrt(-4 + SD)*sqrt(SD)) +
          ((-2 + TD)*log((sqrt(4 - TD) + sqrt(-TD))/2.))/
           (sqrt(4 - TD)*sqrt(-TD)) +
          ((2 - UD)*log((sqrt(4 - UD) + sqrt(-UD))/2.))/(sqrt(4 - UD)*sqrt(-UD))
          ) + ((-4 + 2*SD)*(-pow(pi,2)/6. +
            pow(log((sqrt(-4 + SD) + sqrt(SD))/2.),2) -
            log((sqrt(-4 + SD) + sqrt(SD))/2.)*log(-4 + SD) +
            TMath::DiLog((-2 + SD - sqrt(-4*SD + pow(SD,2)))/2.)))/
        sqrt(-4*SD + pow(SD,2))))/pi);

}

//Construct the Tree-Level Cross Section (CMS)
double RadMoller_Gen::tree_cs(double x)
    {
    return 0.5*pow(8.0*pi,-2)*M2(x)*pow(Ecm,-2);
    }

double RadMoller_Gen::tree_cs_b(double x)
    {
    return 0.5*pow(8.0*pi,-2)*M2b(x)*pow(Ecm,-2);
    }

//Construct the Soft-Brehmsstralung-Corrected Cross-Section (CMS)
double RadMoller_Gen::mCSfunc(double x, double dE)
    {
        return Lumi*hbarc2*tree_cs(x)*(1+SoftPhoton_Moller(x,dE));//(1.+soft_cs_tsai(x,dE));
    }

double RadMoller_Gen::bCSfunc(double x, double dE)
    {
        //Extra factor of two because e+,e- are distinguishable
        return 2.0*Lumi*hbarc2*tree_cs_b(x)*(1+SoftPhoton_Bhabha(x,dE));
    }


//Construct the e-e- Bremsstrahlung Cross Section
double RadMoller_Gen::bremCS(double E_k,
  TLorentzVector *p3, TLorentzVector *k)
    {
        double bCS;
        double Cosa = cos(k->Angle(p3->Vect()));
        bCS = (E_k*me)/
        (2.*Ecmp*sqrt(-pow((2.*Ecmp)/me - E_k/me,2.) +
       (pow(Cosa,2.)*pow(E_k,2.))/pow(me,2.) +
       (4.*pow(Ecmp,2.)*pow(Ecmp/me - E_k/me,2.))/pow(me,2.))*Pcmp)*pow(p3->P(),2.);

        return 0.5*bCS*Lumi*Mh2(p1,p2,p3,k)*pow(2*pi,-5)*pow(me,-3)*hbarc2/32;
    }

//Construct the e-e+ Bremsstrahlung Cross Section
double RadMoller_Gen::bremCSb(double E_k,
  TLorentzVector *p3, TLorentzVector *k)
    {
        double bCS;
        double Cosa = cos(k->Angle(p3->Vect()));
        bCS = (E_k*me)/
        (2.*Ecmp*sqrt(-pow((2.*Ecmp)/me - E_k/me,2.) +
       (pow(Cosa,2.)*pow(E_k,2.))/pow(me,2.) +
       (4.*pow(Ecmp,2.)*pow(Ecmp/me - E_k/me,2.))/pow(me,2.))*Pcmp)*pow(p3->P(),2.);

        return bCS*Lumi*Mh2b(p1,p2,p3,k)*pow(2*pi,-5)*pow(me,-3)*hbarc2/32;
    }


class RadMoller_Gen::TFDISTRAD: public TFoamIntegrand{
public:
  double theta0;
  RadMoller_Gen* RG;
  TFDISTRAD(RadMoller_Gen *RG0, double thetaq){
    theta0=thetaq;
    RG = RG0;
  };
  Double_t Density(Int_t nDim, Double_t *Xarg){
    double Ek0 = Xarg[0]*(RG->k0*(1.-1.e-3)-RG->dE)+RG->dE;
    double tk0 = Xarg[1]*RG->pi;
    double phik0 = Xarg[2]*RG->twopi;
    double val = RG->bremInt(Ek0, tk0, phik0, theta0,0.);
    return val;
  }
};

class RadMoller_Gen::GenRandTR: public TRandom{
public:
  RandGen *RR;
  GenRandTR(Int_t i=0){};
  void SetRandom(RandGen *R0){RR = R0;}
  Double_t Uniform(Double_t x1=1){
      return x1*RR->randomOne();
  }
  Double_t Uniform(Double_t x1, Double_t x2){
    return x1+(x2-x1)*RR->randomOne();
  }
  Double_t Rndm(){
    return RR->randomOne();
  }
  void RndmArray(Int_t n, Double_t *array){
    for(int i=0;i<n;i++){
      array[i] = RR->randomOne();
    }
  }
  void RndmArray(Int_t n, Float_t *array){
    for(int i=0;i<n;i++){
      array[i] = RR->randomOne();
    }
  }
};

double RadMoller_Gen::bremInt(double Ek0, double tk0, double phik0, double theta0,double pqr0){
    TLorentzVector *kcm0 = new TLorentzVector;
    TLorentzVector *p3cm0 = new TLorentzVector;
    TVector3 *p3r0 = new TVector3(0,0,0);

    p3r0->SetXYZ(sin(theta0)*cos(pqr0),sin(theta0)*sin(pqr0),cos(theta0));
    kcm0->SetPxPyPzE(Ek0*sin(tk0)*cos(phik0),Ek0*sin(tk0)*sin(phik0),Ek0*cos(tk0),Ek0);
    p3cm0->SetPxPyPzE(p3u(p3r0,kcm0)*p3r0->X(),p3u(p3r0,kcm0)*p3r0->Y(),
        p3u(p3r0,kcm0)*p3r0->Z(),eps1u(p3r0,kcm0));

    double val;
    if(mb_flag==1){
        val = bremCS(Ek0,p3cm0,kcm0);
    }
    else if(mb_flag==0){
        val = bremCSb(Ek0,p3cm0,kcm0);
    }
    // cout<<"val: "<<val<<endl;
    delete kcm0;
    delete p3cm0;
    delete p3r0;
    
    if(val<0){

    cout<<"ERROR VAL < 0: "<<val<<endl;
    // cout<<"Ek: "<<Ek0<<endl;
    // cout<<"kcm: "<<kcm0->X()<<", "<<kcm0->Y()<<", "<<kcm0->Z()<<", "<<kcm0->E()<<endl;
    // cout<<"p3cm: "<<p3cm0->X()<<", "<<p3cm0->Y()<<", "<<p3cm0->Z()<<", "<<p3cm0->E()<<endl;
    }
    if(std::isnan(val)){
        cout<<"ERROR NAN in FILL"<<endl;
        cout<<"Ek: "<<Ek0<<endl;
        cout<<"..vs k0: "<<k0<<endl;
        cout<<"kcm: "<<kcm0->X()<<", "<<kcm0->Y()<<", "<<kcm0->Z()<<", "<<kcm0->E()<<endl;
        cout<<"p3cm: "<<p3cm0->X()<<", "<<p3cm0->Y()<<", "<<p3cm0->Z()<<", "<<p3cm0->E()<<endl;

    }
    return val*sin(tk0);

}



void RadMoller_Gen::InitGenerator_RadMoller(){
    me = 0.510998910;
    Ebeam = Tbeam+me;
    alpha = 1./137.035999074;
    hbarc2 = pow(1.97326e-11,2.);
    pi = 4.0*atan(1.0);
    twopi = 2.*pi;
    Pbeam = sqrt(pow(Ebeam,2.)-pow(me,2.));
    // betacm = Pbeam/(Ebeam+me);
    // gammacm = 1./sqrt(1.-pow(betacm,2.));
    // Ecm = gammacm*Ebeam - gammacm*betacm*Pbeam + gammacm*me;
    cm = new TLorentzVector(0.,0.,Pbeam,Ebeam+me);
    p1 = new TLorentzVector(0,0,Pbeam,Ebeam);
    p2 = new TLorentzVector(0,0,0,me);
    p1->Boost(-cm->BoostVector());
    p2->Boost(-cm->BoostVector());
    Ecm = p1->E()+p2->E();

    Pcm = sqrt(pow(Ecm/2.,2)-pow(me,2.));//momentum of either
    Ecmp = Ecm/2.; //Ecm per particle
    Pcmp = sqrt(pow(Ecmp,2)-pow(me,2.));//momentum of either b
    ec = sqrt(4.*pi*alpha); //electron charge
    se = Ecm*Ecm; //Elastic Mandelstam S ("s" was unavail`
    dE = dE_frac*Ecm;
    EkMax = (Ecm*Ecm-4.*me*me)/(2.*Ecm);
    k0 = me*(2*Ecmp*(-1 + Ecmp/me))/((-1 + (2*Ecmp)/me)*me); //units of MeV

    cout<<"EkMax: "<<EkMax<<endl;
    cout<<"Ecm: "<<Ecm<<endl;

    photonInt = new TFoam*[pRes];
    PseRan = new GenRandTR();
    PseRan->SetRandom(random);

    RHO = new TFDISTRAD*[pRes];

    MCvect =new Double_t[3];

    for(int i = 0;i<pRes;i++){//for every theta
            double theta0 = thetaCut0 + (thetaCut1-thetaCut0)*double(i)/double(pRes-1);
            double pqr0 = 0.;
            RHO[i] = new TFDISTRAD(this,theta0);
            photonInt[i] = new TFoam("PhotonIntegrator");
            photonInt[i]->SetkDim(3);         // No. of dimensions, obligatory!
            photonInt[i]->SetnCells(pCells);     // Optionally No. of cells, default=2000
            photonInt[i]->SetRho(RHO[i]);  // Set 2-dim distribution, included below
            photonInt[i]->SetPseRan(PseRan);  // Set random number generator
            photonInt[i]->Initialize();       // Initialize simulator, may take time...
    }

}

void RadMoller_Gen::Generate_Event(){


    theta = thetaCut0+(thetaCut1-thetaCut0)*random->randomOne();
    thetaWeight = sin(theta)*(thetaCut1-thetaCut0);

    pqr = phiCut0+(phiCut1-phiCut0)*random->randomOne();
    pqrWeight = phiCut1-phiCut0;

    pickProc = random->randomOne();

    delete p3cm;
    delete p4cm;
    delete p3;
    delete p4;

    if (pickProc<radFrac){//Bremsstrahlung

        Ek   = dE+(EkMax-dE)*random->randomOne();//ekiCDF(random->randomOne());//
        ekWeight   = EkMax-dE;//1./ekFunc(Ek);//
        // ekWeight = 1.;
        delete qcm;
        delete p3r;
        delete k;
        delete kcm;


        p3r = new TVector3(sin(theta)*cos(pqr),sin(theta)*sin(pqr),cos(theta));
        // kcm = new TLorentzVector(Ek*sin(tk)*cos(phik),Ek*sin(tk)*sin(phik),Ek*cos(tk),Ek);

        if (Ek>k0){

            double aRand = random->randomOne();
            delete newAxis;
            newAxis = new TVector3(-sin(theta)*cos(pqr),-sin(theta)*sin(pqr),-cos(theta));
            cosaMax = -((sqrt(pow((2*Ecmp)/me - Ek/me,2) -
                (4*pow(Ecmp,2)*pow(Ecmp/me - Ek/me,2))/pow(me,2))*me)/Ek);

            if(1){//Uniform
                        tk  = TMath::ACos(1.-(1.+cosaMax)*random->randomOne());
                        phik = twopi*random->randomOne();

                        kcm = new TLorentzVector(Ek*sin(tk)*cos(phik),Ek*sin(tk)*sin(phik),Ek*cos(tk),Ek);
                        kcm->RotateUz(*newAxis);

                        tkWeight  = (1.+cosaMax);
                        phikWeight = twopi;

                        if (aRand>0.5){
                            p3cm = new TLorentzVector(p3u(p3r,kcm)*p3r->X(),p3u(p3r,kcm)*p3r->Y(),
                                p3u(p3r,kcm)*p3r->Z(),eps1u(p3r,kcm));
                        }
                        else if (aRand <0.5){
                            p3cm = new TLorentzVector(p3l(p3r,kcm)*p3r->X(),p3l(p3r,kcm)*p3r->Y(),
                                p3l(p3r,kcm)*p3r->Z(),eps1l(p3r,kcm));
                        }
            }



            aFlag = 2.; //weight up by 2.
            p4cm = new TLorentzVector(*p1+*p2-*p3cm-*kcm);
            if(mb_flag==1){//Moller
                weight = bremCS(Ek,p3cm,kcm)*symWeight(p4cm->Theta(),p4cm->Phi())/radFrac\
                *ekWeight*tkWeight*phikWeight*thetaWeight*pqrWeight*aFlag;
                // cout<<"ek "<<ekWeight<<" tkw "<<tkWeight<<" pkw "<<phikWeight<<" thetaw "<<thetaWeight<<" pqrw "<<pqrWeight<<endl;
            }

            if(mb_flag==0){//Bhabha
                weight = bremCSb(Ek,p3cm,kcm)/radFrac\
                *ekWeight*tkWeight*phikWeight*thetaWeight*pqrWeight*aFlag;
            }


        }

        else if (Ek<k0){
            kcm = new TLorentzVector;
            p3cm = new TLorentzVector;
            double tBinD = (theta-thetaCut0)/(thetaCut1-thetaCut0)*(double(pRes)-1.);
            int tBin = (int) floor(tBinD+0.5);


            photonInt[tBin]->MakeEvent();            // generate MC event
            photonInt[tBin]->GetMCvect(MCvect);

            double EkF = MCvect[0]*(k0*(1.-1.e-3)-dE)+dE;
            tk = MCvect[1]*pi;
            phik = MCvect[2]*twopi+pqr;


            kcm->SetPxPyPzE(EkF*sin(tk)*cos(phik),EkF*sin(tk)*sin(phik),EkF*cos(tk),EkF);
            p3cm->SetPxPyPzE(p3u(p3r,kcm)*p3r->X(),p3u(p3r,kcm)*p3r->Y(),
                p3u(p3r,kcm)*p3r->Z(),eps1u(p3r,kcm));

            // delete intPhoton;
            // photonInt[tBin]->GetMCwt(MCwt);
            // cout<<"WEIGHT: "<<MCwt<<endl;
            photonInt[tBin]->GetIntegMC(MCResult,MCError);
            Double_t *XX = new Double_t[3];
            XX[0] = MCvect[0];
            XX[1] = MCvect[1];
            XX[2] = MCvect[2];
            aFlag = 1.;  //no reweighting
            p4cm = new TLorentzVector(*p1+*p2-*p3cm-*kcm);
            if(mb_flag==1){//Moller
              weight = MCResult*(bremCS(EkF,p3cm,kcm)/RHO[tBin]->Density(3,XX))*\
              symWeight(p4cm->Theta(),p4cm->Phi())/radFrac*thetaWeight*pqrWeight*ekWeight*pi*twopi*sin(tk)*aFlag;
            }//sin(tk)*

            if(mb_flag==0){//Bhabha
              weight = MCResult*(bremCSb(EkF,p3cm,kcm)/RHO[tBin]->Density(3,XX))*\
              1./radFrac*thetaWeight*pqrWeight*ekWeight*pi*sin(tk)*twopi*aFlag;
            }

         }


        p3 = new TLorentzVector(*p3cm);
        p4 = new TLorentzVector(*p4cm);
        k = new TLorentzVector(*kcm);

        p3->Boost(cm->BoostVector());
        p4->Boost(cm->BoostVector());
        k->Boost(cm->BoostVector());

        elFlag = 1;
    }

    if (pickProc >radFrac){//Elastic Kinematics

        p3cm = new TLorentzVector(Pcmp*sin(theta)*cos(pqr),\
            Pcmp*sin(theta)*sin(pqr),Pcmp*cos(theta),Ecmp);
        p4cm = new TLorentzVector(-Pcmp*sin(theta)*cos(pqr),\
            -Pcmp*sin(theta)*sin(pqr),-Pcmp*cos(theta),Ecmp);

        p3 = new TLorentzVector(*p3cm);
        p4 = new TLorentzVector(*p4cm);
        if(mb_flag==1){//Moller
        weight = mCSfunc(theta,dE)*symWeight(p4cm->Theta(),p4cm->Phi())*thetaWeight*pqrWeight/(1.-radFrac);//
        }

        if(mb_flag==0){//Bhabha
        weight = bCSfunc(theta,dE)*thetaWeight*pqrWeight/(1.-radFrac);
        }

        p3->Boost(cm->BoostVector());
        p4->Boost(cm->BoostVector());
        elFlag = 0;
    }



}
