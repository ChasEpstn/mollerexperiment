//
// $Id: G4Aprime.cc 67971 2013-03-13 10:13:24Z gcosmo $
//
// 
// ----------------------------------------------------------------------
//      GEANT 4 class implementation file
//
//      History: first implementation, based on object model of
//      4-th April 1996, G.Cosmo
// ****************************************************************
//  Implemented by Jan Balewski, MIT, August 2014
//  Based on  impelemenatation of G4Gamma
// ----------------------------------------------------------------

#include <assert.h>
#include "G4Aprime.hh"
#include "G4SystemOfUnits.hh"
#include "G4ParticleTable.hh"

#include "G4PhaseSpaceDecayChannel.hh"
#include "G4DecayTable.hh"

// ######################################################################
// ###                           A'= dark photon 
// ######################################################################
G4Aprime* G4Aprime::theInstance = 0;


G4Aprime*  G4Aprime::Definition()
{
  if (theInstance !=0) return theInstance;
  assert(22==555);
}

G4Aprime* G4Aprime::AprimeDefinition(double massX,  double  ctauX) 
{
  double  mwidthX=10*keV;
  assert (theInstance ==0);// may be called only once per run
  const G4String name = "aprime";

  // search in particle table
  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);
  if (anInstance ==0)
  {
    printf("AprimeDefinition was created\n");
  // create particle, modified G4Gamma-particle
  //      
  //    Arguments for constructor are as follows 
  //               name             mass          width         charge
  //             2*spin           parity  C-conjugation
  //          2*Isospin       2*Isospin3       G-parity
  //               type    lepton number  baryon number   PDG encoding
  //             stable         lifetime    decay table 
  //             shortlived      subType    anti_encoding
   anInstance = new G4ParticleDefinition(
		name,            massX,       mwidthX,         0.0, 
		    2,              -1,            -1,          
		    0,               0,             0,             
	      "gamma",               0,             0,          43,
	        false,           ctauX/(3e8*m/s),          NULL,
		false,    "darkphoton");
  }
  //http://pdg.lbl.gov/2011/reviews/rpp2011-rev-monte-carlo-numbering.pdf

  //create Decay Table, based on pi0--> gamma gamma
  G4DecayTable* table = new G4DecayTable();

  // create a decay channel:  A' -> e+ e- 
  G4VDecayChannel*  mode 
    = new G4PhaseSpaceDecayChannel("aprime",1.0,2,"e-","e+");
  table->Insert(mode);
  
  anInstance->SetDecayTable(table);

  theInstance = reinterpret_cast<G4Aprime*>(anInstance);

  return theInstance;  
}


G4Aprime*  G4Aprime::Aprime() 
{
  if (theInstance !=0) return theInstance;
  assert(22==555);
}
