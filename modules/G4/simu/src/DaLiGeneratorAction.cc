// $Id$
//
/// \file DaLiGeneratorAction.cc
/// \brief Implementation of the DaLiGeneratorAction class
// - Modified Jan Balewski, MIT,  October 2013

#include <assert.h>
#include "G4ParticleTable.hh"

#include "DaLiGeneratorAction.h"

#include "DaLiGen_readMadEve.h"
#include "DaLiGen_oneTrack.h"
#include "DaLiGen_inlineElastic.h"
#include "DaLiGen_epRadiative.h"
#include "DaLiGenVertex.h"
#include "DaLiGen_radMoller.h"
#include "DaLiGen_pScan.h"
#include <DLmadEvent.h>
#include <DLg4Event.h>

//JB to re-direct some of the warnings to /dev/null- not sure if this is thread-safe
#include <G4strstreambuf.hh>


//=======================================
DaLiGeneratorAction* DaLiGeneratorAction::fgInstance = 0; // why?? Jan

//=======================================
const DaLiGeneratorAction* DaLiGeneratorAction::Instance() {
  // Static acces function via G4RunManager 
  return fgInstance;
}      

//=======================================
DaLiGeneratorAction::DaLiGeneratorAction( char * geomX): G4VUserPrimaryGeneratorAction(), fParticleGun(0){
  fgInstance = this; // why do I need it, JanB ?
  fParticleGun  = new G4ParticleGun();
  eveCntr = 0;
  inputEventGen=0;
  inputVertexGen=0;
  eventDLg4 =0;  
  baseGeometryG4=geomX; 
}

//=======================================
void DaLiGeneratorAction::SetPhysicsGenerator(G4String physGen, char *physParam){

  if(physGen=="madGraph") {
    printf("SetPhysicsGenerator = MagGraph\n");
    inputEventGen=new DaLiGen_readMadEve(physParam);
  } else if(physGen=="oneTrack") {
    printf("SetPhysicsGenerator = oneTrack\n");
    inputEventGen=new DaLiGen_oneTrack(physParam); 
  } else if(physGen=="pScan") {
    printf("SetPhysicsGenerator = pScan\n");
    inputEventGen=new DaLiGen_pScan(physParam); 
  } else if(physGen=="epElastic" || physGen=="Moller") {
    printf("SetPhysicsGenerator = inline %s\n", physGen.data());
    inputEventGen=new DaLiGen_inlineElastic(physParam,physGen);
  } else if (physGen=="epRadiative") {
      printf("SetPhysicsGenerator = epRadiative %s\n", physGen.data());
      inputEventGen=new DaLiGen_epRadiative(physParam);
  } else if(physGen=="RadMoller") {
  printf("SetPhysicsGenerator = RadMoller \n");
  inputEventGen=new DaLiGen_radMoller(physParam);
  }  else {
    printf(" unexpected string1=%s=\n", physGen.data());
    assert(3==88);
  }
  inputEventGen->generatorName=physGen;
}


//=======================================
void DaLiGeneratorAction::SetVertexGenerator(char *allParamZ,char *allParamT){
  //  printf("VVVGGG Z=%s T=%s\n",allParamZ, allParamT);
  inputVertexGen=new DaLiGenVertex(allParamZ, allParamT);
}


//=======================================
DaLiGeneratorAction::~DaLiGeneratorAction(){
  delete fParticleGun;
  delete inputEventGen;
  delete inputVertexGen;
  fgInstance = 0;
}


//=====================================
void DaLiGeneratorAction::GeneratePrimaries(G4Event* anEvent){
  //this function is called at the begining of each event
  //1 printf("::GeneratePrimaries eve=%d\n", eveCntr);
  eventDLg4->clear();

  //.............  generate vertex .... 
  G4ThreeVector  vert3D=inputVertexGen->NextVertex();
  //1 printf("GenVertex  x,y,z/cm =%.2f %.2f %.2f\n", vert3D.x()/cm, vert3D.y()/cm, vert3D.z()/cm);
  fParticleGun->SetParticlePosition(vert3D );//set it for G4 before track loop 

  //...........  generate event kinematics ........
  eventDLmad =inputEventGen->NextEvent();  
  if( eventDLmad ==0) return; // end of input event stream, runManager->AbortRun() was already called

  //assert(2==3);  
  // printf("GGG DaLiGeneratorAction::GeneratePrimaries p=%p  n=%d \n", (void*)eventDLmad,eveCntr);
  //1 eventDLmad ->print();
     
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  // http://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf
  // 11=e-,  gam=22, 2212=p
  
  for (int i = 0; i < eventDLmad->nOutPart(); i++)    {
      DLmadTrack* track = eventDLmad->getOutParticle(i);
      int pid = track->hepId;
      if (pid == 101) continue; //101 is the new ID for A' particles, skip it
      //if (pid != -11) continue; // accept only e+, tmp
      //if (pid != 11) continue; // accept only e-, tmp
      // if (pid != 2212) continue; // accept only protons, tmp
      //if(fabs(pid)==11 ) pid=22; // replace leptons in to gammas, tmp
      // printf("added pid=%d\n",pid);      

      fParticleGun->SetParticleDefinition(particleTable->FindParticle(pid));
      TLorentzVector p = track->P4;

      // JB to re-direct some of LOG stream  to /dev/null : START
      G4strstreambuf* oldBuffer = dynamic_cast<G4strstreambuf*>(G4cout.rdbuf(0));
      fParticleGun->SetParticleMomentum(G4ParticleMomentum(p.Px()*MeV,p.Py()*MeV,p.Pz()*MeV));
      G4cout.rdbuf(oldBuffer);// restore default log-stream

      fParticleGun->GeneratePrimaryVertex(anEvent);
      //  break;//tmp
  }
  eveCntr++;

  // propagate some info  to g4 output event
  eventDLg4->SetEventID(eventDLmad->id);
  eventDLg4->SetWeight(eventDLmad->weight);
  eventDLg4->SetBaseGeometry((char*)baseGeometryG4.data());
  eventDLg4->SetGeneratorName(inputEventGen-> generatorName);    

  eventDLg4->GetVertex()->SetX(vert3D.x());
  eventDLg4->GetVertex()->SetY(vert3D.y());
  eventDLg4->GetVertex()->SetZ(vert3D.z());


}
