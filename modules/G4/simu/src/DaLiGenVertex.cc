// $Id$
//
/// \file DaLiGenVertex.cc
/// \brief  multi modal vertex generator
// - Modified Jan Balewski, MIT, October 2013

#include <assert.h>
#include <TObjArray.h>
#include <TF1.h>
#include <TMath.h>
#include <TObjString.h>
#include <TRandom.h>

#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>

#include "DaLiGenVertex.h"

//=======================================
DaLiGenVertex::DaLiGenVertex(TString parListZ, TString parListT) {
  printf("in  DaLiGenVertex , z-profile=%s,  transverse profile=%s\n",parListZ.Data(),parListT.Data());

  modeNameZ="fixMe1";
  x0=y0=z0=0*cm; // default for vertex location
  fzGen=0; // switch of Z spread
  ftGen=0; // switch of transverse spread

  double a,b,c,d;
  a=b=c=d=-99999;

  TObjArray * objA=parListZ.Tokenize("_");
  //printf("size =%d \n",objA->GetEntries());
  TIter*  iter = new TIter(objA);  
  TObject* obj = 0;
  int k=0;

  // decoding parameters of generator

  while( (obj=iter->Next())) {    
    k++;
    // obj->Print();
    TString ss= (( TObjString *)obj)->GetString();
    //printf("k=%d, item=%s=\n",k,ss.Data()); 

    switch (k) {
    case 1:  modeNameZ=ss; break;
    case 2: a=atof(ss.Data()); break;
    case 3: b=atof(ss.Data()); break;
    case 4: c=atof(ss.Data()); break;
    case 5: d=atof(ss.Data()); break;
    default:
      assert(345==987); // unexpected
    }
  }
  
  if(modeNameZ=="xyz") {
    x0=a*cm; y0=b*cm; z0=c*cm;
    fzGen=0; // just in case
    printf("SetVertex at fixed location x,y,z/cm =%.2f %.2f %.2f\n", x0/cm, y0/cm,z0/cm);
   } else if(modeNameZ=="ztrap") {
    x0=y0=0*cm; // just in case
    fzGen=SetZtrapezoid(a,b,c,d);
    printf("SetVertex Z is trapezoid w/ central value  x,y/cm =%.2f %.2f \n", x0/cm, y0/cm);
  } else assert(345==456); // unexpected string1

  //..... deal with transverse beam profile
  if(parListT=="delta" ) { // do nothing
    ftGen=0; // just in case
    printf("Set no transverse beam spread\n");
  } else  if(parListT=="full" ) {
    printf("Set full transverse beam spread as in published JLAB test\n");
    ftGen=SetTransverseBeamProfile(-1*mm); // eclipseR ignored
  }  else  if(parListT=="eclipse" ) {
    double eclipseR=1.0*mm;
    printf("Set transverse beam spread w/ R>eclipseR=%.2fmm\n",eclipseR/mm);
    ftGen=SetTransverseBeamProfile(eclipseR); // eclipseR ignored
  } else {
    assert(89==90); // eclipse not implemented
  }
}



//=====================================
G4ThreeVector   DaLiGenVertex::NextVertex(){
  G4ThreeVector V(x0,y0,z0); // default fix position
  if(fzGen) V.setZ(fzGen->GetRandom()*cm);
  if(ftGen==0) return V;

  // generate displacement
  double r=ftGen->GetRandom();
  double phi=gRandom->Uniform(2*pi);
  double x=r*sin(phi);
  double y=r*cos(phi);
  V.setX(x0+x*mm);
  V.setY(y0+y*mm);
  //printf("gen vert x,y,z/mm=%f, %f, %f\n",V.x()/mm,V.y()/mm,V.z()/mm);
  return V; 
}


//=====================================
TF1*   DaLiGenVertex::
  SetZtrapezoid(double a, double b, double c, double zz){

  /*  trapezoid model of the target density 

         _______________
        /|              |\
       / |              |  \    
      /  |              |    \  
...../.a.|....b.........|....c.\....==> +Z axis 
                               zz             
    A    B              C                         */

  double A=zz-a-b-c;
  double B=zz-b-c;
  double C=zz-c;
  printf("ztarp nodes@Z:  %f %f %f %f\n",A,B,C,zz);
  assert(A<=B);
  assert(B<=C);
  assert(C<=zz);
  assert(A<zz);
  TF1 *f1 = new TF1("myZtrapShape","([0]<x)*(x<[1])*(x-[0])/([1]-[0])  + ([1]<x)*(x<[2])  +  ([2]<x)*(x<[3])*([3]-x)/([3]-[2])",A-5,zz+5);
  
  f1->SetParameters(A,B,C,zz);

  return f1;
#if 0 // enable for graphics, works only if this whole function is executed as root .C macro
  TCanvas *can=new TCanvas(); 
  can->Divide(1,2);
  can->cd(1);  f1->Draw();
  h=new TH1F("gg","generated vertices;Z vertex",100,A-5,zz+5);
  for(int i=0;i<10000; i++) {
    double z=f1->GetRandom();
    h->Fill(z);
  }
  can->cd(2); h->Draw("e");
#endif
}

//Start new MG
//=====================================
TF1*   DaLiGenVertex::
SetTransverseBeamProfile( double eclipseR,double sigmaBeam, double sigmaHalo, double HoverB_meas, double rHalo_meas){

  /*  Gaussian model of the transverse beam profile

        Beam Gaussian with sigmaBeam (typically 50 microns)
      + Halo Gaussian with sigmaHalo (typically 1 mm)
        normalized in relative so that 
                HoverB = integrated Halo (r>rHalo_meas) / Beam
       (as per published JLAB beam test, rHalo_meas = 1mm, HoverB_meas = 3 ppm)

       Optional : eclipseR>0 will block generating too small radii , 
        usefull for background studies

        Assume radial distribution from r=0 to 5*sigmaHalo, 
            i.e. H and V distributions are identical

        Unit = mm
  */

  assert(HoverB_meas < 0.001);
  assert(rHalo_meas >= 1.);
  assert(sigmaBeam > 0.);
  assert(sigmaBeam < 0.5);         // 500 microns maximal beam size allowed
  assert(sigmaHalo > 2*sigmaBeam);

  TF1 *f1 = new TF1("myBeamProfile","((1./[0])*TMath::Exp(-0.5*(x/[0])*(x/[0]))+([2]/[1])*TMath::Exp(-0.5*(x/[1])*(x/[1])))*(x>[3])",0,5*sigmaHalo);

  double norm = HoverB_meas / (1-TMath::Erf(rHalo_meas/(TMath::Sqrt(2)*sigmaHalo)));
  f1->SetParameters(sigmaBeam,sigmaHalo,norm,eclipseR);




  return f1;


#if 0 // enable for graphics, works only if this whole function is executed as root .C macro
  TCanvas *can=new TCanvas();
  can->SetLogy();
  can->Divide(2,1);
  can->cd(1);  f1->Draw();
  double mxR=5*sigmaHalo;
  h=new TH1F("grbp","generated radial beam profile; R (mm)",100,0,mxR);
  h2=new TH2F("grbp2D","generated X-Y  beam profile;x(mm);y(mm)",100,-mxR,mxR,100,-mxR,mxR);
  gPad->SetLogy();
  for(int i=0;i<100000000; i++) {
    double r=f1->GetRandom();
    double phi=gRandom->Uniform(2*pi);
    double x=r*sin(phi);
    double y=r*cos(phi);
    h->Fill(r);
    h2->Fill(x,y);
    
  }
  can->cd(2); h->Draw("e");
  gPad->SetLogy();

  TCanvas *can=new TCanvas();
  h2->Draw("colz"); gPad->SetLogz(); gPad->SetRightMargin(0.15);
#endif
}

#if 0 // the macro header
double mm=1, um=1e-3, pi=3.1416;
void plTransBeam() {
  SetTransverseBeamProfile();
}

#endif
//End new MG




