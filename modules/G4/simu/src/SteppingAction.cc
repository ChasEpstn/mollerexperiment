//
//
/// \file electromagnetic/TestEm4/src/SteppingAction.cc
/// \brief Implementation of the SteppingAction class
//
//
// $Id$
//
// 
#include "DLg4Event.h"
#include "assert.h"
#include "TH2F.h"
#include "TFile.h"
#include "SteppingAction.h"
#include "G4SystemOfUnits.hh"
#include "DaLiEventAction.h"

#include "G4Track.hh"
using namespace CLHEP;

SteppingAction::SteppingAction(DaLiEventAction* EvAct)
:fEventAction(EvAct) { 
  eveID=-1;
}

//-------------------------------------------
//-------------------------------------------
void SteppingAction::initHisto(){
  memset(hA,0,sizeof(hA));
  TH1*h;
  const int np=5, nd=4;

  hA[0]=h=new TH1D("myStat","type of events",40,0,40);  h->GetXaxis()->SetTitleOffset(0.4);  h->GetXaxis()->SetLabelSize(0.06);
  h->GetXaxis()->SetTitleSize(0.05); h->SetMinimum(0.);  h->SetLineColor(kBlue);h->SetLineWidth(2);  h->SetMarkerSize(2);//<-- large text

  //predefine order of bins
  {
    TString partNB[np]={"gam","e-","e+","pro","neu"};

    hA[0]->Fill("in e-",0.0);
    hA[0]->Fill("trans e-",0.0);

    hA[0]->Fill("*** PROTON-DET ***",0.0);    
    for(int ip=0;ip<np;ip++){
      TString txtL="proD "+partNB[ip];
      hA[0]->Fill(txtL+" lowP",0.0);  
      hA[0]->Fill(txtL+" higP",0.0);  
    } 

    hA[0]->Fill("***  LEPTON-CY-1 ***",0.0);    
    for(int ip=0;ip<np;ip++){
      TString txtL="lepD "+partNB[ip];
      hA[0]->Fill(txtL+" lowP",0.0);  
      hA[0]->Fill(txtL+" higP",0.0);  
    } 

    hA[0]->Fill("***  PHOTON-DET ***",0.0);    
    for(int ip=0;ip<np;ip++){
      TString txtL="gamD "+partNB[ip];
      hA[0]->Fill(txtL+" lowP",0.0);  
      hA[0]->Fill(txtL+" higP",0.0);  
    } 
    
#if 0 //bb
    hA[0]->Fill("***  DOSE-MON ***",0.0);
    for(int ip=0;ip<np;ip++){
      TString txtL="doseD "+partNB[ip];
      hA[0]->Fill(txtL+" lowP",0.0);
      hA[0]->Fill(txtL+" higP",0.0);
    }
#endif

    hA[0]->Fill("** QA",0.0);
    hA[0]->Fill("dblTrk",0.0);
    hA[0]->Fill("manyTrk",0.0);
    //    hA[0]->Fill(,0.0);
  }


  TString partNA[np]={"gamma","electron","positron","proton","neutron"};
  TString detNA[nd]={"protDet", "leptDet", "photDet","doseDet"};  
  
  // uses histo in range [1, 150+40+5=195]
  for(int id=0;id<nd;id++)
  for(int ip=0;ip<np;ip++){
    int poff=10*ip;
    int doff=50*id;
    int off=poff+doff;
    TString core=partNA[ip]+"_"+detNA[id];
    double yMax=55; // cm
    if(id==3) yMax=110; // does monitor is far away
    hA[1+off]=h=new TH1D(core+"P",core+" inp kinE;  kin ene (MeV)",1010,0,101);
    hA[2+off]=new TH2D(core+"RZ",core+"  entry, kinE>1MeV; world Z (cm) ; world Rxy (cm)",130,-120,170,110,0,yMax);
    hA[3+off]=0;
    hA[4+off]=new TH2D(core+"Kin",core+" in kinematics, kinE>1MeV; PZ (MeV/c); PT (MeV/c)",220,-110,110,90,0,90);
    hA[5+off]=new TH2D(core+"V",core+" source vertex, kinE>1MeV; world Z (cm) ; world Rxy (cm)",240,-120,170,550,0,yMax);
  }

  // histos 200+ is in us
  //  hA[200]= new TH1F("hFeve","gamma in event; phi of the gamma (rad)",120,-pi,pi); //WARN: cleared per event
  enum {mxFB=100 };
  Float_t xBB[mxFB]={ 
    1.0000e-05, 1.0970e-05, 1.2020e-05, 1.3180e-05, 1.4460e-05, 1.5850e-05, 1.7380e-05, 1.9060e-05, 2.0900e-05, 2.2920e-05, 
    2.5130e-05, 2.7560e-05, 3.0220e-05, 3.3130e-05, 3.6330e-05, 3.9840e-05, 4.3690e-05, 4.7900e-05, 5.2530e-05, 5.7600e-05, 
    6.3160e-05, 6.9260e-05, 7.5940e-05, 8.3270e-05, 9.1310e-05, 1.0010e-04, 1.0980e-04, 1.2040e-04, 1.3200e-04, 1.4480e-04, 
    1.5870e-04, 1.7410e-04, 1.9090e-04, 2.0930e-04, 2.2950e-04, 2.5160e-04, 2.7590e-04, 3.0260e-04, 3.3180e-04, 3.6380e-04, 
    3.9890e-04, 4.3740e-04, 4.7970e-04, 5.2600e-04, 5.7670e-04, 6.3240e-04, 6.9340e-04, 7.6040e-04, 8.3380e-04, 9.1430e-04, 
    1.0030e-03, 1.0990e-03, 1.2050e-03, 1.3220e-03, 1.4490e-03, 1.5890e-03, 1.7430e-03, 1.9110e-03, 2.0950e-03, 2.2980e-03, 
    2.5200e-03, 2.7630e-03, 3.0290e-03, 3.3220e-03, 3.6430e-03, 3.9940e-03, 4.3800e-03, 4.8030e-03, 5.2660e-03, 5.7750e-03, 
    6.3320e-03, 6.9430e-03, 7.6140e-03, 8.3490e-03, 9.1540e-03, 1.0040e-02, 1.1010e-02, 1.2070e-02, 1.3230e-02, 1.4510e-02, 
    1.5910e-02, 1.7450e-02, 1.9130e-02, 2.0980e-02, 2.3010e-02, 2.5230e-02, 2.7660e-02, 3.0330e-02, 3.3260e-02, 3.6470e-02, 
    3.9990e-02, 4.3850e-02, 4.8090e-02, 5.2730e-02, 5.7820e-02, 6.3400e-02, 6.9520e-02, 7.6230e-02, 8.3590e-02, 9.1660e-02 };
  for(int i=0;i<mxFB;i++) xBB[i]*=1000.;

  hA[201]= h=new TH1F("hFgam","gammas in NaI scoring ring  ; Photon energy (MeV)",mxFB-1,xBB);
  h->SetLineColor(kBlue);
  hA[202]= h=new TH1F("hFele","electrons in NaI scoring ring  ; electron energy (MeV)",mxFB-1,xBB);
  hA[203]= h=new TH1F("hFpos","positrons in NaI scoring ring  ; positron energy (MeV)",mxFB-1,xBB);
  hA[204]= h=new TH1F("hFneu","neutron in NaI scoring ring  ; neutron energy (MeV)",mxFB-1,xBB);

}

//-------------------------------------------
//-------------------------------------------
void SteppingAction::saveHisto(TString fname) {
  // rescale some histos w/ # of events

  //  hA[2]->Scale(1./nEve);

  hA[0]->SetMinimum(0.1);

 TString  fnameF= fname+".hist.root";
  TFile f(fnameF.Data(),"recreate");
  assert(f.IsOpen());
  printf(" histos are written  to '%s' " ,fnameF.Data());
  for(int i=0;i<mxH;i++) {
    if(hA[i]==0) continue;
    hA[i]->Write();
  }
  f.ls();//list saved histos  
  printf("           save Ok %s\n",f.GetName());

  printf("          for plots exec:  root -l ../macros/plStepp.C\'(\"%s\",1)\'\n",fname.Data());

  f.Close();  assert(!f.IsOpen());
}  //if(phyVol->GetName()=="doseSens" && nextVol->GetName()=="doseShield" )  { doff=150; txtL="doseD";} 



//-------------------------------------------
//-------------------------------------------
void SteppingAction::UserSteppingAction(const G4Step* aStep){
  int thisEveID= fEventAction->eventDLg4->GetEventID();

  double par_minEkin=1.0*MeV;

  const G4Track *track= 	aStep->GetTrack () ; 
  G4VPhysicalVolume* phyVol=track->GetVolume();

  G4VPhysicalVolume* nextVol=track->GetNextVolume();
  const G4ThreeVector  p3=track->GetMomentum ();
  const G4ThreeVector r=track->GetPosition();  
  const G4ParticleDefinition* partDef=track->GetDefinition();
  const  G4String& partName=partDef->GetParticleName();
  G4double trkEkin=track->GetKineticEnergy ();
  G4ThreeVector trkVert= track->GetVertexPosition(); 
  int thistrkID = track->GetTrackID();
  // printf("TrackID: %d\n",track->GetTrackID());
  if(eveID!=thisEveID) {
    eveID=thisEveID;
    trkID = -1;
    memset(trackScore,0,sizeof(trackScore));
    hA[0]->Fill("in e-",1.);
    // printf("************  new event **** eveID=%d\n",eveID);
  }

  if(trkID!=thistrkID){
    trkID = thistrkID;
   fEventAction->holdTrackInfo.eid = thisEveID;
   if(partName=="e-"){
      fEventAction->holdTrackInfo.pid = 1;
    }
   else if(partName=="e+"){
      fEventAction->holdTrackInfo.pid = 2;
    }
   else if(partName=="gamma"){
      fEventAction->holdTrackInfo.pid = 3;
    }
   else {
      fEventAction->holdTrackInfo.pid = 0;
    }

   fEventAction->holdTrackInfo.trkid = track->GetTrackID();
   fEventAction->holdTrackInfo.stepx = trkVert.x()/cm;
   fEventAction->holdTrackInfo.stepy = trkVert.y()/cm;
   fEventAction->holdTrackInfo.stepz = trkVert.z()/cm;
   fEventAction->fullTrackInfo.push_back(fEventAction->holdTrackInfo);
 }

#if 0
  // double theta=p3.theta();
  G4double trkEtot=track->GetTotalEnergy ();
  double stepLen=aStep->GetStepLength();
  double cosTh=p3.cosTheta();
  
   G4StepPoint *stpp=aStep->GetPreStepPoint();  

   double trkLen=track->GetTrackLength();
   printf(" HEL  eve=%d xyz/cm=%.2f,%.2f,%.2f R=%.2f  logN=%s.%d, trID=%d,%s  Etot/MeV=%.4g Ekin/MeV=%.4g cosTh=%.3f stpL=%.1fcm\n",
	  eveID, r.x()/cm,r.y()/cm,r.z()/cm,r.perp()/cm,phyVol->GetName().data(), 
	  stpp->GetTouchableHandle() ->GetCopyNumber(),track->GetTrackID(),partName.data(), trkEtot/MeV, trkEkin/MeV,cosTh,stepLen/cm);
   printf("   track vert/cm=%.2f,%.2f,%.2f len/cm=%.1f\n",trkVert.x()/cm,trkVert.y()/cm,trkVert.z()/cm,trkLen/cm);
   if(nextVol) printf("nextVol=%s\n",nextVol->GetName().data());
#endif

#if 1
   // fEventAction->holdTrackInfo = {1,track->GetTrackID(),r.x()/cm,r.y()/cm,r.z()/cm};
   // fEventAction->pid.push_back(1);
   // fEventAction->trkid.push_back(track->GetTrackID());
   // fEventAction->stepx.push_back(r.x()/cm);
   // fEventAction->stepy.push_back(r.y()/cm);
   // fEventAction->stepz.push_back(r.z()/cm);
   fEventAction->holdTrackInfo.eid = thisEveID;
   if(partName=="e-"){
      fEventAction->holdTrackInfo.pid = 1;
    }
   else if(partName=="e+"){
      fEventAction->holdTrackInfo.pid = 2;
    }
   else if(partName=="gamma"){
      fEventAction->holdTrackInfo.pid = 3;
    }
   else {
      fEventAction->holdTrackInfo.pid = 0;
    }
   fEventAction->holdTrackInfo.trkid = track->GetTrackID();
   fEventAction->holdTrackInfo.stepx = r.x()/cm;
   fEventAction->holdTrackInfo.stepy = r.y()/cm;
   fEventAction->holdTrackInfo.stepz = r.z()/cm;

   fEventAction->fullTrackInfo.push_back(fEventAction->holdTrackInfo);
#endif

  if(nextVol==0) return;

  int doff=-1;
  TString txtL="";

  // below are traps for particles entering the selected 4 volumes
  if(phyVol->GetName()=="targetEnvelope" && nextVol->GetName()=="proCy" )  { doff=0; txtL="proD";} 
  if(phyVol->GetName()=="lepTrack" && nextVol->GetName()=="lepCy1" )  { doff=50; txtL="lepD";} 
  if(phyVol->GetName()=="photLead" && nextVol->GetName()=="phoCy1" )  { doff=100; txtL="gamD";} 
  //bb  if(phyVol->GetName()=="doseShield" && nextVol->GetName()=="doseSens" )  { doff=150; txtL="doseD";} 
  //if(phyVol->GetName()=="aluCube" && nextVol->GetName()=="DLKworld" )  { doff=150; txtL="doseD";} 

   if(doff<0)  return;
  
  // avoid double counting
  int trkId=track->GetTrackID();
  assert(trkId>=0);
  if (trkId>=mxTrk) {hA[0]->Fill("manyTrk",1.); return;}
  trackScore[trkId]++; 
  if(trackScore[trkId]==2) hA[0]->Fill("dblTrk",1.);
  if(trackScore[trkId]>1) return;
   
  int poff=0;
  if(partName=="gamma") { poff=0; txtL+=" gam"; }
  else if(partName=="e-") { poff=10; txtL+=" e-"; }
  else if(partName=="e+") { poff=20; txtL+=" e+"; }
  else if(partName=="proton")  { poff=30; txtL+=" pro"; }
  else if(partName=="neutron") { poff=40; txtL+=" neu"; }
  else { hA[0]->Fill("other",1.); return; }
  
  int off=poff+doff;

  hA[1+off]->Fill(trkEkin/MeV);
  
  // radiation monitor - special case
  if( doff==150  ) { // photons for dosimetry
    if( poff==0 ) hA[201]->Fill(trkEkin/MeV);
    if( poff==10) hA[202]->Fill(trkEkin/MeV);
    if( poff==20) hA[203]->Fill(trkEkin/MeV);
    if( poff==40) hA[204]->Fill(trkEkin/MeV);

    //double phi=r.phi();
    //printf("e=%e phi=%f\n",trkEkin,phi);
    //    hA[200]->Fill(phi,trkEkin/GeV);
 }

  if( trkEkin<par_minEkin ) {
    hA[0]->Fill(txtL+" lowP",1.);
    return;
  }

  hA[0]->Fill(txtL+" higP",1.);
    

  double rxy=sqrt(r.x()*r.x() + r.y()*r.y());  
  
  
  ((TH2F*) hA[2+off])->Fill(r.z()/cm,rxy/cm);
  ((TH2F*)hA[4+off])->Fill(p3.z()/MeV,p3.perp()/MeV);
  ((TH2F*)hA[5+off])->Fill(trkVert.z()/cm,trkVert.perp()/cm);


   //example of saving random number seed of this event, under condition
   //// if (condition) G4RunManager::GetRunManager()->rndmSaveThisEvent();  
}

