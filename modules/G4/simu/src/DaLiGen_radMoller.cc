// $Id$
//
/// \file DaLiGen_radMoller.cc
// - Charles S Epstein , MIT, March 2014

#include <assert.h>

#include "DaLiGen_radMoller.h"
#include "DLmadEvent.h"
#include <TObjArray.h>
#include <TH1.h>
#include <TRandom.h>
#include <TObjString.h>
#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>
#include <iostream>
#include "RadMoller.h"
#include "Riostream.h"
#include "TFile.h"
#include "TFoam.h"
#include "TNtuple.h"
#include "TLorentzVector.h"
#include "TF1.h"
#include "TF2.h"
#include "TH1.h"
#include "TH2D.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TRandom3.h"
#include "TSystem.h"
#include "TStyle.h"
#include <complex>
#include <stdio.h>
#include <TObjArray.h>
#include <TObjString.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <assert.h>
#include "RandT3.h"

using namespace std;



// //===================================
// //===================================
// //===================================
// double E_elastic(double x){
//   double EkinBeam = 100.;
//   double me=0.510998928;//MeV
//   return (EkinBeam*me + pow(me,2) + EkinBeam*me*pow(cos(x),2) - pow(me,2)*pow(cos(x),2))/
//   (EkinBeam + me - EkinBeam*pow(cos(x),2) + me*pow(cos(x),2));

// }

double DaLiGen_radMoller::LabToCM(double theta){
	double EkinBeam = 100.;
	double me=0.510998928;//MeV
	double Ebeam = EkinBeam+me;
	double Pbeam = sqrt(pow(Ebeam,2.)-pow(me,2.));
	double betacm = Pbeam/(Ebeam+me);
	double gammacm = 1./sqrt(1.-pow(betacm,2.));

  double E = (EkinBeam*me + pow(me,2) + EkinBeam*me*pow(cos(theta),2) - pow(me,2)*pow(cos(theta),2))/
  (EkinBeam + me - EkinBeam*pow(cos(theta),2) + me*pow(cos(theta),2));
  double p = sqrt(E*E-me*me);

  double pt = p*sin(theta);
  double pz = -betacm*gammacm*E + gammacm*p*cos(theta);
  // printf("Pt: %g, pz: %g\n",pt,pz);
  return atan2(pt,pz);
	// return pi-atan2(p*sin(theta),(-gammacm*betacm*E+gammacm*p*cos(theta)));
}
DaLiGen_radMoller::DaLiGen_radMoller(TString parList) {
	printf("Radiative Moller Generator, parList=%s\n",parList.Data());

	madEvent=new DLmadEvent;
	theta1=theta2=0;  
	phi1=0; phi2=TMath::TwoPi();
	TObjArray * objA=parList.Tokenize("_");
	//printf("size =%d \n",objA->GetEntries());
	TIter*  iter = new TIter(objA);  
	TObject* obj = 0;
	int k=0;
	while( (obj=iter->Next())) {    
	  k++;
	  // obj->Print();
	  TString ss= (( TObjString *)obj)->GetString();
	  // printf("k=%d, item=%s=\n",k,ss.Data()); 
	  switch (k) {
	  case 1: assert(ss=="elth"); break;			
	  case 2: theta1=atof(ss.Data()); break;
	  case 3: theta2=atof(ss.Data()); break;
	  case 4: assert(ss=="phi"); break;			
	  case 5: phi1=atof(ss.Data()); break;
	  case 6: phi2=atof(ss.Data()); break;
	  default:
	    assert(2345==33987); // unexpected
	  }
	}// end of while
	
	assert(theta1>0);
	assert(theta2>theta1);
	assert(phi2>phi1);
	// rMollerGen = new RadMoller_Gen();
	// rMollerGen->SetMoller();
	// rMollerGen->SetRadFrac(0.75);
	// rMollerGen->SetTCuts(0.0001,0.0001,0.0001);
	// rMollerGen->SetECut(1.e-3);
	// rMollerGen->SetLumi(1.e36);//for picobarns
	// rMollerGen->SetTBeam(100.);
	// rMollerGen->InitGenerator_RadMoller();
	randomGenerator = new RandT3;
    rMollerGen = new RadMoller_Gen;

    rMollerGen->setRandom(randomGenerator);
    rMollerGen->SetRadFrac(0.5);
    
    rMollerGen->SetMoller();
    // double tolerance = 5*pi/180.;
    printf("Final Thetas: %.15f, %.15f\n",LabToCM(theta1),LabToCM(theta2));
    rMollerGen->SetTCuts(LabToCM(theta1),LabToCM(theta2),phi1,phi2);    
    rMollerGen->SetECut(1.e-4);
    rMollerGen->SetLumi(1.e36);
    rMollerGen->SetTBeam(100);
    rMollerGen->SetpRes(2);
    rMollerGen->SetpCells(500);
    rMollerGen->SetLab();
    rMollerGen->InitGenerator_RadMoller();

}



//=====================================
DLmadEvent*  DaLiGen_radMoller::NextEvent() {
	madEvent->clear();
	madEvent->id=eventCntr++;
    rMollerGen->Generate_Event();
	madEvent->weight=rMollerGen->GetWeight();
	// printf("Event weight: %g\n",rMollerGen->GetWeight());
	//printf(" eve=%d theta/rad=%.6g\n",eventCntr,theta);	
	
	if (rMollerGen->GetRadiativeFlag()==0){//Corrected-Elastic
		
		p3 = rMollerGen->Getp3();
		p4 = rMollerGen->Getp4();

		/*  Geant HEP convention,     http://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf
		 11=e-, -11=e+,  gam=22, 2212=p   */
		
		int myPID=11; // electron 
		madEvent->addOutParticleMeV(myPID,p3->Px(),p3->Py(),p3->Pz(),p3->E());
        madEvent->addOutParticleMeV(myPID,p4->Px(),p4->Py(),p4->Pz(),p4->E());

		
	}
	
	if (rMollerGen->GetRadiativeFlag()==1){//Radiative
		p3 = rMollerGen->Getp3();
		p4 = rMollerGen->Getp4();
		kph = rMollerGen->Getk();

        int myPID=11; // electron 
        madEvent->addOutParticleMeV(myPID,p3->Px(),p3->Py(),p3->Pz(),p3->E());
        madEvent->addOutParticleMeV(myPID,p4->Px(),p4->Py(),p4->Pz(),p4->E());
        myPID=22; //photon
        madEvent->addOutParticleMeV(myPID,kph->Px(),kph->Py(),kph->Pz(),kph->E());


	}
	
	
	
	return madEvent; 
}


