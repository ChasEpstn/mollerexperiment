// $Id$
//
/// \file DaLiGen_epRadiative.cc
/// \brief  port of Olympus ep radiative generator to darklight
// - Written Jan Bernauer, MIT, February 2015

#include <assert.h>

#include "radiative/generatortree.h"
#include "radiative/radiative.h"
#include "DaLiGen_epRadiative.h"
#include "DLmadEvent.h"
#include <TObjArray.h>
#include <TH1.h>
#include <TRandom.h>
#include <TObjString.h>
#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>
#include <iostream>
using namespace std;



//===================================
//===================================
//===================================
DaLiGen_epRadiative::DaLiGen_epRadiative(TString parList) {
	printf("in constr DaLiGen_epRadiative , parList=%s\n",parList.Data());
        procName = "epRadiative";
	madEvent=new DLmadEvent;
	theta1=theta2=0;  
	//......... decoding parameters of generator .....
	
	TObjArray * objA=parList.Tokenize("_");
	//printf("size =%d \n",objA->GetEntries());
	TIter*  iter = new TIter(objA);  
	TObject* obj = 0;
	int k=0;
	int thetadist=0;
	while( (obj=iter->Next())) {    
	  k++;
	  // obj->Print();
	  TString ss= (( TObjString *)obj)->GetString();
	  //	  printf("k=%d, item=%s=\n",k,ss.Data()); 
	  switch (k) {
	  case 1: assert(ss=="elth"); break;			
	  case 2: theta1=atof(ss.Data()); break;
	  case 3: theta2=atof(ss.Data()); break;
	  case 4: assert(ss=="thetadist"); break;
	  case 5: thetadist=atoi(ss.Data());break;
	  default:
	    assert(345==339872); // unexpected
	  }
	}// end of while
	
	assert(theta1>0);
	assert(theta2>theta1);
	gevent=new GeneratorEvent();
	generator=new GeneratorRadiative(0,0);
	generator->setThetaRange(theta1*180/M_PI,theta2*180/M_PI);
	generator->setPhiRange(1); // that's +-90 on both sides.
	generator->setDeltaECut(false,0); // no deltaE cut
	generator->setSoftFraction(0.5); // 50% are soft
	generator->setPushPhoton(true); // enable photon in output
	generator->setThetaDistribution(thetadist);
	generator->setBeamEnergy(100);
	generator->setBeamCharge(-1);
	generator->Initialize();
	eventCntr=0;
	sumW=0;
	sumW2=0;


}


DaLiGen_epRadiative::~DaLiGen_epRadiative()
{
  printf("%s totXsect(cm2)=%.2e +-%.2e  for theta=[%.4f %.1f] rad\n",procName.Data(),sumW/cm2,sqrt(sumW2)/cm2, theta1,theta2);
}


//=====================================
DLmadEvent*  DaLiGen_epRadiative::NextEvent() {
	madEvent->clear();
	madEvent->id=eventCntr++;

	generator->generate(gevent);
	madEvent->weight=gevent->weight.get_default() * 1e36 * 36; //cm^2 to pbar plus z^2
	sumW+=madEvent->weight;
	sumW2+=madEvent->weight*madEvent->weight;
	for (std::vector<GeneratorParticle>::iterator p=gevent->particles.begin();p!=gevent->particles.end();++p)
	  {
	    DLmadTrack track;
	    track.hepId=0;
	    if (p->particle=="e-")
	      track.hepId=11;
	    if (p->particle=="proton")
	      track.hepId=2212;
	    if (p->particle=="gamma")
	      track.hepId=22;
	    
	    //	    cout<<p.particle<<" "<<track.hepId<<" "<<p.momentum.P()<<endl;
	    assert(track.hepId!=0);
	    track.P4=p->momentum;
	    madEvent->out.push_back(track);
	  }
		
	
	return madEvent; 
}


