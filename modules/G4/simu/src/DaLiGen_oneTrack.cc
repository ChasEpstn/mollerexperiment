// $Id$
//
/// \file DaLiGen_oneTrack.cc
/// \brief  implementation of single track generator
// - Modified Jan Balewski, MIT, October 2013

#include "assert.h"

#include "TObjArray.h"
#include "TObjString.h"
#include "TRandom.h"

#include "DaLiGen_oneTrack.h"
#include "DLmadEvent.h"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "G4ParticleTable.hh"

using namespace CLHEP;


//=======================================
DaLiGen_oneTrack::DaLiGen_oneTrack(TString parList) {
  printf("in constr DaLiGen_oneTrack , parList=%s\n",parList.Data());
  madEvent=new DLmadEvent;
  myPID= 0;
  
  TObjArray * objA=parList.Tokenize("_");
  //printf("size =%d \n",objA->GetEntries());
  TIter*  iter = new TIter(objA);  
  TObject* obj = 0;
  int k=0;
  phi1=0; phi2=TMath::TwoPi();

  // decoding parameters of generator
  while( (obj=iter->Next())) {    
    k++;
    // obj->Print();
    TString ss= (( TObjString *)obj)->GetString();
    //printf("k=%d, item=%s=\n",k,ss.Data()); 
    switch (k) {
    case 1: // particle type // Geant HEP convention
      // http://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf
      //http://pdg.lbl.gov/2011/reviews/rpp2011-rev-monte-carlo-numbering.pdf 

      if     (ss=="ele") myPID=11;  
      else if(ss=="pos")  myPID=-11;  
      else if(ss=="pro")  myPID=2212;  
      else if(ss=="gam")  myPID=22;   
      else if(ss=="mum")  myPID=13;  
      else if(ss=="aprime")  myPID=43;  
      else if(ss=="lambda")  myPID=3122;  
      else assert(123==678); // undefined particle
      break;
    case 2: assert(ss=="pt"); break;
    case 3: pt1=atof(ss.Data()); break;
    case 4: pt2=atof(ss.Data()); break;
    case 5: assert(ss=="pz"); break;
    case 6: pz1=atof(ss.Data()); break;
    case 7: pz2=atof(ss.Data()); break;
    case 8: assert(ss=="phi"); break;			
    case 9: phi1=atof(ss.Data()); break;
    case 10: phi2=atof(ss.Data()); break;
      
   default:
      assert(345==987); // unexpected
    }
  }
  
  assert(myPID);
  assert(pt1>=0.);
  assert(pt2>=pt1);
  assert(pz2>=pz1);
  assert(phi2>phi1);

  printf("set oneTrack: pid=%d  pt/MeV=[%.1f,%.1f]  pz/MeV=[%.1f,%.1f] & phi=[%.2f %.2f] rad \n",myPID, pt1,pt2, pz1,pz2,phi1,phi2);
}



//=====================================
DLmadEvent*  DaLiGen_oneTrack::NextEvent() {
  madEvent->clear();
  madEvent->id=eventCntr++;
  madEvent->weight=1;

  int special=0; // if enabled it discretizes pt & phi vs. event ID
  //use:  ./daliSimG4 --physGen oneTrack  --physParam  ele_pt_30_30_pz_-30_30 --vertexGen xyz_0.0_0.0_0.0  -n 34


  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();

  int par_nPartPerEve=1;
  // printf(" DaLiGen_oneTrack::NextEvent(), eveid=%d  special=%d, np=%d\n",eventCntr,special,par_nPartPerEve);

  for(int np=0; np< par_nPartPerEve; np++ ) { // decide on # of particles form the same vertex
    
    double phi = phi1+ (phi2-phi1)*gRandom->Uniform();
    double pt = pt1 + (pt2-pt1) * gRandom->Uniform();
    double pz = pz1 + (pz2-pz1) * gRandom->Uniform();
    if(special) {
      int j=eventCntr/6;// quantize
      phi= (j*50.)*deg;
      pt=(22 + 5*j)*MeV;  //default
      // pt=(10 + 5*j)*MeV;  // for B-filed study
      pz=5 *MeV;
      printf("eveid=%d j=%d phi=%f pt=%f pz=%f \n",eventCntr,j,phi,pt,pz);
    }

    double me=particleTable->FindParticle(myPID)->GetPDGMass(); //MeV, default gamma mass
    // printf("pid=%d M/MeV=%f  \n", myPID,me/MeV );	  

    double energy=sqrt(pt*pt+pz*pz +me*me);
    madEvent->addOutParticleMeV(myPID,pt*cos(phi),pt*sin(phi),pz,energy);
  }

  return madEvent; 
}
