//*+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
//
// Executed one per event
//
//*--  Author: Jan Balewski, MIT, December 2012
//
// $Id: DaLiEventAction.cc,v 1.3 2012/02/01 20:21:34 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****


#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4ios.hh"

#include "DaLiEventAction.h"

//Jb , needed to save hits/tracks in to a file
#include "TTree.h"
#include "DLg4Event.h"

DaLiEventAction::DaLiEventAction(){
  // printf("DaLiEventAction() constructed\n");
  SetDebug(0);
}


//=======================================
//=======================================
//=======================================
void DaLiEventAction::BeginOfEventAction(const G4Event* evt){
  //printf("mm2 # of vert=%d \n", evt->GetNumberOfPrimaryVertex () );
  assert( treeDL);
  fullTrackInfo.clear();

  G4int event_id = evt->GetEventID();
  G4int nVert=evt->GetNumberOfPrimaryVertex();
  if(event_id%10000==0||myDebug ) printf("\n\n******\nBBB BeginOfDaLiEventAction INPUT eveID=%d has %d primVert\n",event_id,nVert);
  if(myDebug==0) return;

  //..... below is just printing of input event

   for(G4int iv=0;iv<nVert;iv++) {
    G4PrimaryVertex* vert=evt->GetPrimaryVertex(iv); assert(vert);
    G4int nPart=vert->	GetNumberOfParticle ();
    printf("  vertId=%d x,y,z=%.2f %2f %.2f  has %d particles\n",iv, vert->GetX0(), vert->GetY0(), vert->GetZ0(), nPart);
    for(G4int it=0;it<nPart;it++) {
      G4PrimaryParticle * track=  vert->GetPrimary(it); assert(vert);
      G4ThreeVector  P=	track->GetMomentum ()/MeV;
      G4ParticleDefinition * partCode=	track->GetG4code();
	printf("    itr=%d  Pxyz/MeV=%.2f,%.2f,%.2f name=%s\n",it,P.x(),P.y(),P.z(), partCode->GetParticleName().data());
    }
  }
  // assert(13==1);
}

//=======================================
//=======================================
//=======================================
void DaLiEventAction::EndOfEventAction(const G4Event* ){
  // printf("EEEE EndOfDaLiEventAction eveID=%d, fill DL-tree\n",  eventDLg4->GetEventID());
  treeDL->Fill();

  int goodEvent = 0;
  for(int i = 0; i < eventDLg4->GetNumTracks(); i++){
    // printf("Num tracks: %d\n",eventDLg4->GetNumTracks());
    if(eventDLg4->GetTrack(i)->GetNumHits()>0){
      goodEvent ++;
    // printf("Num hits: %d\n",eventDLg4->GetTrack(i)->GetNumHits());
    }
  }

//UNCOMMENT THIS TO SAVE TRACK DATA TO A TEXT FILE
// if(goodEvent>0){
//   for (std::vector<trackInfo>::iterator it = fullTrackInfo.begin(); it != fullTrackInfo.end(); ++it) {
//       fprintf(trkFile,"%d %d %d %g %g %g \n",it->eid,it->pid,it->trkid,it->stepx,it->stepy,it->stepz);
//       // printf("%d %d %d %g %g %g \n",it->eid,it->pid,it->trkid,it->stepx,it->stepy,it->stepz);
//
//   }
// }


  fullTrackInfo.clear();

}



// $Log: DaLiEventAction.cc,v $
