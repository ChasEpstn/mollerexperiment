// $Id$
//
/// \file DaLiGen_readMadEve.cc
/// \brief Implementation of the DaLiGen_readMadEve class
// - Modified Jan Balewski, MIT, January 2012

#include "DaLiGen_readMadEve.h"

#include <TChain.h>
#include "DLmadEvent.h"

#include "G4RunManager.hh"// to abort events

//=======================================
DaLiGen_readMadEve::DaLiGen_readMadEve(TString filename) {
  printf("in constr DaLiGen_readMadEve , inp=%s\n",filename.Data());
  inTreeChain=0;
  SetInputTree(filename);
}

//=====================================
void DaLiGen_readMadEve::SetInputTree(TString filename) {
  TString fname = filename;
  printf("try open input tree=%s=\n",filename.Data());
  inTreeChain= new TChain("DLmadEventTree");
  inTreeChain->Add(filename+".DLmad.root"); 
  madEvent=0; // if not then set branch will not work
  inTreeChain->SetBranchAddress("DLmad",&madEvent);
  int nEve = (int)inTreeChain->GetEntries();
  printf("Opened DLmadEvent file(s)=%s nEve=%d \n",filename.Data(),nEve ); 
  assert(nEve>0);
  // inTreeChain->Print();
  //tmp
  //  inTreeChain->GetEntry(1);  madEvent->print();assert(13==45);
}



//=====================================
DLmadEvent*  DaLiGen_readMadEve::NextEvent() {
  
  if(inTreeChain->GetEntries()<=eventCntr) {
    printf("::GeneratePrimaries_MadGraph: WARN exhausted input event tree, not found event %d, ending geant4 gracefully\n",eventCntr);
    G4RunManager * runManager =G4RunManager::GetRunManager();
    runManager->AbortRun();
    return 0;
  }
 
  // read input event
  inTreeChain->GetEntry(eventCntr);
  eventCntr++;
  return madEvent; 
}
