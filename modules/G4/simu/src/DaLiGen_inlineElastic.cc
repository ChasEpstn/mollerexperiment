// $Id$
//
/// \file DaLiGen_inlineElastic.cc
/// \brief  implementation of elastic scattering generator
// - Modified Jan Balewski, MIT, October 2013
// - Modified Charles S Epstein , MIT, October 2013

#include <assert.h>

#include "DaLiGen_inlineElastic.h"
#include "DLmadEvent.h"

#include <TObjArray.h>
#include <TH1.h>
#include <TRandom.h>
#include <TObjString.h>
#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>
#include <iostream>
using namespace std;

//=======================================
// many util functions 

////Scattered Moller Total Energy (exact)
double DaLiGen_inlineElastic::Ep(double x){
	return (EkinBeam*me + pow(me,2) + EkinBeam*me*pow(cos(x),2) - pow(me,2)*pow(cos(x),2))/
	(EkinBeam + me - EkinBeam*pow(cos(x),2) + me*pow(cos(x),2));}

//Scattered momentum
double DaLiGen_inlineElastic::pf(double x) {
	return sqrt(pow(Ep(x),2)-pow(me,2));}

//Mandelstam T ("t" was unavailable)
double DaLiGen_inlineElastic::tv(double x) {
	double p = sqrt(pow(EkinBeam,2)-pow(me,2)); //Beam momentum
	return (2.0*me*me-2.0*(EkinBeam*Ep(x)-p*pf(x)*cos(x)));}


//Mandelstam U
double DaLiGen_inlineElastic::u(double x) {
	double sv = 2.0*me*me+2.0*me*EkinBeam; //Mandelstam S ("s" was unavailable)
	return 4.0*pow(me,2) - sv - tv(x);}

//Squared Matrix element - does not take m->0 limit
double DaLiGen_inlineElastic::M2(double x){
	double sv = 2.0*me*me+2.0*me*EkinBeam; //Mandelstam S ("s" was unavailable)
	return 64.0*pow(pi,2)*pow(1.0/137.0,2)*(pow(me,4)/pow(tv(x),2)*((pow(sv,2)+pow(u(x),2))/(2*pow(me,4))+4*tv(x)/pow(me,2)-4.0)+
						pow(me,4)/pow(u(x),2)*((pow(sv,2)+pow(tv(x),2))/(2.0*pow(me,4))+4.0*u(x)/pow(me,2)-4)+
						pow(me,4)/(u(x)*tv(x))*(sv/pow(me,2)-2.0)*(sv/pow(me,2)-6.0));}

//Moller Cross section, including factor of 2*pi*sin(x) to make it dsigma/dtheta, and 2x for inclusiveness
double DaLiGen_inlineElastic::moller_CS(double x) {
  //printf("I'm moller_CS \n");
  double p = sqrt(pow(EkinBeam,2)-pow(me,2)); //Beam momentum
  return 2.0*2.0*pi*sin(x)*pow(1.97e-11,2)*pow(8.0*pi,-2)*
    (0.5)*M2(x)*pow(pf(x),2)/(me*p*((EkinBeam+me)*pf(x)-p*Ep(x)*cos(x)));
} 

//Elastic Scatttered electron energy
double DaLiGen_inlineElastic::TpE(double x) {
	return EkinBeam*mp/(mp+EkinBeam*(1.-cos(x)));}
//Elastic Q^2
double DaLiGen_inlineElastic::Q2(double x) {
	return 4.*EkinBeam*TpE(x)*pow(sin(x/2.),2);}
//Elastic GE
double DaLiGen_inlineElastic::GE(double x) {
	return pow(1.+Q2(x)/710000.,-2);}
//Elastic GM
double DaLiGen_inlineElastic::GM(double x) {
	return 1.79*pow(1.+Q2(x)/710000.,-2);}
//Elastic tau
double DaLiGen_inlineElastic::tau(double x) {
	return Q2(x)/(4.*pow(mp,2));}
//Elastic Cross-Section, including factor of 2*pi*sin(x) to make it dsigma/dtheta
double DaLiGen_inlineElastic::epElastic_CS(double x) {
  //printf("I'm epElastic_CS \n");
  return 2.*pi*sin(x)*4.0*pow(1.97e-11,2)*(1./137.)*(1./137.)*pow(TpE(x),2)*pow(cos(x/2.),2)/pow(Q2(x),2)*TpE(x)/100.*((pow(GE(x),2)+tau(x)*pow(GM(x),2))/(1.+tau(x))+2.*tau(x)*pow(GM(x),2)*pow(tan(x/2.),2));}

//======== end of util fumnctions


//===================================
//===================================
//===================================
void DaLiGen_inlineElastic::InitGenerator( double (DaLiGen_inlineElastic::*csFunc)(double theta)){
  int nb=xSecHist->GetNbinsX();
  for (int i=0;i<nb;i++){
    double x=xSecHist->GetBinCenter(i+1);
    double y = (this->*csFunc)(x);
    // y=1;// activate it to see uniformly popuylated ellipse
    // execute command: ./daliSimG4 --physGen Moller  --physParam  elth_0.005_0.9 -n 10
    xSecHist->SetBinContent(i+1,y);    
  }
  totXsect = xSecHist->Integral("width") * (cm2); // this assures the reuslt is in G4-x-section units.
  
}


//===================================
//===================================
//===================================
DaLiGen_inlineElastic::DaLiGen_inlineElastic(TString parList, TString hName) {
	printf("in constr DaLiGen_inlineElastic , parList=%s\n",parList.Data());
	procName = hName;

	madEvent=new DLmadEvent;
	theta1=theta2=0;  
	me=0.510998928;//MeV
	mp=938.0; //MeV
	phi1=0; phi2=TMath::TwoPi();
	EkinBeam = 100.0; //MeV
	int xSecHistnBins = 10000;
	//......... decoding parameters of generator .....
	
	TObjArray * objA=parList.Tokenize("_");
	//printf("size =%d \n",objA->GetEntries());
	TIter*  iter = new TIter(objA);  
	TObject* obj = 0;
	int k=0;
	while( (obj=iter->Next())) {    
	  k++;
	  // obj->Print();
	  TString ss= (( TObjString *)obj)->GetString();
	  // printf("k=%d, item=%s=\n",k,ss.Data()); 
	  switch (k) {
	  case 1: assert(ss=="elth"); break;			
	  case 2: theta1=atof(ss.Data()); break;
	  case 3: theta2=atof(ss.Data()); break;
	  case 4: assert(ss=="phi"); break;			
	  case 5: phi1=atof(ss.Data()); break;
	  case 6: phi2=atof(ss.Data()); break;
	  default:
	    assert(2345==33987); // unexpected
	  }
	}// end of while
	
	assert(theta1>0);
	assert(theta2>theta1);
	assert(phi2>phi1);
	xSecHist = new TH1D("CSeval","Cross Section",xSecHistnBins,theta1,theta2);	

	if(procName=="Moller") { 
	  InitGenerator( &DaLiGen_inlineElastic::moller_CS); 
	}  else if(procName=="epElastic") { 
	  InitGenerator(&DaLiGen_inlineElastic::epElastic_CS);
	}

	//.......  final computation ......
	double phiFact=(phi2-phi1)/TMath::TwoPi();
	double xSect=totXsect* phiFact;
	printf("%s xSect(cm2)=%.2e for theta=[%.4f %.4f] & phi=[%.2f %.2f] rad, ",procName.Data(),xSect/cm2, theta1,theta2,phi1,phi2);
	cout<<"procName = "<<procName<<endl;
	cout<<"[xSect ]*2.5e36 1/cm2/sec --> rate = "<<xSect*2.5e36/cm2<<" [eve/sec]  @ DL nominal lumi-2"<<endl;
	// restored 'old' lumi
	//cout<<"[totXsect]*6e35 1/cm2/sec --> "<<GetIntegratedLumi(6e35)<<" [eve/sec]  @ DL nominal lumi-1"<<endl;
	
}

/*
in constr DaLiGen_inlineElastic , parList=elth_0.02_0.9
epElastic totXsect(cm2)=6.49e-26 for theta=[0.0200 0.9] rad, procName = epElastic
[totXsect ]*2.5e36 1/cm2/sec --> 1.62146e+11 [eve/sec]  @ DL nominal lumi-2
*/

//=====================================
DLmadEvent*  DaLiGen_inlineElastic::NextEvent() {
	madEvent->clear();
	madEvent->id=eventCntr++;
	madEvent->weight=totXsect/picobarn;

	double phi = phi1+ (phi2-phi1)*gRandom->Uniform();
	double theta=xSecHist->GetRandom();
	
	//printf(" eve=%d theta/rad=%.7g phi/rad=%.6g  phi1=%.6g Dphi=%.6g rnd=%.4f\n",eventCntr,theta,phi,phi1, (phi2-phi1),0.); //gRandom->Uniform());	
	
	if (procName=="Moller"){//Moller 
		double Eerg = Ep(theta); 
		double p = sqrt(pow(Eerg,2.)-me*me);
		double pt=p*sin(theta);
		double pz=p*cos(theta);
		
		/*  Geant HEP convention,     http://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf
		 11=e-, -11=e+,  gam=22, 2212=p   */
		
		int myPID=11; // electron
		madEvent->addOutParticleMeV(myPID,pt*cos(phi),pt*sin(phi),pz,Eerg);		
	}
	
	if (procName=="epElastic"){//Elastic
		double Eerg = EkinBeam*mp/(mp+EkinBeam*(1.-cos(theta))); 
		double p = sqrt(pow(Eerg+me,2.)-me*me);
		double pt=p*sin(theta);
		double pz=p*cos(theta);
		double pbeam = sqrt(pow(EkinBeam+me,2.)-me*me);
	
		int myPID=11; // electron
		madEvent->addOutParticleMeV(myPID,pt*cos(phi),pt*sin(phi),pz,Eerg+me);
		
		myPID=2212;//proton
		madEvent->addOutParticleMeV(myPID,-pt*cos(phi),-pt*sin(phi),pbeam-pz,EkinBeam-Eerg+mp);
	}
		
	return madEvent; 
}


