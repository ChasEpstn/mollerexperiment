// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
// Physics routine.
//
//  - D.K. Hasell 1/1/10
//  - Jan Balewski, March 2013
//  - Jan Balewski, added Aprime particle , August 2014
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

// Include the Physics header file.

#include "DaLiPhysics.h"

// Include the GEANT4 header files used in this file.

#include "G4VUserPhysicsList.hh"
#include "G4ProcessManager.hh"
#include "G4ParticleTypes.hh"

#include "G4Aprime.hh"
#include "G4Lambda.hh"

#include "G4ComptonScattering.hh"
#include "G4GammaConversion.hh"
#include "G4PhotoElectricEffect.hh"

#include "G4eMultipleScattering.hh"
#include "G4MuMultipleScattering.hh"
#include "G4hMultipleScattering.hh"

#include "G4eIonisation.hh"
#include "G4eBremsstrahlung.hh"
#include "G4eplusAnnihilation.hh"

#include "G4MuIonisation.hh"
#include "G4MuBremsstrahlung.hh"
#include "G4MuPairProduction.hh"

#include "G4hIonisation.hh"

#include "G4StepLimiter.hh"
#include "G4UserSpecialCuts.hh"


DaLiPhysics::DaLiPhysics():  G4VUserPhysicsList() {
   defaultCutValue = 1.0 * mm;
   SetAprime(30*MeV, 10*mm); // mass , c*tau
   SetVerboseLevel(0);
}

// Member routine to construct the particles that can be used and/or produced.

void DaLiPhysics::ConstructParticle() {
  printf("\nJan: DaLiPhysics::ConstructParticle(), verbose=%d\n",verboseLevel );
  ConstructBosons();
  ConstructLeptons();
  ConstructMesons();
  ConstructBaryons();
}

// Bosons.

void DaLiPhysics::ConstructBosons() {

   // Pseudo-particles.

   G4Geantino::GeantinoDefinition();
   G4ChargedGeantino::ChargedGeantinoDefinition();

   // Photon.

   G4Gamma::GammaDefinition();

   // Heavy Photon
   G4Aprime::AprimeDefinition( aprim_mass, aprim_ctau); // mass,  proper decay time

}

// Leptons.

void DaLiPhysics::ConstructLeptons() {

   // e+/-
   G4Electron::ElectronDefinition();
   G4Positron::PositronDefinition();

   // mu+/-
   G4MuonPlus::MuonPlusDefinition();
   G4MuonMinus::MuonMinusDefinition();

   // nu_e
   G4NeutrinoE::NeutrinoEDefinition();
   G4AntiNeutrinoE::AntiNeutrinoEDefinition();

   // nu_mu
   G4NeutrinoMu::NeutrinoMuDefinition();
   G4AntiNeutrinoMu::AntiNeutrinoMuDefinition();
}

// Mesons.

void DaLiPhysics::ConstructMesons() {

   G4PionPlus::PionPlusDefinition();
   G4PionMinus::PionMinusDefinition();
   G4PionZero::PionZeroDefinition();

   G4Eta::EtaDefinition();
   G4EtaPrime::EtaPrimeDefinition();

   G4KaonPlus::KaonPlusDefinition();
   G4KaonMinus::KaonMinusDefinition();
   G4KaonZero::KaonZeroDefinition();
   G4AntiKaonZero::AntiKaonZeroDefinition();
   G4KaonZeroLong::KaonZeroLongDefinition();
   G4KaonZeroShort::KaonZeroShortDefinition();
}

// Baryons.

void DaLiPhysics::ConstructBaryons() {

   G4Proton::ProtonDefinition();
   G4AntiProton::AntiProtonDefinition();

   G4Neutron::NeutronDefinition();
   G4AntiNeutron::AntiNeutronDefinition();

   G4Lambda::LambdaDefinition();
}

// Define the construction processes.

void DaLiPhysics::ConstructProcess() {
  printf("\nJan: DaLiPhysics::ConstructProcess(), verbose=%d\n",verboseLevel );
   AddTransportation();
   ConstructEM();
   ConstructGeneral();
}

// Electromagnetic interactions.

void DaLiPhysics::ConstructEM() {

   theParticleIterator->reset();

   while( (*theParticleIterator)() ){

      G4ParticleDefinition * particle = theParticleIterator->value();
      G4ProcessManager * pmanager = particle->GetProcessManager();
      G4String particleName = particle->GetParticleName();
     
      if ( particleName == "gamma" ) {
	
	pmanager->AddDiscreteProcess( new G4PhotoElectricEffect );
	pmanager->AddDiscreteProcess( new G4ComptonScattering );
	pmanager->AddDiscreteProcess( new G4GammaConversion );	

      } 
      else if ( particleName == "aprime" ||  particleName == "lambda" ) {
	double mX= particle->GetPDGMass();
	double wX= particle->GetPDGWidth();
	double lenX= particle->GetPDGLifeTime ();
	printf("\nJan: DaLiPhysics::ConstructEM %s is defined: m/MeV=%.1f width/keV=%.1f tau=%.2e(ns)=%.2e(mm)\n",particleName.data(),mX/MeV, wX/keV, lenX/ns,lenX*3e11*mm/s); 

      }  
      else if ( particleName == "e-" ) { 
	printf("\nJan: DaLiPhysics::ConstructEM e-\n");
	pmanager->AddProcess( new G4eMultipleScattering, -1, 1, 1 );
	pmanager->AddProcess( new G4eIonisation,         -1, 2, 2 );
	pmanager->AddProcess( new G4eBremsstrahlung,     -1, 3, 3 );      	

      }
      else if ( particleName == "e+" ) {
	
	pmanager->AddProcess( new G4eMultipleScattering, -1,  1, 1 );
	pmanager->AddProcess( new G4eIonisation,         -1,  2, 2 );
	pmanager->AddProcess( new G4eBremsstrahlung,     -1,  3, 3 );
	pmanager->AddProcess( new G4eplusAnnihilation,    0, -1, 4 );

      }
      else if( particleName == "mu+" || 
               particleName == "mu-"    ) {

         pmanager->AddProcess( new G4MuMultipleScattering, -1, 1, 1 );
         pmanager->AddProcess( new G4MuIonisation,        -1, 2, 2 );
         pmanager->AddProcess( new G4MuBremsstrahlung,    -1, 3, 3 );
         pmanager->AddProcess( new G4MuPairProduction,    -1, 4, 4 );       
     
      }
      else if( !particle->IsShortLived() &&
               particle->GetPDGCharge() != 0.0 && 
               particle->GetParticleName() != "chargedgeantino" ) {

         pmanager->AddProcess( new G4hMultipleScattering, -1,  1, 1 );
         pmanager->AddProcess( new G4hIonisation,         -1,  2, 2 );

         pmanager->AddProcess( new G4StepLimiter,         -1, -1, 3 );         
         //pmanager->AddProcess( new G4UserSpecialCuts,     -1, -1, 4);  
      }
   }
}

#include "G4Decay.hh"

void DaLiPhysics::ConstructGeneral() {

   // Add Decay Process

   G4Decay* theDecayProcess = new G4Decay();

   theParticleIterator->reset();

   while( (*theParticleIterator)() ){
      G4ParticleDefinition* particle = theParticleIterator->value();
      G4ProcessManager* pmanager = particle->GetProcessManager();

      if ( theDecayProcess->IsApplicable(*particle) ) { 

         pmanager ->AddProcess( theDecayProcess );
         // set ordering for PostStepDoIt and AtRestDoIt
         pmanager ->SetProcessOrdering( theDecayProcess, idxPostStep );
         pmanager ->SetProcessOrdering( theDecayProcess, idxAtRest );
      }
   }
}

void DaLiPhysics::SetCuts() {

  //G4VUserPhysicsList::SetCutsWithDefault method sets 
  //the default cut value for all particle types 
  
  SetCutsWithDefault();

  if (verboseLevel >0){
    G4cout << "PhysicsList JJ::SetCuts:";
    G4cout << "CutLength : " << defaultCutValue/mm << " (mm)" << G4endl;
  }
     
  // set cut values for gamma at first and for e- second and next for e+,
  // because some processes for e+/e- need cut values for gamma 
  SetCutValue(defaultCutValue, "gamma");
  SetCutValue(defaultCutValue, "e-");
  SetCutValue(defaultCutValue, "e+");
     
  SetCutValue(defaultCutValue, "mu-");
  SetCutValue(defaultCutValue, "mu+");
     
   if ( verboseLevel>1 ) DumpCutValuesTable();
}
