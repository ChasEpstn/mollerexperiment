//
// ------------------------------------------------------------
// GEANT 4 'normal G4' - it  does full G4 particles propagation &
// in grahic mode displays tracks+hits
// ------------------------------------------------------------
//
// History:
// - Modified Jan Balewski, MIT, January 2012
// - added argument-interpreter, October 2013
// - Ross added MRI geometry as alternative, August 2014
//
#include <TRandom3.h>
#include "DaLiPhysics.h"
#include "DaLiDetectorConstruction.h"
#include "DaLiGeneratorAction.h"

#include "DaLiTrackingAction.h"
#include "DaLiEventAction.h"
#include "SteppingAction.h" // to monitor particles entering detectors

#include <G4RunManager.hh>
#include <G4UImanager.hh>
#include <G4VisExecutive.hh>

//jb  for GUI-QT
#include <G4UIQt.hh>
#include "Randomize.hh"
#include <QMainWindow>
//Jb saving TTree
#include <TFile.h>
#include <TTree.h>
#include "DLUnits.h"
#include "DLg4Event.h"
#include "DLmadEvent.h"
#include <TRandom.h> // to seet random seed

//---------------------------------------------------------------------
//---------------------------------------------------------------------
// Run-time arguments interpreter declarations
// Descripion: a swiss-army-knife for parsing run time arguments
// Author: Jan Balewski  (MIT 2013)
//---------------------------------------------------------------------
//---------------------------------------------------------------------
#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <assert.h>

void argUsage(char  *argv0 , const char *message=NULL);
void argUsageLong(const char *message=NULL);
void setRandomSeed(char *cSeed);


//---------------------------------------------------------------------
//---------------------------------------------------------------------
//---------------------------------------------------------------------

int main(int argc,char** argv){
  char  *userName    = getenv("USER");

  // switches, can be changed at runtime
  int    numEve=3;
  char  *outPath  = (char *)".";
  char  *coreName = (char *)"jan-job1";
  char  *geoName  = (char *)"geomMRI_verD";

  char  *physGen  = (char *)"oneTrack";
  char  *physParam  = (char *)"ele_pt_50_50_pz_-30_30";
  //2 char  *physGen  = (char *)"epElastic";
  //2 char  *physParam  = (char *)"elth_0.005_0.9";

  char  *setSeed  = (char *)"1230"; // common, for all random generators, 0=random
  char  *magField  = NULL;
  asprintf(&magField,"%s/.darklight/shared/externalGeom/2013-10-25d-CDS.dat",getenv("HOME"));// crude steps grid


  //char  *magField  = (char *)"zero";
  //1  char  *vertexGen = (char *)"xyz_0_0_0";
  char  *vertexGen = (char *)"ztrap_2_76_2_50"; // for geom12
  char  *beamWidth = (char *)"delta"; //full, eclipse, delta
  int    debugMode  = false;
  int    batchMode  = false;
  char  *baseDir =NULL;
  asprintf(&baseDir,"%s/.darklight/shared/externalGeom/",getenv("HOME"));

  //============================================================
  {//========================== START argv parsing ============== for 100+ lines
      // working variables
  int    optInd   = 0;
  char   optChar  = 0;

  static struct option optLong[] = {
    { "debug"      , 0 , &debugMode   , true},
    { "batch"      , 0 , &batchMode   , true},
    { "help"       , 0 , 0    , 'h' },
    { "longHelp"   , 0 , 0    , 'H' },
    { "numEve"     , 1 , 0    , 'n' }, // has one param & is equivalent to -n
    { "coreName"   , 1 , 0    , 'c' },
    { "geoName"    , 1 , 0    , 'g' },
    { "setSeed"    , 1 , 0    , 's' },
    { "magField"   , 1 , 0    , 'm' },
    { "outPath"    , 1 , 0    , 'o' },
    { "baseDir"    , 1 , 0    , 'D' },
    { "physGen"    , 1 , 0    , 'G' },
    { "physParam"  , 1 , 0    , 'P' },
    { "vertexGen"  , 1 , 0    , 'V' },
    { "beamWidth"  , 1 , 0    , 'B' },
    { 0, 0, 0, 0}
  };


  // arguments and init
  char  * argv0 = argv[0];

  opterr = 0;
  while((optChar = getopt_long(argc,argv,"hdn:c:g:bV:P:G:o:Hm:s:B:D:",optLong,&optInd)) != EOF) {
    switch(optChar) {
    case  0  :   break;
    case 'c' : coreName = optarg;    break;
    case 'g' : geoName  = optarg;    break;
    case 'o' : outPath  = optarg;    break;
    case 'G' : physGen  = optarg;    break;
    case 'P' : physParam  = optarg;  break;
    case 'V' : vertexGen  = optarg;  break;
    case 'B' : beamWidth  = optarg;  break;
    case 'm' : magField  = optarg;   break;
    case 'D' : baseDir  = optarg;   break;
    case 's' : setSeed  = optarg;   break;
    case 'd' : debugMode= true;      break;
    case 'b' : batchMode= true;      break;
    case 'n' : numEve      = atoi(optarg);     break;
    case 'h' : argUsage(argv0); return(0);     break;
    case 'H' : argUsage(argv0);  argUsageLong(argv0);  return(0);       break;
    case '?':
      if (isprint (optopt))
	fprintf (stderr, "\nUnknown option `-%c'\n\n", optopt);
      else
	fprintf (stderr,"\nUnknown option character `\\x%x'.\n\n",optopt);
    default  : argUsage(argv0,"unknown option");    break;
    };
  }

 /* Print any remaining command line arguments (not options).   */
  if (optind < argc)    {
      printf ("\n WARN, non-options ARGV-elements: ");
      while (optind < argc)
	printf ("%s ", argv[optind++]);
      putchar ('\n');
      return -3;
  }

  printf("\n**** Final paramater choice made by user=%s  *** \n",userName);
  printf("Executing  %s  nEve=%d  coreName='%s' geoName='%s'  baseDir='%s'\n",argv0,numEve,coreName,geoName,baseDir);
  printf("physics: gen=%s  param=%s vertex=%s  beamWidth=%s setSeed=%s=\n",physGen, physParam,vertexGen, beamWidth,setSeed);

  printf("magnetif filed =%s \n",magField);
  printf("outPath=%s  modes: batch=%d   debug=%d\n\n", outPath,batchMode,debugMode);


  }//========================== END argv parsing ==============
  //============================================================

  printf("main %s  START... \n",coreName);

  // output geant4-event container
  DLg4Event *eventDLg4= new DLg4Event();
  DLg4Track *trackDLg4= new DLg4Track();
  setRandomSeed(setSeed);

  G4RunManager * runManager = new G4RunManager;

  // Set mandatory initialization classes
  {// select geometry
    TString bpath=baseDir;
    DaLiDetectorConstruction *myDetBuilder=new DaLiDetectorConstruction(geoName,magField,bpath);
    runManager->SetUserInitialization(myDetBuilder);
    myDetBuilder-> eventDLg4 = eventDLg4;
    myDetBuilder-> trackDLg4 = trackDLg4;
    if(debugMode) myDetBuilder-> SetDebug(1);
  }


  // Physics list
  //  G4VUserPhysicsList *myPhysList=new DaLiPhysics;
  DaLiPhysics *myPhysList=new DaLiPhysics;
  myPhysList->SetAprime(30*MeV, 0*mm); //:removed mass & c*tau=80
  if(debugMode) myPhysList->SetVerboseLevel(1);
  runManager->SetUserInitialization(myPhysList);


  // my Primary generator action
  DaLiGeneratorAction * myGenAction=new DaLiGeneratorAction(geoName);
  myGenAction->SetPhysicsGenerator(physGen, physParam);
  myGenAction->SetVertexGenerator(vertexGen, beamWidth);
  myGenAction-> eventDLg4 = eventDLg4;
  runManager->SetUserAction(myGenAction);


  DaLiEventAction *myEveAction=new DaLiEventAction;
  myEveAction-> eventDLg4 = eventDLg4;
  if(debugMode) myEveAction-> SetDebug(1);
  runManager->SetUserAction(myEveAction);

#if 1 //j
  // trace lepton tracker  intruders
  SteppingAction *myStepAct=new SteppingAction(myEveAction);
  runManager->SetUserAction(myStepAct);
  myStepAct->initHisto();
#endif


  DaLiTrackingAction *myG4TrkAction=new DaLiTrackingAction;
  myG4TrkAction-> eventDLg4 =eventDLg4;
  myG4TrkAction-> trackDLg4 =trackDLg4;
  runManager->SetUserAction( myG4TrkAction);
  if(debugMode) myG4TrkAction-> SetDebug(1);

  // Initialize G4 kernel
  runManager->Initialize();

  // Initialize outgoing ttree, Note the tree & event variables are global
  TString outTreeFname = Form("%s/%s",outPath, coreName);
  TFile *fileTout = new TFile(outTreeFname+".DLg4.root", "RECREATE", "G4 DarkLight events");
  if (fileTout->IsZombie()){
    printf("Unable to open file \"%s\" for writing.  Aborting.\n",fileTout->GetName());
    exit(31);
  } else {
    printf("Open file \"%s\" for writing, OK.\n",fileTout->GetName());
 }

  myEveAction->treeDL= new TTree("DaLiEventTree","Combined Tree Mad+G4",99);
  myEveAction->treeDL->Branch("DLg4Event",&eventDLg4);
  myEveAction->treeDL->Branch("DLmadEvent",&( myGenAction-> eventDLmad));
  // TString trackFName = "_TrackData.txt";
  // myEveAction->trkFile = fopen(coreName+trackFName,"w");
  //if we were adding our own analysis and output tree types, we'd have, eg:
  //DLkarimakiTracks *kariEve=new ....;
  //outTree->Branch("DLkarimakiTracks",&kariEve);

  // Get the pointer to the User Interface manager
  G4UImanager* UImanager = G4UImanager::GetUIpointer();

  //  if(debugMode) UImanager->ApplyCommand("/process/list"); // print short list

  if (batchMode) {
    UImanager->ApplyCommand(Form("/run/beamOn %d",numEve));
    //    UImanager->ApplyCommand("/run/verbose 0");
  }  else { // graphic mode
   // Declare and intialise visualisation manager.
    G4VisManager * visManager = new G4VisExecutive;
    visManager->Initialize();

    // Define a session to run interactively.
    // from Jan Braurer, use Qt
    // G4UIsession* session =new G4UIQt(argc, argv);
    G4UIQt* session =new G4UIQt(argc, argv);

    session->GetMainWindow()->resize(1920,2160);
    session->GetMainWindow()->move(QPoint(1920,0));
    // Execute the visualisation initialisation macro.
    // UImanager->ApplyCommand( "/control/execute macros/vis.mac" );
    UImanager->ApplyCommand( Form("/control/execute %s/.darklight/shared/macros/vis.mac",getenv("HOME") ));

    UImanager->ApplyCommand(Form("/run/beamOn %d",numEve));
    session->SessionStart();
    delete session;// End interactive session.
  }


#if 0 // save histos
  // save energy-loss histos
  TString outElosFname = Form("%s/%s.elos",outPath, coreName);
  printf(" main: output histo saved %s\n",outElosFname.Data());
  myDetBuilder->saveHisto(outElosFname);
#endif

  TString outStepFname = coreName;
   myStepAct->saveHisto( Form("%s/%s.step",outPath, coreName));

#if 1 //save ttree
  printf(" main: output tree saved %s\n",fileTout->GetName());
  //treeDLg4->Print();
  fileTout->Write();
  fileTout->Close();

  printf("      for hit-dump exec:   root -l ../rdDLg4_dump.C\'(\"%s\",5)\'\n",outTreeFname.Data());
  printf("      for rate-ana exec:   root -l ../rdDLg4_evalRate.C\'(\"%s\")\'\n",outTreeFname.Data());

#endif


  printf(" main: finished\n");
  delete runManager;
  fclose(myEveAction->trkFile);

  return 0;
}


//----------------------------------------
void setRandomSeed(char *cSeed) {
  int iseed=atoi(cSeed);
  gRandom->SetSeed(iseed);
  if(iseed) printf("setRandomSeed FIXED to iseed=%d\n",iseed);
  else printf("setRandomSeed to VARY with each job \n");

  // if needed in the future, cost me some time to figure it out, Jan.pl
#if 0  //choose the RanecuEngine random engine
  CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine);
  //using TRandom for varied - not very smart.
  long mySeeds[2];
  mySeeds[0]=gRandom->Integer(2000000000);   //pick 2 seeds for random
  mySeeds[1]=gRandom->Integer(2000000000);
  CLHEP::HepRandom::setTheSeeds(mySeeds);
#endif

}


//----------------------------------------
void argUsage(char  *argv0 , const char *message){
  if (message) fprintf(stderr,"%s: %s\n",argv0,message);
  fprintf(stderr,"usage: %s  [OPTIONS]\n",argv0);
  fprintf(stderr," -o | --outPath <out>                    : full path for all outputs \n");
  fprintf(stderr," -G | --physGen <oneTrack>               : our generators: madGraph,epElastic,epRadiative,Moller\n");
  fprintf(stderr," -P | --physParam  <ele_pt_5_5_pz_-20_30>   : auxiliary params, use --longHelp for details  \n");
  fprintf(stderr," -V | --vertexGen  <ztrap_10_50_20_80>   : Z-trapezoid (cm) or fixed xyz_[x]_[y]_[z]\n");
  fprintf(stderr," -B | --beamWidth  <delta>          : transverse beam profile: delta,full,eclipse, use --longHelp for details \n");
  fprintf(stderr," -S | --setSeed <1230>                   : fixed seed, default: each job uses different sequence\n");
  fprintf(stderr," -g | --geoName <geom?>    : geom12_verA for phase 2, geomMRI_verC for QED optimized phase 1, \nor geomMRI_verD for ep scattering optimized phase 1, use --longHelp for details  \n");
  fprintf(stderr," -m | --magField  <2013-10-24-CDS.dat>    : model magnetic field [flat|step]_dump\n");
  fprintf(stderr," -c | --coreName <jan-job1>              : for all outputs from this job\n");
  fprintf(stderr," -n | --numEve <10>                      : number of thrown events \n");
  fprintf(stderr," -D | --baseDir '../'        : path to /externalGeom/ \n");
  fprintf(stderr," -b | --batch        : disable graphics (default: enabled)\n");
  fprintf(stderr," -d | --debug        : set debug mode on\n");
  fprintf(stderr," -h | --help         : this short help\n");
  fprintf(stderr," -H | --longHelp     : detailed explanation of switches\n");
  if(message) exit(-1);
  return;
}

//----------------------------------------
void argUsageLong(const char *message){
  fprintf(stderr,"\n -------------------------------\nDetailed explanation of switches %s\n",message);

  fprintf(stderr,"\n*)------ MadGraph input events (DLmad TTree) --------\n"
	  "enabled by switch  --physGen madGraph\n"
	  "Expected content of --physParam  [full path to TTree] \n"
	  "  you do not specif the '.DLmad.root'  TTree extension\n"
	  "  Warn:  MadGraph events are weighted - you must accumulate weights in histos\n"
	  " Example: to use A40 events set flags  \n"
	  "           ... --physGen  madGraph --physParam ~/goodEvents/madAm/ep_ep_A40_ee_jesseCuts  .... \n"
	  "\n");

  fprintf(stderr,"\n*)------ inline  generators --------\n"
	  "enabled by switch  --physGen Moller or  epElastic or epRadiative \n"
	  "Expected content of --physParam  elth_[ang1]_[ang2]_phi_[ang3]_[ang4] \n"
	  "     the abreviated form --physParam  elth_[ang1]_[ang2]  assumes 2pi isotropy\n"
	  "   the range of theta for the electron is [ang1, ang2], float values in radians\n"
	  "  Action:  throws both outgoing particles according to dSig/dOmega \n"
	  "           within theta range for the electron and uniform in phi\n"
	  " Example: to generate mollers for theta=[0.005,0.9] use flags  \n"
	  "           ...  --physGen Moller  --physParam elth_0.005_0.9  .... \n"
	  "\n");

    fprintf(stderr,"\n*)------ inline ep radiative generator --------\n"
	  "enabled by switch  --physGen  epRadiative \n"
	  "Expected content of --physParam  elth_[ang1]_[ang2]_thetadist_[dist] or \n"
	  "     the abreviated form --physParam  elth_[ang1]_[ang2]  assumes thetadist=0  \n"
	  "   the range of theta for the electron is [ang1, ang2], float values in radians\n"
	  "   the theta distribution as [dist], with 0=flat, 1=according to Rutherford\n"
	  "  Action:  throws both outgoing particles according to dSig/dOmega \n"
	  "           within theta range for the electron and uniform in phi\n"
	  " Example: to generate for theta=[0.005,0.9] with flat distribution use flags  \n"
	  "           ...  --physGen epRadiative  --physParam elth_0.005_0.9_thetadist_0  .... \n"
	  "\n");


  fprintf(stderr,"\n*)------ one particle generator --------\n"
	  "enabled by switch  --physGen oneTrack .\n"
	  "Expected content of --physParam [name]_pt_[pt1]_pz_[pz1]_phi_[ang3]_[ang4]  \n"
	  "   where [name] is the thrown particle: 'ele', 'pos', 'pro', 'gam', 'mum' \n"
	  "   whose positive [pt1,pt2] (MeV), any [pz1,pz2] (MeV)\n"
	  "   for the abreviated form --physParam [name]_pt_[pt1]_pz_[pz1]  assumes 2pi isotropy\n"
	  " Example: to generate positrons w/ PT=50 MeV  heading toward photon detector use flags  \n"
	  "           ... --physGen oneTrack   --physParam pos_pt_50_pz_2 ... \n"

	  "\n");


  fprintf(stderr,"\n*)------ vertex Z-distribution  --------\n"
	  "modified by the  switch  --vertexGen ztrap_[a]_[b]_[c]_[z0] \n"
	  "defines the z-distribution of the primary vertex as a trapezoid, Z in (cm), \nwith height being probability of an event at that point.\n"
	  "         _______________\n"
	  "        /|              |\\ \n"
	  "       / |              |  \\ \n"
	  "      /  |              |    \\ \n"
	  " ..../.a.|....b.........|..c...\\....==> +Z axis \n"
          "                               z0 \n"
	  "\n z0 fixes location of the right edge.\n"
	  " x=y=0 for ztrap\n \n"
	  " Alternatively the fixed vertex position in 3D is set by --vertexGen xyz_[x]_[y]_[z] \n"
	  "\n");


  fprintf(stderr,"\n*)------ beam transverse distribution  --------\n"
	  "modified by the  switch  --beamWidth [shape] \n"
	  "  where [shape] can be: 'delta'  - fixed position set by --vertexGen.  Idealized beam of zero width.  \n"
	  "   'full' - double gauss model proposed by Michel.  sigmas for each gaussian cannot be changed here.\n"
	  "   'eclipse' - same as 'full' but without 1mm radius hole in the center.  Like an eclipse.\n"
	  "\n");

  fprintf(stderr,"\n*)------ geometry components ON/OFF --------\n"
	  "modified by the  switch  --geoName  geom12_verX_on_[x]_[y]..._off_[z]... \n"
	  "where [x],[y],[z] are hardcoded identifires for certain geometry elements\n"
	  " for geom12_ those are : 1=aft straw, 2=forward straw+disc, \n 3=moller C-plug, 4=moller Fe-plug, 5=protDet, 6=leptReadout, 7=Be beamPipe,\n 8=world air or vacuum \n"
	  "The on option is a legacy code from a time when all geometry elements were not on by default.  \nThere is no reason to use it now.  Simply turn off any elements you do not want.  \nIt is mentioned in the longHelp to remind interested persons to someday get rid of it.\n"
	  " geom12 is the geometry for DL-phase2.\n"
	  " verA,B,C  selects 4 or 5 cylinders of lepton detector, see log file\n"
	  " geom12_verA is the likely geometry for phase 2.  It has 4 cylinders of lepton detector.\n"
	  " geom12_verB also has 4 cylinders of lepton detector, but they are closer to the target.\n"
	  " geom12_verC has 5 cylinders of lepton detector.\n"
	  " the default is :  geom12_verA \n"
	  " Example 1: to remove moller dump & aft straw flag\n"
	  "         ....  --geoName  geom12_verA_off_3  .... \n"
	  " For DL-phase1  geometry use 'MRI' instead of '12' \n"
	  " At the moment there are 2 variations for DL-phase1 \n"
	  " geomMRI_verC -optimized for QED event detection, with GEMs very close to the beginning of the target\n"
	  " geomMRI_verD -optimized for ep elastic scattering, with one lepton tracker pushed far forward\n"
	  " Example 2: to include the QED optimized phase 1 geometry with no proton detector\n"
	  "         ....  --geoName geomMRI_verC_off_5  ....  \n"
	  "Example 3:  to include the ep optimized phase 1 geometry with everything left in place\n"
	  "         ....  --geoName geomMRI_verD  ....  \n"
	  " Also the JLAB beam test geometry is kept, use instead  --geoName geomJlabDose_on_1\n"
	  "\n");

  fprintf(stderr,"\n*)------ magnetic filed  model --------\n"
	  "modified by the  switch  --magField [name][_dump] \n"
	  "where [name] = 3D grid of B-vector field, file name includes full path\n"
	  "         or  = 'zero' for 0.0001T  ~no field \n"
	  "         or  = 'flat' for 0.5T  flat field along Z\n"
	  "         or  = 'step' for 0.5T flat field at |Z|<60 cm, zero otherwise\n"
	  " if _dump string is appended to [name]  the PlotBfiled() is called and job quits\n"
	  "\n");

}
