//
// $Id$
//
/// \file DaLiGen_readMadEve.hh
/// \brief  implementation of MadGraph event reader
// - Modified Jan Balewski, MIT, October 2013

#ifndef DaLiGen_readMadEve_h
#define DaLiGen_readMadEve_h 1
#include <TString.h>
#include "DaLiGenEvent.h"

class DLmadEvent;
class TChain;

class DaLiGen_readMadEve : public DaLiGenEvent {
 public:
  DaLiGen_readMadEve(TString filename);    
  DLmadEvent* NextEvent();  
 private:
  void SetInputTree(TString filename);
  DLmadEvent* madEvent; 
  TChain    *inTreeChain; 
};

#endif


