//
// $Id$
//
/// \file DaLiGen_oneTrack.hh
/// \brief  implementation of elastic scattering generator
// - Modified Jan Balewski, MIT, October 2013
// - Modified Charles S Epstein , MIT, October 2013

#ifndef DaLiGen_inlineElastic_h
#define DaLiGen_inlineElastic_h 1
#include <TString.h>
#include "DaLiGenEvent.h"
class DLmadEvent;
class  TH1D;
class DaLiGen_inlineElastic;

class DaLiGen_inlineElastic : public DaLiGenEvent {
 public:
  DaLiGen_inlineElastic(TString parList, TString hName);
  DLmadEvent* NextEvent();  

 private:
  void InitGenerator(double (DaLiGen_inlineElastic::*csFunc)(double));
  TString  procName; 
  DLmadEvent* madEvent; 
  TH1D *xSecHist; //
  double totXsect; // G4-units
  double theta1, theta2; // rad
  double phi1,phi2; //rad
  double me,mp,EkinBeam; //MeV
  double Ep(double x);
  double pf(double x);
  double tv(double x);
  double u(double x);
  double M2(double x);
  double TpE(double x);
  double Q2(double x);
  double GE(double x);
  double GM(double x);
  double tau(double x);
  double moller_CS(double x);
  double epElastic_CS(double x);
};

#endif



