//
// $Id$
//
/// \file DaLiGen_epElastic.h

#ifndef DaLiGen_epRadiative_h
#define DaLiGen_epRadiative_h 1
#include <TString.h>
#include "DaLiGenEvent.h"
class DLmadEvent;
class  TH1D;
class DaLiGen_epRadiative;
class GeneratorEvent;
class GeneratorRadiative;

class DaLiGen_epRadiative : public DaLiGenEvent {
 public:
  DaLiGen_epRadiative(TString parList);
  ~DaLiGen_epRadiative();
  double GetIntegratedLumi(double nEve);
  DLmadEvent* NextEvent();  

 private:
  DLmadEvent* madEvent; 
  TString  procName; 
  double theta1, theta2; // rad
  GeneratorEvent *gevent;
  GeneratorRadiative *generator;
  double sumW,sumW2;
};

#endif



