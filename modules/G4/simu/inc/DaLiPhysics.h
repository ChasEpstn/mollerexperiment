// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
// Physics header file.
//
//  - D.K. Hasell 1/1/10
//  - Jan Balewski, March 2013
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#ifndef DaLiPhysics_H
#define DaLiPhysics_H 1

#include "G4VUserPhysicsList.hh"
using namespace CLHEP;
class DaLiPhysics: public G4VUserPhysicsList {

public:
     DaLiPhysics();
     ~DaLiPhysics(){};
     void SetAprime(double m, double x) {aprim_mass=m; aprim_ctau=x;}
protected:
     void ConstructParticle();
     void ConstructProcess();
 
     void SetCuts();
   
     // These methods Construct particles.
     void ConstructBosons();
     void ConstructLeptons();
     void ConstructMesons();
     void ConstructBaryons();

     // These methods Construct physics processes and register them.
     void ConstructGeneral();
     void ConstructEM();
 private:
     double aprim_mass,aprim_ctau;

};

#endif
