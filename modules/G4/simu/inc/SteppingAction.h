//
//
/// \file electromagnetic/TestEm4/include/SteppingAction.hh
/// \brief Definition of the SteppingAction class
//
//
// $Id$
//

#ifndef SteppingAction_h
#define SteppingAction_h 1

#include "TString.h"
#include "G4UserSteppingAction.hh"

class TH1;
class DaLiEventAction;

class SteppingAction : public G4UserSteppingAction {
 public:
  SteppingAction(DaLiEventAction*);
  ~SteppingAction(){ };
  
  virtual void UserSteppingAction(const G4Step*);
  void   initHisto();
  void   saveHisto(TString fname="italia");
  
 private:
  enum{ mxH=256,mxTrk=1000};
  TH1 *hA[mxH];
  int eveID;
  int trkID;
  int trackScore[mxTrk];
  
  DaLiEventAction* fEventAction;
};

#endif
