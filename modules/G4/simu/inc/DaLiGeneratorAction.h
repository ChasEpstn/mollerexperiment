//
// $Id$
//
/// \file DaLiGeneratorAction.hh
/// \brief Definition of the DaLiGeneratorAction class
// - Modified Jan Balewski, MIT, October 2013

#ifndef DaLiGeneratorAction_h
#define DaLiGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ParticleGun.hh"
#include "globals.hh"

class G4ParticleGun;
class G4Event;

class DaLiGenEvent;
class DaLiGenVertex; 
class DLg4Event;
class DLmadEvent;

class DaLiGeneratorAction : public G4VUserPrimaryGeneratorAction {
  public:
    DaLiGeneratorAction( char * geomX);    
    virtual ~DaLiGeneratorAction();

    // static access method
    static const DaLiGeneratorAction* Instance();

    virtual void GeneratePrimaries(G4Event*);         
    void SetPhysicsGenerator(G4String physGen, char *physParam);
    void SetVertexGenerator(char *allParamZ,char *allParamT);

    const G4ParticleGun* GetParticleGun() const { return fParticleGun; }  
    DLmadEvent *eventDLmad; // input event , contains physical particles
    DLg4Event *eventDLg4; // output event , contains hits & G4 tracks
    G4String  baseGeometryG4;

  private:
    static DaLiGeneratorAction* fgInstance; // why static?
    G4int  eveCntr;

    G4ParticleGun*  fParticleGun; // pointer a to G4 gun class
    DaLiGenEvent  *inputEventGen;    // source of events
    DaLiGenVertex *inputVertexGen; // source of vertices     
};


#endif


