// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
// Executed one per every track
//
//*--  Author: Jan Balewski, MIT, December 2012
//
// $Id: DaLiTrackingAction.h,v 1.2 2011/12/15 21:02:49 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#ifndef DaLiTrackingAction_H
#define DaLiTrackingAction_H

#include "G4UserTrackingAction.hh"
class DLg4Track;
class DLg4Event;

class DaLiTrackingAction : public G4UserTrackingAction {
  public:
    DaLiTrackingAction();
    ~DaLiTrackingAction();

    void PreUserTrackingAction(const G4Track* aTrack);
    void PostUserTrackingAction(const G4Track*);
    void PrintSummary();
    void SetDebug(int x) { myDebug=x ;}
    // tmp, make those below private
    DLg4Event *eventDLg4;
    DLg4Track *trackDLg4;

 private:
    int myDebug; 
};

#endif

// $Log: DaLiTrackingAction.h,v $
// Revision 1.2  2011/12/15 21:02:49  balewski
// ready to export hits in ttree
//
// Revision 1.1  2011/12/15 18:09:13  balewski
// start
//
    
