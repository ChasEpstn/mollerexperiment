//
// $Id$
//
/// \file DaLiGenVertex.hh
/// \brief  multi modal vertex generator
// - Modified Jan Balewski, MIT, October 2013

#ifndef DaLiGenVertex_h
#define DaLiGenVertex_h 1
#include "TString.h"
#include "G4ThreeVector.hh"
using namespace CLHEP;
class TF1;

class DaLiGenVertex  {
 public:
  DaLiGenVertex(TString parListZ,TString parListT);    
  G4ThreeVector  NextVertex();  
 private:
  TString modeNameZ;
  TF1 *fzGen; // distribution in z-direction, serves as a switch
  TF1 *ftGen; // distribution in transverse direction, serves also as  a switch
  G4double x0,y0,z0; // offset in x,y,z

  // utility function
  TF1 *SetZtrapezoid(double a, double b, double c, double z0);
  TF1* SetTransverseBeamProfile(double eclipseR, double sigmaBeam=50*um, double sigmaHalo=1*mm, double HoverB_meas=3e-6, double rHalo_meas=1*mm);
};

#endif


