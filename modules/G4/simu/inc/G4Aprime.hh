//
// $Id: G4Aprime.hh 67971 2013-03-13 10:13:24Z gcosmo $
//
// 
// ------------------------------------------------------------
//      GEANT 4 class header file
//
//      History: first implementation, based on object model of
//      4-th April 1996, G.Cosmo
// ****************************************************************
// 
//  Implemented by Jan Balewski, MIT, August 2014
//  Based on  impelemenatation of G4Gamma
//
// ----------------------------------------------------------------


#ifndef G4Aprime_h
#define G4Aprime_h 1

#include "globals.hh"
#include "G4ios.hh"
#include "G4ParticleDefinition.hh"

// ######################################################################
// ###                           GAMMA                                ###
// ######################################################################

class G4Aprime : public G4ParticleDefinition
{
 private:
   static G4Aprime* theInstance;

 private: // hide constructor as private
   G4Aprime(){}

 public:
   ~G4Aprime(){}

   static G4Aprime* Definition();
   static G4Aprime* AprimeDefinition(double massX,  double ctauX);
   static G4Aprime* Aprime();
};


#endif


