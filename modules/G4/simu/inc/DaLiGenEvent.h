//
// $Id$
//
/// \file DaLiGenEvent.hh
/// \brief base for implementation of physical event generators for DL
// - Modified Jan Balewski, MIT, October 2013

#ifndef DaLiGenEvent_h
#define DaLiGenEvent_h 1

class DLmadEvent;

class DaLiGenEvent  {
 public:
  DaLiGenEvent(){eventCntr=0; generatorName="fixGenName";};    
  virtual ~DaLiGenEvent(){};
  virtual DLmadEvent* NextEvent()=0;  
  TString generatorName;
 private:

 protected:
  int  eventCntr;
};

#endif


