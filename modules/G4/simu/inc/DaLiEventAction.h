// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
// Executed one per event
//
//*--  Author: Jan Balewski, MIT, December 2012
//
// $Id: DaLiEventAction.h,v 1.1 2011/12/15 18:09:12 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
 
#ifndef DaLiEventAction_h
#define DaLiEventAction_h 1

#include "G4UserEventAction.hh"
// #include "Riostream.h"
using namespace CLHEP;
class G4Event;
class DLg4Event;
class TTree ;


class DaLiEventAction : public G4UserEventAction {
  private:
   int myDebug;

  public:
    DaLiEventAction();
    ~DaLiEventAction(){};
    void BeginOfEventAction(const G4Event*);
    void EndOfEventAction(const G4Event*);
    void SetDebug(int x) { myDebug=x ;}
    // tmp, make those below private
    DLg4Event *eventDLg4;
    TTree     *treeDL;
    
    std::vector<int> pid;
    std::vector<int> trkid;
    std::vector<double> stepx;
    std::vector<double> stepy;
    std::vector<double> stepz;
    struct trackInfo{
          int eid;
          int pid;
          int trkid;
          double stepx;
          double stepy;
          double stepz;
    };
    trackInfo holdTrackInfo;
    std::vector<trackInfo> fullTrackInfo;
    FILE *trkFile;

    // int pid0;
    // int trkid0;
    // double stepx0;
    // double stepy0;
    // double stepz0;

};


#endif


// $Log: DaLiEventAction.h,v $
// Revision 1.1  2011/12/15 18:09:12  balewski
// start
//
    
