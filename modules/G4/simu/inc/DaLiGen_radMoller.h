
#ifndef DaLiGen_radMoller_h
#define DaLiGen_radMoller_h 1
#include <TString.h>
#include "RadMoller.h"
#include "DaLiGenEvent.h"
#include "TLorentzVector.h"
#include "TH1.h"
#include "TRandom3.h"
#include "TMath.h"
#include "RandT3.h"

class DLmadEvent;
class TH1D;
class DaLiGen_radMoller;

class DaLiGen_radMoller : public DaLiGenEvent {
 public:
  DaLiGen_radMoller(TString parList);
  DLmadEvent* NextEvent();  
private:
  double LabToCM(double);
  DLmadEvent* madEvent; 
  RadMoller_Gen *rMollerGen;
  RandT3 *randomGenerator; 
  TLorentzVector *p3;
  TLorentzVector *p4;
  TLorentzVector *kph;
  double theta1, theta2; // rad
  double phi1,phi2; //rad

};


#endif



