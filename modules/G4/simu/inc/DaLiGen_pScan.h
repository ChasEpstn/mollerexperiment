//
// $Id$
//
/// \file DaLiGen_oneTrack.hh
/// \brief  implementation of single track generator
// - Modified Jan Balewski, MIT, October 2013

#ifndef DaLiGen_pScan_h
#define DaLiGen_pScan_h 1
#include "TString.h"
#include "DaLiGenEvent.h"

class DLmadEvent;
class DaLiGen_pScan : public DaLiGenEvent {
 public:
  DaLiGen_pScan(TString parList);    
  DLmadEvent* NextEvent();  
 private:
  DLmadEvent* madEvent; 
  int myPID; // Geant HEP convention, 
  /* http://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf
     11=e-, -11=e+,  gam=22, 2212=p   */
  // double pt1, pt2, pz1,pz2; // MeV
  double momentum, theta, delt, pct;
  double phi1,phi2; //rad
};

#endif


