//! \file
//!
//! Source file for GridMagneticField class.
//!
//! This defines the GridMagneticField class and the member routines which
//! construct the OLYMPUS toroidal magnetic field.
//!
//! \author D.K. Hasell,  modified by Jan Balewski
//! \version 2.0
//! \date 2010-10-14, 2013-10-02
//!
//!
//! \ingroup detector

// Include the GridMagneticField and other user header files.

#include "GridMagneticField.h"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
using namespace CLHEP;

// Include the GEANT4 header files referenced here.

#include "globals.hh"
#include "G4ios.hh"
#include <assert.h>

// Include the C++ header files referenced here.

#include <fstream>
#include <iostream>
#include <stdio.h>
#include <TString.h>

// Constructor for GridMagneticField class.

GridMagneticField::GridMagneticField( const char * filename, G4double scale,int type)
  : G4MagneticField() {

  G4FieldManager  *fldMgr=G4TransportationManager::GetTransportationManager()->GetFieldManager();
  fldMgr->SetDetectorField(this);    
  fldMgr->CreateChordFinder(this);

  G4cout << Form(" reading Adam's field grid=%s , isBinary=%d\n",filename,type) << flush;
  G4cout << " BField scale set to " << scale*100 << "%" << " of nominal"<<G4endl;
  TString dumTxt;
  if (type==0)    {      // Open file with grid data.
      ifstream grid( filename );
      if (!grid.is_open())	{
	G4cerr  << "Cannot open magnetic field grid file >"
		<< filename  << "<, exiting..."  << G4endl;
	exit(1);
      }
      grid >>  dumTxt;
      G4cout << dumTxt<<G4endl;
      grid >>  dumTxt; //skip some text
     
      // Read the number of steps, starting value, and step size for grid.
      grid >> cyl >> fNdata >> fNx >> fNy >> fNz
	   >> fXmin >> fYmin >> fZmin
	   >> fDx >> fDy >> fDz;

      G4cout << Form("B-grid: cyl=%d  fNdata=%d  fNx=%d fNy=%d  fNz=%d ",cyl , fNdata , fNx , fNy , fNz) <<G4endl;
      G4cout << Form("B-grid: fXmin=%f fYmin=%f  fZmin=%f  fDx=%f  fDy=%f  fDz=%.1f\n   units:1cm=%.1f 1gaus=%.1e",fXmin,fYmin,fZmin,fDx,fDy,fDz,cm,gauss) <<G4endl;
      grid >>  dumTxt; //skup some text
    
      // Convert to proper units.
      fXmin *= cm;      fYmin *= cm;      fZmin *= cm;
      fDx *= cm;        fDy *= cm;        fDz *= cm;
      fiDx =1/fDx;      fiDy =1/fDy;      fiDz =1/fDz;

      assert(fNx*fNy*fNz==fNdata);
      pfBx=new G4double[fNx*fNy*fNz];
      pfBy=new G4double[fNx*fNy*fNz];
      pfBz=new G4double[fNx*fNy*fNz];
  
      fNxy=fNx*fNy;
  
      // Read field data and fill vectors with field components in proper units.      
      double bx, by, bz;
      double rx,ry,rz,bmag;
      int n3Val=0;
      for( G4int ix = 0; ix < fNx; ++ix ) {		
	for( G4int iy = 0; iy < fNy; ++iy ) {
	  for( G4int iz = 0; iz < fNz; ++iz ) {
	    grid >>rx >>ry >>rz>> bx >> by >> bz>>bmag;
	    double mz= fZmin+iz*fDz;
	    double my= fYmin+iy*fDy;
	    double mx= fXmin+ix*fDx;
	    //printf("iii=%d (cm)  rz=%f  mz=%f\n", n3Val,rz,mz/cm);
	    //printf("iii=%d bz=%f  EOF=%d\n",n3Val,bz,grid.peek() != EOF);
	    assert(grid.peek() != EOF); // watch for too short input file
	    assert(fabs(rz-mz/cm) <1e-4); // to avoid gross mismatch in the input table
	    assert(fabs(ry-my/cm) <1e-4); // to avoid gross mismatch in the input table
	    assert(fabs(rx-mx/cm) <1e-4); // to avoid gross mismatch in the input table
	    pfBx[ix+fNx*iy+fNxy*iz]=bx*gauss*scale; 
	    pfBy[ix+fNx*iy+fNxy*iz]=by*gauss*scale;
	    pfBz[ix+fNx*iy+fNxy*iz]=bz*gauss*scale;
	    n3Val++;
	    // assert(n3Val<120);
	  }
	}
        G4cout << "." << flush;
      }
      G4cout << Form(" reading ASCII Bfiled grid finished, total 3Val=%d",n3Val)<<endl;
      assert(n3Val==fNz*fNy*fNx);
      // Close data file.
      grid.close();
      //  assert(1==3);
    }  else    {
      assert(1==333); // never tested for DL , Jan B.
      ifstream grid( filename ,ifstream::binary);
      // Check if file has been opened successfully:
      if (!grid.is_open())
	{
	  G4cerr  << "Cannot open magnetic field grid file >"
		 << filename  << "<, exiting..." << G4endl;
	  exit(1);
	};
      grid.read((char *)&cyl,sizeof(cyl));
      grid.read((char *)&fNdata,sizeof(fNdata));
      grid.read((char *)&fNx,sizeof(fNx));
      grid.read((char *)&fNy,sizeof(fNy));
      grid.read((char *)&fNz,sizeof(fNz));
      grid.read((char *)&fXmin,sizeof(fXmin));
      grid.read((char *)&fYmin,sizeof(fYmin));
      grid.read((char *)&fZmin,sizeof(fZmin));
      grid.read((char *)&fDx,sizeof(fDx));
      grid.read((char *)&fDy,sizeof(fDy));
      grid.read((char *)&fDz,sizeof(fDz));
      pfBx=new G4double[fNx*fNy*fNz];
      pfBy=new G4double[fNx*fNy*fNz];
      pfBz=new G4double[fNx*fNy*fNz];
      fNxy=fNx*fNy;
      grid.read((char *)pfBx,sizeof(G4double)*fNxy*fNz);
      grid.read((char *)pfBy,sizeof(G4double)*fNxy*fNz);
      grid.read((char *)pfBz,sizeof(G4double)*fNxy*fNz);
      grid.close();
      fiDx =1/fDx;
      fiDy =1/fDy;
      fiDz =1/fDz;  
    }
}


//=============================================
GridMagneticField::~GridMagneticField() {
  delete [] pfBx;
  delete [] pfBy;
  delete [] pfBz;
}



// Member function to return the magnetic field value at a given point.
//=============================================
void GridMagneticField::GetFieldValue( const G4double Point[4],
                                    G4double * Bfield ) const {

  // Jan: grid map is only for 1/8 of sloid angle, detector is symmetric vs. all 3 axis
  G4double x = fabs(Point[0]);
  G4double y = Point[1]; // no +/-Y symmetry
  G4double z = fabs(Point[2]);


  // printf("GridMagneticField::GetFieldValue( INP: x,y,z (cm) =%f %f %f )\n",Point[0]/cm,Point[1]/cm,Point[2]/cm);
  // printf(" grid x,y,z=%f %f %f \n", x,y,z);
 
  // Find grid cubic containing the point.
  // Declare fractions of interval.
  G4double ffx0, ffx1, ffy0, ffy1, ffz0, ffz1;
  
  ffx0=(x - fXmin ) * fiDx;
  ffy0=(y - fYmin ) * fiDy;
  ffz0=(z - fZmin ) * fiDz;

  G4int ix0=(int) floor(ffx0);
  G4int iy0=(int) floor(ffy0);
  G4int iz0=(int) floor(ffz0);
  
    
  G4int base=ix0+iy0*fNx+iz0*fNxy; 
   
  // Test that point is within the range of the grid.
  if( ix0 >= 0 && ix0 < fNx-1 &&
			iy0 >= 0 && iy0 < fNy-1 &&
					  iz0 >= 0 && iz0 < fNz-1 ) {
	// Calculate the fractions of the interval for the given point.
    ffx0-=ix0;
    ffy0-=iy0;
    ffz0-=iz0;
       
    ffx1=1-ffx0;
    ffy1=1-ffy0;
    ffz1=1-ffz0;
    
	// Interpolate the magnetic field from the values at the 8 corners.
     
	double f=ffz1*ffy1;
	double x1,x2,y1,y2,z1,z2;

	x1= f*pfBx[base];    y1= f*pfBy[base];   z1= f*pfBz[base]; 

	base++;
	x2= f*pfBx[base];  y2= f*pfBy[base]; z2= f*pfBz[base];
	f=ffz1*ffy0;
	base+=fNx;
	x2+=f*pfBx[base]; y2+=f*pfBy[base]; z2+=f*pfBz[base];
	base--;
	x1+=f*pfBx[base];   y1+=f*pfBy[base];   z1+=f*pfBz[base];   

	f=ffz0*ffy0;
	base+=fNxy;
	x1+=f*pfBx[base];  y1+=f*pfBy[base]; z1+=f*pfBz[base];
	base++;
	x2+=f*pfBx[base]; y2+=f*pfBy[base]; z2+=f*pfBz[base];
      
	f=ffz0*ffy1;
	base-=fNx;
	x2+=f*pfBx[base];y2+=f*pfBy[base];z2+=f*pfBz[base];
	base--;
	x1+=f*pfBx[base];y1+=f*pfBy[base];z1+=f*pfBz[base];

	Bfield[0]=x1*ffx1+x2*ffx0;
	Bfield[1]=y1*ffx1+y2*ffx0;
	Bfield[2]=z1*ffx1+z2*ffx0;

	// flip sign for negative x, y, to match the map convetion I got from Adam
	if( Point[0] <0) 	Bfield[0]*=-1;
	// no flip for the Y-axis
	if( Point[2] <0) { Bfield[0]*=-1;     	Bfield[1]*=-1;}
      
  }   else { // out of grid
     Bfield[0] = 0.0;
     Bfield[1] = 0.0;
     Bfield[2] = 0.0;
  }

  // printf(" B (gauss) x,y,z=%f %f %f \n",Bfield[0]/gauss,Bfield[1]/gauss,Bfield[2]/gauss);


  /*
   G4cout << "\nPosition " << x << " " << z << " " << z << G4endl;
   G4cout << "Old field " << Bfield[0] << " " << Bfield[1] << " " << Bfield[2] 
          << G4endl;

   G4double phi = atan2( y, x );
   G4double Bphi = -Bfield[0] * sin( phi ) + Bfield[1] * cos( phi );

   Bfield[0] = -Bphi * sin( phi );
   Bfield[1] =  Bphi * cos( phi );
   Bfield[2] = 0.0;

   G4cout << "Phi " << phi << " " << Bphi << " " << G4endl; 
   G4cout << "New field " << Bfield[0] << " " << Bfield[1] << " " << Bfield[2] 
          << G4endl;

  */
// Get rid of r and z components (only phi component should survive)
	// to get a field with perfect cylindrical symmetry:
        //G4double phi = atan2(y,x);
	//Btot is total mag. field which is B phi
	//G4double Btot = -Bfield[0]*sin(phi)+Bfield[1]*cos(phi);

	//newly found mag. field components
	//G4double bxnew =-Btot*sin(phi);
	//G4double bynew =Btot*cos(phi);
	//G4double bznew =0.00;

	//then I print out the results
	//	printf("before: Bfield[0]: %f Bfield[1]: %f phi: %f Btot: %f bxnew: %f bynew: %f fScale: %f\n", Bfield[0],Bfield[1],phi,Btot,bxnew,bynew,fScale);

	//Bfield[0] = bxnew;
	//Bfield[1] = bynew;
	//Bfield[2] = bznew;
	
//printf("after: Bfield[0]: %f Bfield[1]: %f phi: %f Btot: %f bxnew: %f bynew: %f fScale: %f\n", Bfield[0],Bfield[1],phi,Btot,bxnew,bynew,fScale);

   

	// This is a pure magnetic field so set electric field to zero just in case.

	Bfield[3] = 0.0;
	Bfield[4] = 0.0;
	Bfield[5] = 0.0;

	return;

}



//=============================================
void GridMagneticField::save(const char *filename) {
  assert(3==555) ; // never tested for DL, Jan Balewski
   ofstream outfile (filename,ofstream::binary);

   if (!outfile.is_open())	{
      G4cerr  << "Could not open output file of magnetic field  >"
             << filename  << "<, exiting..."  << G4endl;
      exit(1);
	}

   outfile.write((const char *)&fNdata,sizeof(fNdata));
   outfile.write((const char *)&fNx,sizeof(fNx));
   outfile.write((const char *)&fNy,sizeof(fNy));
   outfile.write((const char *)&fNz,sizeof(fNz));
   outfile.write((const char *)&fXmin,sizeof(fXmin));
   outfile.write((const char *)&fYmin,sizeof(fYmin));
   outfile.write((const char *)&fZmin,sizeof(fZmin));
   outfile.write((const char *)&fDx,sizeof(fDx));
   outfile.write((const char *)&fDy,sizeof(fDy));
   outfile.write((const char *)&fDz,sizeof(fDz));
   outfile.write((const char *)pfBx,sizeof(G4double)*fNxy*fNz);
   outfile.write((const char *)pfBy,sizeof(G4double)*fNxy*fNz);
   outfile.write((const char *)pfBz,sizeof(G4double)*fNxy*fNz);
   outfile.close();
}

