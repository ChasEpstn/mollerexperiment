#include "DaLiDetectorConstruction.h"

#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Polycone.hh"

#include "G4VisAttributes.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"


//===================================== 
//===================================== 
void  DaLiDetectorConstruction::assemble_magnet(G4LogicalVolume * mother_logic=0){
  printf("DetectorBuilder::assemble_magnet((),  START ... \n");

  if(1){ //--------   magnet coil
    G4String matName="G4_Cu";
    G4String volName="magnetCoil";
    double plugLen=118*cm;  
    G4Material* matV=fNist_man->FindOrBuildMaterial( matName);   assert(matV );
    G4ThreeVector xyz=G4ThreeVector(0,0,0);
    G4VSolid *solidV = new G4Tubs(volName+"S",375*mm,500*mm,plugLen/2, 0, 2*pi*rad);
    G4LogicalVolume * logicV=new G4LogicalVolume(solidV,matV,volName+"L",0,0,0);
    logicV->SetVisAttributes(G4VisAttributes(1, G4Colour(0.2,0.1,0.7))); //vis-here
    new G4PVPlacement(0,xyz, logicV, volName,mother_logic, false, 1, mySurfChk); 
  }

}



//===================================== 
//===================================== 
void  DaLiDetectorConstruction::assemble_moller_dump(G4LogicalVolume * mother_logic=0){

  // mother==world, 
  // 2014-11-18 : extended moller dump by 50 cm downstream
  float zOff2=75*cm; // variable offset of C,Fe moller dump.
  // WARN:  Legal value of zOff2  [20,75]cm

  printf("DetectorBuilder::assemble_moller_dump((), zOff2/cm=%.1f  START ... \n",zOff2/cm);
 
  double ORfill=14.8*cm; // outer radius of moller dump filler
  double IRfill=1.0*cm; // inner radius of moller dump filler
  double frontZ=705*mm; // absolute position where it starts
  double CplugLen=34*cm; // carbon plug
  double FeplugLen=14*cm; // Iron plug

  if(1 &&  geomUse.moll_C){ //--------   moller dump C-plug
    G4String matName="G4_C";
    G4String volName="dumpC_plug";  
    G4Material* matV=fNist_man->FindOrBuildMaterial( matName);   assert(matV );
    G4ThreeVector xyz=G4ThreeVector(0,0,frontZ+2*cm+CplugLen/2.+zOff2); 
    G4VSolid *solidV = new G4Tubs(volName+"S",IRfill,ORfill,CplugLen/2, 0, 2*pi*rad);
    G4LogicalVolume * logicV=new G4LogicalVolume(solidV,matV,volName+"L",0,0,0);
    logicV->SetVisAttributes(G4VisAttributes(1, G4Colour(0.4,0.4,0.))); //vis-here
    new G4PVPlacement(0,xyz, logicV, volName,mother_logic, false, 1, mySurfChk); 
  }

  if(1 &&  geomUse.moll_Fe){ //--------     moller dump Fe-plug
    G4String matName="G4_Fe";
    G4String volName="dumpFe_plug";
    G4Material* matV=fNist_man->FindOrBuildMaterial( matName);   assert(matV );
    G4ThreeVector xyz=G4ThreeVector(0,0,frontZ+CplugLen+3*cm+FeplugLen/2.+zOff2); 
    G4VSolid *solidV = new G4Tubs(volName+"S",IRfill,ORfill,FeplugLen/2, 0, 2*pi*rad);
    G4LogicalVolume * logicV=new G4LogicalVolume(solidV,matV,volName+"L",0,0,0);
    logicV->SetVisAttributes(G4VisAttributes(1, G4Colour(0.5,0.3,0.4))); //vis-here
    new G4PVPlacement(0,xyz, logicV, volName,mother_logic, false, 1, mySurfChk); 
  }

  if(1){ //--------   2 endcaps of the drum
    G4String matName="G4_Fe";
    G4String volName="drumFe_endcap";
    double plugLen=12.7*mm; // air will be the rest
    G4Material* matV=fNist_man->FindOrBuildMaterial( matName);   assert(matV );
    G4VSolid *solidV = new G4Tubs(volName+"S",76*mm,152*mm,plugLen/2, 0, 2*pi*rad);
    G4LogicalVolume * logicV=new G4LogicalVolume(solidV,matV,volName+"L",0,0,0);
    logicV->SetVisAttributes(G4VisAttributes(1, G4Colour(0.2,0.1,0.7))); //vis-here
    G4ThreeVector xyz=G4ThreeVector(0,0,frontZ+6*mm);
    new G4PVPlacement(0,xyz, logicV, volName,mother_logic, false, 1, mySurfChk); 
    xyz=G4ThreeVector(0,0,frontZ+CplugLen+FeplugLen+5*cm+zOff2); 
    new G4PVPlacement(0,xyz, logicV, volName,mother_logic, false, 2, mySurfChk); 
  }

  if(1){ //--------   dump barrel
    G4String matName="G4_Fe";
    G4String volName="drumFe_barrel";
    double plugLen=3*cm+CplugLen+FeplugLen+zOff2; // air will be the rest 
    G4Material* matV=fNist_man->FindOrBuildMaterial( matName);   assert(matV );
    G4ThreeVector xyz=G4ThreeVector(0,0,frontZ+12.7*mm+plugLen/2);
    G4VSolid *solidV = new G4Tubs(volName+"S",149*mm,152*mm,plugLen/2, 0, 2*pi*rad);
    G4LogicalVolume * logicV=new G4LogicalVolume(solidV,matV,volName+"L",0,0,0);
    logicV->SetVisAttributes(G4VisAttributes(1, G4Colour(0.1,0.2,0.6))); //vis-here
    new G4PVPlacement(0,xyz, logicV, volName,mother_logic, false, 1, mySurfChk); 
 
  }


}

