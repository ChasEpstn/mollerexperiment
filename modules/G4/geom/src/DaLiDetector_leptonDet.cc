///
/// History:
///  Created Jan Balewski, MIT, December 2013

#include "DaLiDetectorConstruction.h"

#include "G4NistManager.hh"
#include "G4Tubs.hh"
#include "G4Polycone.hh"

#include "G4VisAttributes.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "UniversalSD.h"

//===================================== 
//===================================== 
void  DaLiDetectorConstruction::assemble_lepton_tracker(G4LogicalVolume   * mother_logic=0){
  printf("DetectorBuilder::assemble_lepton_tracker(%s) w/ sensitive cylinders,  START ... \n", geomUse.version.Data());

  /* this is a very generic lapton tracker,
     regular cylinder filled with HeIsoButane80:20
     inside there are 3 sensitve layer, 3mm thick, filled with Ar
  */

  // define fiducial volume 
  double fidOR=25.5*cm; // most outer limit
  double fidIR=9.5*cm; // most inner limit
  double fidLen=800*mm; // total length of tracker fiducial volume, was 776
 
  G4LogicalVolume   * gas_logic=0;

  if(1){//.... lepton tracker fiducial volume
    G4String volName="lepTrack";
    G4Material*  matV=fNist_man->FindOrBuildMaterial("G4_AIR");
    if (geomUse.worldAir==0) matV=fNist_man->FindOrBuildMaterial("G4_Galactic");
    assert(matV );
    G4VSolid *solidV = new G4Tubs(volName+"S", fidIR, fidOR, fidLen/2, 0, 2*pi*rad);

    G4ThreeVector xyz=G4ThreeVector(0,0,88*mm);
    G4LogicalVolume * logicV=new G4LogicalVolume(solidV, matV,volName+"L",0,0,0);
    new G4PVPlacement(0,xyz, logicV, volName, mother_logic, false, 0, mySurfChk);
    logicV->SetVisAttributes(G4VisAttributes(0, G4Colour(0.2,0.3,0.3)));//vis-here
    gas_logic=logicV;
  }
  

  if(1) { // sensitive tracker is simplified as a set of nested cylinders made of the same gas
    assert(gas_logic);

    const int NP=7; // number of plains,  DL 'official' design marked with  *
    //                   0*       1*      2      3*       4       5      6*
    double IRplane[NP]={110*mm, 130*mm, 150*mm, 170*mm, 190*mm, 210*mm, 250*mm};
    double Lplane[NP] ={610*mm, 630*mm, 650*mm, 670*mm, 690*mm, 710*mm, 750*mm};
    double delRsens=3.*mm; // thickens of sensitive layer
    double delRdead=2*mm; // thickens of dead  layer
    double delRoffset=5*mm; // offset between dead & sens layer front faces


    if( geomUse.version=="verA" ) 
      printf("assemble_lepton_tracker: Nominal DL geom11 with 4 disperse  planes\n");
    else if( geomUse.version=="verB" ) 
      printf("assemble_lepton_tracker: Jan's  DL geom11 with 4 compact planes\n");
    else if( geomUse.version=="verC" ) 
      printf("assemble_lepton_tracker: Ross's  DL geom11 with 5 dispersed planes\n");
    else {  printf("Invalid  DL geom ver=%s, abort\n",geomUse.version.Data()); assert(34==98);}
    G4Material* matVsens=fNist_man->FindOrBuildMaterial("HeIsoButane80:20"); assert(matVsens );
    G4Material*  matVdead=fNist_man->FindOrBuildMaterial("GemReadMix"); assert(matVdead );
    for (int k=0;k<2; k++) { // k=0 dead, k=1 sens
      if ( geomUse.leptDead==0 && k==0 ) continue; // skip dead material
      int kcyl=0;
      for(int ic=0; ic<NP; ic++) { //NP
	if( geomUse.version=="verA"  && ( ic==2 || ic==4 || ic==5) ) continue;
	if( geomUse.version=="verB"  &&  ic>3 ) continue;
	if( geomUse.version=="verC"  && ( ic==2 || ic==4 ) ) continue;
	kcyl++; // index only used cylinders
	double halfLen=Lplane[ic]/2.;
	G4String volName=Form("lepDead%i",kcyl);
	G4double r1=IRplane[ic]-delRoffset, r2=r1+delRdead;
	G4Material*  matV= matVdead;
	if(k==1) { // sens  layer
	  r1=IRplane[ic];  r2=r1+delRsens;
	  volName=Form("lepCy%i",kcyl);
	  matV= matVsens;
	printf("assemble_lepton_tracker: add %s  Rin/mm=%.1f L/mm=%.1f\n",volName.data(),r1/mm,2*halfLen/mm);
	}

      G4VSolid *solidV = new G4Tubs(volName+"S",r1, r2, halfLen, 0, 2*pi*rad);
      G4ThreeVector xyz=G4ThreeVector(0,0, -fidLen/2+halfLen);
      G4LogicalVolume * logicV=new G4LogicalVolume(solidV, matV,volName+"L",0,0,0);
      int ivis=k;
      logicV->SetVisAttributes(G4VisAttributes(ivis,G4Colour(0.4,0.9-kcyl/10.,0.2+k*0.3) ));//vis-here
      new G4PVPlacement(0,xyz, logicV, volName, gas_logic, false, 0, mySurfChk);
      if(k==1)  { logicV->SetSensitiveDetector( mySD ); printf(" %s is uniSD\n",volName.data()); }
      } //end-of-plain loop
    }// end-of-dead/sens loop
  }


}
