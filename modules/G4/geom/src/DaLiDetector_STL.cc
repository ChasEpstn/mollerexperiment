#include "DaLiDetectorConstruction.h"

#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"

#include "G4VisAttributes.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Polyhedron.hh"

// CADMESH //
#include "CADMesh.hh"

//================================
//================================
//================================
void DaLiDetectorConstruction::initStlVolumes(){
  printf(" DaLiDetectorConstruction::initStlVolumes stlPath=%s\n",stlPath.data());
  StlGeomItem *v=0;
//  printf("\n\n WARN: tmeprary disabled magnet and moller dump, It is BAD, Jan\n\n\n");    return;

  v=&stlVol[0]; v->xyzOffset=G4ThreeVector(0,0,168*cm);
  v->xyzOffset=G4ThreeVector(0,0,-99999);
  v->rotation=0;
  v->volName="Quad"; v->stlName="quadruple-simple"; v->matName="G4_Fe";
  v->color= G4Color(0.2,0.4,0.5);  v->visible=1;
  
  v=&stlVol[1]; v->xyzOffset=G4ThreeVector(0,0,0);
  v->rotation=0;
  v->volName="Yoke";  v->matName="G4_Fe";
  v->stlName="YOKE";
  v->color= G4Color(0.3,0.2,0.4);  v->visible=1;


  v=&stlVol[2]; v->xyzOffset=G4ThreeVector(0,0, -70*cm);
  v->rotation=new G4RotationMatrix(G4ThreeVector(0,1,0),180*deg);;
  v->volName="aftBeamPipe";  v->matName="G4_Al";
  v->stlName="BE-PIPE-UPSTR";
  v->color= G4Color(0.3,0.1,0.6);  v->visible=0;


  v=&stlVol[3]; v->xyzOffset=G4ThreeVector(0,0, 70*cm);
  v->rotation=0;
  v->volName="forwBeamPipe";   v->matName="G4_Al";
  v->stlName="BE-PIPE-DOWN";
  v->color= G4Color(0.3,0.1,0.6);  v->visible=0;

#if 0
  v=&stlVol[4]; v->xyzOffset=G4ThreeVector(0,0, -184*cm);
  v->rotation=0;
  v->volName="aft6wayCross";  v->matName="G4_Fe";
  v->stlName="DARK LIGHT TARGET REGION 6 WAY CROSS";
  v->color= G4Color(0.6,0.1,0.1);  v->visible=0;

  v=&stlVol[5]; v->xyzOffset=G4ThreeVector(0,0, 120*cm);
  v->rotation=new G4RotationMatrix(G4ThreeVector(0,1,0),180*deg);
  v->volName="forwDumpDrum"; 
  v->stlName="DARK LIGHT MOLLER DUMP REGION WITHOUT CARBON OR Fe"; 
  v->matName="G4_Fe";
  v->color= G4Color(0.6,0.1,0.1);  v->visible=1;
  /* run time warnings:
------- WWWW ------- G4Exception-START -------- WWWW -------
*** G4Exception : GeomNav1002
      issued by : G4Navigator::ComputeStep()
Track stuck or not moving.
          Track stuck, not moving for 10 steps
          in volume -forwDumpDrum- at point (-39.4078,-17.7143,1558.01)
          direction: (-0.138219,0.200438,0.969907).
          Potential geometry or navigation problem !
          Trying pushing it of 1e-07 mm ...Potential overlap in geometry!

*** This is just a warning message. ***
-------- WWWW -------- G4Exception-END --------- WWWW -------

   */

#endif 


  printf("Define STL volumes, path=%s\n",stlPath.data());
  int nStl=0;
  for (int i=0;i<mxStlVol;i++){
    if(stlVol[i].isNull()) continue;
    nStl++;
    printf(" iVol=%d ",i);
    stlVol[i].print();
  }
  printf("end of STL volumes, initialized=%d\n", nStl);
}



//===================================== 
//===================================== 
//===================================== 
void DaLiDetectorConstruction::assemble_STL_volumes(G4LogicalVolume   * world_logic){
  printf("DetectorBuilder::assemble_STL_volumes() START\n");

  G4LogicalVolume * logicV;
  G4Material* matV;


  //------ MANY Inventor volumes in STL format --------
    for (int i=0;i<mxStlVol;i++){
      StlGeomItem *v=stlVol+i;
      //if(i==1) continue;
      if(v->isNull()) continue;
      printf("Assembly iVol=%d ",i); v->print();
      G4ThreeVector cadOffsetV=G4ThreeVector(0,0,0);
      G4String fullName=stlPath+v->stlName+".stl";

      CADMesh *mesh = new CADMesh((char*)fullName.data(), (char*)"STL", mm, cadOffsetV, false);
      G4VSolid *solidV = mesh->TessellatedMesh();

      double volumeCm3=solidV->GetPolyhedron ()->GetVolume ()/cm/cm/cm;
      printf("Creating  STL.fname=%s  volume/cm3=%.1f \n" ,solidV->GetName().data(),volumeCm3);
      assert(volumeCm3>100.); // a guess of minimal sensible volume, abort otherwise

      matV=fNist_man->FindOrBuildMaterial(v->matName );   assert(matV );
      logicV=new G4LogicalVolume(solidV, matV, v->getNameL(), 0, 0, 0);

      logicV->SetVisAttributes( G4VisAttributes( v->visible,v->color ));

      //..... place physical volumes
      if(v->skipPlacement() ) { // just store the pinter
	v->logicV=logicV;
      } else { // place this volume
        new G4PVPlacement(v->rotation, v->xyzOffset, logicV, v->getNameP(), world_logic, false, 0,mySurfChk);//... G4bool pMany, G4int pCopyNo, G4bool pSurfChk
      }
      
    }// end of loop over STL-volumes

}


