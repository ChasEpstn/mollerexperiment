//
/// \file DaLiDetectorConstruction.cc
/// \brief Implementation of the DaLiDetectorConstruction class
// - Modified Jan Balewski, MIT, January 2012

#include <iostream>
using namespace std;
#include "TString.h"
#include "TObjArray.h"
#include "TObjString.h"

#include "TH2.h"
#include "TFile.h"
#include "TVector3.h"

#include "DaLiDetectorConstruction.h"
#include "GridMagneticField.h"
#include "MollerMagneticField.h"
#include "SimpleMagneticField.h"

#include "UniversalSD.h"

#include "G4SDManager.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include <iostream>

#include "G4VisAttributes.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4UserLimits.hh"

#include "G4SystemOfUnits.hh"
#include "G4ios.hh"

G4bool myVariedField=true; // activate  varied magnetic field

//================================
//================================
//================================
DaLiDetectorConstruction::DaLiDetectorConstruction(char * geomX , char * bfieldPar, TString xpathSTL) {
  SetGeometrySwitches(geomX);
  geomUse.dump();

  fMagField =0;
  SetMagField(bfieldPar);

  // create UserLimits
  fUserLimits = new G4UserLimits();
  eventDLg4=0; // used also as switch to disable hit accumulation in G4E
  trackDLg4=0;

  mySD = 0;
  SetDebug(0);
  stlPath=xpathSTL;
  initStlVolumes(); //assert(2==3);
  fNist_man = G4NistManager::Instance();
  mySurfChk=true; // activate if new volumes are added, slows down initialization

}


//================================
//================================
//================================
DaLiDetectorConstruction::~DaLiDetectorConstruction(){
  delete fMagField;
}


//================================
//================================
//================================
void DaLiDetectorConstruction::SetGeometrySwitches(TString geomList){

  int state=-1;

  TObjArray * objA=geomList.Tokenize("_");
  //printf("size =%d \n",objA->GetEntries());
  TIter*  iter = new TIter(objA);
  TObject* obj = 0;
  int k=0;
  // decoding parameters of generator
  while( (obj=iter->Next())) {
    k++;
    // obj->Print();
    TString ss= (( TObjString *)obj)->GetString();
    printf("k=%d, item=%s=\n",k,ss.Data());
    switch (k) {
    case 1: // check for geom base
      assert(ss=="geom12" || ss=="geomMRI" || ss=="Moller");
      geomUse.base=ss;      break;
    case 2: // pick geom  version
      geomUse.version=ss;      break;
    default:
      if (ss=="off") { state=0; break; }
      if (ss=="on") { state=1; break; }
      assert(state>=0); // must see on/off keyword first
      if (ss=="1") { geomUse.aftStraw=state; break; } // straw & C-disc
      if (ss=="2") { geomUse.forwStraw=state; break;}// straw & C-disc & spider
      if (ss=="3") { geomUse.moll_C=state; break; } //moller dump C-plug
      if (ss=="4") { geomUse.moll_Fe=state; break; }  //moller dump Fe-plug
      if (ss=="5") { geomUse.protDet=state; break; } // full prot detector
      if (ss=="6") { geomUse.leptDead=state; break; } // dead material in lepton det
      if (ss=="7") { geomUse.beamPipe=state; break; } // just Be section
      if (ss=="8") { geomUse.worldAir=state; break; } // use Galactic vaccum instead
    }
  }

}



//================================
//================================
//================================
G4VPhysicalVolume* DaLiDetectorConstruction::Construct(){
  //  Make some detectors sensitive.
  G4SDManager * SDMgr = G4SDManager::GetSDMpointer();

  if( eventDLg4 ) { // we are in Geant4 simulation mode
    mySD =new UniversalSD( "mySD" );  // used for output TTrees
    mySD-> eventDLg4 = eventDLg4;
    mySD-> trackDLg4 =trackDLg4;
    mySD->setCuts(1*eV, 1000, false); // ElosMin, maxHits/track, onlyPrimTracks
    mySD->printCuts();
    SDMgr->AddNewDetector( mySD );
  }

  Construct_materials();

  if(myDebug){ // lot of printout about all materials
    G4cout << G4endl << "The materials defined are : " << G4endl << G4endl;
    G4cout << *(G4Material::GetMaterialTable()) << G4endl;  // Print all materials
  }

  G4Material* matV=0;
  double halfZ=200*cm,halfXY=110*cm ;

  //jx  if( geomUse.doseMon) halfZ=300*cm; // need more space for the dosimeter

  // -------------------------------------------------------------------------
  //http://geant4.web.cern.ch/geant4/workAreaUserDocKA/Backup/Docbook_UsersGuides_beta/ForApplicationDeveloper/html/apas08.html

  // Define the World volume as a box of Air
  matV=fNist_man->FindOrBuildMaterial("G4_AIR");
  if (geomUse.worldAir==0) matV=fNist_man->FindOrBuildMaterial("G4_Galactic");
  assert(matV);

  G4VSolid     * world_solid = new  G4Box( "DLKworldS",halfXY,halfXY,halfZ);
  G4LogicalVolume   * world_logic = new  G4LogicalVolume( world_solid,matV, "DLKworldL", 0, 0, 0, false );
  G4VPhysicalVolume * physiWorld = new  G4PVPlacement( 0, G4ThreeVector( 0.0, 0.0, 0.0 ), "DLKworld", world_logic, 0, false, 0, false );
  G4VPhysicalVolume * mollerWorld;
  world_logic->SetVisAttributes(G4VisAttributes( true, G4Color(0.5,0.3,0.5)));
  G4LogicalVolume * target_logic;

  if (geomUse.base=="geom12"){
    assemble_STL_volumes(world_logic);
    assemble_moller_dump(world_logic);
    assemble_magnet(world_logic);
    target_logic= assemble_target_cell(world_logic);
    // detectors:
    assemble_proton_detector(target_logic);
    assemble_lepton_tracker(world_logic);
    assemble_photon_calorimeter(world_logic);
   } else  if (geomUse.base=="geomMRI") { // MRI phase1 experiment
    assemble_STL_volumes(world_logic);
    assemble_moller_dump(world_logic);
    assemble_magnet(world_logic);
    target_logic= assemble_target_cell(world_logic);
    // detectors:

    if (geomUse.version=="verE"){
      assemble_mri_verE(world_logic);
      assemble_mri_verE_inside_target(target_logic);
    }else{
      assemble_proton_detector_mri(target_logic);
      assemble_lepton_tracker_mri(world_logic);
      assemble_trigger_paddles_mri(world_logic);
    }
  } else if (geomUse.base=="Moller") { //Moller experiment
    mollerWorld = assemble_moller_detector();
    return mollerWorld;
  }
  return physiWorld;
}




//=====================================
//=====================================
//=====================================
void DaLiDetectorConstruction::Construct_materials(){
  printf("DetectorBuilder::Construct_materials() START mm=%f MeV=%f\n",mm, MeV);
  /* nice example of different material mixtures:
     http://fismed.ciemat.es/GAMOS/download/GAMOS.3.0.0/uncompiled/geant4.9.4.p01.gamos/examples/advanced/lAr_calorimeter/src/FCALMaterialConsultant.cc

     http://www.slac.stanford.edu/accel/ilc/codes/geant4.0/LCBDS/src/LCBDSDetectorConstruction.cc
  */

   // Define materials.


  G4double a,z,density,fractionmass;
  G4String name,symbol;
  G4int nel;
  //------------
  // elements, needed for material definitions
  //------------

  G4Element* elH=new G4Element(name="Hydrogen",symbol="H2",z=1.,a=1.01*g/mole);
  G4Element* elC=new G4Element(name="Carbon",symbol="C",z=6.,a=12.01*g/mole);
  G4Element* elN=new G4Element(name="Nitrogen",symbol="N2",z=7.,a=14.01*g/mole);
  G4Element*elO=new G4Element(name="Oxygen",symbol="O2",z=8.,a=16.*g/mole);
  G4Element*elCl=new G4Element(name="Chlorine",symbol="Cl2",z=17.,a=35.45*g/mole);
  G4Element*elAl=new G4Element(name="Aluminium",symbol="Al",z=13.,a=26.98*g/mole);
  G4Element*elCu=new G4Element(name="Copper",symbol="Cu",z=29.,a=63.5*g/mole);
  G4Element*elAr=new G4Element(name="Argon",symbol="Ar",z=18.,a=39.95*g/mole);
  G4Element*elHe=new G4Element(name="Helium",symbol="He",z=2.,a=4.*g/mole);
  G4Element*elSi=new G4Element(name="Silicon",symbol="Si", z=14., a= 28.0855*g/mole);


  //-------------------
  // simple materials
  //-------------------

  //.... target gas + 9 gradients of  target gas ....
  //old: density = 3.6e-6*g/cm3; // 30 Torr=1/25 Atm
  density =9.5e-7 *g/cm3; // 8 Torr, per Chris request 2014-02-28
  density /=2.;  // 4 Torr, per DL-meeting on 2014-03-6
  G4String gasName="H2gasTarget";
  new G4Material(gasName,z=1.,a=1.01*g/mole,density);
  // add 9 lower density targets
  for(int k=1; k<10; k++){
    G4String gasName1=gasName+ Form("%d",k);
    double density1=k/10.*density;
    new G4Material(gasName1,z=1.,a=1.01*g/mole,density1);
  }

  G4Material* G4_Ar =  fNist_man->ConstructNewGasMaterial("Argon","G4_Ar",300.*kelvin, 1*bar);
  G4Material* G4_CO2 =  fNist_man->ConstructNewGasMaterial("CO2","G4_CARBON_DIOXIDE",300.*kelvin, 1*bar);

  new G4Material("Al_100",z=13.,a=26.98*g/mole,density=0.027 *g/cm3);


  //------------------
  // mixtures, some are not used
  //------------------

  density	= 0.0018*g/cm3; // to be checked
  G4Material* ArCO2Mix = new G4Material("ArCO2Mix90:10", density, nel=2);
  ArCO2Mix->AddMaterial(G4_Ar,fractionmass = 0.9);
  ArCO2Mix->AddMaterial(G4_CO2,fractionmass = 0.1);



  density = 1.42*g/cm3;
  G4Material* Kapton = new G4Material(name="Kapton",density, nel=4);
  Kapton->AddElement(elH, fractionmass = 0.0273);
  Kapton->AddElement(elC, fractionmass = 0.7213);
  Kapton->AddElement(elN, fractionmass = 0.0765);
  Kapton->AddElement(elO, fractionmass = 0.1749);
  // cout<<Kapton<<endl; assert(2==3);

  G4Material* xmat;
  xmat = new G4Material(name="GemReadMix",density=0.356*g/cm3, nel=7);
  xmat->AddElement(elO, fractionmass = 0.160);
  xmat->AddElement(elC, fractionmass = 0.508);
  xmat->AddElement(elH, fractionmass = 0.020);
  xmat->AddElement(elN, fractionmass = 0.051);
  xmat->AddElement(elAl, fractionmass = 0.003);
  xmat->AddElement(elCu, fractionmass = 0.255);
  xmat->AddElement(elAr, fractionmass = 0.003);

   xmat=new G4Material(name="GemNOMEX",density=0.090*g/cm3,nel=8);
   xmat->AddElement(elO, fractionmass=0.090);
   xmat->AddElement(elC, fractionmass=0.796);
   xmat->AddElement(elH, fractionmass=0.023);
   xmat->AddElement(elCl, fractionmass=0.019);
   xmat->AddElement(elN, fractionmass=0.025);
   xmat->AddElement(elAl, fractionmass=0.002);
   xmat->AddElement(elCu, fractionmass=0.039);
   xmat->AddElement(elAr, fractionmass=0.006);

  xmat=new G4Material(name="GemMix",density=0.079*g/cm3,nel=6);
   xmat->AddElement(elO, fractionmass=0.095);
   xmat->AddElement(elC, fractionmass=0.301);
   xmat->AddElement(elH, fractionmass=0.011);
   xmat->AddElement(elN, fractionmass=0.032);
   xmat->AddElement(elCu, fractionmass=0.547);
   xmat->AddElement(elAr, fractionmass=0.014);


  xmat = new G4Material(name="HeIsoButane80:20",density=0.00107*g/cm3, nel=3);
  xmat->AddElement(elHe, fractionmass = 0.22);
  xmat->AddElement(elC, fractionmass = 0.22);
  xmat->AddElement(elH, fractionmass = 0.56);

   xmat=new G4Material(name="SiWafer",z=14., a=28.0855 *g/mole, density=2.33 *g/cm3);


   xmat = new G4Material("NemaG10", density= 1.800*g/cm3, nel=4);//this is actuall FR4
   xmat->AddElement(elSi, fractionmass = 0.281);
   xmat->AddElement(elO , fractionmass = 0.467);
   xmat->AddElement(elC , fractionmass = 0.220);
   xmat->AddElement(elH , fractionmass = 0.032);


}



//=====================================
//=====================================
//=====================================
void DaLiDetectorConstruction::PrintSmallFieldMap(){

  double x=0,y=0,z=0, Point[4];

  for(int i=0;i<100;i++){
    z=1*i;

    {
      Point[0]=x*cm;
      Point[1]=y*cm;
      Point[2]=z*cm;
      G4double Bfield[3];
      fMagField->GetFieldValue(Point,Bfield);
      printf("xyz/cm=%.1f,%.1f,%.1f --> B/tesla=%.2f,%.2f.%.2f\n",Point[0]/cm,Point[1]/cm,Point[2]/cm,Bfield[0]/tesla,Bfield[1]/tesla,Bfield[2]/tesla);

    }
  }
}



//================================
//================================
//================================
void DaLiDetectorConstruction::SetMagField(TString parList){

  // set magnetic field
  fMagField =0;

  TObjArray * objA=parList.Tokenize("_");
  //printf("size =%d \n",objA->GetEntries());
  TIter*  iter = new TIter(objA);
  TObject* obj = 0;
  int k=0;
  // decoding parameters of generator

  while( (obj=iter->Next())) {
    k++;
    // obj->Print();
    TString ss= (( TObjString *)obj)->GetString();
    printf("k=%d, item=%s=\n",k,ss.Data());
    switch (k) {
    case 1: // particle type
      if(ss=="zero") fMagField = new SimpleMagneticField(0);
      else if(ss=="flat") fMagField = new SimpleMagneticField(1);
      else if(ss=="step")  fMagField = new SimpleMagneticField(true);
      else if(ss.Contains('Moller')) fMagField = new MollerMagneticField(ss,geomUse.version,1.,0.);
      else fMagField = new GridMagneticField(ss,5./5.); // B-scale hardcoded here
      break;
    case 2: if(ss=="dump") PlotBfiled(); exit(0); break;
    default:
      assert(1345==2987); // unexpected
    }
  }

  assert( fMagField );

}



//====================================================
//====================================================
void  DaLiDetectorConstruction::PlotBfiled(){
  enum {mxH=6, nxyz=3};

  double rMax=65, zMax=120; //cm
  //--------  init histos ----------
  TH2F *hA[mxH];
  memset(hA,0,sizeof(hA));
  TString partNA[nxyz]={"viewBZ","viewBX","viewBY"};
  for(int ip=0;ip<nxyz;ip++){
     TString core=partNA[ip];
     hA[ip]=new TH2F(core+"rz",core+"  Bfiled (Gauss) ; world Z (cm) ; world Rxy (cm)",200,-zMax,zMax,100,-rMax,rMax);
     hA[ip+nxyz]=new TH2F(core+"xy",core+"  Bfiled (Gauss) ; world X (cm) ; world Y (cm)",100,-rMax,rMax,100,-rMax,rMax);
  }

  // hA[2]->SetTitle("phi (rad) of Bfiled");
  G4double Point[4];
  G4double  Bfield[6]; // B-vector & E-vector
  Point[3]=0; // time ccorinate is not changing

  if(1){
    //------------ fill histos in R-Z plane
    //    double phi0=0.6, roff=-10; // phi vs. Y-axis, roff was -10 (cm)
    double phi0=0, roff=-30; // was -10 (cm)

    double sphi=sin(phi0), cphi=cos(phi0);
    TH2F *h=hA[0];
    TAxis *xAx=h->GetXaxis();
    TAxis *yAx=h->GetYaxis();
    printf(" plotRZ nPix=%d\n", xAx->GetNbins()*yAx->GetNbins());
    for(int kx=1;kx<=xAx->GetNbins(); kx++){
      double xval=xAx->GetBinCenter(kx);
      for(int ky=1;ky<=yAx->GetNbins(); ky++){
        double yval=yAx->GetBinCenter(ky);

        Point[0]=(-sphi* yval  + cphi*roff) *cm;
        Point[1]=(cphi* yval  + sphi*roff) *cm;
        Point[2]=xval*cm;
        fMagField ->GetFieldValue( Point,  Bfield);
        TVector3 B(Bfield[0],Bfield[1],Bfield[2]);
        B=B*(-1/gauss);
        // if(kx!=ky) continue;
        hA[0]->SetBinContent(kx,ky,B.z());
        hA[1]->SetBinContent(kx,ky,B.x());
        hA[2]->SetBinContent(kx,ky,B.y());
        //printf("PP kx=%d  z/cm=%f  z/cm=%f  Bz/gauss=%.2f\n",kx,Point[0]/cm,Point[2]/cm, B.z());
      }
    }
  }


  {
    //------------ fill histos in X-Y plane
    double zOff=-35; //was -35
    TH2F *h=hA[3];
    TAxis *xAx=h->GetXaxis();
    TAxis *yAx=h->GetYaxis();

    printf(" plotX-Y nPix=%d\n", xAx->GetNbins()*yAx->GetNbins());
    for(int kx=1;kx<=xAx->GetNbins(); kx++){
      double xval=xAx->GetBinCenter(kx);
      for(int ky=1;ky<=yAx->GetNbins(); ky++){
        double yval=yAx->GetBinCenter(ky);

        Point[0]=xval*cm;
        Point[1]=yval*cm;
        Point[2]=zOff*cm;
        fMagField ->GetFieldValue( Point,  Bfield);
        TVector3 B(Bfield[0],Bfield[1],Bfield[2]);
        B=B*(-1/gauss);
        hA[3]->SetBinContent(kx,ky,B.z());
        hA[4]->SetBinContent(kx,ky,B.x());
        hA[5]->SetBinContent(kx,ky,B.y());
      }
    }
  }


  //------- saveHisto --------

  TString fname="bFiled";
  TString  fnameF= fname+".hist.root";
  TFile f(fnameF.Data(),"recreate");
  assert(f.IsOpen());
  printf(" histos are written  to '%s' " ,fnameF.Data());
  for(int i=0;i<mxH;i++) {
    if(hA[i]==0) continue;
    hA[i]->Write();
  }
  f.ls();//list saved histos
  printf("           save Ok %s\n",f.GetName());

  printf("          for plots exec:  root -l ../macros/plEleE.C\'(\"%s\",201)\'\n",fname.Data());

  f.Close();


}
