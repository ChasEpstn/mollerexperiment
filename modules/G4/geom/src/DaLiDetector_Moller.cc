///
/// History:
///  Created by Ross Corliss, March 2015

#include "DaLiDetectorConstruction.h"
#include "G4NistManager.hh"
#include "G4Tubs.hh"
#include "G4Box.hh"
#include "G4Polycone.hh"

#include "G4VisAttributes.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "UniversalSD.h"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4GDMLParser.hh"
#include "MollerMagneticField.h"
// #include <Python.h>



// class MollerMagField: public G4MagneticField{
// 	double momentum;
// 	double bendrad;
// 	double fieldVal;
// 	double theta;
// 	public:
// 		MollerMagField(double p, double r, double t){
// 			momentum = p;
// 			bendrad = r;
// 			theta = t;
// 		    fieldVal=1.e4*p*0.001/(0.3*r);
// 		    // printf("momentum: %g, bend radius: %g, field value: %g, theta: %g\n",momentum,bendrad,fieldVal,theta);
// 			// G4FieldManager  *fldMgr=G4TransportationManager::GetTransportationManager()->GetFieldManager();
// 			// fldMgr->SetDetectorField(this);
// 			// fldMgr->CreateChordFinder(this);
// 		}
//
//     double E_elastic(double x){
//       double EkinBeam = 100.;
//       double me=0.510998928;//MeV
//       return (EkinBeam*me + pow(me,2) + EkinBeam*me*pow(cos(x),2) - pow(me,2)*pow(cos(x),2))/
//       (EkinBeam + me - EkinBeam*pow(cos(x),2) + me*pow(cos(x),2));
//
//     }
//
//     double p(double x){
//       double me=0.510998928;//MeV
//       double energy = E_elastic(x);
//       return sqrt(energy*energy-me*me)*0.001;
//     }
//
//     double B(double x,double r){
//       return 1.e4*p(x)/(0.3*r);//r in m,
//     }
//
// 	void GetFieldValue(const G4double Point[4], G4double *Bfield) const{
// 		double x = Point[0]/mm;
// 		double y = Point[1]/mm;
// 		double z = Point[2]/mm;
// 		// [-157.26043638552892, 228.6005, 337.24609415111996] = pos of center piece
// 		double dist = sqrt(157.26043638552892*157.26043638552892+337.24609415111996*337.24609415111996);
// 		double xINIT = dist*sin(theta);
// 		double zINIT = dist*cos(theta);
// 		double xp = x + xINIT;
// 		double yp = y - 228.6005;
// 		double zp = z - zINIT;
// 		// radii 187.325,269.875
// 		double xpr = xp*cos(theta) + zp*sin(theta);
// 		double ypr = yp;
// 		double zpr = -xp*sin(theta) + zp*cos(theta);
// 		double radius = sqrt(ypr*ypr+zpr*zpr);
// 		double th = atan2(-ypr,zpr);
//
// 		// int flags = 0;
// 		// if(fabs(xpr)<12.5){
// 		// 	flags |= 0x1;
// 		// }
// 		// if((radius<269.875)&&(radius>187.325)){
// 		// 	flags |= 0x2;
// 		// }
// 		// if((th>0)&&(th<pi/2)){
// 		// 	flags |= 0x4;
// 		// }
//
// 		// if(flags<7){
// 		// 	Bfield[0] = Bfield[1] = Bfield[2] = 0;
// 		// }
// 		if((abs(xpr)<12.5)&&(radius<269.875)&&(radius>187.325)&&(th>0)&&(th<pi/2)){
// 			// printf("Field Pos: %g %g %g \n",x,y,z);
// 			// printf("R: %g, Theta: %g, wid: %g",radius,th,xpr);
// 		    Bfield[2]=-fieldVal*sin(theta)*gauss;
// 		    Bfield[1]=0;
// 		    Bfield[0]=-fieldVal*cos(theta)*gauss;
// 		}
// 		else{
// 			Bfield[0] = Bfield[1] = Bfield[2] = 0;
// 		}
// 	    //assert(2==3);
// 	  }
//
// };

//=====================================
//=====================================
G4VPhysicalVolume *  DaLiDetectorConstruction::assemble_moller_detector(){
  printf("DetectorBuilder::assemble_moller_detector(%s) START ... \n", geomUse.version.Data());
  TString arg1 = "/home/cepstein/MollerExperiment/GDML_Generator/MollerGDMLGen/procGDML.py "+geomUse.version;
  // TString arg2 = "./hello.py";
  system(arg1.Data());


  // G4String volName = "world1";
  G4GDMLParser parser;
  // parser.SetOverlapCheck(true);
  // parser.Read("outputTest1.gdml", false);

  // parser.Read("../GDML_Generator/gdml_files/Geant4_nominal.gdml", false);
  parser.Read("../GDML_Generator/gdml_files/Geant4Moller.gdml", false);
  G4VPhysicalVolume *world = parser.GetWorldVolume();
  // double bendRadius = (187.325+269.875)/2.*mm;
  // double analyzeAngle = 25;//p(analyzeAngle*pi/180.)*1000*0.92
  double targetP = atof(geomUse.version);

//Need this!
  // MollerMagField *mollerField = new MollerMagField(targetP,bendRadius/m,analyzeAngle*pi/180.);

  // G4FieldManager *globalFieldMgr = G4TransportationManager::GetTransportationManager()->GetFieldManager();
  // parser.Write("outputTest1.gdml",world);
//Need these!

  // MollerMagneticField *mollerField = new MollerMagneticField("/home/cepstein/MollerExperiment/modules/G4/geom/externalGeom/Moller1BHighResFormatted.fld+25+1",1,0);

  // globalFieldMgr->SetDetectorField(mollerField);
  // globalFieldMgr->CreateChordFinder(mollerField);
  // globalFieldMgr->GetChordFinder()->SetDeltaChord(0.01*cm);


  // G4double minEps= 1.0e-5;  //   Minimum & value for smallest steps
  // G4double maxEps= 1.0e-5;  //   Maximum & value for largest steps

  // globalFieldMgr->SetMinimumEpsilonStep( minEps );
  // globalFieldMgr->SetMaximumEpsilonStep( maxEps );
  // globalFieldMgr->SetDeltaOneStep( 0.5e-3 * mm );  // 0.5 micrometer
   ///////////////////////////////////////////////////////////////////////
   //
   // Example how to retrieve Auxiliary Information for sensitive detector
   //
   const G4GDMLAuxMapType* auxmap = parser.GetAuxMap();
   // The same as above, but now we are looking for
   // sensitive detectors setting them for the volumes
   for(G4GDMLAuxMapType::const_iterator iter=auxmap->begin();
       iter!=auxmap->end(); iter++)
   {
     G4cout << "Volume " << ((*iter).first)->GetName()
            << " has the following list of auxiliary information: "
            << G4endl << G4endl;
     for (G4GDMLAuxListType::const_iterator vit=(*iter).second.begin();
          vit!=(*iter).second.end();vit++)
     {
       if ((*vit).type=="SensDet")
       {
         G4cout << "Attaching sensitive detector " << (*vit).value
                << " to volume " << ((*iter).first)->GetName()
                <<  G4endl << G4endl;

         // G4VSensitiveDetector* mydet =
         //   SDMgr->FindSensitiveDetector((*vit).value);
         if(1)//mydet
         {
           G4LogicalVolume* myvol = (*iter).first;
           myvol->SetSensitiveDetector(mySD);
         }
         else
         {
           G4cout << (*vit).value << " detector not found" << G4endl;
         }
       }
     }
   }
   //
   // End of Auxiliary Information block
   //
   ////////////////////////////////////////////////////////////////////////



  // G4LogicalVolume* lv = 0;
  // lv = FindLogicalVolume("Detector_log");        // FindLogicalVolume makes use of
  // if ( lv )                                    // GDMLProcessor which keeps track
  //   lv->SetSensitiveDetector(mySD);   // of all Geant4 objects it has created
  // lv = FindLogicalVolume( "Gap" );             // during the GDML data processing
  // if ( lv )                                    // In the same way one can retrieve
  //   lv->SetSensitiveDetector(calorimeterSD);   // materials or rotations, etc.
  // G4VUserDetectorConstruction* detector = new GDMLDetectorConstruction(parser.GetWorldVolume());
  // mother_logic->AddDaughter(parser.GetWorldVolume());
  // G4Material *vacuum=fNist_man->FindOrBuildMaterial("G4_Galactic");
  // G4VSolid *testCube=new G4Box("MrTestCubeS", 10*cm,10*cm,10*cm);//cube with length 20cm in each dim.
  // G4ThreeVector xyz=G4ThreeVector(0*cm,0*cm,0*cm); //centered at the center of mother_logic
  // G4LogicalVolume *testCubeLogical=new G4LogicalVolume(testCube,vacuum,"MrTestCubeL",0,0,0); //make an instance of 'testCube', out of vacuum, call it 'MrTestCubeL', and set no special states yet.
  // new G4PVPlacement(0,xyz,testCubeLogical,"MrTestCube",mother_logic,false, 0, mySurfChk);
  // testCubeLogical->SetSensitiveDetector( mySD );//manually tell G4 to handle energy deposits in this volume using 'mySD'.
  // testCubeLogical->SetVisAttributes(G4VisAttributes(1, G4Colour(0.9,0.1,0.0)));//set the color for drawing this in the viewer. RGB.
  // if(1){
  // 	new G4PVPlacement(0,0,w,volName,mother_logic,false,0,mySurfChk);
  // }

// //=====================================
// //=====================================

//   // define fiducial volume
//   double fidOR=27*cm; // most outer limit
//   double fidIR=9.5*cm; // most inner limit
//   double fidLen=776*mm; // total length of tracker fiducial volume
//   double delRsens=3.*mm; // thickness of sensitive layer
//   double delRdead=2*mm; // thickness of dead  layer
//   double delRoffset=5*mm; // offset between dead & sens layer front faces
//   double in = 2.54*cm;
//   G4LogicalVolume   * gas_logic=0;

//   if(0){//.... lepton tracker fiducial volume
//     G4String volName="lepTrack";
//     G4Material*  matV=fNist_man->FindOrBuildMaterial("G4_AIR");
//     if (geomUse.worldAir==0) matV=fNist_man->FindOrBuildMaterial("G4_Galactic");
//     assert(matV );
//     G4VSolid *solidV = new G4Tubs(volName+"S", fidIR, fidOR, fidLen/2, 0, 2*pi*rad);

//     G4ThreeVector xyz=G4ThreeVector(0,0,88*mm);
//     G4LogicalVolume * logicV=new G4LogicalVolume(solidV, matV,volName+"L",0,0,0);
//     new G4PVPlacement(0,xyz, logicV, volName, mother_logic, false, 0, mySurfChk);
//     logicV->SetVisAttributes(G4VisAttributes(1, G4Colour(0.2,0.3,0.3)));//vis-here
//     gas_logic=logicV;
//   }

//   if(0){ //Moller test
//     G4Material*  matV=fNist_man->FindOrBuildMaterial("G4_Fe");
//     G4VSolid *solidV = new G4Box("MollerOutline",10*mm,10*mm,10*mm);
//     G4ThreeVector xyz=G4ThreeVector(0,0,0);
//     G4LogicalVolume * logicV=new G4LogicalVolume(solidV, matV,"MollerOutline",0,0,0);
//     logicV->SetVisAttributes(G4VisAttributes(1,G4Colour(0.2,0.3,0.3) ));//vis-here
//     new G4PVPlacement(0,xyz, logicV, "MollerOutline", mother_logic, false, 0, mySurfChk);
//     // logicV->SetSensitiveDetector( mySD );
//   }

//   if(0){
//       G4Material*  matV=fNist_man->FindOrBuildMaterial("G4_Cu");
//       double innerRad = 18*cm;
//       double outerRad = 28*cm;
//       double coilWidth = (outerRad - innerRad)/2;
//       double bendRad = (outerRad + innerRad)/2;
//       double width = 6*cm;
//       double arcLen = pi/2;
//       double arctheta = pi; //starting arctheta


//       double spacing = 5*cm; //spacing between arcs

//       double analyzeTheta = 25*deg;


//       double theta = pi/2-analyzeTheta/rad; //what theta to analyze - second number!
//       double dist = 38*cm; //how far from the vertex
//       double di = bendRad*bendRad/dist;//1/(-1./bendRad+1./dist);

//       double vertRot = atan(1+(dist-bendRad)/(2*bendRad))*rad;

//       if(0){  //Left arc
//         G4String volName = "MagnetArcL";
//         G4VSolid *solidV = new G4Tubs(volName,innerRad,outerRad,width/2,arctheta,arcLen); //make a magnet segment
//         G4ThreeVector xyz=G4ThreeVector(-dist*std::cos(theta)-(width+spacing)/2*std::sin(theta),outerRad-coilWidth,dist*std::sin(theta)-(width+spacing)/2*std::cos(theta)); //position
//         G4RotationMatrix rotm  = G4RotationMatrix();
//         rotm.rotateY(theta);
//         G4Transform3D transform = G4Transform3D(rotm,xyz);
//         G4LogicalVolume * logicV=new G4LogicalVolume(solidV, matV,volName,0,0,0);
//         logicV->SetVisAttributes(G4VisAttributes(1,G4Colour(0.2,0.3,0.3) ));//vis-here
//         new G4PVPlacement(transform, volName, logicV, world, false, 0, mySurfChk);
//       }
//       if(0){     //Right arc
//         G4String volName = "MagnetArcR";
//         G4VSolid *solidV = new G4Tubs(volName,innerRad,outerRad,width/2,arctheta,arcLen); //make a magnet segment
//         G4ThreeVector xyz=G4ThreeVector(-dist*std::cos(theta)+(width+spacing)/2*std::sin(theta),outerRad-coilWidth,dist*std::sin(theta)+(width+spacing)/2*std::cos(theta)); //position
//         G4RotationMatrix rotm  = G4RotationMatrix();
//         rotm.rotateY(theta);
//         G4Transform3D transform = G4Transform3D(rotm,xyz);
//         G4LogicalVolume * logicV=new G4LogicalVolume(solidV, matV,volName,0,0,0);
//         logicV->SetVisAttributes(G4VisAttributes(1,G4Colour(0.2,0.3,0.3) ));//vis-here
//         new G4PVPlacement(transform, volName, logicV, world, false, 0, mySurfChk);
//       }

//       if(1){  //center field arc
//         G4String volName = "MagnetArcC";
//         G4Material*  matX=fNist_man->FindOrBuildMaterial("G4_Galactic");

//         double fieldStrength = B(analyzeTheta/rad,bendRad/m)*gauss*1.1;//280*gauss;

//         G4UniformMagField* magField = new G4UniformMagField(G4ThreeVector(-fieldStrength*std::sin(theta),0,-fieldStrength*std::cos(theta)));
//         // G4FieldManager* fieldMgr = G4TransportationManager::GetTransportationManager()->GetFieldManager();
//         // fieldMgr->SetDetectorField(magField);
//         // fieldMgr->CreateChordFinder(magField);
//         G4FieldManager* localFieldMgr= new G4FieldManager(magField);
//         localFieldMgr->SetDetectorField(magField);
//         localFieldMgr->CreateChordFinder(magField);

//         G4VSolid *solidV = new G4Tubs(volName,innerRad,outerRad,spacing/2,arctheta,arcLen); //make a magnet segment
//         G4ThreeVector xyz=G4ThreeVector(-dist*std::cos(theta),outerRad-coilWidth,dist*std::sin(theta)); //position
//         G4RotationMatrix rotm  = G4RotationMatrix();
//         rotm.rotateY(theta);
//         G4Transform3D transform = G4Transform3D(rotm,xyz);
//         G4LogicalVolume * logicV=new G4LogicalVolume(solidV, matX,volName,0,0,0);
//         logicV->SetVisAttributes(G4VisAttributes(1,G4Colour(0.2,0.3,0.3) ));//vis-here
//         logicV->SetFieldManager(localFieldMgr,true);
//         new G4PVPlacement(transform, volName, logicV, world, false, 0, mySurfChk);
//       }
//       if(0){ //Detector
//         G4String volName = "DetPlane";
//         G4Material*  matX=fNist_man->FindOrBuildMaterial("G4_AIR");
//         G4Material* matVsens=fNist_man->FindOrBuildMaterial("HeIsoButane80:20"); assert(matVsens );
//         G4VSolid *solidV = new G4Box(volName,75*mm,1*mm,25*mm); //make a magnet segment
//         G4ThreeVector xyz=G4ThreeVector(-(dist+bendRad)*std::cos(theta),bendRad+di,(dist+bendRad)*std::sin(theta)); //position
//         G4RotationMatrix rotm  = G4RotationMatrix();
//         rotm.rotateZ(pi-vertRot/rad);
//         rotm.rotateY(theta);
//         G4Transform3D transform = G4Transform3D(rotm,xyz);
//         G4LogicalVolume * logicV=new G4LogicalVolume(solidV, matVsens,volName,0,0,0);
//         logicV->SetVisAttributes(G4VisAttributes(1,G4Colour(0.2,0.3,0.3) ));//vis-here
//         logicV->SetSensitiveDetector( mySD );
//         new G4PVPlacement(transform, volName, logicV, world, false, 0, mySurfChk);

//       }
// }
//       // if(1){//Shield
//       //   G4String volNameV = "Shield";
//       //   G4String volNameX = "Shield";
//       //   G4String volNameY = "Shield";

//       //   G4Material*  matZ=fNist_man->FindOrBuildMaterial("G4_Pb");
//       //   double iRad = 10*cm;
//       //   double oRad = 20*cm;
//       //   double length = 100*cm;
//       //   double tolerance = 1*deg;//pi/2+analyzeTheta/rad-0.5*tolerance/rad
//       //   double gap = 2*cm;

//       //   G4VSolid *solidV = new G4Tubs(volNameV,iRad,oRad,gap/2,pi/2+analyzeTheta/rad+0.5*tolerance/rad,2*pi-tolerance/rad); //make a magnet segment

//       //   G4VSolid *solidXY = new G4Tubs(volNameX,iRad,oRad,length/4,0,2*pi); //make a magnet segment

//       //   G4ThreeVector xyzX=G4ThreeVector(0,length/4+gap/2,0); //position
//       //   G4RotationMatrix rotm  = G4RotationMatrix();
//       //   rotm.rotateX(90*deg);

//       //   G4ThreeVector xyzY=G4ThreeVector(0,-length/4-gap/2,0); //position
//       //   G4ThreeVector xyzV=G4ThreeVector(0,0,0); //position



//       //   G4Transform3D transformV = G4Transform3D(rotm,xyzV);
//       //   G4Transform3D transformX = G4Transform3D(rotm,xyzX);
//       //   G4Transform3D transformY = G4Transform3D(rotm,xyzY);

//       //   G4LogicalVolume * logicV=new G4LogicalVolume(solidV, matZ,volNameV,0,0,0);
//       //   G4LogicalVolume * logicX=new G4LogicalVolume(solidXY, matZ,volNameX,0,0,0);
//       //   G4LogicalVolume * logicY=new G4LogicalVolume(solidXY, matZ,volNameY,0,0,0);

//       //   logicV->SetVisAttributes(G4VisAttributes(1,G4Colour(0.2,0.3,0.3) ));//vis-here
//       //   logicX->SetVisAttributes(G4VisAttributes(1,G4Colour(0.2,0.3,0.3) ));//vis-here
//       //   logicY->SetVisAttributes(G4VisAttributes(1,G4Colour(0.2,0.3,0.3) ));//vis-here

//       //   new G4PVPlacement(transformV, logicV, volNameV, mother_logic, false, 0, mySurfChk);

//       //   new G4PVPlacement(transformX, logicX, volNameX, mother_logic, false, 0, mySurfChk);

//       //   new G4PVPlacement(transformY, logicY, volNameY, mother_logic, false, 0, mySurfChk);
//       // }

//       if(1){//Vertical pipe
//       	  G4String volNameV = "Vertical Pipe";
//       	  G4Material*  matZ=fNist_man->FindOrBuildMaterial("G4_Al");
//       	  double iRad = 6.540*in/2.;
//       	  double oRad = 6.741*in/2.;
//       	  double length = 11.148*in;
//       	  G4VSolid *solidV = new G4Tubs(volNameV,iRad,oRad,length/2.,0,2*pi);
//       	  G4ThreeVector xyzX=G4ThreeVector(0,0.,0); //position
//       	  G4RotationMatrix rotm  = G4RotationMatrix();
//       	  rotm.rotateX(90*deg);
//       	  G4ThreeVector xyzV=G4ThreeVector(0,0,0); //position


//       	  G4Transform3D transformV = G4Transform3D(rotm,xyzV);

//       	  G4LogicalVolume * logicV=new G4LogicalVolume(solidV, matZ,volNameV,0,0,0);

//       	  logicV->SetVisAttributes(G4VisAttributes(1,G4Colour(0.2,0.3,0.3) ));//vis-here

//       	  new G4PVPlacement(transformV, logicV, volNameV, mother_logic, false, 0, mySurfChk);



//       	  G4String volNameBP = "Beam Pipe";
//       	  double bpIRAD = 2.87*in/2.;
//       	  double bpORAD = 3.00*in/2.;
//       	  double bpLength = 6.00*in;
//       	  G4VSolid *solidBP = new G4Tubs(volNameBP,bpIRAD,bpORAD,bpLength/2.,0,2*pi);
//       	  G4ThreeVector xyzBP=G4ThreeVector(0,0,-5.523*in); //position
//       	  G4LogicalVolume *logicBP = new G4LogicalVolume(solidBP,matZ,volNameBP,0,0,0);
//       	  logicV->SetVisAttributes(G4VisAttributes(1,G4Colour(0.2,0.3,0.3) ));//vis-here
//       	  new G4PVPlacement(0,xyzBP, logicBP, volNameBP, mother_logic, false, 0, mySurfChk);

//       }

//   }
  return world;
}
