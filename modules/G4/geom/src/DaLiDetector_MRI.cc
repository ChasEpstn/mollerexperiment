///
/// History:
///  Created by Ross Corliss, August 2014

/* Jan's notes for verD
   trig scint is defined around line 80

   proton detector is defined around line 220
  
   gem detector is defined around line 320
   _verD is using this model: GetNewTripleGem(..)
   which is defined around line 410-550, the frame color is set in line 530

   details of GEM implementation around line 460
 */

#include "DaLiDetectorConstruction.h"

#include "G4NistManager.hh"
#include "G4Tubs.hh"
#include "G4Box.hh"
#include "G4Polycone.hh"
#include "G4String.hh"
#include <cmath>
#include <string>

#include "G4VisAttributes.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "UniversalSD.h"

//===================================== 
//===================================== 
void  DaLiDetectorConstruction::assemble_trigger_paddles_mri(G4LogicalVolume   * mother_logic=0){

 //calculate Z of the start of the target:
  //from the center of the gas volume, go upstream half the length of the target volume, then go downstream the length of the straw, and of the wall holding it.
  double effectiveTargetOffset=-230*mm-1860/2.*mm+854*mm+3*cm+2*mm;
  double eto=effectiveTargetOffset;//easier to add it if it's a shorter name.
  
  // define fiducial volume 
  double fidLen=776*mm; // total length of tracker fiducial volume



   if ( geomUse.version=="verB"){
      //create trigger paddles:
      G4Material* scintillator =fNist_man->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE");
      assert(scintillator);
      int nScint=2;
      double scintRadiusB[]={180*mm,220*mm};
      double scintHeightB[]={280*mm,280*mm};
      double scintLengthB[]={600*mm,600*mm};
      double scintHOffsetB[]={0*mm,0*mm};
      double scintThickB=5*mm;
      printf("assemble_lepton_tracker: Ross's MRI trigger paddles\n");

      for(int ic=0; ic<nScint; ic++) { //NP
	double halfLen=scintLengthB[ic]/2.;
	double halfHeight=scintHeightB[ic]/2.;
	double halfThick=scintThickB/2;
	double y=scintHOffsetB[ic];
	G4String volName=Form("trig%i",ic+1);
	G4double x=scintRadiusB[ic];
	G4Material*  matV= scintillator;
	x+=halfThick;
	printf("assemble_lepton_tracker: add trigger %s  Rin/mm=%.1f L/mm=%.1f\n",volName.data(),x/mm,2*halfLen/mm);
	for (int j=0;j<2;j++) { // j=0 right, j=1 left
	  if (j){
	    x=-1*x;
	    volName=volName+"L";
	  }	  
	  else {
	    volName=volName+"R";
	  }
	  G4VSolid *solidV = new G4Box(volName+"S",halfThick, halfHeight, halfLen);
	  //the old center:G4ThreeVector xyz=G4ThreeVector(x,y, -fidLen/2+halfLen+globalOffset);
	  G4ThreeVector xyz=G4ThreeVector(x,y, eto+halfLen);
	  G4LogicalVolume * logicV=new G4LogicalVolume(solidV, matV,volName+"L",0,0,0);
	  logicV->SetVisAttributes(G4VisAttributes(1,G4Colour(0.2,0.9-ic/10.,0.2+ic*0.3) ));//vis-here
	  new G4PVPlacement(0,xyz, logicV, volName, mother_logic, false, 0, mySurfChk);
	  logicV->SetSensitiveDetector( mySD );
	  printf(" %s is uniSD\n",volName.data());
	} //end of right-left loop
      } //end-of-plane loop
    }
   else if ( geomUse.version=="verC" || geomUse.version=="verD"){
    //create trigger paddles:
    G4Material* scintillator =fNist_man->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE");
    assert(scintillator);

    
    int nScint=0;
    double *scintRadius;
    double *scintWidth;
    double *scintLength;
    double *scintThick;
    double *scintHOffset;
    double *scintLOffset;
    double *scintRot;
    const G4String *scintName;
    //double scintThick=8*mm;
    double globalOffset=88*mm;

    if (geomUse.version=="verC"){
    const int nScintC=4;
    double scintRadiusC[]={230*mm,230*mm,96*mm,96*mm};
    double scintWidthC[]={280*mm,280*mm,130*mm,130*mm};
    double scintLengthC[]={400*mm,400*mm,400*mm,400*mm};
    double scintThickC[]={8*mm,8*mm,8*mm,8*mm};
    double scintHOffsetC[]={0*mm,0*mm,0*mm,0*mm};
    double scintLOffsetC[]={0*mm,0*mm,0*mm,0*mm};
    double scintRotC[]={0*degree, 180*degree,90*degree, 270*degree};
    static const G4String scintNameC[]={"sciA","sciB","sciC","sciD"};
    nScint=nScintC;
    scintRadius=scintRadiusC;
    scintWidth=scintWidthC;
    scintLength=scintLengthC;
    scintHOffset=scintHOffsetC;
    scintLOffset=scintLOffsetC;
    scintRot=scintRotC;
    scintName=scintNameC;
    scintThick=scintThickC;
    }else if (geomUse.version=="verD"){
    const int nScintD=6;
    double scintRadiusD[]={64*mm,151*mm,154*mm, 64*mm,151*mm,154*mm};
    double scintWidthD[]={12*cm,12*cm,12*cm,12*cm,12*cm,12*cm};
    double scintThickD[]={0.25*mm,1*mm,1*mm, 0.25*mm,1*mm,1*mm};
    double scintLengthD[]={400*mm,400*mm,400*mm,400*mm,400*mm,400*mm};
    double scintHOffsetD[]={0*mm,0*mm,0*mm,10*mm,10*mm,10*mm};
    //two lines copied from below, make sure to change it also for GEMs
    double stackDz=3*cm; //each 3GEM stack is offset from the z position of the previous by this.
    double firstLayerZ=10*cm; // position of closest gem to beam pipe

    double scintLOffsetD[]={firstLayerZ,firstLayerZ+stackDz*3,firstLayerZ+stackDz*3,
			    firstLayerZ,firstLayerZ+stackDz*3,firstLayerZ+stackDz*3};
    double scintRotD[]={0*degree,0*degree,0*degree,
			180*degree,180*degree,180*degree};
    static const G4String scintNameD[]={"sciA1","sciA2","sciA3",
					"sciB1","sciB2","sciB3"};
    nScint=nScintD;
    scintRadius=scintRadiusD;
    scintWidth=scintWidthD;
    scintLength=scintLengthD;
    scintHOffset=scintHOffsetD;
    scintLOffset=scintLOffsetD;
    scintRot=scintRotD;
    scintName=scintNameD;
    scintThick=scintThickD;
    }else { printf("Unrecognized version=%s.  Scintillators not built.\n",geomUse.version.Data()); return;}
 
    
    for(int i=0; i<nScint; i++) { //NP
	double halfLen=scintLength[i]/2.;
	double halfWidth=scintWidth[i]/2.;
	double halfThick=scintThick[i]/2;
	double r=scintRadius[i]+halfThick;
	double z=scintLOffset[i]+halfLen;
	double angle=scintRot[i];
	//y+=halfThick;
	printf("assemble_trigger_paddles_mri: add scint %s  Rin/mm=%.1f L/mm=%.1f\n",
	       scintName[i].data(),r/mm,scintLength[i]);

	
	G4VSolid *solidV = new G4Box(scintName[i].data(),halfWidth,halfThick, halfLen);
	G4ThreeVector axis=G4ThreeVector(0,0,1); //rotation around z axis.
	G4RotationMatrix* rot=new G4RotationMatrix(axis,-angle);
	G4ThreeVector xyzbase=G4ThreeVector(scintHOffset[i],
					    r,
					    eto+z);
	G4ThreeVector xyz=xyzbase.rotateZ(angle);
	G4LogicalVolume * logicV=new G4LogicalVolume(solidV, scintillator,scintName[i].data(),0,0,0);
	logicV->SetVisAttributes(G4VisAttributes(1,G4Colour(0.2,0.8,0.2) ));//vis-here
	new G4PVPlacement(rot,xyz, logicV, scintName[i].data(), mother_logic, false, 0, mySurfChk);
	logicV->SetSensitiveDetector( mySD );
	printf(" %s is uniSD\n",scintName[i].data());
    } //end-of-plane loop


    if (geomUse.version=="verC"){
    //upstream scint:
    double upIR=85*mm;
    double upOR=21*cm;
    double upThick=8*mm;
    G4String volName="sciE";
    G4VSolid *solidV =  new G4Tubs(volName+"S", upIR, upOR, upThick/2, 0, 2*pi*rad);
    G4ThreeVector xyz=G4ThreeVector(0,0, -fidLen/2+upThick/2+globalOffset-50*mm);
    G4LogicalVolume * logicV=new G4LogicalVolume(solidV, scintillator,volName+"L",0,0,0);
    logicV->SetVisAttributes(G4VisAttributes(1,G4Colour(0.2,0.9,0.2) ));//vis-here
    new G4PVPlacement(0,xyz, logicV, volName, mother_logic, false, 0, mySurfChk);
    logicV->SetSensitiveDetector( mySD );
    }
  }
}



//===================================== 
//===================================== 
void  DaLiDetectorConstruction::assemble_proton_detector_mri(G4LogicalVolume   * mother_logic=0){

  //proton detector lives inside the target, so relative coordinates are different from the other detectors.
  //from the center of the gas volume, go upstream half the length of the target volume, then go downstream the length of the straw, and of the wall holding it.
  double effectiveTargetOffset=-1860/2.*mm+854*mm+3*cm+2*mm;
  double eto=effectiveTargetOffset;//easier to add it if it's a shorter name.

  if (geomUse.version=="verA" || geomUse.version=="verB"){
    printf(" NO proton detector of MRI and geomUse.version=%s\n",geomUse.version.Data());
    return;
  }

  assert(geomUse.version=="verC" || geomUse.version=="verD");
  

  double protIR=6.7*cm;
  double protWidth=4*cm;
  double protLength=76.9*6*mm;
  double protRotation=185*degree;
  double protHOffset=32*mm;//still a guess
  double protLOffset=0*mm;
  
  if ( !(geomUse.version=="verC")) {  // Rbeam=6cm
    protIR=5.4*cm;
    protLength=4*76.9*mm;
    protHOffset=0*mm;
  }

  if (geomUse.version=="verE"){
    protRotation=-90*degree;
  }
  
  G4String protName=Form("pro");//
  G4LogicalVolume *protL=GetNewSiPlane(protName,protWidth,protLength);
  G4ThreeVector axis=G4ThreeVector(0,0,1); //define rotation around z axis.
  G4RotationMatrix* rot=new G4RotationMatrix(axis,-protRotation);//rotation about the axis in its own local coords.
  G4ThreeVector xyzbase=G4ThreeVector(protHOffset,protIR,eto+protLength/2+protLOffset);
  G4ThreeVector xyz=xyzbase.rotateZ(protRotation);
  
  protL->SetVisAttributes(G4VisAttributes(0,G4Colour(0.8,0.2,0.2) ));//vis-here
  
  new G4PVPlacement(rot,xyz,protL,protName,mother_logic,false,1,mySurfChk);
 
}

//===================================== 
//===================================== 
void  DaLiDetectorConstruction::assemble_lepton_tracker_mri(G4LogicalVolume   * mother_logic=0){
  printf("DetectorBuilder::assemble_lepton_tracker(%s) w/ sensitive cylinders,  START ... \n", geomUse.version.Data());

  //calculate Z of the start of the target:
  //from the center of the gas volume, go upstream half the length of the target volume, then go downstream the length of the straw, and of the wall holding it.
  double effectiveTargetOffset=-230*mm-1860/2.*mm+854*mm+3*cm+2*mm;
  double eto=effectiveTargetOffset;//easier to add it if it's a shorter name.
  
  // define fiducial volume 
  double fidOR=27*cm; // most outer limit
  double fidIR=8.6*cm; // most inner limit
  double fidLen=776*mm; // total length of tracker fiducial volume


  G4LogicalVolume   * gas_logic=mother_logic;
  double globalOffset=88*mm;
  if(0){//.... lepton tracker fiducial volume
    G4String volName="lepTrack";
    G4Material*  matV=fNist_man->FindOrBuildMaterial("G4_AIR");
    if (geomUse.worldAir==0) matV=fNist_man->FindOrBuildMaterial("G4_Galactic");
    assert(matV );
    G4VSolid *solidV = new G4Tubs(volName+"S", fidIR, fidOR, fidLen/2, 0, 2*pi*rad);

    G4ThreeVector xyz=G4ThreeVector(0,0,88*mm);
    G4LogicalVolume * logicV=new G4LogicalVolume(solidV, matV,volName+"L",0,0,0);
    new G4PVPlacement(0,xyz, logicV, volName, mother_logic, false, 0, mySurfChk);
    logicV->SetVisAttributes(G4VisAttributes(1, G4Colour(0.2,0.3,0.3)));//vis-here
    gas_logic=logicV;
    globalOffset=0;
  }
  


   // sensitive tracker is simplified as a set of flat tie-fighter planes made of the same gas
  int nPlanes=0;
  double *flatIRplane; //array of the inner radius of each plane. dist. from x=y=0 at point of closest approach
  double *flatLplane; //full length of plane in z
  double *flatHplane; //full height of plane in y
  double *flatHoffset; //y position of center of plane vertical offset of plane from y=0.
  double *flatLoffset;
  double *flatRot;
  const G4String *flatName;
  bool useDetailedGEMs=false; //whether to use the 4-layered approximation, or a single-layer approximation.

  if(geomUse.version=="verA" ||geomUse.version=="verB" ||geomUse.version=="verC" ||geomUse.version=="verD") {
    if (geomUse.version=="verA"){
      const int numPlanesVerA=8;
      double irVerA[]={145*mm, 170*mm, 195*mm, 220*mm,
		       145*mm, 170*mm, 195*mm, 220*mm};
      double lVerA[]={ 600*mm, 600*mm, 600*mm, 600*mm,
		       600*mm, 600*mm, 600*mm, 600*mm};
      double hVerA[]={ 280*mm, 280*mm, 280*mm, 280*mm,
		       280*mm, 280*mm, 280*mm, 280*mm};
      double offVerA[]={ 0*mm,   0*mm,   0*mm,   0*mm,
			 0*mm,   0*mm,   0*mm,   0*mm};
      double rotVerA[]={90*degree,90*degree,90*degree,90*degree,
			270*degree,270*degree,270*degree,270*degree};
      static const G4String nameVerA[]={"lepCy1R","lepCy2R","lepCy3R","lepCy4R",
			   "lepCy1L","lepCy2L","lepCy3L","lepCy4RL"};
      flatIRplane=irVerA;
      flatLplane=lVerA;
      flatHplane=hVerA;
      flatHoffset=offVerA;
      flatLoffset=offVerA;
      flatRot=rotVerA;
      flatName=nameVerA;
      nPlanes=numPlanesVerA;
      useDetailedGEMs=false;
    }else if (geomUse.version=="verB"){
      printf("assemble_lepton_tracker: Ross's MRI modified geom11 with 4 partial planes\n");
      const int numPlanesVerB=8;
      double irVerB[]={100*mm, 120*mm, 140*mm, 160*mm,
		       100*mm, 120*mm, 140*mm, 160*mm};
      double lVerB[]={ 600*mm, 600*mm, 600*mm, 600*mm,
		       600*mm, 600*mm, 600*mm, 600*mm};
      double hVerB[]={ 140*mm, 150*mm, 160*mm, 170*mm,
		       140*mm, 150*mm, 160*mm, 170*mm};
      double offVerB[]={ 0*mm,   0*mm,   0*mm,   0*mm,
			 0*mm,   0*mm,   0*mm,   0*mm};
      double rotVerB[]={90*degree,90*degree,90*degree,90*degree,
			270*degree,270*degree,270*degree,270*degree};
      static const G4String nameVerB[]={"lepCy1R","lepCy2R","lepCy3R","lepCy4R",
			   "lepCy1L","lepCy2L","lepCy3L","lepCy4RL"};
      flatIRplane=irVerB;
      flatLplane=lVerB;
      flatHplane=hVerB;
      flatHoffset=offVerB;
      flatLoffset=offVerB; //caution -- using the zeros twice!
      flatRot=rotVerB;
      flatName=nameVerB;
      nPlanes=numPlanesVerB;
      useDetailedGEMs=false;
     }else if (geomUse.version=="verC"){
       printf("assemble_lepton_tracker: MRI modified geom11 with planes packed close to beamline \n");
      const int numPlanesVerC=8;
      //thickness=2cm, spacing=1cm, OD of pipe=7.6cm ==> start =9.6cm
      double irVerC[]={96*mm, 126*mm, 156*mm, 186*mm,
		       96*mm, 126*mm, 156*mm, 186*mm};//centerpoint, as written
      double lVerC[]={ 410*mm, 410*mm, 410*mm, 410*mm,
		       410*mm, 410*mm, 410*mm, 410*mm};
      double hVerC[]={ 240*mm, 240*mm, 240*mm, 240*mm,
		       240*mm, 240*mm, 240*mm, 240*mm};
      double offVerC[]={ 0*mm,   0*mm,   0*mm,   0*mm,
			 0*mm,   0*mm,   0*mm,   0*mm};
      double offLVerC[]={ 0*mm,   0*mm,   0*mm,   0*mm,
			 0*mm,   0*mm,   0*mm,   0*mm};
      double rotVerC[]={0*degree,0*degree,0*degree,0*degree,
			180*degree,180*degree,180*degree,180*degree};
      static const G4String nameVerC[]={"lepA1","lepA2","lepA3","lepA4",
			   "lepB1", "lepB2", "lepB3", "lepB4"};
      flatIRplane=irVerC;
      flatLplane=lVerC;
      flatHplane=hVerC;
      flatHoffset=offVerC;
      flatLoffset=offLVerC;
      flatRot=rotVerC;
      flatName=nameVerC;
      nPlanes=numPlanesVerC;
      useDetailedGEMs=true;
     } else if (geomUse.version=="verD"){
       printf("assemble_lepton_tracker: MRI modified geom11 with Rbeam=6cm\n");
      const int numPlanesVerD=8;
      // DCA to beam
      //two lines copied from below, make sure to change it also for TRIG-scint
      double stackDz=3*cm; //each 3GEM stack is offset from the z position of the previous by this.
      double firstLayerZ=10*cm; // position of closest gem to beam pipe
      
      double irVerD[]={76*mm, 97*mm, 118*mm, 139*mm,
		       76*mm, 97*mm, 118*mm, 139*mm};
      // active area length
      double lVerD[]={ 400*mm, 400*mm, 400*mm, 400*mm,
		       400*mm, 400*mm, 400*mm, 400*mm};
      // active area width
      double hVerD[]={ 120*mm, 120*mm, 120*mm, 120*mm,
		       120*mm, 120*mm, 120*mm, 120*mm};
      double offVerD[]={ 0*mm,   0*mm,   0*mm,   0*mm, //A=top
			 10*mm,   10*mm,   10*mm,   10*mm}; // B=bottom
      double offLVerD[]={ firstLayerZ, firstLayerZ+stackDz, firstLayerZ+stackDz*2, firstLayerZ+stackDz*3,
			  firstLayerZ, firstLayerZ+stackDz, firstLayerZ+stackDz*2, firstLayerZ+stackDz*3};
      double rotVerD[]={0*degree,0*degree,0*degree,0*degree,
			180*degree,180*degree,180*degree,180*degree}; //+x chamber
      static const G4String nameVerD[]={"lepA1","lepA2","lepA3","lepA4",
			   "lepB1", "lepB2", "lepB3", "lepB4"};
      flatIRplane=irVerD;
      flatLplane=lVerD;
      flatHplane=hVerD;
      flatHoffset=offVerD;
      flatLoffset=offLVerD;
      flatRot=rotVerD;
      flatName=nameVerD;
      nPlanes=numPlanesVerD;
      useDetailedGEMs=true;
    }else {printf("Invalid  DL geomMRI ver=%s, abort\n",geomUse.version.Data()); assert(34==98);}
  
    // sensitive tracker is simplified as a set of flat tie-fighter planes made of the same gas
    assert(gas_logic);


    
    if (useDetailedGEMs){
      printf("building detailed GEMs.....\n");
    }else{
      printf("building simplified GEMs.....\n");
    }
    
    G4LogicalVolume *gemL;
    for (int i=0;i<nPlanes;i++){
      if (useDetailedGEMs){
	gemL=GetNewTripleGem(flatName[i],flatHplane[i],flatLplane[i]);
      } else {
	gemL=GetNewSimpleGem(flatName[i],flatHplane[i],flatLplane[i]);
      }

      double angle=flatRot[i];
      double r=flatIRplane[i];
      double z=flatLoffset[i]+flatLplane[i]/2;
      gemL->SetVisAttributes(G4VisAttributes(0,G4Colour(0.1,0.1,0.1) ));//hide enclosing volume.
      
      G4ThreeVector axis=G4ThreeVector(0,0,1); //rotation around z axis.
      G4RotationMatrix* rot=new G4RotationMatrix(axis,-angle);
      G4ThreeVector xyzbase=G4ThreeVector(flatHoffset[i],r,eto+z);
      G4ThreeVector xyz=xyzbase.rotateZ(angle);
      new G4PVPlacement(rot,xyz,gemL,flatName[i],gas_logic,false,1,mySurfChk);
    }
  }

}


				    
  
					       
G4LogicalVolume * DaLiDetectorConstruction::GetNewTripleGem(G4String gemName, double gemWidth, double gemLength){
  //printf("Trying to build a new TripleGem...\n");
  //builds a triple-gem approximation, centered at (0,0,0) and lying in the XZ plane

  double supportThickness=5*mm; //original phase1 guess was (4.88+3.36)*mm;//from the 12.88 design of the FGT.
  double activeThickness=3.00*mm;
  double gemsThickness=6.12*mm;
  double readoutThickness=2.88*mm; //increased to account for gas volume boundaries etc.  Probably overestimated.

  double alFrameWidth=10*mm;
  double gasFrameSpacing=10*mm;
  double g10FrameWidth=30*mm;

  //  double fullWidth=gemWidth+2*frameWidth+2*airFrameWidth+2*alFrameWidth;
  //  double fullLength=gemLength+2*frameWidth+2*airFrameWidth+2*alFrameWidth;

  G4Material* aluminum=fNist_man->FindOrBuildMaterial("Al_100"); assert(aluminum);
  G4Material* nomex=fNist_man->FindOrBuildMaterial("GemNOMEX"); assert(nomex);
  G4Material* ArCO2=fNist_man->FindOrBuildMaterial("ArCO2Mix90:10"); assert(ArCO2);
  G4Material* gemfoils=fNist_man->FindOrBuildMaterial("GemMix"); assert(gemfoils);
  G4Material* gemrdo=fNist_man->FindOrBuildMaterial("GemReadMix"); assert(gemrdo);
  G4Material* g10=fNist_man->FindOrBuildMaterial("NemaG10"); assert(g10);
  G4Material* air=fNist_man->FindOrBuildMaterial("G4_AIR"); assert(air);
  //printf("Got all materials needed.\n");

  G4Colour brightFrame(0.9,0.9,0.9);
  G4Colour dimFrame(0.3,0.3,0.3);


  //layers:
  const int nLayers=7;//how many layers not counting frames
  G4Material* mat[]={nomex,ArCO2,ArCO2,ArCO2,gemfoils,ArCO2,gemrdo};
  static const G4String suffix[]={"gap","gap","active","gap","gems","gap","readout"};
  double thickness[]={supportThickness,1*mm,activeThickness,1*mm,gemsThickness,1*mm,readoutThickness};
  
  double sumThickness=0;
  //sum all the stacked layers' thicknesses to compute mother volume:
  for (int i=0;i<nLayers;i++){
    sumThickness+=thickness[i];
  }

  //frames:
  const int nFrames=3; //how many nested layers of frame
  G4Material* frameMat[]={g10,ArCO2,aluminum};
  double frameWidth[]={g10FrameWidth,gasFrameSpacing,alFrameWidth};
  //static const G4String frameSuffix[]={"g10frame","gasvol","alframe"};
  
  double sumWidth=gemWidth;
  double sumLength=gemLength;
  //sum all the frame widths to compute mother volume:
  for (int i=0;i<nLayers;i++){
    sumWidth+=2*frameWidth[i];
    sumLength+=2*frameWidth[i];
  }



  
   //mother volume contains all of the parts:
  G4LogicalVolume *gemMotherL=new G4LogicalVolume(new G4Box(gemName,sumWidth/2.0,sumThickness/2.0,sumLength/2.0),fNist_man->FindOrBuildMaterial("G4_AIR"),gemName,0,0,0);



  
  //create and position the layers:
  G4LogicalVolume *gemL[nLayers];
  G4PVPlacement *gemP[nLayers];
  G4VSolid *gemS; //only need one pointer. 
  G4ThreeVector xyz;
  double runningTotalThickness=0;
  for (int i=0;i<nLayers;i++){
    if (suffix[i]=="gap"){
     runningTotalThickness+=thickness[i];
    }else{
      //printf("building %s\n",suffix[i].data());
    gemS=new G4Box(gemName+suffix[i],gemWidth/2.0,thickness[i]/2.0,gemLength/2.0);
    gemL[i]=new G4LogicalVolume(gemS,mat[i],gemName+suffix[i],0,0,0);
    gemL[i]->SetVisAttributes(G4VisAttributes(1, G4Colour(0.4,0.1,0.1)));//color etc
    xyz=G4ThreeVector(0,-sumThickness/2.0+runningTotalThickness+thickness[i]/2.0,0);
    //y pos= bottom edge + total thickness of previous layers + half thickness of current layer
    runningTotalThickness+=thickness[i];
    gemP[i]=new G4PVPlacement(0,xyz,gemL[i],gemName+suffix[i],gemMotherL,false,1,mySurfChk);
    }
  }
  //highlight the active layer and set it to readout hits:
  printf("activating active layer=%s%s\n",gemName.data(),suffix[2].data());
  gemL[2]->SetVisAttributes(G4VisAttributes(1, G4Colour(0.8,0.2,0.2)));//make active region brighter
  gemL[2]->SetSensitiveDetector(mySD);//switch this to myElosSD for energy loss data instead of tracking.



  
  //create and position the frames:
  //printf("adding frames\n");
  G4LogicalVolume *frameL;
  G4VSolid *frameS;
  G4PVPlacement *frameP;
  double runningTotalWidth=gemWidth;
  double runningTotalLength=gemLength;
  G4Colour gColFr=G4Colour(0.1,0.3,0.3);

  for (int i=0;i<nFrames;i++){
    //Z=const (width = gemWidth+two frame lengths)
    frameS=new G4Box(gemName+"frame",runningTotalWidth/2.0+frameWidth[i],sumThickness/2.0,frameWidth[i]/2.0);
    frameL=new G4LogicalVolume(frameS,frameMat[i],gemName+"frame",0,0,0);
    frameL->SetVisAttributes(G4VisAttributes(1, gColFr));//color etc
    //z=negative
    xyz=G4ThreeVector(0,0,(runningTotalLength+frameWidth[i])/2.0);
    frameP=new G4PVPlacement(0,xyz,frameL,gemName+"frame",gemMotherL,false,1,mySurfChk);
    //z=positive
    xyz=G4ThreeVector(0,0,-(runningTotalLength+frameWidth[i])/2.0);
    frameP=new G4PVPlacement(0,xyz,frameL,gemName+"frame",gemMotherL,false,1,mySurfChk);
    
    //X=const (length=gemLength with no additional)
    frameS=new G4Box(gemName+"frame",frameWidth[i]/2.0,sumThickness/2.0,runningTotalLength/2.0);
    frameL=new G4LogicalVolume(frameS,frameMat[i],gemName+"frame",0,0,0);
    frameL->SetVisAttributes(G4VisAttributes(1,  gColFr));//color etc
    //x=negative
    xyz=G4ThreeVector((runningTotalWidth+frameWidth[i])/2.0,0,0);
    frameP=new G4PVPlacement(0,xyz,frameL,gemName+"frame",gemMotherL,false,1,mySurfChk);
    //x=positive
    xyz=G4ThreeVector(-(runningTotalWidth+frameWidth[i])/2.0,0,0);
    frameP=new G4PVPlacement(0,xyz,frameL,gemName+"frame",gemMotherL,false,1,mySurfChk);

    runningTotalWidth+=2*frameWidth[i];
    runningTotalLength+=2*frameWidth[i];
  }

    
  return gemMotherL;
}


G4LogicalVolume *DaLiDetectorConstruction::GetNewSimpleGem(G4String gemName, double gemWidth, double gemLength){

  //G4String gemName=cgemName;
  double halfLen=gemLength/2.;
  double halfWidth=gemWidth/2;
  

  G4Material* matVsens=fNist_man->FindOrBuildMaterial("HeIsoButane80:20"); assert(matVsens );
  G4Material*  matVdead=fNist_man->FindOrBuildMaterial("GemReadMix"); assert(matVdead );
  G4Material* air=fNist_man->FindOrBuildMaterial("G4_AIR"); assert(air);

  //simplified design of gems:
  double delRsens=3.*mm; // thickness of sensitive layer
  double delRdead=2*mm; // thickness of dead  layer
  double delRoffset=5*mm; // offset between dead & sens layer front faces

  const int nLayers=3;
  G4Material *mat[]={matVsens,air,matVdead};
  double thick[]={delRsens,delRoffset,delRdead};
  static const G4String suffix[]={"","gap","dead"};

  double sumThickness=0;
  for (int i=0;i<nLayers;i++)
    sumThickness+=thick[i];


  //mother volume contains all of the parts:
  G4LogicalVolume *gemMotherL=new G4LogicalVolume(new G4Box(gemName,halfWidth,sumThickness/2,halfLen),fNist_man->FindOrBuildMaterial("G4_AIR"),gemName,0,0,0);


  double runningSum=0;
  for (int i=0;i<nLayers;i++){
    if(suffix[i]=="gap" || (suffix[i]=="dead" && geomUse.leptDead==0)){
      //skip blank layers
      runningSum+=thick[i];
    } else{
      G4VSolid *solidV = new G4Box(gemName+suffix[i],halfWidth,thick[i]/2,halfLen);
      G4LogicalVolume * logicV=new G4LogicalVolume(solidV, mat[i],gemName+"L",0,0,0);
      G4ThreeVector xyz=G4ThreeVector(0,-sumThickness/2.+runningSum+thick[i]/2.,0);
      logicV->SetVisAttributes(G4VisAttributes(1,G4Colour(0.4,0.1,0.1) ));//vis-here
      new G4PVPlacement(0,xyz, logicV, gemName, gemMotherL, false, 0, mySurfChk);
      if(suffix[i]=="")  { logicV->SetSensitiveDetector( mySD ); printf(" %s is uniSD\n",gemName.data()); logicV->SetVisAttributes(G4VisAttributes(1,G4Colour(0.8,0.2,0.2) ));}    
    }
  }

  return gemMotherL;
}


G4LogicalVolume *DaLiDetectorConstruction::GetNewSiPlane(G4String volName,double width,double length){

  //G4String volName=cvolName;
  G4Material* silicon=fNist_man->FindOrBuildMaterial("SiWafer"); assert(silicon );
  G4Material* air=fNist_man->FindOrBuildMaterial("G4_AIR"); assert(air );
  G4Material* aluminum=fNist_man->FindOrBuildMaterial("Al_100"); assert(aluminum );
  G4Material* deadmat=fNist_man->FindOrBuildMaterial("GemReadMix"); assert(deadmat );
  //support?
  double alThick=250*um;
  double spacing=10*um;
  double siThick=300*um;
  double deadThick=2*mm;

  const int nLayers=5;
  double thick[]={alThick,spacing,siThick,spacing,deadThick};
  G4Material *mat[]={silicon,air,aluminum,air,deadmat};
  static const G4String suffix[]={"range","gap","active","gap","dead"};
  //mother volume:
  double sumThickness=0;
  for (int i=0;i<nLayers;i++)
    sumThickness+=thick[i];


  //mother volume contains all of the parts:
  G4LogicalVolume *protMotherL=new G4LogicalVolume(new G4Box(volName,width/2.0,sumThickness/2.0,length/2.0),air,volName,0,0,0);

  double runningSum=0;
  for (int i=0;i<nLayers;i++){
    if(suffix[i]=="gap"){
      runningSum+=thick[i];
    }else {
      G4VSolid *solidV = new G4Box(volName+"S"+suffix[i],width/2,thick[i]/2, length/2);
      G4ThreeVector xyz=G4ThreeVector(0,-sumThickness/2+runningSum+thick[i]/2,0);
      G4LogicalVolume * logicV=new G4LogicalVolume(solidV, mat[i],volName+"L",0,0,0);
      logicV->SetVisAttributes(G4VisAttributes(0,G4Colour(0.0,0.4,0.4) ));//vis-here
      new G4PVPlacement(0,xyz, logicV, volName+suffix[i], protMotherL, false, 0, mySurfChk);
      if(i==2){//special rules for active detector volume:
	logicV->SetVisAttributes(G4VisAttributes(1,G4Colour(0.0,0.9,0.9) ));//vis-here
	logicV->SetSensitiveDetector( mySD );
      }
      runningSum+=thick[i];
    }
  }
  return protMotherL;
}
