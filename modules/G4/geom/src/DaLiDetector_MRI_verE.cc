// DaLiDetector_MRI_verE
// Ross Corliss, 07/2015
//
// To make simulations easier to modify, verE is now split out into its own set of functions
// These still make reference to earlier functions for making the TripleGEM and other objects

#include "DaLiDetectorConstruction.h"

#include "G4NistManager.hh"
#include "G4Tubs.hh"
#include "G4Box.hh"
#include "G4Polycone.hh"
#include "G4String.hh"
#include <cmath>
#include <string>

#include "G4VisAttributes.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "UniversalSD.h"

//===================================== 
//=====================================

  

void DaLiDetectorConstruction::assemble_mri_verE(G4LogicalVolume   * mother_logic=0){

  //in order to properly center this, we have to convert from the expected mother volume coordinate to the upstream edge of the gas target:

  //from the center of the gas volume, go upstream half the length of the target volume, then go downstream the length of the straw, and of the wall holding it.
  double effectiveTargetOffset=-230*mm-1860/2.*mm+854*mm+3*cm+2*mm;
  double eto=effectiveTargetOffset;//easier to add it if it's a shorter name.
  



  //Position GEM+scint stacks:
  G4LogicalVolume *stack[]={GetNewStackVerE("A",1*cm),GetNewStackVerE("B",-1*cm)};
  G4ThreeVector axis=G4ThreeVector(0,0,1);
  G4RotationMatrix* rot=new G4RotationMatrix(axis,45*degree);
  G4ThreeVector xyz=G4ThreeVector(-8*cm,20*cm,eto+40*cm/2+14.5*cm);//4.5*cm from stack center to center of first layer + 10*cm downstream
  new G4PVPlacement(rot,xyz,stack[0],"stackA",mother_logic,false,1,mySurfChk);
  rot=new G4RotationMatrix(axis,-225*degree);
  xyz=G4ThreeVector(-8*cm,-20*cm,eto+40*cm/2+24.5*cm);
  new G4PVPlacement(rot,xyz,stack[1],"stackB",mother_logic,false,1,mySurfChk);
}


  
void DaLiDetectorConstruction::assemble_mri_verE_inside_target(G4LogicalVolume *mother_logic=0){

  //proton detector lives inside the target, so relative coordinates are different from the other detectors.
  //from the center of the gas volume, go upstream half the length of the target volume, then go downstream the length of the straw, and of the wall holding it.
  double effectiveTargetOffset=-1860/2.*mm+854*mm+3*cm+2*mm;
  double eto=effectiveTargetOffset;//easier to add it if it's a shorter name.

  double protWidth=4*cm;
  double protLength=76.9*4*mm;
  double protRotation=-90*degree;

  G4String protName=Form("pro");//
  G4LogicalVolume *protL=GetNewSiPlane(protName,protWidth,protLength);
  G4ThreeVector axis=G4ThreeVector(0,0,1); //define rotation around z axis.
  G4RotationMatrix* rot=new G4RotationMatrix(axis,-protRotation);//rotation about the axis in its own local coords.
  G4ThreeVector xyz=G4ThreeVector(5.4*cm,0,eto+protLength/2+0*cm);
  protL->SetVisAttributes(G4VisAttributes(0,G4Colour(0.8,0.2,0.2) ));//vis-here
  
  new G4PVPlacement(rot,xyz,protL,protName,mother_logic,false,1,mySurfChk);

  return;
}


G4LogicalVolume *DaLiDetectorConstruction::GetNewStackVerE(G4String stackName, double scintOffset){

  G4Material* scintillator =fNist_man->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE");
  assert(scintillator);


  double gemWidth=12*cm, gemLength=40*cm;//not including 3cm frames on each side
  double gemFrame=5*cm;
  double scintWidth=12*cm, scintLength=40*cm;
  
  double thickness[]={0.25*mm,2*cm,2*cm,2*cm,2*cm,1*mm};
  double relZ[]={-6*cm,-4.5*cm,-1.5*cm,1.5*cm,4.5*cm,3*cm};
  double relX[]={scintOffset,0,0,0,0,scintOffset};
  
  G4ThreeVector boxSize(gemWidth+gemFrame*2,10.125*cm,gemLength+gemFrame*2+9*cm);
  G4LogicalVolume *motherL=new G4LogicalVolume(new G4Box(stackName,boxSize.x()/2,boxSize.y()/2,boxSize.z()/2),fNist_man->FindOrBuildMaterial("G4_AIR"),stackName,0,0,0);
  motherL->SetVisAttributes(G4VisAttributes(0,G4Colour(0.1,0.1,0.1)));
  G4LogicalVolume *childL;

  int n=0;
  double bottomY=-boxSize.y()/2;
  G4String volName;
  
  volName=Form("sci%s1",stackName.data());
  childL=new G4LogicalVolume(new G4Box(volName,scintWidth/2,thickness[n]/2,scintLength/2), scintillator,volName,0,0,0);
  childL->SetVisAttributes(G4VisAttributes(1,G4Colour(0.2,0.8,0.2) ));//vis-here
  new G4PVPlacement(0,G4ThreeVector(relX[n],bottomY+thickness[n]/2,relZ[n]),childL,volName,motherL,false,1,mySurfChk);
  childL->SetSensitiveDetector( mySD );
 
  bottomY+=thickness[n]; n++;

  bottomY+=1*cm; //1 cm spacing between scint and dets

  for (;n<5;){
    volName=Form("lep%s%d",stackName.data(),n);
    childL=GetNewTripleGem(volName,gemWidth,gemLength);
    childL->SetVisAttributes(G4VisAttributes(1,G4Colour(0.1,0.1,0.1) ));//visibility and colors of inner layers are set in GetNewTripleGem
   new G4PVPlacement(0,G4ThreeVector(relX[n],bottomY+thickness[n]/2,relZ[n]),childL,volName,motherL,false,1,mySurfChk);
   bottomY+=thickness[n]; n++;
  }
  bottomY+=1*cm; //1 cm spacing between scint and dets

  volName=Form("sci%s2",stackName.data());
  childL=new G4LogicalVolume(new G4Box(volName.data(),scintWidth/2,thickness[n]/2,scintLength/2), scintillator,volName,0,0,0);
  childL->SetVisAttributes(G4VisAttributes(1,G4Colour(0.2,0.8,0.2) ));//vis-here
  new G4PVPlacement(0,G4ThreeVector(relX[n],bottomY+thickness[n]/2,relZ[n]),childL,volName,motherL,false,1,mySurfChk);
  childL->SetSensitiveDetector( mySD );

  bottomY+=thickness[n]; n++;

  return motherL;
}
