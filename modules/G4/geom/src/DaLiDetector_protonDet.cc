#include "DaLiDetectorConstruction.h"

#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"


#include "G4VisAttributes.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "UniversalSD.h"

//===================================== 
//===================================== 
void  DaLiDetectorConstruction::assemble_proton_detector(G4LogicalVolume   * mother_logic=0){
  printf("DetectorBuilder::assemble_proton_detector((),  START ... \n");
  if ( geomUse.protDet==0) { printf(" proton_detector dropped\n"); return; }
  double halfLen=25*cm; // half length of the proton detector
  double IR=7.0*cm; // inner radii
  double alThick=250*um; // thickens of diluted Al to range out protons
  double siThick=300*um; // thickens of silicon detector
  
  double deadThick=2*mm; // thickens of dead material for cables
  double zOff=23.*cm; // now prot-det is centered at Z=0
  
  if(1) { // sensitive cylinder & dead cylinder
    assert(mother_logic);
    G4Material*  matVrange=fNist_man->FindOrBuildMaterial("Al_100"); assert(matVrange );
    G4Material*  matVsens=fNist_man->FindOrBuildMaterial("G4_Si"); assert(matVsens );
    if(myDebug) {G4cout << G4endl << "proton sensor : " << matVsens<< G4endl;}
    G4Material*  matVdead=fNist_man->FindOrBuildMaterial("GemReadMix"); assert(matVdead );
    for (int k=0;k<3; k++) { // from inside-->out k=0 Al-absorber, k=1 sens-Si, 2= cables=dead
      G4String volName="protRange";
      G4double r1=IR+1*mm, r2=r1+alThick;
      G4Material*  matV= matVrange;
      if(k==1) { // sense layer
	r1=IR+2*mm;  r2=r1+siThick;  volName="proCy"; matV= matVsens;	}
      if(k==2) { // dead+cables layer
	r1=IR+3*mm;  r2=r1+deadThick;  volName="protDead"; matV= matVdead;	}
      G4VSolid *solidV = new G4Tubs(volName+"S",r1, r2, halfLen, 0, 2*pi*rad);
      G4ThreeVector xyz=G4ThreeVector(0,0, zOff);
      G4LogicalVolume * logicV=new G4LogicalVolume(solidV, matV,volName+"L",0,0,0);
      logicV->SetVisAttributes(G4VisAttributes(1,G4Colour(0.4,0.4,0.2+k*0.3) ));//vis-here
      new G4PVPlacement(0,xyz, logicV, volName, mother_logic, false, 0, mySurfChk);
      if(k==1)  { logicV->SetSensitiveDetector( mySD ); printf(" %s is uniSD\n",volName.data()); }
    }// end-of-dead/sens loop
  }
}

 
