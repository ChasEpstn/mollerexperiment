#include "DaLiDetectorConstruction.h"

#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Polycone.hh"

#include "G4VisAttributes.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"

//===================================== 
//===================================== 
G4LogicalVolume   *  DaLiDetectorConstruction::assemble_target_cell(G4LogicalVolume   * world_logic=0){
  printf("DetectorBuilder::assemble_target_cell((),  START ... \n");

  double h2FullLen=1860*mm; // full length of H2-volume
  double bpFullLen=750*mm; // full length of the pipe
  double ORh2=75*mm; // outer radius of  H2-volume
  double bpZoffset=-31*cm; //offset of center of beam pipe
  
  double IRbp=90*mm; // inner radius of beam pipe
  double ORbp=92*mm; // outer radius of beam pipe
  G4Material *beampipeMat=fNist_man->FindOrBuildMaterial("G4_Be"); 

  if (geomUse.base=="geomMRI" && geomUse.version=="verC" ){
    //before 2015/04/15:
    //IRbp=75*mm;
    //ORbp=76*mm;
    IRbp=94*mm;
    ORbp=95.5*mm;
    ORh2=94*mm;
    bpFullLen=720*mm;
    bpZoffset=-30.5*cm;
    beampipeMat=fNist_man->FindOrBuildMaterial("G4_Al");
  }

  double ORbpThick, bpThickLen[]={250*mm,500*mm};
  if (geomUse.base=="geomMRI" && geomUse.version=="verD"){
    //changed to Rbeam=6mm on 2015/05/11
    IRbp=12.38/2.0*cm;
    ORbp=12.48/2.0*cm;
    ORbpThick=12.70/2.0*cm;
    ORh2=IRbp;
    bpFullLen=350*mm;
    bpZoffset=-27*cm;
    beampipeMat=fNist_man->FindOrBuildMaterial("G4_Al");
  }

  
  double ORaftVac=76.2*mm/2.; // outer radius of aft vaccum Fe-pipe
  double IRaftVac= ORaftVac-2.41*mm ; // inner radius of aft vaccum Fe-pipe
  double LaftVac=854*mm; // lenght of of aft vaccum Fe-pipe
  
  double IRaft=3*mm/2.; // inner radius of aft conductance limiter
  double Laft=3*cm; // length of aft straw

  double strawDR=130*um; // wall thickness of straw reducing gas flow A
  double strawMountDZ=2*mm; // wall thickness of the discs holding straws

  double aftStrawZoffset=-h2FullLen/2.+LaftVac+strawMountDZ;
  double forwStrawZoffset=h2FullLen/2.-20*cm;

    G4LogicalVolume  *logicVgas=0;
 

  /* this is nested target structure consisting G4String ; of:

     abstract  H2 gas pipe volume  which contains
      - Be beam pipe
      - two gradient presssure 10-towers: OD=3mm (aft), OD=3mm (front)
      - two  flow limiting straws, 3, 20?? cm long 
      - two mounting discs for straws, 
  */

  if(1){ //--------  H2 nn-Torr vessel
    G4String volName="targetEnvelope";
    G4Material* matV=fNist_man->FindOrBuildMaterial( "H2gasTarget");   assert(matV );
    G4ThreeVector xyz=G4ThreeVector(0,0,-230*mm);
    
    double halfLen=h2FullLen/2;
    G4VSolid *solidV = new G4Tubs(volName+"S",0, ORh2, halfLen, 0, 2*pi*rad);
    G4LogicalVolume * logicV=new G4LogicalVolume(solidV,matV,volName+"L",0,0,0);
    logicV->SetVisAttributes(G4VisAttributes(1, G4Colour(0.2,0.1,0.2))); //vis-here
    new G4PVPlacement(0,xyz, logicV, volName , world_logic, false, 0, mySurfChk);
    logicVgas=logicV;
  }


  if( 1 &&  geomUse.beamPipe){  //..........  central Al or Be beam pipe
    G4String volName="BeBeam";
    G4Material*  matV=beampipeMat; assert(matV);
    if(myDebug) { G4cout << G4endl << "My Be : " << matV<< G4endl; }
    double halfLen=bpFullLen/2;
    G4VSolid *solidV = new G4Tubs( volName+"S",IRbp, ORbp, halfLen, 0, 2*pi*rad); 
    G4LogicalVolume * logicV=new G4LogicalVolume(solidV,matV,volName+"L",0,0,0);
    logicV->SetVisAttributes(G4VisAttributes(1, G4Colour(0.4,0.5,0.4))); //vis-here
    G4ThreeVector xyz=G4ThreeVector(0,0,+halfLen+bpZoffset);
    new G4PVPlacement(0,xyz, logicV, volName,  world_logic, false, 1, mySurfChk);

    //add two more cylinders to be the thicker sections of pipe:
    if (geomUse.base=="geomMRI" && geomUse.version=="verD"){
      for (int i=0;i<2;i++){
	volName=Form("FeBeam%d",i); 
	solidV= new G4Tubs( volName+"S",IRbp, ORbpThick, bpThickLen[i]/2.0, 0, 2*pi*rad);
	logicV=new G4LogicalVolume(solidV,matV,volName+"L",0,0,0);
	logicV->SetVisAttributes(G4VisAttributes(1, G4Colour(0.4,0.5,0.4))); //vis-here
	xyz=G4ThreeVector(0,0,bpZoffset+halfLen*(i*2)+(i*2-1)*bpThickLen[i]/2.0);
	new G4PVPlacement(0,xyz, logicV, volName,  world_logic, false, 1, mySurfChk);
      }
    }
  } 


  if( 1){  //..........  upstream vaccum pipe w/o nozzle
    G4String volName="aftVaccPipe";
    G4Material*  matV=fNist_man->FindOrBuildMaterial("G4_Fe"); assert(matV); 
    double halfLen=LaftVac/2;
    G4VSolid *solidV = new G4Tubs( volName+"S",IRaftVac, ORaftVac, halfLen, 0, 2*pi*rad); 
    G4LogicalVolume * logicV=new G4LogicalVolume(solidV,matV,volName+"L",0,0,0);
    logicV->SetVisAttributes(G4VisAttributes(1, G4Colour(0.2,0.8,0.5))); //vis-here
    G4ThreeVector xyz=G4ThreeVector(0,0,-503*mm);   
    new G4PVPlacement(0,xyz, logicV, volName, logicVgas, false, 1, mySurfChk);
  } 

  if(1){ //--------   upstream vacuum drum
    G4String volName="aftVacDrum";
    G4Material* matV=fNist_man->FindOrBuildMaterial( "G4_Galactic");   assert(matV );
    G4ThreeVector xyz=G4ThreeVector(0,0,aftStrawZoffset-40*cm); // relative to BPgas 
    double halfLen=78*cm/2.;
    G4VSolid *solidV = new G4Tubs(volName+"S",0, IRaftVac, halfLen,0,2*pi*rad); 

    G4LogicalVolume * logicV=new G4LogicalVolume(solidV,matV,volName+"L",0,0,0);
    logicV->SetVisAttributes(G4VisAttributes(0, G4Colour(0.3,0.2,0.1))); //vis-here
    new G4PVPlacement(0,xyz, logicV, volName,logicVgas, false, 1, mySurfChk); 
  }


  if(1  && geomUse.aftStraw){ //--------   upstream C-disc for straw
    G4String volName="aftDisc";
    G4Material* matV=fNist_man->FindOrBuildMaterial( "G4_C");   assert(matV );
    double halfLen=strawMountDZ/2.;
    G4VSolid *solidV = new G4Tubs(volName+"S",IRaft, ORaftVac, halfLen, 0, 2*pi*rad);
    G4LogicalVolume * logicV=new G4LogicalVolume(solidV,matV,volName+"L",0,0,0);
    logicV->SetVisAttributes(G4VisAttributes(1, G4Colour(0.5,0.3,0.2))); //vis-here
    G4ThreeVector xyz=G4ThreeVector(0,0,aftStrawZoffset-halfLen);
    new G4PVPlacement(0,xyz, logicV, volName,logicVgas, false, 1, mySurfChk); 
  }

  
  if(1){ //--------   aft & forw  straws
    G4String volName="Cstraw";
    G4Material* matV=fNist_man->FindOrBuildMaterial( "G4_C");   assert(matV );
    double halfLen=Laft/2.;
    G4VSolid *solidV = new G4Tubs(volName+"S",IRaft, IRaft+strawDR, halfLen, 0, 2*pi*rad);
    G4LogicalVolume * logicV=new G4LogicalVolume(solidV,matV,volName+"L",0,0,0);
    logicV->SetVisAttributes(G4VisAttributes(1, G4Colour(0.7,0.3,0.8))); //vis-here
    G4ThreeVector xyz=G4ThreeVector(0,0,aftStrawZoffset+ Laft/2. );
    if(geomUse.aftStraw) new G4PVPlacement(0,xyz, logicV, volName,logicVgas, false, 1, mySurfChk); 
    //forward straw is now the same type
    xyz=G4ThreeVector(0,0,forwStrawZoffset-Laft/2.);
    if(geomUse.forwStraw) new G4PVPlacement(0,xyz, logicV, volName,logicVgas, false, 2, mySurfChk); 
  }

 if(1) { // assembly aft gradient of target
    buildGasGradientOne(IRaft, Laft,aftStrawZoffset, logicVgas,"aftGasGrad");
  }

  if(1 && geomUse.forwStraw){ //--------   down-stream C-disc
    G4String volName="forwDisc";
    G4Material* matV=fNist_man->FindOrBuildMaterial( "G4_C");   assert(matV );
    double halfLen=strawMountDZ/2.;
    G4VSolid *solidV = new G4Tubs(volName+"S",IRaft, ORh2, halfLen, 0, 2*pi*rad);
    G4LogicalVolume * logicV=new G4LogicalVolume(solidV,matV,volName+"L",0,0,0);
    logicV->SetVisAttributes(G4VisAttributes(1, G4Colour(0.5,0.3,0.2))); //vis-here
    G4ThreeVector xyz=G4ThreeVector(0,0,forwStrawZoffset+halfLen);
    new G4PVPlacement(0,xyz, logicV, volName,logicVgas, false, 1, mySurfChk); 
  }


  if(1 ) { // assembly forward  gradient of target
    buildGasGradientOne(IRaft, Laft,forwStrawZoffset, logicVgas,"forwGasGrad");
  }
  
  if(1){ //--------   forw vacuum drum
    G4String volName="forwVacDrum";
    G4Material* matV=fNist_man->FindOrBuildMaterial( "G4_Galactic");   assert(matV );
    G4ThreeVector xyz=G4ThreeVector(0,0,forwStrawZoffset+10*cm); // relative to BPgas 
    double halfLen=18*cm/2.;
    G4VSolid *solidV = new G4Tubs(volName+"S",0, ORh2, halfLen,0,2*pi*rad); 

    G4LogicalVolume * logicV=new G4LogicalVolume(solidV,matV,volName+"L",0,0,0);
    logicV->SetVisAttributes(G4VisAttributes(1, G4Colour(0.3,0.3,0.1))); //vis-here
    new G4PVPlacement(0,xyz, logicV, volName,logicVgas, false, 1, mySurfChk); 
  }
    

  return logicVgas;
}
 

//====================================================
//====================================================
//===================  gas gradient ==================
void  DaLiDetectorConstruction::buildGasGradientOne(double OR, double lenTot, double zoff, G4LogicalVolume *logicVmother,  G4String volName0) {  

  int par_nsect=10; // how many sectio#if 1ns it should be divided
  G4String gasName0="H2gasTarget";
  double delz=lenTot/par_nsect;
  for(int k=1;k<par_nsect;k++) {
    G4String volName=volName0+ Form("%d",k);
    G4String gasName=gasName0+ Form("%d",k);
    G4Material* matV=fNist_man->FindOrBuildMaterial( gasName );   assert(matV );
    G4ThreeVector xyz=G4ThreeVector(0,0,zoff+(k-1)*delz); // relative to BPgas, for aft section
    if(zoff>0) xyz=G4ThreeVector(0,0,zoff-(k-1)*delz); // forward section

    double halfLen=delz/2.;
    G4VSolid *solidV = new G4Tubs(volName+"S",0, OR, halfLen, 0, 2*pi*rad);
    G4LogicalVolume * logicV=new G4LogicalVolume(solidV,matV,volName+"L",0,0,0);
    logicV->SetVisAttributes(G4VisAttributes(1, G4Colour(0.2,0.2+k/15.,0.5))); //vis-here
    new G4PVPlacement(0,xyz, logicV, volName,logicVmother, false, 1, mySurfChk);
 
  }
} 

