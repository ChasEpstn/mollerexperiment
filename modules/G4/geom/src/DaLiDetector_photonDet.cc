#include "DaLiDetectorConstruction.h"

#include "G4NistManager.hh"
#include "G4Tubs.hh"

#include "G4VisAttributes.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "UniversalSD.h"

//===================================== 
//===================================== 
void  DaLiDetectorConstruction::assemble_photon_calorimeter(G4LogicalVolume   * mother_logic=0){
  printf("DetectorBuilder::assemble_photon_calorimeter(),  START ... \n");

  double OR=35*cm; // most outer position 

  // 15-cyl  photon det, DeltaR=7.5cm
  double IR=26*cm; // most inner position 
  const int mxCyl=15;
  double Dlead[mxCyl], Dsens[mxCyl];
  for(int ii=0;ii<mxCyl;ii++) {  
    Dlead[ii]=2*mm; Dsens[ii]=3*mm;}  

  double phFullLen=100*cm; // full length of the photon detector

  G4LogicalVolume   * photo_logic=0;
  if(1){ //--------   fiducial cylinder
    G4String volName="photLead";
    G4Material* matV=fNist_man->FindOrBuildMaterial( "G4_Pb");   assert(matV );
    G4ThreeVector xyz=G4ThreeVector(0,0,0);
    G4VSolid *solidV = new G4Tubs(volName+"S",IR, OR,phFullLen/2. , 0, 2*pi*rad);
    G4LogicalVolume * logicV=new G4LogicalVolume(solidV,matV,volName+"L",0,0,0);
    logicV->SetVisAttributes(G4VisAttributes(0, G4Colour(0.5,0.5,0.5))); //vis-here
    new G4PVPlacement(0,xyz, logicV, volName, mother_logic, false, 1, mySurfChk); 
    photo_logic=logicV;
  }

 
  if(1){ //--------   sensitive layer
    //nominal    double Rstep=1.5*cm, Dsens=0.99*cm, Roff=0.5*cm;
    double r2=IR;
    for(int k=1; k<=mxCyl; k++) {
      G4String volName=Form("phoCy%d",k);
      double r1=r2+Dlead[k-1];
      r2=r1+Dsens[k-1];
      assert(r1>=IR);
      assert(r2<=OR);
      printf("assemble_photon_calorimeter: %s scint R[%.1f, %.1f]/mm\n",volName.data(),r1/mm,r2/mm);
      double halfLen=phFullLen/2.;//nom - (4-k)*3*cm;
      G4Material* matV=fNist_man->FindOrBuildMaterial( "G4_C");   assert(matV );
      G4ThreeVector xyz=G4ThreeVector(0,0,0);
      G4VSolid *solidV = new G4Tubs(volName+"S",r1, r2,halfLen , 0, 2*pi*rad);
      G4LogicalVolume * logicV=new G4LogicalVolume(solidV,matV,volName+"L",0,0,0);
      int iSeen=0;
      if(k==1 || k==15)  iSeen=1;
      logicV->SetVisAttributes(G4VisAttributes(iSeen, G4Colour(0.6,0.2,0.0))); //vis-here
      new G4PVPlacement(0,xyz, logicV, volName, photo_logic, false, 0, mySurfChk); 
      // tmp change to reduce event size, introduced 2015-01-20
      if(iSeen)
	logicV->SetSensitiveDetector( mySD ); //printf("  is uniSD\n");
      else
	printf(" WARN:assemble_photon_calorimeter: inert  %s  (temp TTree size reduction) Jan.pl\n",volName.data());
    }
  }


}
