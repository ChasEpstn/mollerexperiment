///
/// \file SimpleMagneticField.cc
/// \brief Implementation of the SimpleMagneticField class
// - Modified Jan Balewski, MIT, January 2012
// - added varied magnetic field, July 2013

#include "SimpleMagneticField.h"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"


//=============================================
SimpleMagneticField::SimpleMagneticField(int flag ) { 
  fUniformField=0;
  printf("DaLiKalMagneticField( flag=%d)\n", flag);

  if(flag <2){
    double Bfield=0.000*tesla;
    if(flag==1) Bfield=-0.5*tesla; // sign consistent with 3D filed map from OPERA
    fUniformField=new G4UniformMagField( G4ThreeVector(0,Bfield*1e-5,Bfield));
    printf("DaLiKalMagneticField(), set uniform Bfiled : %.2f/tesla\n",Bfield/tesla);
  } 

  G4FieldManager  *fldMgr=G4TransportationManager::GetTransportationManager()->GetFieldManager();
  fldMgr->SetDetectorField(this);
  fldMgr->CreateChordFinder(this);

}

//=============================================
void SimpleMagneticField::GetFieldValue(const G4double Point[4], G4double*Bfield) const{
  if(fUniformField) fUniformField->GetFieldValue(Point,Bfield);
  else { // tabularized constant B-filed to match Ricardo's on-axis filed
    double az=fabs(Point[2]/cm);
    double ar=sqrt(Point[0]*Point[0]+Point[1]*Point[1])/cm;
    double Bz=-0.5*tesla; // sign consistent with 3D filed map from OPERA
    if (az>50. || ar> 50.) Bz=0.001*tesla;
    Bfield[2]=Bz;
    Bfield[0]=    Bfield[1]=0;
    //assert(2==3);
  }

}

