#!/usr/bin/env python

from __future__ import division
import numpy as np
import re
import shutil, os
# fileIn = open("Moller1BHighRes.fld",'r')
# fileOut = open("Moller1BHighResFormatted.fld",'w')

headers = ['MollerMagneticField,2016-1',
    'dims:cyl,fNdata,fNx,fNy,fNz,fXmin,fYmin,fZmin,fDx,fDy,fDz:units=cm',
    '0 1584 18 11 8 -43.8 -3.5 -18.5 5 5 5',
    '3Dmap:X,Y,Z,BX,BY,BZ,BMAG:units=cm,gauss'
]

#Add some space to the beginning of the file. We will overwrite this at the end
#with the actual headers

xVals = set()
yVals = set()
zVals = set()

info = ''
lineCounter = -1
#highestVal = 0
with open("Moller1BFieldGrid5newCoarse.fld",'r') as fileIn, open('tmp.txt','w') as fileOut:
    # for h in headers:
    #     fileOut.write('\n')
    #for i in xrange(17262000):
    #    fileIn.next()
    # fileIn.seek(17262000)
    for line in fileIn:
        lineCounter += 1
        if lineCounter == 0:
            info = re.findall('\[(.*?)\]', line)
            continue
        if lineCounter == 1:
            continue
        # if lineCounter > 100000:
        #     break

        line2 = line
        line = re.sub('\x00','',line)
        line = line.split()
        # line = line.decode('utf-16-le').split()
        if len(line) == 6:
            xS,yS,zS = line[0:3] #get string versions
            xVals.add(xS.strip())
            yVals.add(yS.strip())
            zVals.add(zS.strip())

            out = np.zeros(7)
            out[0:6] += np.array([float(i) for i in line])
            out[0:3] *= 100.0
            out[3:6] *= 1.0e4
            out[6] = np.sqrt(out[3]**2 + out[4]**2 + out[5]**2)
            #if abs(out[6]) > highestVal:
            #    highestVal = abs(out[6])
            # line = [float(i) for i in line]
            # print line
            # X = line[0]*100.0
            # Y = line[1]*100.0
            # Z = line[2]*100.0
            # BX = line[3]*1.0e4
            # BY = line[4]*1.0e4
            # BZ = line[5]*1.0e4
            # BMAG = np.sqrt(BX**2 + BY**2 + BZ**2)
            fileOut.write('{} {} {} {} {} {} {} \n'.format(*out.tolist()))
            if lineCounter % 10000 ==0:
                print 'Processed line {}\r'.format(lineCounter)
        else:
            print 'line is wrong length!'
            # line = [l.strip('\x00') for l in line]
            print line#.decode('utf-16')
            break
# print info

    min = np.array([float(a.strip('mm')) for a in info[0].split()])
    delt = np.array([float(a.strip('mm')) for a in info[2].split()])
    min = min/10.0
    delt = delt/10.0

    print len(xVals)
    print len(yVals)
    print len(zVals)
    #print "Highest:",highestVal
    vals = [0,lineCounter-2,len(xVals),len(yVals),len(zVals)]+min.tolist()+delt.tolist()
    print lineCounter
    headers[2] = ('{} '*len(vals)).format(*vals)

with open("Moller1B5FnewCoarse.fld",'w') as outFile, open('tmp.txt','r') as fileOut:
    for h in headers:
        outFile.write(h)
        outFile.write('\n')
    shutil.copyfileobj(fileOut, outFile)

os.system('rm tmp.txt')
