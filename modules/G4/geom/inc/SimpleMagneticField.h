//
/// \file SimpleMagneticField.hh
/// \brief Definition of the DaLiKalMagneticField class
// - Modified Jan Balewski, MIT, February 2012
// - added varied magnetic field, July 2013
//

#ifndef SimpleMagneticField_H
#define SimpleMagneticField_H

#include "G4UniformMagField.hh"
using namespace CLHEP;
class G4FieldManager;

class SimpleMagneticField: public G4MagneticField {
 private:
  G4UniformMagField *fUniformField;

  public:
  
   SimpleMagneticField( int flag);  //   type of the filed
   ~SimpleMagneticField(){};  


  virtual void  GetFieldValue( const G4double Point[4],
			     G4double *Bfield ) const;
protected:

  // Find the global Field Manager
  G4FieldManager* GetGlobalFieldManager();   // static 
};

#endif
