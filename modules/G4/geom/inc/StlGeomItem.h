// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
//  Defines geometry object in STL format
//
//*--  Author: Jan Balewski, MIT, October 2012
//
// $Id:  $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#ifndef StlGeomItem_H
#define StlGeomItem_H
#include "G4String.hh"
#include "G4Color.hh"
#include "G4RotationMatrix.hh"
using namespace CLHEP;


class StlGeomItem {
 public:
  G4String volName, stlName, matName;
  G4Color color;
  G4ThreeVector xyzOffset;
  G4RotationMatrix  *rotation;
  G4bool visible;
  G4LogicalVolume * logicV;

  StlGeomItem(){ volName="";rotation=0;  
    xyzOffset=G4ThreeVector(0,0,888*cm) ; logicV=0;}  

  void print(){
    printf("STL vol=%s, stl=%s, mat=%s, xyzOffset/cm=%.1f,%.1f,%.1f  visible=%d\n",volName.data(), stlName.data(),matName.data(),xyzOffset.x(),xyzOffset.y(),xyzOffset.z(),visible);
  }
  bool isNull() {return volName.isNull();}
  bool skipPlacement() {return fabs(xyzOffset.z())>200*cm;}

  G4String getNameL() {return volName+"L";} // logical name
  // volume name is automatically assigned my meshIO.LoadMesh as STL-file name
  G4String getNameP() {return volName;} // physical name
};



#endif

// $Log: $
