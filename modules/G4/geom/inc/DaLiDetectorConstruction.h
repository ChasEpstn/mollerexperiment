///
/// History:
///  Updated Jan Balewski, MIT, February 2013
///  Modified Jan Balewski, MIT, October 2013
///  code refactored for public release Jan Balewski & Jan Bernauer, September 2014
//

#ifndef DaLiDetectorConstruction_hh
#define DaLiDetectorConstruction_hh 1

#include "TString.h"

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"

class G4LogicalVolume;
class G4UserLimits;
class G4MagneticField;
class G4NistManager;

class DLg4Event;
class DLg4Track;
class UniversalSD;

// Jan's CADMESH interface 
#include "StlGeomItem.h"

//------------------------------------------------------------------------

class DaLiGeomSwitches {
 public:
  int aftStraw, forwStraw, moll_C, moll_Fe, protDet, leptDead;
  int beamPipe, worldAir;
  TString base,version;
  DaLiGeomSwitches (){
    aftStraw=1;  forwStraw=1;  moll_C=1; moll_Fe=1; protDet=1; leptDead=1;
    beamPipe=1;  worldAir=1;
    base="bad1"; version="bad2";
  };
  void dump() { printf("GeomSwitches: base=%s ver=%s  aftStraw=%d,  forwStraw=%d,  moll_C=%d moll_Fe=%d  protDet=%d leptDead=%d beamPipe=%d  worldAir=%d\n",
		       base.Data(),version.Data(),
		       aftStraw, forwStraw, moll_C, moll_Fe, protDet, leptDead,
		       beamPipe, worldAir); 	
  }  
    
};

//------------------------------------------------------------------------

class DaLiDetectorConstruction : public G4VUserDetectorConstruction {
public:
  
  DaLiDetectorConstruction(char * geomX , char * bfieldPar, TString pathSTL);
  ~DaLiDetectorConstruction();
  DLg4Event *eventDLg4; // used also as switch to disable hit accumulation in G4E
  DLg4Track *trackDLg4;
  G4VPhysicalVolume* Construct();
  void PrintSmallFieldMap();
  void PlotBfiled();
  void SetDebug(int x) { myDebug=x ;}

private:
  void SetMagField(TString bfieldPar);

  G4UserLimits    * fUserLimits;  
  G4MagneticField * fMagField;   // pointer to the magnetic field   
  G4NistManager   * fNist_man; // knows all about materials

  UniversalSD     *  mySD;
  G4double par_hitElosMin;
  int myDebug;
  G4bool mySurfChk;
  G4String geomName;
  DaLiGeomSwitches geomUse;

  void SetGeometrySwitches(TString geomList);

  // STL geometry definition
  void initStlVolumes();
  enum {mxStlVol=16};
  StlGeomItem stlVol[mxStlVol];
  G4String stlPath;
  
  // construction of geometry is done separately for logical units
  void Construct_materials();
  void assemble_STL_volumes(G4LogicalVolume   * world_logic);
  G4LogicalVolume   *assemble_target_cell(G4LogicalVolume   * world_logic);
  void assemble_proton_detector(G4LogicalVolume   * mother_logic);
  void assemble_lepton_tracker(G4LogicalVolume   * mother_logic);
  void assemble_photon_calorimeter(G4LogicalVolume   * mother_logic);
  void assemble_moller_dump(G4LogicalVolume   * world_logic);
  void assemble_magnet(G4LogicalVolume   * world_logic);
  void buildGasGradientOne(double OR, double lenTot, double zoff, G4LogicalVolume *logicVmother,  G4String volName0);
  void assemble_dosimeter2(G4LogicalVolume   * mother_logic,double zFront);

  // MRI alternatives to certain geometries
  void assemble_lepton_tracker_mri(G4LogicalVolume   * mother_logic);
  void assemble_proton_detector_mri(G4LogicalVolume   * mother_logic);
  void assemble_trigger_paddles_mri(G4LogicalVolume   * mother_logic);
  void assemble_mri_verE(G4LogicalVolume   * mother_logic);
  void assemble_mri_verE_inside_target(G4LogicalVolume   * mother_logic);

  
  //phase-1 detailed components:
  G4LogicalVolume *GetNewStackVerE(G4String stackName, double scintOffset);
  G4LogicalVolume *GetNewTripleGem(G4String gemName, double gemWidth, double gemLength);
  G4LogicalVolume *GetNewSimpleGem(G4String gemName, double gemWidth, double gemLength);
  G4LogicalVolume *GetNewSiPlane(G4String volName,double width,double length);
  

  //Moller detector components:
  // void assemble_moller_detector(G4VPhysicalVolume   * world_logic);
  G4VPhysicalVolume *assemble_moller_detector();
};


#endif
