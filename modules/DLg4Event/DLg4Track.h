// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
// Container for DL G4 Tracks
//
//*--  Author: Jan Balewski, MIT, December 2012
//
// $Id: DLg4Track.h,v 1.2 2011/12/15 21:02:48 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****


#ifndef DLg4Track_H
#define DLg4Track_H

#include "DLg4Hit.h"
#include <vector>
#include <TLorentzVector.h>

class DLg4Track : public TObject
{
  public:
    DLg4Track();
    DLg4Track(const DLg4Track& right);
    ~DLg4Track();
        
    Int_t GetTrackID() const;
    void SetTrackID(Int_t t);
    
    TString GetParticleName() const;
    void SetParticleName(TString p);
    
    Int_t GetParentID() const;
    void SetParentID(Int_t pp);
    
    const TVector3 GetOrigin() const;
    void SetOrigin(TVector3 pos);
    void SetX(Double_t x);
    void SetY(Double_t y);
    void SetZ(Double_t z);
    
    const TVector3 GetMomentum() const;
    const TLorentzVector Get4Momentum() const;
    void  SetMomentumPxPyPzE(Double_t px, Double_t py, Double_t pz, Double_t e);
    const std::vector<DLg4Hit>* GetHits() const;
    DLg4Hit* GetHit(Int_t h);
    Int_t GetNumHits() const;
    void AddHit(DLg4Hit* h);
    
    void print(int flag=0);
    void clear();
    
  private:
    Int_t trackID;
    Int_t parentTrackID;
    TVector3 origin;
    TString particleName;
    TLorentzVector  momentumP4; // 4-momentum vector in MeV

    std::vector<DLg4Hit> hits;
    
  public:
    ClassDef(DLg4Track,3);
};

#endif

// $Log: DLg4Track.h,v $
// Revision 1.2  2011/12/15 21:02:48  balewski
// ready to export hits in ttree
//
// Revision 1.1  2011/12/15 18:10:15  balewski
// adjusted to ver DLG5_1
//
