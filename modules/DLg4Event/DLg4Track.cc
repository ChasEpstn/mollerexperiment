// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
// Container for DL G4 Tracks
//
//*--  Author: Jan Balewski, MIT, December 2012
//
// $Id: DLg4Track.cc,v 1.6 2011/12/15 21:02:48 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****


#include "DLg4Track.h"
//=====================================


DLg4Track::DLg4Track(){
  clear();
}

//=====================================
DLg4Track::DLg4Track(const DLg4Track& right) : TObject(){
  trackID = right.GetTrackID();
  particleName = right.GetParticleName();
  parentTrackID = right.GetParentID();
  origin = right.GetOrigin();
  momentumP4 = right.Get4Momentum();
  const std::vector<DLg4Hit>* b = right.GetHits();
  int n = b->size();
  for (int i = 0; i < n; i++)
    hits.push_back(b->at(i));
}


//=====================================

DLg4Track::~DLg4Track(){
  if (!hits.empty()) hits.clear();
}

Int_t DLg4Track::GetTrackID() const {return trackID;}
void DLg4Track::SetTrackID(Int_t t) {trackID = t;}

TString DLg4Track::GetParticleName() const {return particleName;}
void DLg4Track::SetParticleName(TString p) {particleName = p;}

Int_t DLg4Track::GetParentID() const {return parentTrackID;}
void DLg4Track::SetParentID(Int_t pp) {parentTrackID = pp;}

const TVector3 DLg4Track::GetOrigin() const {return origin;}
void DLg4Track::SetOrigin(TVector3 pos) {origin.SetXYZ(pos.X(),pos.Y(),pos.Z());}
void DLg4Track::SetX(Double_t x) {origin.SetX(x);}
void DLg4Track::SetY(Double_t y) {origin.SetY(y);}
void DLg4Track::SetZ(Double_t z) {origin.SetZ(z);}

const TVector3 DLg4Track::GetMomentum() const {return momentumP4.Vect();}
const TLorentzVector  DLg4Track::Get4Momentum()const {return momentumP4;}
void  DLg4Track::SetMomentumPxPyPzE(Double_t px, Double_t py, Double_t pz, Double_t e) { momentumP4.SetPxPyPzE(px,py,pz,e); }

const std::vector<DLg4Hit>* DLg4Track::GetHits() const {return &hits;}
DLg4Hit* DLg4Track::GetHit(Int_t h) {return &(hits[h]);}
Int_t DLg4Track::GetNumHits() const{return hits.size();}
void DLg4Track::AddHit(DLg4Hit* h) {hits.push_back(*h);}

//=====================================

void DLg4Track::print(int flag){
  
  printf("\tTRACK:");
  printf("\ttrackID = %d",trackID);
  printf("\tparentID = %d",parentTrackID);
  printf("\tparticle = %s",particleName.Data());
  printf("\torygin x=(%5.1f,%5.1f,%5.1f) mm",origin.X(),origin.Y(),origin.Z());
  printf("\tp=(%.2g,%.2g,%.2g), |p|=%.2f , E=%.2f  MeV",momentumP4.X(),momentumP4.Y(),momentumP4.Z(), momentumP4.Vect().Mag(),momentumP4.E());
  int nHit=(int)hits.size();
  printf("\tnHit=%d ",nHit);
  printf("\n");
  if(flag==0) return;

  for (int i = 0; i < nHit; i++)  {
    printf("%d::",i);
    hits[i].print();
    printf("\n");
  }

}

//=====================================

void DLg4Track::clear(){
  trackID = -1;
  particleName = "fixMe";
  parentTrackID = 0;
  origin.SetXYZ(0.,0.,0.);
  momentumP4.SetXYZT(0.,0.,0.,0);
  hits.clear();
}

// $Log: DLg4Track.cc,v $
// Revision 1.6  2011/12/15 21:02:48  balewski
// ready to export hits in ttree
//
// Revision 1.5  2011/12/15 18:10:15  balewski
// adjusted to ver DLG5_1
//
    
