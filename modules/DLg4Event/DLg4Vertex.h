// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
// Container for DL G4 Hit
//
//*--  Author: Jan Balewski, MIT, February 2013
//
// $Id: DLg4Vertex.h,v 1.1 2011/12/15 18:10:15 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#ifndef DLg4Vertex_H
#define DLg4Vertex_H

#include <TObject.h>
#include <TVector3.h>
#include <TString.h>

class DLg4Vertex : public TObject
{
  public:
    DLg4Vertex();
    DLg4Vertex(const DLg4Vertex& right);
    ~DLg4Vertex();
        
    const TVector3 GetPosition() const;
    void SetPosition(TVector3 pos);
    void SetX(Double_t x);
    void SetY(Double_t y);
    void SetZ(Double_t z);
       
    void print();
    
  private:
    TVector3 position;

  public:
    ClassDef(DLg4Vertex,1);
};

#endif

// $Log: DLg4Vertex.h,v $
// Revision 1.1  2011/12/15 18:10:15  balewski
// adjusted to ver DLG5_1
//
    
