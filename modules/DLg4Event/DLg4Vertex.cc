// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
// Container for DL G4 Hit
//
//*--  Author: Jan Balewski, MIT, December 2012
//
// $Id: DLg4Vertex.cc,v 1.7 2011/12/15 18:10:14 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#include "DLg4Vertex.h"
//=====================================


DLg4Vertex::DLg4Vertex(){
  position.SetXYZ(0.,0.,9999.);

}
//=====================================


DLg4Vertex::DLg4Vertex(const DLg4Vertex& right){
  position = right.GetPosition();
}

//=====================================
DLg4Vertex::~DLg4Vertex(){
  /* nada */
}
//=====================================

    
//=====================================
const TVector3 DLg4Vertex::GetPosition() const {return position;}
void DLg4Vertex::SetPosition(TVector3 pos) {position.SetXYZ(pos.X(),pos.Y(),pos.Z());}
void DLg4Vertex::SetX(Double_t x) {position.SetX(x);}
void DLg4Vertex::SetY(Double_t y) {position.SetY(y);}
void DLg4Vertex::SetZ(Double_t z) {position.SetZ(z);}

//=====================================
void DLg4Vertex::print() {
  printf("G4Vertex:");
  printf(" xyz=(%.1f,%.1f,%.1f) mm",position.X(),position.Y(),position.Z());
}


// $Log: DLg4Vertex.cc,v $
