// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
// Container for DL G4 events
//
//*--  Author: Jan Balewski, MIT, December 2012
//
// $Id: DLg4Event.h,v 1.1 2011/12/15 18:10:14 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#ifndef DLg4Event_H
#define DLg4Event_H

#include "DLg4Track.h"
#include "DLg4Vertex.h"

class DLg4Event : public TObject{
  public:
    DLg4Event();
    ~DLg4Event();
        
    Int_t GetEventID() const;
    void SetEventID(Int_t id);
    
    Double_t GetWeight() const;
    void SetWeight(Double_t wght);

    void SetBaseGeometry( TString geomX);
    Bool_t matchBaseGeometry( TString geomX);
    
    void SetGeneratorName( TString physGenX);

    std::vector<DLg4Track>* GetTracks();
    void AddTrack(DLg4Track* track);
    Int_t GetNumTracks();
    DLg4Track* GetTrack(Int_t t);
 
    DLg4Vertex *GetVertex(){ return &vertex;}
       
    void print(int flag=0) ;
    void clear();

    //--------- scan all primary tracks, compute invM for each e+ e- pair, returns firts match within tolerance
    int  findAprimDaughters(double goalM, double epsM, int parentId, DLg4Track ** eplusTr, DLg4Track ** eminusTr);
     
  private:
    Int_t eventID;
    Double_t weight;
    TString baseGeometry, generatorName;
    std::vector<DLg4Track> tracks;
    DLg4Vertex vertex;
  public:
    ClassDef(DLg4Event,5);
};

#endif


// $Log: DLg4Event.h,v $
// Revision 1.1  2011/12/15 18:10:14  balewski
// adjusted to ver DLG5_1
//
