// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
// Container for DL G4 events
//
//*--  Author: Jan Balewski, MIT, December 2012
//
// $Id: DLg4Event.cc,v 1.7 2011/12/15 21:02:48 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#include "DLg4Event.h"
#include "../../include/DLUnits.h"
using namespace dali;  //uses DLUnits without needing dali::___

ClassImp(DLg4Event)

//=====================================
DLg4Event::DLg4Event(){
  eventID = 0;
  weight = 0;
}

//=====================================
DLg4Event::~DLg4Event(){
  clear();
}

//=====================================
Int_t DLg4Event::GetEventID() const {return eventID;}
void DLg4Event::SetEventID(Int_t id) {eventID = id;}

Double_t DLg4Event::GetWeight() const {return weight;}
void DLg4Event::SetWeight(Double_t wght) {weight = wght;}

void  DLg4Event::SetBaseGeometry( TString geomX) {  baseGeometry=geomX; }

Bool_t  DLg4Event::matchBaseGeometry( TString geomX) {  
  return baseGeometry==geomX; }    

void  DLg4Event::SetGeneratorName( TString physGenX) {   
  generatorName=physGenX; }


std::vector<DLg4Track>* DLg4Event::GetTracks() {return &tracks;}
void DLg4Event::AddTrack(DLg4Track* track) {tracks.push_back(*track);}
Int_t DLg4Event::GetNumTracks() {return tracks.size();}
DLg4Track* DLg4Event::GetTrack(Int_t t) {return &(tracks[t]);}


//=====================================
void DLg4Event::print(int flag)  {
  printf("\nDLG4_EVENT:");
  printf("\teventID = %d, weight = %g (pb), nTrack=%d, geom=%s, gener=%s ", eventID, weight, (int)tracks.size(), baseGeometry.Data(), generatorName.Data());
  vertex.print();
  if (flag==0) return;
  int totHits=0;
  for (int i = 0; i < (int)tracks.size(); i++)  {
    printf("\nTr%d:", i);
    tracks[i].print(flag-1);
    totHits+=tracks[i].GetNumHits();
  }
 // printf("\n end of eve: tot Hits=%d\n",totHits);
 printf("\n");
}

//=====================================
void DLg4Event::clear(){
  SetEventID(-1);
  SetWeight(0);
  SetBaseGeometry( (char *) "fixBaseGeom");
  if (!tracks.empty()) tracks.clear();

}

//=====================================
int   DLg4Event::findAprimDaughters(double goalM, double epsM , int parentId, DLg4Track ** eplusTr, DLg4Track ** eminusTr){
/*  scan all primary tracks, compute invM for each e+ e- pair, returns first match within tolerance
 */
  //printf("\n------DLg4Event::findAprimDaughters():  search for A' daughers M(A')/MeV=%.2f +/- %.2f parentId=%d, g4Eve->GetNumTracks()=%d\n", goalM/MeV, epsM/MeV,parentId,this->GetNumTracks());

  // select all primary e+, e-
  std::vector< DLg4Track *> emTr;
  *eminusTr=*eplusTr=0;
  for(int itr=0; itr<this->GetNumTracks(); itr++) {
    DLg4Track* g4Trk=this->GetTrack(itr);
    if( g4Trk->GetParentID()!=parentId) continue;
    if( g4Trk->GetParticleName()=="e-") emTr.push_back(g4Trk);
    if( g4Trk->GetParticleName()=="e+")  *eplusTr=g4Trk;
    // g4Trk->print(0); // only for primary tracks
  }

  //  printf("Found prim tracks: %d e+, %d e-  %p\n",eplusTr!=0,emTr.size(),*eplusTr);

  // loop over all possible invM pairs, until firts match
  if(*eplusTr==0) return -1;
  if(emTr.size()<1) return -2;
  
  TLorentzVector posP4=(*eplusTr)->Get4Momentum();
  //printf("pos  invM/MeV=%f\n", posP4.M());

  for(int itr=0; itr<emTr.size(); itr++) {
    DLg4Track* g4Trk=emTr[itr];
    TLorentzVector eleP4=g4Trk->Get4Momentum();
    // printf("ele  invM/MeV=%f\n", eleP4.M());
    TLorentzVector sumP4=eleP4+posP4;
    double invM=sumP4.M();
    // printf("itr=%d  invM/MeV=%f trId=%d\n",itr,invM,g4Trk->GetTrackID());
    if( fabs(invM -  goalM) <  epsM ) {
      // printf(" got wanted invM, eps/MeV=%.2f \n", invM -  goalM);
      *eminusTr=g4Trk;
      return 0;
    }
  }
  return -3; // none of pairs have requested invM
}   



// $Log: DLg4Event.cc,v $
// Revision 1.7  2011/12/15 21:02:48  balewski
// ready to export hits in ttree
//
// Revision 1.6  2011/12/15 18:10:14  balewski
// adjusted to ver DLG5_1
//
