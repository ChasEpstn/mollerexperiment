// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
// Container for DL G4 Hit
//
//*--  Author: Jan Balewski, MIT, December 2012
//
// $Id: DLg4Hit.cc,v 1.7 2011/12/15 18:10:14 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#include "DLg4Hit.h"
#include "../../include/DLUnits.h"
using namespace dali;  //uses DLUnits without needing dali::___

//=====================================
DLg4Hit::DLg4Hit(){
  clear();
}

//=====================================
DLg4Hit::DLg4Hit(const DLg4Hit& right){
  detectorID = right.GetDetectorID();
  dE = right.GetDE();
  kinE = right.GetKinE();
  stepLen = right.GetStepLen();
  position = right.GetPosition();
  momentum = right.GetLocalMomentum();
  localPosition= right.GetLocalPosition();
  globalTime= right.GetGlobalTime();
}
//=====================================


DLg4Hit::~DLg4Hit(){
  /* nada */
}
//=====================================

    
TString DLg4Hit::GetDetectorID() const {return detectorID;}
void    DLg4Hit::SetDetectorID(TString d) {detectorID = d;}

//=====================================

const 
TVector3 DLg4Hit::GetPosition() const {return position;}
void     DLg4Hit::SetPosition(TVector3 pos) {
  position.SetXYZ(pos.X(),pos.Y(),pos.Z());
}
void DLg4Hit::SetPosition(Double_t x, Double_t y, Double_t z) {
  position.SetXYZ(x,y,z); 
}

//=====================================

const 
TVector3 DLg4Hit::GetLocalPosition() const {return localPosition;}
void     DLg4Hit::SetLocalPosition(TVector3 pos) {
  localPosition.SetXYZ(pos.X(),pos.Y(),pos.Z());
}
void DLg4Hit::SetLocalPosition(Double_t x, Double_t y, Double_t z) {
  localPosition.SetXYZ(x,y,z);
}

//=====================================

const
TVector3 DLg4Hit::GetMomentum() const{printf("USING LOCAL MOMENTUM AS GLOBAL!\n"); return momentum;}
const 
TVector3 DLg4Hit::GetLocalMomentum() const {return momentum;}
void     DLg4Hit::SeLocalMomentum(TVector3 p3) {
  momentum.SetXYZ(p3.X(),p3.Y(),p3.Z());
}
void DLg4Hit::SetLocalMomentum(Double_t x, Double_t y, Double_t z) {
  momentum.SetXYZ(x,y,z);
}

//=====================================

Double_t DLg4Hit::GetDE() const {return dE;}
void     DLg4Hit::SetDE(Double_t e) {dE = e;}

Double_t DLg4Hit::GetKinE() const {return kinE;}
void     DLg4Hit::SetKinE(Double_t e) {kinE = e;}

Double_t DLg4Hit::GetStepLen() const {return stepLen;}
void     DLg4Hit::SetStepLen(Double_t x) {stepLen=x; }

//=====================================

Double_t DLg4Hit::GetGlobalTime() const {return globalTime;}
void     DLg4Hit::SetGlobalTime(Double_t t) {globalTime=t;}

//=====================================

void DLg4Hit::print() {
  printf("HIT:");
  printf(" detectorID = %s",detectorID.Data());
  printf(" dE=%.2g MeV, kinE=%.1fMeV, stepLen/mm=%.2f", dE,kinE,stepLen);
  printf(" glob xyz=(%.1f,%.1f,%.1f) mm,  t=%.3g (ns)",position.X(),position.Y(),position.Z(),GetGlobalTime()/ns);
  printf(" loc Pxyz=(%.1f,%.1f,%.1f) MeV",momentum.X(),momentum.Y(),momentum.Z());
}

//=====================================

void DLg4Hit::clear(){ //const Option_t* option
  detectorID = "";
  dE = 0.;
  kinE = 0.;
  stepLen=0;
  SetGlobalTime(0.);
  position.SetXYZ(0.,0.,0.);
  localPosition.SetXYZ(0.,0.,0.);
  momentum.SetXYZ(0.,0.,0.);
}

// $Log: DLg4Hit.cc,v $
// Revision 1.7  2011/12/15 18:10:14  balewski
// adjusted to ver DLG5_1
//
    
