// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
// Container for DL G4 Hit
//
//*--  Author: Jan Balewski, MIT, January 2012
//
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#ifndef DLg4Hit_H
#define DLg4Hit_H

#include <TObject.h>
#include <TVector3.h>
#include <TString.h>

class DLg4Hit : public TObject
{
  public:
    DLg4Hit();
    DLg4Hit(const DLg4Hit& right);
    ~DLg4Hit();
        
    TString GetDetectorID() const;
    void SetDetectorID(TString d);
    
    const TVector3 GetPosition() const;
    void SetPosition(TVector3 pos);
    void SetPosition(Double_t x, Double_t y, Double_t z);
    const TVector3 GetLocalPosition() const;
    void SetLocalPosition(TVector3 pos);
    void SetLocalPosition(Double_t x, Double_t y, Double_t z);

    const TVector3 GetMomentum() const;
    const TVector3 GetLocalMomentum() const;
    void SeLocalMomentum(TVector3 p3);
    void SetLocalMomentum(Double_t x, Double_t y, Double_t z);
    
    Double_t GetDE() const;
    void SetDE(Double_t e);

    Double_t GetKinE() const;
    void SetKinE(Double_t e);
    
    Double_t GetStepLen() const;
    void SetStepLen(Double_t x);

    Double_t GetGlobalTime() const;
    void SetGlobalTime(Double_t);
    
    void print();
    void clear();
    
  private:
    TVector3 position; // global
    TVector3 localPosition; //local
    TVector3 momentum; //local   
    Double_t dE, kinE, stepLen, globalTime;
    TString detectorID;

  public:
    ClassDef(DLg4Hit,8);
};

#endif

// $Log: DLg4Hit.h,v $
// Revision 1.1  2011/12/15 18:10:15  balewski
// adjusted to ver DLG5_1
//
    
