// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
//
//*--  Author: Jan Balewski, MIT, January, 2012
//
// $Id: JFitEvent.cc,v 1.7 2011/12/15 21:02:48 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#include <math.h>

#include "JFitEvent.h"


//=====================================
void JFitEvent::print(int flag) {

  printf("\nJFGT_EVENT: id=%d run=%d  nTracks=%d  nParticles=%d fitPass=%d  weight=%g\n",id,run,sizeTracks(),sizeParticles(),pass,weight);
  printf(" vertex position (mm)  X=%.1f  Y=%.1f Z=%.1f\n",vertPos.X(), vertPos.Y(), vertPos.Z()); 


  if(flag<1) return;
  for (int i = 0; i < sizeParticles()  ; i++)  {
    printf("ipart=%d: ",i); particles[i].print();
  }

  if(flag<2) return;
  for (int i = 0; i < sizeTracks()  ; i++)  {
    printf("itr=%d: ",i); tracks[i].print();
  }
  
}


//=====================================
void JFitEvent::clear(){
  // printf("JFitEvent::clear() *******\n");
  id = run=-1;
  vertPos=TVector3(0,0,223344);  
  pass=-1;
  weight=0;

  tracks.clear();
  particles.clear();

}


// $Log: JFitEvent.cc,v $
