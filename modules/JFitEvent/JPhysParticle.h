// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
// Container for physical particle
//
//*--  Author: Jan Balewski, MIT, April, 2013
//
// $Id: $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#ifndef JPhysParticle_H
#define JPhysParticle_H

#include <TLorentzVector.h>
#include <TString.h>

//------------------------------
//------------------------------
class JPhysParticle  : public TObject {
  public:
  ClassDef(JPhysParticle,1);
  
  TLorentzVector mom4;
  TString name;
  int charge;

  JPhysParticle(){clear();}
  ~JPhysParticle(){clear();}
  bool isValid(){ return charge<90;}
                
  void print(){ printf("JPhysPart %s Q=%d  P=%f %f %f  E=%f  m=%f PT=%f\n",name.Data(),charge, mom4.Px(),mom4.Py(),mom4.Pz(),mom4.E(),mom4.M(),mom4.Pt());}
    void clear(){ charge=999; name="bad"; mom4=TLorentzVector(0,0,0,-999);}
};

#endif


// $Log:  $
