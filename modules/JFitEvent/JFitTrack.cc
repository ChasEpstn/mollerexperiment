// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
//
//*--  Author: Jan Balewski, MIT, April, 2013
//
// $Id: JFitEvent.cc,v 1.7 2011/12/15 21:02:48 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#include <math.h>
#include "JFitTrack.h"


//=====================================
void JFitTrack::print(int flag) {
  printf("trackNodes  len=%d  isLepton=%d  g4TrackID=%d globChi2dof=%.2e ",sizeNodes(),isLepton,g4TrackID, globChi2dof);
  if( !originValid()) printf("origin=none\n");
  else  printf("\n  origin="); origin.hit.print();
  if(flag<0) return;
  for (int i = 0; i < sizeNodes()  ; i++)  {
    printf("i=%d: ",i); nodes[i].print();
  }
}


//=====================================
void JFitTrack::setNodesError(double sig, char *coreName){
  for (int i = 0; i < sizeNodes()  ; i++) 
    if(coreName && nodes[i].hit.name.Contains(coreName)) {
      float fac=1;  
      nodes[i].hit.sig1D=sig*fac;
    }  else
      printf("setNodesError skip ih=%d name=%s sig=%f for core=%s\n", i, nodes[i].hit.name.Data(),sig,coreName);
}



// $Log: JFitEvent.cc,v $
