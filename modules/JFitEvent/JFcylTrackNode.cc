// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
//
//*--  Author: Jan Balewski, MIT, January 2012
//
// $Id: JFcylTrackNode.cc,v 1.6 2011/12/15 21:02:48 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#include <TMath.h>

#include "JFcylTrackNode.h"
#define deg2rad (TMath::DegToRad())


//=====================================

JFcylTrackNode::JFcylTrackNode( TString name1,  double Rxy1, double phi1,double z1, double sig1 ){
  hit.name=name1;
  hit.Rxy=Rxy1;
  hit.phi=phi1;
  hit.z=z1;
  hit.inFit=1; // by default  use all nodes in fitting
  hit.sig1D=sig1;// mm
  other=0; // default is no mapping
}


//=====================================

JFcylTrackNode::~JFcylTrackNode(){
 
}


//=====================================

void JFcylTrackNode::print(){
  hit.print();
  // printf(" other=%p\n",(void*)other);
}

//=====================================

void JFcylTrackNode::clear(){
  hit.clear();
  other=0;

}


// $Log: JFcylTrackNode.cc,v $
