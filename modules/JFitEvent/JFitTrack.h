// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
// Container for FitTrack
//
//*--  Author: Jan Balewski, MIT, April, 2013
//
// $Id: JFitEvent.h,v 1.1 2011/12/15 18:10:14 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#ifndef JFitTrack_H
#define JFitTrack_H

#include <TString.h>

#include "JFcylTrackNode.h"

//------------------------------
//------------------------------
class JFitTrack  : public TObject {
 public:  
  ClassDef(JFitTrack,4);
  JFitTrack(){clear();}
  ~JFitTrack(){}
  
    void print(int flag=0);
    void clear(){ nodes.clear(); origin.clear();  origin.hit.sig1D=-2;  g4TrackID=-3; isLepton=-1;  globChi2dof=-1;} 
    
    int isLepton; // based on hit pattern
    int g4TrackID; // filled only for  inpEve for later association w/ G4
    //.... best approximation of origin for this track
    JFcylTrackNode origin;
    bool originValid() { return origin.hit.sig1D>=0; }//negative errro means invalid

    float globChi2dof; // global track chi2/dof
    //.... nodes with hits
    std::vector<JFcylTrackNode> nodes;
    JFcylTrackNode*  getNode(Int_t sp) {return &(nodes[sp]);}
    int            sizeNodes()  {return (int) nodes.size();}
    void           addNode(JFcylTrackNode tt) {nodes.push_back(tt);}
    void           setNodesError(double sig, char *coreName=0);

};

#endif


// $Log: JFitEvent.h,v $
