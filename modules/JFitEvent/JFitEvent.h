// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
// Container for FitEven
//
//*--  Author: Jan Balewski, MIT, January 2012
//
// $Id: JFitEvent.h,v 1.1 2011/12/15 18:10:14 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****

#ifndef JFitEvent_H
#define JFitEvent_H
#include <TObject.h>
#include <TVector3.h>
#include <TString.h>

#include "JFitTrack.h"
#include "JPhysParticle.h"

//------------------------------
//------------------------------
class JFitEvent : public TObject {
  public:
    ClassDef(JFitEvent,1);

  JFitEvent(){clear();}
    ~JFitEvent(){clear();}
                
    void print(int flag=0);
    void clear();

    Int_t id;  // event ID    
    Int_t run; // run No
    double weight;
    TVector3  vertPos;
    int pass; // -1 bad, 99=truth, 0=seed, 1=fitSequential, 2=fitSimultanous

    std::vector<JPhysParticle> particles; // synchronized with tracks
    JPhysParticle*  getParticle(Int_t sp) {return &(particles[sp]);}
    int            sizeParticles()  {return (int) particles.size();}
    void            addParticle( JPhysParticle tt) {particles.push_back(tt);}

    std::vector<JFitTrack> tracks;
    JFitTrack*   getTrack(Int_t sp) {return &(tracks[sp]);}
    int         sizeTracks()  {return (int) tracks.size();}
    void         addTrack(JFitTrack tt) {tracks.push_back(tt);}


};

#endif


// $Log: JFitEvent.h,v $
