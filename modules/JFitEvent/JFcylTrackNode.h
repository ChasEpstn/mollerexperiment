// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****
// 
// Container for Fgt events Helix tracker
//
//*--  Author: Jan Balewski, MIT, January 2012
//
// $Id: JFcylTrackNode.h,v 1.2 2011/12/15 21:02:48 balewski Exp $
//
// *+****1****+****2****+****3****+****4****+****5****+****6****+****7****+****


#ifndef JFcylTrackNode_H
#define JFcylTrackNode_H

#include <TObject.h>
#include <TString.h>
#include <TVector3.h>

class JFcylHit : public TObject {
 public:
  ClassDef(JFcylHit,1);
  TString name;  
  double Rxy,phi,z;
  int inFit; // 0-ignore in fit
  double sig1D; // error of this hit, for now it is 1D value, to be used independently for dx & dxy. It is correct for 2D gauss smearing in X-Y plane - for helix fit
  
  void print(){ printf("cylHit in=%s Rxy=%.2f/mm  phi=%.3f/rad  z=%.2f/mm inFit=%d  sig1D=%.3f\n",name.Data(),Rxy, phi,z,inFit,sig1D);}

  void clear(){Rxy=0.;z=9999; sig1D=88888;inFit=0;}
  TVector3 getV3(){  TVector3 v3(1,2,3); v3.SetPerp(Rxy);  v3.SetPhi(phi);  v3.SetZ(z); 
    return v3;}
};


class JFcylTrackNode  : public TObject  {
 public:
  ClassDef( JFcylTrackNode,1);

  JFcylTrackNode( TString name1,  double Rxy1, double phi1,double z1, double sig1=0. );
  JFcylTrackNode(){clear();}
  ~JFcylTrackNode();
    
  void print();
  void clear();  
  JFcylHit hit;
  JFcylTrackNode *other; //! the slash-slash-bang opening of this comment instructs root not to save this pointer when writing an instance of this class to file.
};


#endif

// $Log: JFcylTrackNode.h,v $
