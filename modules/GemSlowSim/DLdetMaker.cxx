#include <stdio.h>
#include <assert.h>
#include <map>

#include <TObjArray.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TF2.h>
#include <TFile.h>
#include <TMath.h>
#include <iostream>
#include <TLorentzVector.h>
#include <TMatrixD.h>
#include <TMatrixDSym.h>
#include <TVector3.h>
#include <TTree.h>
#include <TRandom3.h>
#include <TRandom.h>

#include "DLdetMaker.h"
#include "DLg4Event.h"
#include "DLg4Track.h"
#include "DLg4Hit.h"
#include "DLdetEvent.h"
#include "DLdetMap.h"
#include "DLUnits.h"

using namespace std;
using namespace dali;

ClassImp(DLdetMaker);


//-------------------------------------------
//-------------------------------------------
DLdetMaker::DLdetMaker(DLdetMap *d, double width=1.5*mm, double enePerADC=1*eV) {
  //create bare event:
  detEve=new DLdetEvent();
  HList=new TObjArray(0);

  //load detector map:
  detMap=d;

  //set shower model:
  showerWidth=width;

  //set gain:
  gain=enePerADC;
}

//void DLdetMaker::~DLdetMaker()
void DLdetMaker::initHisto() {
  return;
}  



//-------------------------------------------
//-------------------------------------------
DLdetEvent* DLdetMaker::CreateDetEvent(DLg4Event* g4Eve){
  detEve->clear();//make sure we start fresh.

  //create the ordered set that will hold the energy depositions before digitization:
  std::map<int,double> energyInStrip;
  
  
  int sideSamples=1;
  int nSamples=2*sideSamples+1; //NxN grid.  user should tune this!
  float nsigma=2;
  //charge model is a 2D gaussian.
  double gaussianWidth=showerWidth;
  TF2 *chargeModel=new TF2("charge","[0]*TMath::Gaus(x,[1],[2])*TMath::Gaus(y,[3],[4])",-nsigma*gaussianWidth, nsigma*gaussianWidth,-nsigma*gaussianWidth, nsigma*gaussianWidth);

  int nTracks=g4Eve->GetNumTracks();
  float weight=g4Eve->GetWeight();
  int id=g4Eve->GetEventID();

  detEve->weight=weight;
  detEve->id=id;
  
  for (int i=0;i<nTracks;i++){
    DLg4Track *track=g4Eve->GetTrack(i);

    int nHits=track->GetNumHits();
    for (int j=0;j<nHits;j++){
      DLg4Hit *hit=track->GetHit(j);
      TVector3 localpos=hit->GetLocalPosition();
      double ene=hit->GetDE();
      const char *detectorid=hit->GetDetectorID().Data();
      TString detIdBrief((hit->GetDetectorID())(0,5));
      //charge model is a gaussian centered on the hit.
      chargeModel->SetParameters(ene/(3.14*gaussianWidth*gaussianWidth),localpos.X(),gaussianWidth,localpos.Z(),gaussianWidth);

      //printf("processing hit in %s(%s) at (%2.2f,%2.2f,%2.2f), E=%2.2fkeV\n",detIdBrief.Data(),detectorid,localpos.X(),localpos.Y(),localpos.Z(),ene/keV);

      //find the lower edge of the grid system of the strips:      
      double stripPitch=1*mm;//detMap->GetStripPitch(); //this shouldn't be hard-coded.
      double halfPitch=stripPitch/2;
      double xEdge=TMath::Floor(localpos.X()/stripPitch)*stripPitch;
      double zEdge=TMath::Floor(localpos.Z()/stripPitch)*stripPitch;
      
      //for each sampling site, integrate the charge model and add to the strip beneath:
      for (int k=-sideSamples;k<=sideSamples;k++){
	double xLow=xEdge+k*stripPitch;
	double xHigh=xLow+stripPitch;
	double xCenter=xLow+halfPitch;
	for (int m=-sideSamples;m<=sideSamples;m++){
	  double zLow=zEdge+m*stripPitch;
	  double zHigh=zLow+stripPitch;
	  double zCenter=zLow+halfPitch;
	  TVector3 temppos(xCenter,0,zCenter);
	  int stripId=detMap->LocalPositionToStripId(detIdBrief,temppos);
	  double enePortion=chargeModel->Integral(xLow,xHigh,zLow,zHigh);
	  //printf("trying to add %2.2fkeV to strip %d of %s\n",enePortion/keV,stripId, detIdBrief.Data());

	  std::map<int,double>::iterator it;
	  it=energyInStrip.find(stripId);
	  if (it!=energyInStrip.end()){
	    energyInStrip[stripId]+=enePortion;
	  }
	  else {
	    energyInStrip[stripId]=enePortion;
	  }
	}
      }
    }    
  }

  //we've filled the map.  now we digitize (not keen on this, but DLdetHit currently only supports digital) each element in turn:
  for (std::map<int,double>::iterator it=energyInStrip.begin(); it!=energyInStrip.end();it++){
    double totalEnergy=it->second;
    int stripId=it->first;
    detEve->addLepDetHit(stripId,totalEnergy/gain);
  }
  
  return detEve;
}


//-------------------------------------------------
//-------------------------------------------------
//-------------------------------------------------
void DLdetMaker::finish(){

  printf("DLdetMaker:finish() - saving hist\n");
     
  return;
}

