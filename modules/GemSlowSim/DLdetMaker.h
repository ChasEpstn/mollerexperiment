// \class  DLdetMaker
// Class to build DLdetEvent from DLg4Event
// \author Ross Corliss
// $Id: DLdetMaker.h,v 1.0 2015/04/10 22:54:47 rcorliss Exp $

#ifndef DLdetMaker_h
#define DLdetMaker_h

//#include <TObjArray.h>
#include <TObject.h>
//#include <TTree.h>
//#include <TFile.h>

//class TTree;

class TH1F;
class TH2F;

//class TFile;
class TObjArray;
class TVector3;
class DLg4Event;
class DLdetEvent;
class DLdetMap;



class DLdetMaker :public TObject{ 
 protected:

  TObjArray *HList;
  DLdetEvent *detEve;
  DLdetMap *detMap;

  double showerWidth;
  double gain;
  
 public:

  DLdetMaker(DLdetMap *d, double width, double enePerADC);
  ~DLdetMaker(){};
  
  
  void  initHisto();
  //void  saveHisto(TString fname);
  DLdetEvent*  CreateDetEvent( DLg4Event* eve);
  void  finish();
  TObjArray *getHList(){return HList;};

  ClassDef(DLdetMaker,1) 
};
     
#endif

// $Log: DLdetMaker.h,v $
// Revision 1.0  2014/01/16 22:50:16  rcorliss
// start

