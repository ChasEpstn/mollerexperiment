//==================================================
// addTree.cc is a standalone program to demonstrate
//  combining two trees into a single tree with two
//  branches.
//
//  Author: Ross Corliss
//  Date: 2014-05-20
//==================================================


#include "DLUnits.h"
#include <TFile.h>
#include <stdio.h>
#include <stdlib.h>
#include "DLg4Event.h"
#include "DLmadEvent.h"
#include <TTree.h>
using namespace dali;

bool addTree(char* g4EventFilename, char * madEventFilename, char *outputFilename, int nEve=-1){

  //open the g4 event file and get the g4event branch
  TFile *g4EventFile=new TFile(g4EventFilename,"READ");
  if (g4EventFile->IsZombie()) {
      printf("Unable to open file \"%s\".  Aborting.\n",g4EventFilename);
      return false;
    }
  TTree *g4Tree=(TTree*)g4EventFile->Get("DLg4EventTree");
  DLg4Event *g4Eve=0;
  int check=g4Tree->SetBranchAddress("DLg4Events",&g4Eve);
  if (check!=0) {
      printf("Unable to find \"DLg4Events\" branch in file \"%s\".  Aborting.\n",g4EventFilename);
      return false;
    }

  //open te mad event file and get the madevent branch
  TFile *madEventFile=new TFile(madEventFilename,"READ");
  if (madEventFile->IsZombie()) {
      printf("Unable to open file \"%s\".  Aborting.\n",madEventFilename);
      return false;
    }
  TTree *madTree=(TTree*)madEventFile->Get("DLmadEventTree");
  DLmadEvent *madEve=0;
  check=madTree->SetBranchAddress("DLmad",&madEve);
  if (check!=0){//TTree::ESetBranchAddressStatus--kMatch) {
      printf("Unable to find \"DLmad\" branch in file \"%s\".  Aborting.\n",madEventFilename);
      return false;
    }

  //create a new output file and a tree to hold the output
  //thanks to root, outTree defaults to writing here.
  TFile *outFile=new TFile(outputFilename,"RECREATE");
  if (outFile->IsZombie()){
       printf("Unable to open file \"%s\" for writing.  Aborting.\n",outputFilename);
      return false;
    }  
  TTree *outTree=new TTree("eventTree","Combined Event Tree",99);
  //the lines below breaks with convention.  
  //I don't think the g4Eve and g4mad 
  //branches should have pluralized names. 
  outTree->Branch("DLg4Event",&g4Eve);
  outTree->Branch("DLmadEvent",&madEve);

  //if we were adding our own analysis and output tree types, we'd have, eg:
  //DLkarimakiTracks *kariEve;
  //outTree->Branch("DLkarimakiTracks",&kariEve);
  
  if (nEve<0){
    nEve=g4Tree->GetEntries();
  }
  int nMadEve=madTree->GetEntries();
  int nG4Eve=g4Tree->GetEntries();


  if (nMadEve<nEve || nG4Eve<nEve){
    printf("Synchronization error.  madTree (%d) and g4Tree (%d) have different nEntries.\n",nMadEve,nG4Eve);
    return false;
  }
  int tenth=(int)(nEve/10);
  for (int i=0;i<nEve;i++){
    if (tenth==0 || (i+1)%tenth==0) printf("%d/%d\n",i+1,nEve);
    madTree->GetEntry(i,0);
    g4Tree->GetEntry(i,0);

    //if we were adding our own analysis we could have, eg:
    //kariEve=getKariTracks(g4Eve);
    //
    if (!g4Eve) printf("no g4event for this entry!  aborting.\n");
    outTree->Fill();
  }

  //we don't need to explicitly write outTree.  It writes to the default file location.
  //outTree->Write();
  return true;
}

int main(int argc, char **argv){

  if (argc==4){
    bool ret=addTree(argv[1], argv[2],argv[3],-1);
    if (ret) printf("Combined successfully to %s.\n",argv[3]);
    else printf("Aborted.\n");
  }
  else if (argc==5){
    int nEve=atoi(argv[4]);
    if (nEve>0){
      bool ret=addTree(argv[1], argv[2],argv[3],nEve);
      if (ret) printf("Combined successfully to %s.\n",argv[3]);
      else printf("Aborted.\n");
    }
    else { 
      printf("Can't process %s=%d events.\n",argv[4],nEve);
    }
  }
  else {
    printf("addTree usage is:  addTree G4FILE MADFILE OUTFILE [nEve]\n");
  }
  return 0;
}

