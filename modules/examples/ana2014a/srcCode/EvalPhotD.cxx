#include <stdio.h>
#include <assert.h>
 
#include <TH1.h>
#include <TH2.h>
#include <TFile.h>
#include <TLine.h>
#include <TMath.h>

#include "EvalPhotD.h"
#include "DLmadEvent.h"
#include "DLg4Event.h"
#include "DLUnits.h"
using namespace dali;

ClassImp(EvalPhotD)
 
//-------------------------------------------
//-------------------------------------------
EvalPhotD::EvalPhotD(TString corex, float z1, float z2, float dz, float dphi, int mode) { // units: cm
  NtotEve=0;
  totW=0;
  par_z1=z1; par_z2=z2; par_dz=dz;
  par_dphi=dphi;
  par_mode=mode;
  core=corex;
  par_minCellEne=0.7*MeV;
  printf("EvalPhotD(core=%s), mode=%d\n",core.Data(),par_mode); 
}  
 


//-------------------------------------------
//-------------------------------------------
void EvalPhotD::initHisto() {
  memset(hA,0,sizeof(hA)); // input spectra, unweighted
  double pi=TMath::Pi();
  TH1* h;
  TH2D* h2;
  
  //-- pg1
  hA[0]=h=new TH1D(core+"_myCase","No. of events;case",6,0,1);  h->GetXaxis()->SetTitleOffset(0.4);  h->GetXaxis()->SetLabelSize(0.06);  h->GetXaxis()->SetTitleSize(0.05); h->SetMinimum(0.);  h->SetLineColor(kBlue); h->SetFillColor(42);h->SetLineWidth(2);  h->SetMarkerSize(2);//<-- large text
  
  
  hA[1]=new TH1D (core+"_anyTr","G4 tracks  w/ any hits; tracks/event",100,-0.5,99.5); 

  hA[2]=new TH1D (core+"_priTr","G4 tracks w/ photD hits ; tracks/event",10,-0.5,10.5); 
  
  hA[3]=h=new TH1D(core+"_priTrT","G4 track w/ photD hits ; particle type",5,0,1);  h->GetXaxis()->SetTitleOffset(0.4);  h->GetXaxis()->SetLabelSize(0.06);  h->GetXaxis()->SetTitleSize(0.05); h->SetMinimum(0.);  h->SetLineColor(kMagenta);  h->SetFillColor(32);h->SetLineWidth(2);  h->SetMarkerSize(2);//<-- large text

  // prime the order:
  h->Fill("e-",0.);    h->Fill("gamma",0.);   h->Fill("proton",0.);  h->Fill("e+",0.); 

  hA[4]=h=h2=new TH2D(core+"_deHit","energy loss per G4 hit; vol.name; Log10(DE/keV)",20,0,20,30,-4,4.);  h->GetXaxis()->SetTitleOffset(0.4);  h->GetXaxis()->SetLabelSize(0.06);  h->GetXaxis()->SetTitleSize(0.05); h->SetMinimum(0.);  h->SetLineColor(kBlue);h->SetLineWidth(2);  h->SetMarkerSize(2);//<-- large text

  for (int ic=0;ic<15;ic++) h2->Fill(Form("phoCy%d",ic+1),0.0,0.);
 

  int nz= (int)  (par_z2 - par_z1)/ par_dz;
  int nphi=2*pi/ par_dphi;

       
  printf("%s zLen/cm=%.1f nphi=%d nz=%d  nCell=%d \n",core.Data(),(par_z2 - par_z1)/cm,nphi,nz,nphi*nz);
    hES=new TH2D(core+"_eve",core+"eve-histo DE (MeV)  ; z(mm); phi (rad)",nz,par_z1,par_z2,nphi,-pi,pi);

    float maxEne=90*MeV;
    hA[10+0]=h=new TH1D(core+"_hcE",core+" hit-cell ene deposit; energy (MeV)",500,0,maxEne/MeV);
    { TList *tl=h->GetListOfFunctions(); assert(tl);
      double thrX= par_minCellEne/MeV;
      TLine *ln=new TLine(thrX,0,thrX, 1e6);    tl->Add(ln);
      ln->SetLineColor(kMagenta); ln->SetLineStyle(2);
    }

    hA[10+1]=h=(TH1D*) h->Clone(); // and cary also the throeshold line
    h->SetNameTitle(core+"_acE",core+" any cell ene deposit");

        
    hA[10+2]=h=new TH1D(core+"_hcf_z",core+" hit-cell hits>thres  ; z(mm)",nz,par_z1,par_z2);
    h->Sumw2();

    hA[10+3]=h=(TH1D*) h->Clone();
    h->SetNameTitle(core+"_acf_z",core+" any-cell hits>thres ");
 
    hA[10+4]=h=(TH2D*) hES->Clone();
    h->Sumw2();
    h->SetNameTitle(core+"_hcf_zph",core+" hit-cell hits>thres, 2D");
 
    hA[10+5]=h=(TH2D*) hES->Clone();
    h->Sumw2();
    h->SetNameTitle(core+"_acf_zph",core+" any-cell hits>thres, 2D ");

//    hA[10*jcyl+3] =new TH1D(core+"mHit_"+txt, titIC+" multiplicity cell>thr "+txt+" ; cell/event",20,-0.5,19.5);
 
    printf("Histos  initialized core=%s\n",core.Data());

}



//-------------------------------------------
//-------------------------------------------
void EvalPhotD::processEvent(DLmadEvent* eveMad,DLg4Event* eveG4){

  double refRcyl=30.*cm; //  reference cylinder for projections
  // printf("neve=%d\n",NtotEve);
  hES->Reset(); // 2D hit distribution per event

  NtotEve++;
  //if(NtotEve==1) eve->Print();
  double W= eveG4->GetWeight();
  totW+=W;
  hA[0]->Fill("inp",1.);

  if(NtotEve%200000==0)
    printf("EvalPhotD::processing event NO=%.4e, ID=%d, nTr=%d\n",1.*NtotEve,eveG4-> GetEventID(),  eveG4->GetNumTracks());
  
  hA[1]->Fill( eveG4->GetNumTracks());
  

  //.... select primary track of interest
 
  int my_hepId=22; // gamma
  // int my_hepId=13; // mu-
  // int my_hepId=11; // e-

  DLmadTrack* trackMad=0; 
  for (int i = 0; i < eveMad->nOutPart(); i++)    {
    DLmadTrack* track = eveMad->getOutParticle(i);
    int pid = track->hepId;
    if(pid!=my_hepId) continue;
    //printf("added mad pid=%d\n",pid);
    trackMad=track;
    break;
  }

  int hitBinX=-999, hitBinY=-888;  
  if(trackMad) { // truth-track identified
    hA[0]->Fill("madTr",1.);
    //.... compute  eta-phi of hit tower
    // for now ignore curvature of electron track
    DLg4Vertex *vertex=eveG4->GetVertex();
    // vertex->Print();
    TVector3 vector=trackMad->P4.Vect().Unit();
    vector=vector* refRcyl*vector.Perp();
    vector+=vertex->GetPosition();
    // printf("\n vector@cyl (mm)   xyz=%.3f,%.3g,%.3g \n",vector.x(),vector.y(),vector.z());

    //..... get id of hit tower based on mad-tracks
    hitBinX=hES->GetXaxis()->FindBin(vector.z());
    hitBinY=hES->GetYaxis()->FindBin(vector.Phi());
    // printf("  hitBinY,Y=%d,%d\n",hitBinX, hitBinY);
  }

  //:::::::::::::::::::::  Loop over tracks ::::::::::::::::
  
  int nAnyTot=0, nG4TrWhit=0;
  for(int t=0;t< eveG4->GetNumTracks();t++){ // loop over tracks in event
    DLg4Track* track=eveG4->GetTrack(t);
    //TVector3 p=track->GetMomentum(); printf(" pz=%f pt=%f\n",p.Pz(),p.Pt());

    int nPhotHit=0;
    for(int i=0; i<track->GetNumHits(); i++){
      DLg4Hit* hit=track->GetHit(i);
      TString detName=hit->GetDetectorID();
      int kcyl=-1;

      if(par_mode==0) {
	if( detName.Contains("phoCy"))  {
	  kcyl=atoi(detName.Data()+5);
	  nPhotHit++; 
	}
      }	else  if(par_mode==-1) { // special case for debug of lead/sint sampling
	if( detName.Contains("photLead"))  {
	  kcyl=99;
	}	
      }
    
      if(kcyl<0) continue;

      // if(kcyl==1) printf("aa %s kcyl=%d\n",detName.Data()+5,kcyl);
      // hit->Print();      printf("\n");
      nAnyTot++;
      // accumulate energy deposit per volume
      double de= hit->GetDE(); 
      double lgDE_kev=TMath::Log10(de/keV);
      if(lgDE_kev>-5) ((TH2D*)  hA[4])->Fill(detName ,lgDE_kev,1.);

      TVector3 pos=hit->GetPosition(); 
      double phi=pos.Phi();
      // if(kcyl==0)  printf("%s %d  r%f z=%f  de=%f  logDe=%f\n",detName.Data(),kcyl,pos.Perp(),pos.Z(),de_mev*1000., lgDE_kev);      
      ((TH2D*) hES)->Fill(pos.Z(),phi,de);
    }// end of hit-loop
    if(nPhotHit)  hA[3]->Fill( track->GetParticleName(), 1.);
    if(nPhotHit)  nG4TrWhit++; 
 }   // end of loop over tracks
  
  //::::::::::::::::::::  analyze event ::::::::::::::
  if(nG4TrWhit)  hA[2]->Fill( nG4TrWhit);
  if(nAnyTot<=0) return;
  
  TH2D *h2= hES;   assert(h2);   // input
  int nbx=h2->GetNbinsX();
  int nby=h2->GetNbinsY();
  double eveW2= 1./nby;
  int nCell=0;
  for (int jx=1;jx<=nbx;jx++) 
    for (int jy=1;jy<=nby;jy++){
      bool isHitCell= (jx==hitBinX) &&  (jy==hitBinY);
      double ene=h2->GetBinContent(jx,jy);
      if(ene<=0.) continue;
      if( isHitCell) hA[10+0] ->Fill(ene/MeV);
      hA[10+1] ->Fill(ene/MeV);
      if(ene< par_minCellEne) continue;
      nCell++; 
      double zval=h2->GetXaxis()->GetBinCenter(jx);
      double phival=h2->GetYaxis()->GetBinCenter(jy);

      // counts cells  vs. Z, weighted by #phi bins      
      if( isHitCell) { 
	 hA[10+2]->Fill(zval, eveW2); 
	((TH2D *)hA[10+4])->Fill(zval,phival); // counts cells in 2D
      }
      hA[10+3]->Fill(zval, eveW2);
      ((TH2D *)hA[10+5])->Fill(zval,phival); // counts cells in 2D
      //	printf("got hit jx=%d zval=%f \n",jx,zval);
    }
}// end of cyl-loop



//-------------------------------------------
//-------------------------------------------
void EvalPhotD::saveHisto(TFile *fd) {
  
  printf(" histos are written  to '%s' " ,fd->GetName());
  for(int i=0;i<mxH;i++) {
    if(hA[i]) hA[i]->Write();
  }
  
  printf("          , save Ok %d events done\n",NtotEve);
}




