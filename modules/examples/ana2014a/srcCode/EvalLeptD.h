// \class  EvalLeptDEvalLeptD
// rate at cylindrical lepton
// \author Jan Balewski
// $Id: EvalLeptD.h,v 1.1.1.1 2011/12/20 19:08:36 balewski Exp $

#ifndef EvalLeptD_h
#define EvalLeptD_h

class TH1;
class TH2D;
class TFile;
class DLmadEvent;
class DLg4Event;
#include "TString.h"

class EvalLeptD :public TObject{ 
 private:
  int NtotEve;
  double totW; // total weight of read in events
  float par_z1, par_z2, par_dz, par_dphi; // defines tiling 
  int par_mode; // setup switch
  TString core; // prefix for all histos

  const static int mxH=32;
  TH1 *hA[mxH]; // input spectra
  TH2D *hES; // event spectra for sensitive layer, cleared for each event
  double par_minCellEne; // thershold for counting fired strip

 public:

  EvalLeptD(TString corex, float z1, float z2, float dz, float dphi, int kcyl);
  void  initHisto();
  void  saveHisto(TFile *);
  void  processEvent( DLmadEvent* , DLg4Event*);

  ClassDef(EvalLeptD,1) 
};
     
#endif


// $Log: EvalLeptD.h,v $
// Revision 1.1.1.1  2011/12/20 19:08:36  balewski
// start0
//
