//==================================================
// printDLg4Eve.cc is a standalone program that displays
//  all the data from the specified file (see below).
//  It is meant as a demonstrator of how to format the
//  contents of the DLg4Event.
//
// Note that the location of DLUnits.h is hardcoded and
//  will need to be adjusted if you're compiling it
//  yourself.
//
//  Author: Ross Corliss
//  Date: 2014-04-22
//==================================================


#include "DLUnits.h"
#include <TFile.h>
#include <stdio.h>
#include "DLg4Event.h"
#include "DLg4Track.h"
#include "DLg4Hit.h"
#include <TTree.h>
#include <TVector3.h>
#include <vector>
using namespace dali;  //uses DLUnits without needing dali::___

void printDLg4Eve(DLg4Event *eve);
void printDLg4Track(const DLg4Track *tr);
void printDLg4Hit(const DLg4Hit *h);

int main(int argc, char **argv){
  TFile *input=0;
  //printf("starting. argc=%d, argv=%x\n", argc, argv);
  if (argc==1){
    input=TFile::Open("one.fortracking.DLg4.root");
  }
  if (!input) {
    printf("could not open one.fortracking.DLg4.root\n");
    return 0;
  }
  printf("opened file, %x\n",input);
  TTree *tree=(TTree*)input->Get("DLg4EventTree");
  DLg4Event *eve=0; tree->SetBranchAddress("DLg4Events",&eve);

  int nEvents=tree->GetEntries();
    printf("File has %d events\n",nEvents);
  for (int i=0;i<nEvents;i++){
    //printf("(wrapper for event %d)\n",i);
    tree->GetEntry(i,0);
    //printf("(pointer for eve: %x)\n",eve);
    if (!eve){
      printf("Read error.  Could not find event i=%d.\n",i);
      continue;
    }
    printDLg4Eve(eve);
    }
  return 0;
}

void printDLg4Eve(DLg4Event *eve){
  double weight=eve->GetWeight();
  const std::vector<DLg4Track>* tracks=eve->GetTracks();
  int eventID=eve->GetEventID();
  printf("\nDLG4_EVENT:");
  printf("\teventID = %d, weight = %g (pb), nTrack=%d  ", eventID, weight/pb, (int)tracks->size());
  //  vertex.Print();
  int totHits=0;
  for (int i = 0; i < (int)tracks->size(); i++)  {
    printf("\nTr%d:", i);
    printDLg4Track(&(tracks->at(i)) );
    totHits+=(tracks->at(i)).GetNumHits();
  }
  printf("\n end of eve: tot Hits=%d\n",totHits);

  return;
}

void printDLg4Track(const DLg4Track *tr){
  
  int trackID=tr->GetTrackID();
  int parentID=tr->GetParentID();
  TString particleName=tr->GetParticleName();
  TVector3 origin=tr->GetOrigin(); //double check wording.
  TVector3 momentum=tr->GetMomentum();
  const std::vector<DLg4Hit>* hits=tr->GetHits();

  printf("\tTRACK:");
  printf("\ttrackID = %d",trackID);
  printf("\tparentID = %d",parentID);
  printf("\tparticle = %s",particleName.Data());
  printf("\torigin x=(%5.1f,%5.1f,%5.1f) cm",origin.X()/cm,origin.Y()/cm,origin.Z()/cm);
  printf("\tp=(%.2g,%.2g,%.2g), |p|=%.2f  MeV",momentum.X()/MeV,momentum.Y()/MeV,momentum.Z()/MeV, momentum.Mag()/MeV);

  int nHit=(int)hits->size();
  printf("\tnHit=%d ",nHit);
  for (int i = 0; i < nHit; i++)  {
    printf("\n%d::",i);
    printDLg4Hit(&(hits->at(i)));
  }

  return;
}

void printDLg4Hit(const DLg4Hit *h){
  TString detectorID=h->GetDetectorID();
  TVector3 position=h->GetPosition(); //double check wording.
  TVector3 momentum=h->GetLocalMomentum();
  double dE=h->GetDE();
  double kinE=h->GetKinE();

  printf("HIT:");
  printf(" detectorID = %s",detectorID.Data());
  printf(" dE=%.2g MeV, kinE=%.1fMeV,", dE,kinE);
  printf(" global xyz=(%.1f,%.1f,%.1f) cm",position.X()/cm,position.Y()/cm,position.Z()/cm);
  printf(" local Pxyz=(%.1f,%.1f,%.1f) MeV",momentum.X()/MeV,momentum.Y()/MeV,momentum.Z()/MeV);

  return;
}
