//*-- Author : Jan Balewski, MIT, 2011/08/03
//
#ifndef DALI_EVE_THEO__HH
#define DALI_EVE_THEO__HH

#include <TObject.h>
#include <TLorentzVector.h>

// the official DL-units match those of Geant4:  MeV, mm, nsec 

//---------------
class DLmadTrack  : public TObject{ // track info
 public:
  TLorentzVector P4; // 4-momentum vector in MeV
  Int_t hepId;
  //defined here: http://www.slac.stanford.edu/BFROOT/www/Computing/Environment/NewUser/htmlbug/node51.html
  DLmadTrack() { clear();}
  
  void clear() {
    P4=TLorentzVector(0,0,0,-1);
    hepId=99999;
  } 
  
  void print( int flag=0){
    printf(" Track (MeV)   Pxyz=%.3f,%.3g,%.3g  PT=%.3g  E=%.3g M=%.3g  hepId=%d\n",P4.X(), P4.Y(), P4.Z(),P4.Pt(), P4.E(), P4.M(), hepId);
    if(flag){}	
  }
  
  ClassDef(DLmadTrack,1);

};

//---------------
class DLmadEvent : public TObject {
 public:
  // .....variables ....
  //calculates tree-level matrix element, default assumption alpha' = 10^-8
  Double_t weight; // measured in picobarns for events set
  Int_t id; // event ID
  std::vector <DLmadTrack> out; // outgoing particles

  // .... methods ....
  DLmadEvent(){};
  Int_t nOutPart(){ return (int) out.size();} 

  void addOutParticleGeV( int hepID, double pxGeV, double pyGeV, double pzGeV, double eGeV) {
    addOutParticleMeV( hepID,pxGeV*1.e3,pyGeV*1.e3,pzGeV*1.e3,eGeV*1.e3); 
  }

  void addOutParticleMeV( int hepID, double pxGeV, double pyGeV, double pzGeV, double eGeV) {
    DLmadTrack tr;
    tr.P4=TLorentzVector(pxGeV,pyGeV,pzGeV,eGeV); // in MeV
    tr.hepId=hepID;
    out.push_back(tr);
  }

  void addOutParticle(DLmadTrack *madTrk) {
    DLmadTrack tr;
    tr.P4=madTrk->P4;
    tr.hepId=madTrk->hepId;
    out.push_back(tr);
  }

  DLmadTrack *getOutParticle(int i) {
    if(i<0 ||i >= nOutPart()) return NULL;
    return &(out[i]);
  }

  void clear() { 
    id=-55;weight=-66;
    out.clear();
    //printf("event-clear done\n");
  }
  
  void print(int flag=0) {
    printf("\nDLmadEvent:  ID=%d  weight/pb=%g nOutParticles=%d\n",id, weight,nOutPart());
    for(unsigned int i=0;i< out.size();i++) {
      printf(" it=%d ",i); out[i].print(flag);    
    }
  }// end of PRINT

  ClassDef(DLmadEvent,1)

};

#endif


// $Log: DLmadEvent.h,v $
// Revision 1.5  2011/08/03 20:52:30  purnimab
// Fixed memory leak and cleaned up other memory/deletion issues
//
// Revision 1.4  2011/07/22 21:14:55  balewski
// dd units in printout
//
// Revision 1.3  2011/07/22 18:45:57  balewski
// eanup
//
// Revision 1.2  2011/07/16 22:42:36  balewski
// Adjust to new MadGraph units: MeV & ub, DL events reamain in: MeV & pb
//
// Revision 1.1  2011/07/14 17:06:44  balewski
// start
//
//
