from __future__ import division
from math import *

def degToRad(x):
	return x*pi/180;
def radToDeg(x):
	return x*180/pi;
def sqrSum(l):
	return sqrt(sum([i**2 for i in l]))

class Centers:
	def __init__(self, init, theta):
		self.theta = theta
		self.ends = [init]
		self.centers = []
	def append(self, thickness):
		new0 = self.ends[-1][0]+thickness/2.*sin(self.theta)
		new1 = self.ends[-1][1]+thickness/2.*cos(self.theta)
		self.centers.append((new0,new1))
		end0 = self.ends[-1][0]+thickness*sin(self.theta)
		end1 = self.ends[-1][1]+thickness*cos(self.theta)
		self.ends.append((end0,end1))
	def lastCenter(self):
		return [self.centers[-1][0],0,self.centers[-1][1]]

def PolarPosition(r,angle,height):
	theta = -degToRad(angle)
	return [r*sin(theta),height,r*cos(theta)]

def RotateX(pos,angle):
	theta = degToRad(angle)
	return [pos[0],pos[1]*cos(theta) - pos[2]*sin(theta),pos[1]*sin(theta)+pos[2]*cos(theta)]

def RotateY(pos,angle):
	theta = degToRad(angle)
	return [pos[0]*cos(theta) - pos[2]*sin(theta),pos[1],pos[0]*sin(theta)+pos[2]*cos(theta)]

def RotateZ(pos,angle):
	theta = degToRad(angle)
	return [pos[0]*cos(theta) - pos[1]*sin(theta),pos[0]*sin(theta)+pos[1]*cos(theta),pos[2]]

def XYZtoZYX(v):
	x,y,z = [degToRad(i) for i in v]
	
	var1 = -cos(x)*cos(z)*sin(y) + sin(x)*sin(z)
	# = -sin(yprime)
	yprime = -asin(var1)
	
	var2 = cos(z)*sin(x) + cos(x)*sin(y)*sin(z)
	# = cosyprime * sinxprime
	xprime = asin(var2/cos(yprime))
	
	var3 = cos(z)*sin(x)*sin(y) + cos(x)*sin(z)
	# = cosyprime * sinzprime
	zprime = asin(var3/cos(yprime))

	return [radToDeg(i) for i in [xprime,yprime,zprime]]

def minus(l):
	return [-i for i in l]

def subtract(l1,l2):
	return [i-j for i,j in zip(l1,l2)]

def add(l1,l2):
	return [i+j for i,j in zip(l1,l2)]