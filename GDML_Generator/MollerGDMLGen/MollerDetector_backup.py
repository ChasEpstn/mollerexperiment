from __future__ import division
from math import *
from GDML_Shapes import *
from GDML_Util import *
from GeometryGlobals import *

print "Generating Moller Geometry at %d degrees" %(analyzeAngle)

MollerExp = Detector("Moller")
SETEXPERIMENT(MollerExp)


#################################################################
# Target Chamber ...
#################################################################
print "Building Target Chamber"
US_BP_len = 152.4
US_BP_iRad = 36.449
US_BP_oRad = 38.1

UpstreamBeampipe = Tube("UpstreamBP",US_BP_iRad,US_BP_oRad,US_BP_len,80,360)
UpstreamBeampipe.SetPosition([0,0,-140.287])
UpstreamBeampipe.SetMaterial(Al)

UpstreamBeampipeVacuum = UpstreamBeampipe.FillVacuum()
UpstreamBeampipeVacuum.SetVirtual()



USBPCutout = Tube("UpstreamBP_Cutout",0,US_BP_oRad,100,80,360)
USBPCutout.SetVirtual()

TC_height = 300.
TC_iRad = 74.549
TC_oRad = 76.2
TargetChamber0 = Tube("TargetChamber0",TC_iRad,TC_oRad,TC_height,80,360)
TargetChamber0.SetVirtual()

DS_BP_len = 166.624
DS_BP_iRad = 17.399
DS_BP_oRad = 19.05
DownstreamBeampipe = Tube("DownstreamBP",DS_BP_iRad,DS_BP_oRad,DS_BP_len,80,360)
DownstreamBeampipe.SetPosition([0,0,155.386])
DownstreamBeampipe.SetMaterial(Al)

DownstreamBeampipeVacuum = DownstreamBeampipe.FillVacuum()
DownstreamBeampipeVacuum.SetVirtual()



DSBPCutout = Tube("DownstreamBP_Cutout",0,DS_BP_oRad,100,80,360)
DSBPCutout.SetVirtual()


Lumi_BP_len = 203.2
Lumi_BP_iRad = 8.636
Lumi_BP_oRad = 9.525


LumiBeampipe = Tube("LumiBP",Lumi_BP_iRad,Lumi_BP_oRad,Lumi_BP_len,80,360)
LumiPipeDist = sqrSum([74.186,159.0915])
LumiBeampipe.SetPosition([LumiPipeDist*sin(lumiTheta),0,LumiPipeDist*cos(lumiTheta)])
LumiBeampipe.SetRotation([0,-lumiAngle,0])
LumiBeampipe.SetMaterial(Al)

LumiBeampipeVacuum = LumiBeampipe.FillVacuum()
LumiBeampipeVacuum.SetVirtual()


LumiBPCutout = Tube("LumiBPCutout",0,Lumi_BP_oRad,100,80,360)
LumiBPCutout.SetVirtual()

Exit_oRad = 19.05

ExitFlangeCutout = Tube("ExitFlangeCutout",0,Exit_oRad,100,80,360)
ExitFlangeCutout.SetVirtual()


TargetChamberUS = SubtractionSolid("TargetChamberUS",TargetChamber0,USBPCutout,[0,TC_oRad,0],[90,0,0])
TargetChamberUS.SetVirtual()

TargetChamberDS = SubtractionSolid("TargetChamberDS",TargetChamberUS,DSBPCutout,[0,-TC_oRad,0],[90,0,0])
TargetChamberDS.SetVirtual()


TargetChamberL = SubtractionSolid("TargetChamberL",TargetChamberDS,LumiBPCutout,[TC_oRad*sin(lumiTheta),-TC_oRad*cos(lumiTheta),0],[90,0,lumiAngle])
TargetChamberL.SetVirtual()

TargetChamber = SubtractionSolid("TargetChamber",TargetChamberL,ExitFlangeCutout,[TC_oRad*sin(-nominalTheta),-TC_oRad*cos(nominalTheta),0],[90,0,-nominalAngle])
TargetChamber.SetMaterial(Al)
TargetChamber.SetRotation([90,0,0])
TargetChamberVacuum = TargetChamber0.FillVacuum()
TargetChamberVacuum.SetVirtual()


TargetChamberVacuum.SetPosition(TargetChamber.GetPosition())
TargetChamberVacuum.SetRotation(TargetChamber.GetRotation())

ExitFlangeConnector = Tube("ExitFlangeConnector",17.399,19.05,14.225,80,360)
ExitFlangeConnector.SetMaterial(Al)
ExitFlangeConnectorDist = sqrSum([-45.419,0,64.8655])
ExitFlangeConnector.SetPosition(PolarPosition(ExitFlangeConnectorDist,nominalAngle,0))
ExitFlangeConnector.SetRotation([0,nominalAngle,0])

ExitFCVacuum = ExitFlangeConnector.FillVacuum()
ExitFCVacuum.SetVirtual()


ExitFlangeCone = Cone("ExitFlangeCone",69.139,17.399,49.15,19.05,50.8,80,360)
ExitFlangeCone.SetMaterial(Al)
ExitFlangeConeDist = sqrSum([-69.3265,0,99.0085])
ExitFlangeCone.SetPosition(PolarPosition(ExitFlangeConeDist,nominalAngle,0))
ExitFlangeCone.SetRotation([0,nominalAngle,0])

ExitFConeVacuum = ExitFlangeCone.FillVacuum()
ExitFConeVacuum.SetVirtual()


ExitFlangeConflat = Tube("ExitFlangeConflat",49.41,75.819,37.719,80,360)
ExitFlangeConflat.SetMaterial(Al)
ExitFlangeConflatDist = sqrSum([-100.519,0,143.243])
ExitFlangeConflat.SetPosition(PolarPosition(ExitFlangeConflatDist,nominalAngle,0))
ExitFlangeConflat.SetRotation([0,nominalAngle,0])

ExitFConflatVac = ExitFlangeConflat.FillVacuum()
ExitFConflatVac.SetVirtual()

#################################################################
# Bellows and Collimator
#################################################################
print "Building Bellows and Collimator"

ConflatToBellows = Cone("ConflatToBellows",18.974,49.15,58.567,50.8,60.325,80,360)
ConflatToBellows.SetMaterial(Al)
ConflatToBellowsDist = sqrSum([-117.1055,0,167.2435])
ConflatToBellows.SetPosition(PolarPosition(ConflatToBellowsDist,nominalAngle,0))
ConflatToBellows.SetRotation([0,nominalAngle,0])

CTBVac = ConflatToBellows.FillVacuum()
CTBVac.SetVirtual()


Bellows_iRad = 58.675
Bellows_oRad = 60.325

center0 = (175.015,122.547)
bellowsEndDistance = sqrSum([259.261,120.815])
center1 = (bellowsEndDistance*cos(analyzeTheta),bellowsEndDistance*sin(analyzeTheta))
centerDist = sqrSum([i-j for i,j in zip(center1,center0)]) #mm

relativeTheta = nominalTheta-analyzeTheta

thToBeam = -atan((center0[1]-center1[1])/(center0[0]-center1[0]))
print "Theta to Beam:",radToDeg(thToBeam)
angle0 = thToBeam+nominalTheta
print "Starting Angle:",radToDeg(angle0)
angle1 = angle0-relativeTheta
print "Final Angle:",radToDeg(angle1)

scaledBellows_iRad = Bellows_iRad*cos(angle0)
scaledBellows_oRad = Bellows_oRad*cos(angle0)

dx0 = abs(scaledBellows_oRad*tan(angle0))
dx1 = abs(scaledBellows_oRad*tan(angle1))
print "Additional Length: %g and %g" %(dx0,dx1)
cut0 = 2.*dx0*cos(angle0)
cut1 = 2.*dx1*cos(angle1)

totalLength = centerDist + dx0 + dx1
print "total length:",totalLength
dx0vac = abs(scaledBellows_iRad*tan(angle0))
dx1vac = abs(scaledBellows_iRad*tan(angle1))

totalLengthVAC = centerDist + dx0vac + dx1vac


Bellows_Outer = EllipticalTube("Bellows_Outer",scaledBellows_oRad,Bellows_oRad,totalLength/2)
Bellows_Outer.SetVirtual()

Bellows_Inner = EllipticalTube("Bellows_Inner",scaledBellows_iRad,Bellows_iRad,totalLength+1)
Bellows_Inner.SetVirtual()

Bellows_NoCut = SubtractionSolid("Bellows_NoCut",Bellows_Outer,Bellows_Inner)
Bellows_NoCut.SetVirtual()

Cut_Bellows0 = Box("Cut_Bellows0",1000,1000,cut0)
Cut_Bellows0.SetVirtual()

Cut_Bellows1 = Box("Cut_Bellows1",1000,1000,cut1)
Cut_Bellows1.SetVirtual()

Bellows_wCut0 = SubtractionSolid("Bellows_wCut0",Bellows_NoCut,Cut_Bellows0,[0,0,-totalLength/2],[0,-radToDeg(angle0),0])
Bellows_wCut0.SetVirtual()

Bellows_VacInitial = EllipticalTube("Bellows_VacInitial",scaledBellows_iRad,Bellows_iRad,totalLengthVAC/2)
Bellows_VacInitial.SetVirtual()

BellowsVac_wCut0 = SubtractionSolid("BellowsVac_wCut0",Bellows_VacInitial,Cut_Bellows0,[0,0,-totalLengthVAC/2],[0,-radToDeg(angle0),0])
BellowsVac_wCut0.SetVirtual()

Bellows = SubtractionSolid("Bellows",Bellows_wCut0,Cut_Bellows1,[0,0,totalLength/2],[0,-radToDeg(angle1),0])
Bellows.SetMaterial(Al)
Bellows.SetRotation([0,-radToDeg(thToBeam),0])
avgpos = [(i[0]+i[1])/2 for i in zip(center0,center1)]
bCenterDist = sqrSum(avgpos)
bCenterAngle = (analyzeAngle+nominalAngle)/2#radToDeg(atan(avgpos[1]/avgpos[0]))
# print "Bellows Angle:",bCenterAngle
# bCenterDist += 0.5*(dx1-dx0)
bellowsPos = [-avgpos[1],0,avgpos[0]] #PolarPosition(bCenterDist,bCenterAngle,0)
bDeltaPos = PolarPosition(0.5*(dx1-dx0),-radToDeg(thToBeam),0)
# avgpos[0] += Bellows_oRad*cos(nominalTheta) - Bellows_oRad*cos(analyzeTheta)
Bellows.SetPosition([i+j for i,j in zip(bellowsPos,bDeltaPos)])

BellowsVac = SubtractionSolid("BellowsVac",BellowsVac_wCut0,Cut_Bellows1,[0,0,totalLengthVAC/2],[0,-radToDeg(angle1),0])
BellowsVac.SetVirtual()#SetMaterial(Vac)
BellowsVac.SetRotation(Bellows.GetRotation())
# avgposVac = [(i[0]+i[1])/2 for i in zip(center0,center1)]
# avgposVac[0] += Bellows_iRad*cos(nominalTheta) - Bellows_iRad*cos(analyzeTheta)
bVacCenterDist = bCenterDist# + 0.5*(dx1vac-dx0vac)
bVacDeltaPos = PolarPosition(0.5*(dx1vac-dx0vac),-radToDeg(thToBeam),0)
bellowsVacPos = bellowsPos#PolarPosition(bVacCenterDist,bCenterAngle,0)
BellowsVac.SetPosition([i+j for i,j in zip(bVacDeltaPos,bellowsVacPos)])

flangeThk = 18.974
centers = Centers((-center1[1],center1[0]),-analyzeTheta)
centers.append(flangeThk)

BellowsToConflat = CopyObject(ConflatToBellows)
BellowsToConflat.SetName("BellowsToConflat")
BellowsToConflat.SetRotation([0,analyzeAngle-180,0])
BellowsToConflat.SetPosition(centers.lastCenter())

BTCVac = BellowsToConflat.FillVacuum()
BTCVac.SetVirtual()


conflatThk = 37.719
centers.append(conflatThk)
BellowsConflat = CopyObject(ExitFlangeConflat)
BellowsConflat.SetName("BellowsConflat")
BellowsConflat.SetPosition(centers.lastCenter())
BellowsConflat.SetRotation([0,analyzeAngle,0])

BCVac = BellowsConflat.FillVacuum()
BCVac.SetVirtual()


colTHK = 25.4*0.75 #mm
roundTHK = 23.038-(colTHK-6.35)#23.038
centers.append(roundTHK) #round piece thickness
RoundPiece = Tube("RoundPiece",49.1494,50.927,roundTHK,80,360)
RoundPiece.SetMaterial(Al)
RoundPiece.SetPosition(centers.lastCenter())
RoundPiece.SetRotation([0,analyzeAngle,0])

RPVac = RoundPiece.FillVacuum()
RPVac.SetVirtual()


#######################################
#Replacing this with the Collimator
rtsTHK = colTHK#6.35
centers.append(rtsTHK) #round to square transition
colLocation = centers.lastCenter()

# RTS0 = Tube("RTS0",0,50.927,rtsTHK,80,360)
# RTS0.SetVirtual()

# RTS_Cut = Box("RTS_Cut",25.4,82.55,8)
# RTS_Cut.SetVirtual()

# RoundToSquare = SubtractionSolid("RoundToSquare",RTS0,RTS_Cut)
# RoundToSquare.SetMaterial(Al)
# RoundToSquare.SetPosition(centers.lastCenter())
# RoundToSquare.SetRotation([0,analyzeAngle,0])
########################################
#The Collimator:

colAcceptance = degToRad(1.0)
colVert = degToRad(1.0)
colCutTHK = colTHK + 5 #mm

distToColCenter = sqrSum(colLocation)
distToCol_Near = distToColCenter - colCutTHK/2.
distToCol_Far = distToColCenter + colCutTHK/2.

colHorizHalfLen_Near = distToCol_Near * tan(colAcceptance/2)
colHorizHalfLen_Far = distToCol_Far * tan(colAcceptance/2)
colVertHalfLen_Near = distToCol_Near * tan(colVert/2)
colVertHalfLen_Far = distToCol_Far	* tan(colVert/2)

colSize = 150
Collimator0 = Box("Collimator0",colSize,colSize,colTHK)
Collimator0.SetVirtual()

Collimator_cut = Trapezoid("Collimator_cut",colHorizHalfLen_Far,colHorizHalfLen_Near,colVertHalfLen_Far,colVertHalfLen_Near,colCutTHK)
Collimator_cut.SetVirtual()

Collimator = SubtractionSolid("Collimator",Collimator0,Collimator_cut)
Collimator.SetMaterial("G4_W")
Collimator.SetPosition(centers.lastCenter())
Collimator.SetRotation([0,analyzeAngle,0])


distToColVac_Near = distToColCenter - colTHK/2.
distToColVac_Far = distToColCenter + colTHK/2.

colHorizHalfLenVac_Near = distToColVac_Near * tan(colAcceptance/2)
colHorizHalfLenVac_Far = distToColVac_Far * tan(colAcceptance/2)
colVertHalfLenVac_Near = distToColVac_Near * tan(colVert/2)
colVertHalfLenVac_Far = distToColVac_Far	* tan(colVert/2)

CollimatorVac = Trapezoid("Collimator_cut",colHorizHalfLenVac_Far,colHorizHalfLenVac_Near,colVertHalfLenVac_Far,colVertHalfLenVac_Near,colTHK)
CollimatorVac.SetVirtual()#SetMaterial(Vac)
CollimatorVac.SetPosition(Collimator.GetPosition())
CollimatorVac.SetRotation(Collimator.GetRotation())

#################################################################
# Vacuum Chamber and Magnet
#################################################################
print "Building Vacuum Chamber and Magnet"

centers.append(301.625) #chamber position
# print "Chamber_pos \t %g\t 90.4875 \t %g \t mm" % centers.lastCenter()

SpecChamber0 = Box("SpecChamber0",38.1,276.226,301.625)
SpecChamber0.SetVirtual()

SpecChamberCutout1 = Tube("SpecChamberCutout1",0,180.975,50,180,90)
SpecChamberCutout1.SetVirtual()

SpecChamberCutout2 = Tube("SpecChamberCutout2",187.325,269.875,25.4,180,90)
SpecChamberCutout2.SetVirtual()

SpecChamberVac1 = CopyObject(SpecChamberCutout2)
SpecChamberVac1.SetName("SCVac1")
SpecChamberVac1.SetVirtual()

SpecChamberCutout3 = Box("SpecChamberCutout3",25.4,82.55,302.625)
SpecChamberCutout3.SetVirtual()

SpecChamberVac2 = CopyObject(SpecChamberCutout3)
SpecChamberVac2.SetName("SCVac2")
SpecChamberVac2.SetVirtual()

SpecChamberCutout4 = Box("SpecChamberCutout4",25.4,209.55,114.3)
SpecChamberCutout4.SetVirtual()

SpecChamberVac3 = CopyObject(SpecChamberCutout4)
SpecChamberVac3.SetName("SCVac3")
SpecChamberVac3.SetVirtual()

SpecChamberCutout5 = Box("SpecChamberCutout5",50,55.975,27.4)
SpecChamberCutout5.SetVirtual()

SpecChamber1 = SubtractionSolid("SpecChamber1",SpecChamber0,SpecChamberCutout1,[0,138.113,-151],[0,90,0])
SpecChamber1.SetVirtual()

SpecChamber2 = SubtractionSolid("SpecChamber2",SpecChamber1,SpecChamberCutout2,[0,139.113,-151],[0,90,0])
SpecChamber2.SetVirtual()

SpecChamber3 = SubtractionSolid("SpecChamber3",SpecChamber2,SpecChamberCutout3,[0,-90.488,0],[0,0,0])
SpecChamber3.SetVirtual()

SpecChamber4 = SubtractionSolid("SpecChamber4",SpecChamber3,SpecChamberCutout4,[0,-26.988,94.6625],[0,0,0])
SpecChamber4.SetVirtual()

SpecChamber = SubtractionSolid("SpecChamber",SpecChamber4,SpecChamberCutout5,[0,111.1255,138.1125],[0,0,0])
SpecChamber.SetMaterial(Al)
specPos = centers.lastCenter()
specPos[1] += 90.4875
SpecChamber.SetPosition(specPos)
SpecChamber.SetRotation([0,analyzeAngle,0])

pos1 = [0,139.113,-151]
pos2 = [0,-90.488,0]
pos3 = [0,-26.988,94.6625]

pos2m1 = [a-b for a,b in zip(pos2,pos1)]
Vac0RelPos = [-pos2m1[2],pos2m1[1],-pos2m1[0]]
SCVac0 = UnionSolid("SCVac0",SpecChamberVac1,SpecChamberVac2,Vac0RelPos,[0,90,0])
SCVac0.SetVirtual()

pos3m1 = [a-b for a,b in zip(pos3,pos1)]
Vac1RelPos = [-pos3m1[2],pos3m1[1],-pos3m1[0]]
SCVac = UnionSolid("SCVac",SCVac0,SpecChamberVac3,Vac1RelPos,[0,90,0])
SCVac.SetVirtual()#SetMaterial(Vac)
pos0 = [a-b for a,b in zip(specPos,pos1)]
SCVacRot = SpecChamber.GetRotation()
SCVacRot[1] -= 90
SCVac.SetRotation(SCVacRot)
SCVacPos = PolarPosition(sqrSum(centers.lastCenter())-301.625/2,analyzeAngle,276.226/2+90.4875)
SCVac.SetPosition(SCVacPos)

tenthou = 10./1000*25.4 #mm
ExitDumpFoil = Box("ExitDumpFoil",25.4,209.55,tenthou)
ExitDumpFoil.SetMaterial("G4_KAPTON")
ExitDumpFoil.SetPosition(PolarPosition(sqrSum(centers.lastCenter())+301.625/2+tenthou/2,analyzeAngle,209.55/2-90.4875/2))
ExitDumpFoil.SetRotation([0,analyzeAngle,0])

############ Magnet Yoke arcs
Magnet_iRad = 187.325 - 5
Manget_oRad = 269.875 + 5
MagnetCoil_Width = 30
MagnetDist = sqrSum([specPos[0],specPos[2]]) - 301.625/2


CoilSeparation = 75

coilWidth = (Manget_oRad - Magnet_iRad)/2
bendRad = (Manget_oRad + Magnet_iRad)/2

centerPos = PolarPosition(MagnetDist,analyzeAngle,specPos[1]+276.226/2)
deltaPos = PolarPosition(CoilSeparation/2,analyzeAngle-90,0)
leftPos = [sum(c) for c in zip(centerPos,deltaPos)]
rightPos = [c[0]-c[1] for c in zip(centerPos,deltaPos)]

LeftArc = Tube("LeftArc",Magnet_iRad,Manget_oRad,MagnetCoil_Width,180,90)
LeftArc.SetPosition(leftPos)
LeftArc.SetMaterial("G4_Fe")
LeftArc.SetRotation([0,analyzeAngle-90,0])

RightArc = Tube("RightArc",Magnet_iRad,Manget_oRad,MagnetCoil_Width,180,90)
RightArc.SetPosition(rightPos)
RightArc.SetMaterial("G4_Fe")
RightArc.SetRotation([0,analyzeAngle-90,0])

CenterArc = Tube("CenterArc",Magnet_iRad-MagnetCoil_Width,Magnet_iRad,CoilSeparation+MagnetCoil_Width,180,90)
CenterArc.SetPosition(centerPos)
CenterArc.SetMaterial("G4_Fe")
CenterArc.SetRotation([0,analyzeAngle-90,0])

#################################################################
# Detector
#################################################################
print "Building Detector"

d2img = bendRad*bendRad/MagnetDist
print 'Magnet Dist:',MagnetDist
print 'Bending radius:',bendRad

detLen = 150
detWidth = 50
detThk = 1
detCore = "G4_POLYSTYRENE"
detCladding = "G4_PLEXIGLASS"#According to wikipedia, plexiglass is PMMA 
detectorPos = [-(MagnetDist+bendRad)*sin(analyzeTheta),bendRad+d2img,(MagnetDist+bendRad)*cos(analyzeTheta)]
vertRot = radToDeg(atan(1+(MagnetDist-bendRad)/(2*bendRad)))
fThk = 0.2 #mm
fPitchHoriz = 0.2 #mm
fPitchVert = 0.2 #mm
coreFrac = 0.98 #fractional
detThk = 3*fPitchVert+fThk
nX = int(detLen/fThk)
nY = int(detWidth/fThk)
print "Array of %i by %i fibers" %(nX,nY)
def fiberPosX(n,layer):
    fDist = (MagnetDist+bendRad) + n*fPitchHoriz*cos(degToRad(vertRot)) - layer*fPitchVert*sin(degToRad(vertRot))
    fDeltY = n*fPitchHoriz*sin(degToRad(vertRot)) 
    height = bendRad+d2img+layer*fPitchVert*cos(degToRad(vertRot))+fDeltY+fThk/2.
    return  [-(fDist)*sin(analyzeTheta),height,(fDist)*cos(analyzeTheta)]
#fiberPosX(-10,1)
def fiberPosY(n,layer):
    fDist = (MagnetDist+bendRad) - layer*fPitchVert*sin(degToRad(vertRot))
    xDelt = n*fPitchHoriz*cos(analyzeTheta)
    zDelt = n*fPitchHoriz*sin(analyzeTheta)
    height = bendRad+d2img+layer*fPitchVert*cos(degToRad(vertRot))+fThk/2.
    return  [-(fDist)*sin(analyzeTheta)+xDelt,height,(fDist)*cos(analyzeTheta)+zDelt]
#fiberPosY(-10,1)
def cStr(s):
    if (s > 9) and (s < 100):
        return '0'+str(s)
    elif s < 10:
        return '00'+str(s)
    else:
        return str(s)

if False:#Monolithic detector
    Detector = Box("Detector",detWidth,detThk,detLen)
    Detector.SetPosition(detectorPos)
    Detector.SetMaterial(detCore)
    Detector.SetRotation(XYZtoZYX([vertRot,analyzeAngle,0]))
    Detector.SetSensitive("DetPlane")
if True:#Crossed fiber detector
    if False:#Square
        xFiberOuter = Box("xFiberOuter",fThk,fThk,detWidth)
        xFiberInner = Box("xFiberInner",fThk*coreFrac,fThk*coreFrac,detWidth)
        yFiberOuter = Box("yFiberOuter",fThk,fThk,detLen)
        yFiberInner = Box("yFiberInner",fThk*coreFrac,fThk*coreFrac,detLen)
    if True: #Round  
        xFiberOuter = Tube("xFiberOuter",0,fThk/2.,detWidth,0,360)
        xFiberInner = Tube("xFiberInner",0,fThk/2.*coreFrac,detWidth,0,360)
        yFiberOuter = Tube("yFiberOuter",0,fThk/2.,detLen,0,360)
        yFiberInner = Tube("yFiberInner",0,fThk/2.*coreFrac,detLen,0,360)
    
    xFiberClad = SubtractionSolid("xFiberClad",xFiberOuter,xFiberInner)
    yFiberClad = SubtractionSolid("yFiberClad",yFiberOuter,yFiberInner)
    xFiberClad.SetMaterial(detCladding)
    yFiberClad.SetMaterial(detCladding)
    xFiberInner.SetMaterial(detCore)
    yFiberInner.SetMaterial(detCore)
    xFiberOuter.SetVirtual()
    yFiberOuter.SetVirtual()
    xFiberInner.SetVirtual()
    yFiberInner.SetVirtual()
    xFiberClad.SetVirtual()
    yFiberClad.SetVirtual()
    xF1 = []
    xF2 = []
    yF1 = []
    yF2 = []
    fiberRot = XYZtoZYX([vertRot,analyzeAngle,0])
    fiberRotY = XYZtoZYX([vertRot,analyzeAngle,0])
    fiberRotX = [0,analyzeAngle+90,-vertRot]#XYZtoZYX([0,analyzeAngle,0])
    for i in range(int(nX)):
         xF1.append(CopyObject(xFiberInner))
         xF1[-1].UnsetVirtual()
         xF1[-1].SetMaterial(detCore)
         xF1[-1].SetName("xFiberL0N"+str(i))
         xF1[-1].SetPosition(fiberPosX(i-nX/2,0))
         xF1[-1].SetRotation(fiberRotX)
         xF1[-1].SetSensitive("xFiberL0N"+str(i))
    for i in range(int(nX)):
         xF2.append(CopyObject(xFiberInner))
         xF2[-1].UnsetVirtual()
         xF2[-1].SetMaterial(detCore)
         xF2[-1].SetName("xFiberL2N"+str(i))
         xF2[-1].SetPosition(fiberPosX(i-nX/2-1.0/2,2))
         xF2[-1].SetRotation(fiberRotX)
         xF2[-1].SetSensitive("yFiberL2N"+str(i))
    for i in range(nY):
         yF1.append(CopyObject(yFiberInner))
         yF1[-1].UnsetVirtual()
         yF1[-1].SetMaterial(detCore)
         yF1[-1].SetName("yFiberL1N"+str(i))
         yF1[-1].SetPosition(fiberPosY(i-nY/2,1))
         yF1[-1].SetRotation(fiberRotY)
         yF1[-1].SetSensitive("yFiberL1N"+str(i))
    for i in range(nY):
         yF2.append(CopyObject(yFiberInner))
         yF2[-1].UnsetVirtual()
         yF2[-1].SetMaterial(detCore)
         yF2[-1].SetName("yFiberL3N"+str(i))
         yF2[-1].SetPosition(fiberPosY(i-nY/2-1.0/2,3))
         yF2[-1].SetRotation(fiberRotY)
         yF2[-1].SetSensitive("yFiberL3N"+str(i))
    for i in range(nX):
         xF1.append(CopyObject(xFiberClad))
         xF1[-1].UnsetVirtual()
         xF1[-1].SetMaterial(detCladding)
         xF1[-1].SetName("xFiberCladL0N"+str(i))
         xF1[-1].SetPosition(fiberPosX(i-nX/2,0))
         xF1[-1].SetRotation(fiberRotX)
    for i in range(nX):
         xF2.append(CopyObject(xFiberClad))
         xF2[-1].UnsetVirtual()
         xF2[-1].SetMaterial(detCladding)
         xF2[-1].SetName("xFiberCladL2N"+str(i))
         xF2[-1].SetPosition(fiberPosX(i-nX/2-1.0/2,2))
         xF2[-1].SetRotation(fiberRotX)
    for i in range(nY):
         yF1.append(CopyObject(yFiberClad))
         yF1[-1].UnsetVirtual()
         yF1[-1].SetMaterial(detCladding)
         yF1[-1].SetName("yFiberCladL1N"+str(i))
         yF1[-1].SetPosition(fiberPosY(i-nY/2,1))
         yF1[-1].SetRotation(fiberRotY)
    for i in range(nY):
         yF2.append(CopyObject(yFiberClad))
         yF2[-1].UnsetVirtual()
         yF2[-1].SetMaterial(detCladding)
         yF2[-1].SetName("yFiberCladL3N"+str(i))
         yF2[-1].SetPosition(fiberPosY(i-nY/2-1.0/2,3))
         yF2[-1].SetRotation(fiberRotY)



#################################################################
# Detector connector stuff
#################################################################

strTHK = 6.35
STR0 = Tube("STR0",0,50.927,strTHK,80,360)
STR0.SetVirtual()

STR_Cut = Box("STR_Cut",25.4,82.55,8)
STR_Cut.SetVirtual()

RoundToSquare = SubtractionSolid("RoundToSquare",STR0,STR_Cut,[0,0,0],[0,0,-analyzeAngle])
RoundToSquare.SetMaterial(Al)
RoundToSquare.SetPosition([-(MagnetDist+bendRad)*sin(analyzeTheta),centerPos[1]+strTHK/2,(MagnetDist+bendRad)*cos(analyzeTheta)])
RoundToSquare.SetRotation([90,0,0])

RTSVac = Box("RTSVac",25.4,82.55,strTHK)
RTSVac.SetVirtual()#SetMaterial(Vac)
RTSVac.SetRotation([90,analyzeAngle,0])
RTSVac.SetPosition(RoundToSquare.GetPosition())

RoundPiece2 = CopyObject(RoundPiece)
RoundPiece2.SetName("RoundPiece2")
RoundPiece2.SetPosition([-(MagnetDist+bendRad)*sin(analyzeTheta),centerPos[1]+strTHK+roundTHK/2,(MagnetDist+bendRad)*cos(analyzeTheta)])
RoundPiece2.SetRotation([90,0,0])

RP2Vac = RoundPiece2.FillVacuum()
RP2Vac.SetVirtual()

# RP2Vac.SetVirtual()#SetMaterial(Vac)
#RP2Vac.SetPosition(RoundPiece2.GetPosition())
# RP2Vac.SetRotation(RoundPiece2.GetRotation())

FinalConflat = CopyObject(ExitFlangeConflat)
FinalConflat.SetName("FinalConflat")
FinalConflat.SetPosition([-(MagnetDist+bendRad)*sin(analyzeTheta),centerPos[1]+strTHK+roundTHK+conflatThk/2,(MagnetDist+bendRad)*cos(analyzeTheta)])
FinalConflat.SetRotation([90,0,0])

FCVac = FinalConflat.FillVacuum()
FCVac.SetVirtual()

# FCVac.SetVirtual()#SetMaterial(Vac)
#FCVac.SetPosition(FinalConflat.GetPosition())
# FCVac.SetRotation(FinalConflat.GetRotation())

fVacOverlap = 0
fvacStart = FinalConflat.GetPosition()
fvacStart[1] += conflatThk/2 #[-(MagnetDist+bendRad)*sin(analyzeTheta),centerPos[1]+strTHK+roundTHK+conflatThk,(MagnetDist+bendRad)*cos(analyzeTheta)]
fvacStart[1] -= fVacOverlap
fVacH = tan(degToRad(vertRot))*82.55/2
fVacHeight = detectorPos[1]-fvacStart[1]+fVacH -1 
FinalVacuum0 = Box("FinalVacuum0",25.4,fVacHeight,82.55)
FinalVacuum0.SetVirtual()

# dx0FV = 82.55*sin(degToRad(vertRot))
# cut0FV = 2.*dx0FV*cos(degToRad(vertRot))

cut0FV = 82.55*sin(degToRad(vertRot))

FVac_Cut = Box("FVac_Cut",1000,cut0FV,1000)
FVac_Cut.SetVirtual()

FinalVacuum = SubtractionSolid("FinalVacuum",FinalVacuum0,FVac_Cut,[0,fVacHeight/2,0],[-vertRot,0,0])
FVacPos = fvacStart[::]
FVacPos[1] += fVacHeight/2
FinalVacuum.SetPosition(FVacPos)
FinalVacuum.SetRotation([0,analyzeAngle,0])
FinalVacuum.SetVirtual()#SetMaterial(Vac)


fVacLen = 2*fVacH/sin(degToRad(vertRot))
FinalWindow = Box("FinalWindow",25.4,tenthou/2,fVacLen)
FWinPos = detectorPos[::]
FWinPos[1] -= 1
FinalWindow.SetPosition(FWinPos)
FinalWindow.SetRotation(XYZtoZYX([vertRot,analyzeAngle,0]))
FinalWindow.SetMaterial("G4_KAPTON")

#################################################################
# Shielding														
#################################################################
print "Building Shielding"

# shieldTHK = 25 #mm
shieldTHK = 25.4
shieldWidth = 350 #mm
shieldHeight = 500 #mm
shieldDepth = 350 #mm

Shield1 = Box("Shield1",shieldWidth,shieldHeight,shieldTHK)
Shield1.SetVirtual()

Shield2 = Box("Shield2",shieldTHK,shieldHeight,shieldDepth)
Shield2.SetVirtual()

Shield3 = Box("Shield3",shieldTHK,shieldHeight,shieldDepth)
Shield3.SetVirtual()

Shield01 = UnionSolid("Shield01",Shield1,Shield2,[-shieldWidth/2-shieldTHK/2,0,shieldDepth/2-shieldTHK/2])
Shield01.SetVirtual()

Shield = UnionSolid("Shield",Shield01,Shield3,[shieldWidth/2+shieldTHK/2,0,shieldDepth/2-shieldTHK/2])
Shield.SetMaterial("G4_Pb")
Shield.SetPosition(PolarPosition(sqrSum(Collimator.GetPosition()),analyzeAngle,colSize/2+shieldHeight/2))
Shield.SetRotation([0,analyzeAngle,0])

if(False):
	ShieldLower = CopyObject(Shield)
	ShieldLower.SetName("ShieldLower")
	lshieldpos = Shield.GetPosition()
	lshieldpos[1] *= -1
	ShieldLower.SetPosition(lshieldpos)

if(False):
	shieldRLen = 250
	overlap = conflatThk*1.4
	ShieldR = Tube("ShieldR",75.819,85,shieldRLen,0,360)
	shieldPos = FinalConflat.GetPosition()
	shieldPos[1] += shieldRLen/2 + conflatThk/2 - overlap

	ShieldR.SetPosition(shieldPos)
	ShieldR.SetRotation([90,0,0])
	ShieldR.SetMaterial("G4_Pb")

#################################################################
# Vacuum Solids
#################################################################
print "Building Internal Vacuum"

VacInit = Box("VacInit",1,1,1)
VacInit.SetVirtual()

VacBoundary0 = Tube("VacBoundary0",0,49.41-5,conflatThk+10,0,360)
VacBoundary0.SetPosition(ExitFConflatVac.GetPosition())
VacBoundary0.SetRotation(ExitFConflatVac.GetRotation())
VacBoundary0.SetVirtual()

VacBoundary01 = CopyObject(VacBoundary0)
VacBoundary0.SetPosition(BellowsConflat.GetPosition())
VacBoundary0.SetRotation(BellowsConflat.GetRotation())
VacBoundary01.SetVirtual()

VacBoundary1 = Tube("VacBoundary1",0,49.41-7,1.5*conflatThk,0,360)
VacBoundary1.SetPosition([i-j for i,j in zip(BellowsConflat.GetPosition(),PolarPosition(conflatThk,analyzeAngle,0))])
VacBoundary1.SetRotation(BellowsConflat.GetRotation())
VacBoundary1.SetVirtual()

VacBoundary2 = Tube("VacBoundary2",0,49.41-7,conflatThk,0,360)
VacBoundary2.SetPosition([i+j for i,j in zip(ExitFConflatVac.GetPosition(),PolarPosition(conflatThk,nominalAngle,0))])
VacBoundary2.SetRotation(ExitFConflatVac.GetRotation())
VacBoundary2.SetVirtual()

FCVac = FinalConflat.FillVacuum()
FCVac.SetVirtual()

FinalOverlapVac = Box("FinalOverlapVac",25.4,5,82.55)
FinalOverlapVac.SetPosition([i-j for i,j in zip(FinalVacuum.GetPosition(),[0,fVacHeight/2,0])])
FinalOverlapVac.SetRotation(FinalVacuum.GetRotation())
FinalOverlapVac.SetVirtual()

VAC_OBJECTS = [VacInit,TargetChamberVacuum,UpstreamBeampipeVacuum,DownstreamBeampipeVacuum,LumiBeampipeVacuum,ExitFCVacuum,ExitFConeVacuum,ExitFConflatVac,CTBVac,BellowsVac,BTCVac,BCVac,RPVac,CollimatorVac,SCVac,RTSVac,RP2Vac,FCVac,VacBoundary0,VacBoundary01,VacBoundary1,VacBoundary2,FinalOverlapVac,FinalVacuum]

VAC_ALL = UnionChain(VAC_OBJECTS).MakeUnionSolids()
VAC_ALL.SetMaterial(Vac)
VAC_ALL.SetPosition([0,0,0])
VAC_ALL.SetRotation([0,0,0])

print "Done!"
MollerExp.Save()
