from __future__ import division
from math import *
from GDML_Shapes import *
from GDML_Util import *
from GeometryGlobals import *
import argparse

parser = argparse.ArgumentParser(description='Set the angle')
parser.add_argument('angle',metavar='ang',type=int,nargs=1,help='specify the angle')
args = parser.parse_args()
print 'The angle is '+str(args.angle[0])

analyzeAngle = float(args.angle[0])
analyzeTheta = degToRad(analyzeAngle)

print "Generating Moller Geometry at %d degrees" %(analyzeAngle)

MollerExp = Detector("Moller")
SETEXPERIMENT(MollerExp)


#################################################################
# Target Chamber ...
#################################################################
print "Building Target Chamber"
US_BP_len = 152.4
US_BP_iRad = 36.449
US_BP_oRad = 38.1

UpstreamBeampipe = Tube("UpstreamBP",US_BP_iRad,US_BP_oRad,US_BP_len,80,360)
UpstreamBeampipe.SetPosition([0,0,-140.287])
UpstreamBeampipe.SetMaterial(Al)

UpstreamBeampipeVacuum = UpstreamBeampipe.FillVacuum()
UpstreamBeampipeVacuum.SetVirtual()



USBPCutout = Tube("UpstreamBP_Cutout",0,US_BP_oRad,100,80,360)
USBPCutout.SetVirtual()

TC_height = 300.
TC_iRad = 74.549
TC_oRad = 76.2
TargetChamber0 = Tube("TargetChamber0",TC_iRad,TC_oRad,TC_height,80,360)
TargetChamber0.SetVirtual()


#Downstream Beampipe stuff


DS_BP_len1 = 219.5322#292.935

DS_BP_len2 = 254.0#431.8#s166.624
DS_BP_iRad = 22.098/2.#17.399
DS_BP_oRad = 25.4/2.#19.05

DS_BP_len = DS_BP_len1 + DS_BP_len2 + 25.4

DS_BP_iRad2 = 34.798/2.0
DS_BP_oRad2 = 38.1/2.0

DownstreamBeampipe = Tube("DownstreamBP",DS_BP_iRad,DS_BP_oRad,DS_BP_len1,80,360)
DownstreamBeampipe.SetPosition([0,0,DS_BP_len1/2+TC_oRad])
DownstreamBeampipe.SetMaterial(Al)

DownstreamBeampipeVacuum = DownstreamBeampipe.FillVacuum()
DownstreamBeampipeVacuum.SetVirtual()

DS_FLG1 = Tube("DS_FLG1",DS_BP_iRad,69.342/2.0,25.4/2.0,90,360)
DS_FLG1.SetPosition([0,0,TC_oRad + DS_BP_len1 + 25.4/4.0])
DS_FLG1.SetMaterial(Al)

DS_FLG1Vac = DS_FLG1.FillVacuum()
DS_FLG1Vac.SetVirtual()

DS_FLG2 = Tube("DS_FLG2",DS_BP_iRad2,69.342/2.0,25.4/2.0,90,360)
DS_FLG2.SetPosition([0,0,TC_oRad + DS_BP_len1 + 3.0*25.4/4.0])
DS_FLG2.SetMaterial(Al)

DS_FLG2Vac = DS_FLG1.FillVacuum()
DS_FLG2Vac.SetVirtual()




DSBPCutout = Tube("DownstreamBP_Cutout",0,DS_BP_oRad,100,80,360)
DSBPCutout.SetVirtual()

DSBPC_len = 14.224
DSBPC_iRad = 34.798/2.
DSBPC_oRad = 38.1/2.

DSBPC = Tube("DSBPC",DSBPC_iRad,DSBPC_oRad,DSBPC_len+DS_BP_len2,80,360)
DSBPC.SetPosition([0,0,TC_oRad+DS_BP_len1+25.4+(DS_BP_len2+DSBPC_len)/2.0])
DSBPC.SetMaterial(Al)

DSBPCVac = DSBPC.FillVacuum()
DSBPCVac.SetVirtual()




DSBPCone_r1 = DSBPC_iRad
DSBPCone_r2 = DSBPC_oRad
DSBPCone_r3 = 72.898/2.0
DSBPCone_r4 = 76.2/2.0
DSBPCone_l = 79.375

DSBPCone = Cone("DSBPCone",DSBPCone_l,DSBPCone_r1,DSBPCone_r3,DSBPCone_r2,DSBPCone_r4,80,360)
DSBPCone.SetPosition([0,0,DS_BP_len+TC_oRad+DSBPC_len+DSBPCone_l/2.0])
DSBPCone.SetMaterial(Al)

DSBPConeVac = DSBPCone.FillVacuum()
DSBPConeVac.SetVirtual()


DSBPLT_iRad = DSBPCone_r3
DSBPLT_oRad = DSBPCone_r4
DSBPLT_l = 317.5

DSBPLT = Tube("DSBPLT",DSBPLT_iRad,DSBPLT_oRad,DSBPLT_l,80,360)
DSBPLT.SetPosition([0,0,DS_BP_len+TC_oRad+DSBPC_len+DSBPCone_l+DSBPLT_l/2.0])
DSBPLT.SetMaterial(Al)

DSBPLTVac = DSBPLT.FillVacuum()
DSBPLTVac.SetVirtual()

# Box3 = Box("Box3",1,1,1)
# Box3.SetVirtual()
#
# DSBPA = Tube("DSBPA",0,DS_BP_oRad,DS_BP_len1,80,360)
# DSBPA.SetPosition([0,0,DS_BP_len1/2+TC_oRad])
# DSBPA.SetVirtual()
# DSFLGA = Tube("DSFLGA",0,69.342/2.0,25.4,90,360)
# DSFLGA.SetPosition([0,0,TC_oRad + DS_BP_len1 + 25.4/2.0])
# DSFLGA.SetVirtual()
# DSBPCA = Tube("DSBPCA",0,DSBPC_oRad,DSBPC_len+DS_BP_len2,80,360)
# DSBPCA.SetPosition([0,0,TC_oRad+DS_BP_len1+25.4+(DS_BP_len2+DSBPC_len)/2.0])
# DSBPCA.SetVirtual()
#
# DS_Apparatus = UnionChain([Box3,DSBPA,DSFLGA,DSBPCA]).MakeUnionSolids()
# DS_Apparatus.SetVirtual()#Will use this to hollow out Shielding


#Luminosity side stuff

Lumi_BP_len1 = 143.3576#431.615#203.2
Lumi_BP_iRad = 8.636
Lumi_BP_oRad = 9.525
Lumi_BP_len2 = 177.8

Lumi_BP_len = Lumi_BP_len1 + Lumi_BP_len2 + 25.4


LumiBeampipe = Tube("LumiBP",Lumi_BP_iRad,Lumi_BP_oRad,Lumi_BP_len1,80,360)
# LumiPipeDist = sqrSum([74.186,159.0915])
LumiBeampipe.SetPosition(PolarPosition(Lumi_BP_len1/2.0+TC_oRad,-lumiAngle,0))#[LumiPipeDist*sin(lumiTheta),0,LumiPipeDist*cos(lumiTheta)])
LumiBeampipe.SetRotation([0,-lumiAngle,0])
LumiBeampipe.SetMaterial(Al)

LumiBeampipeVacuum = LumiBeampipe.FillVacuum()
LumiBeampipeVacuum.SetVirtual()


LumiBPCutout = Tube("LumiBPCutout",0,Lumi_BP_oRad,100,80,360)
LumiBPCutout.SetVirtual()

LFC_iRad = 38.1/2.0#19.05/2.0
LFC_oRad = 171.45/2.0#151.638/2.0
LFC_l =  19.812


Lumi_FLG1 = Tube("Lumi_FLG1",Lumi_BP_iRad,69.342/2.0,25.4/2.0,90,360)
Lumi_FLG1.SetPosition(PolarPosition(TC_oRad + Lumi_BP_len1 + 25.4/4.0,-lumiAngle,0))
Lumi_FLG1.SetMaterial(Al)
Lumi_FLG1.SetRotation([0,-lumiAngle,0])

Lumi_FLG1Vac = Lumi_FLG1.FillVacuum()
Lumi_FLG1Vac.SetVirtual()

Lumi_FLG2 = Tube("Lumi_FLG2",LFC_iRad,69.342/2.0,25.4/2.0,90,360)
Lumi_FLG2.SetPosition(PolarPosition(TC_oRad + Lumi_BP_len1 + 3.0*25.4/4.0,-lumiAngle,0))
Lumi_FLG2.SetMaterial(Al)
Lumi_FLG2.SetRotation([0,-lumiAngle,0])

Lumi_FLG2Vac = Lumi_FLG1.FillVacuum()
Lumi_FLG2Vac.SetVirtual()



LumiBP2_iRad = 34.798/2.

LumiBP2 = Tube("LumiBP2",LumiBP2_iRad,LFC_iRad,Lumi_BP_len2,80,360)
LumiBP2.SetPosition(PolarPosition(TC_oRad+Lumi_BP_len1+25.4+(Lumi_BP_len2)/2.0,-lumiAngle,0))
LumiBP2.SetMaterial(Al)
LumiBP2.SetRotation([0,-lumiAngle,0])

LumiBP2Vac = LumiBP2.FillVacuum()
LumiBP2Vac.SetVirtual()








#LFC1 is the 6in conflat with a round hole in it

LFC1 = Tube("LFC1",LFC_iRad,LFC_oRad,LFC_l,80,360)
LFCdist = sqrSum([382.262,178.252])-LFC_l/2.0#radial dist of LFC2
LFC1.SetPosition(PolarPosition(LFCdist,-lumiAngle,0))
LFC1.SetRotation([0,-lumiAngle,0])
LFC1.SetMaterial(Al)

LFC1Vac = LFC1.FillVacuum()
LFC1Vac.SetVirtual()#should be in list!


#LFC2 is the 6in conflat flange with a rectangular hole

LFC2_init = Tube("LFC2_init",0,LFC_oRad,LFC_l,80,390)
LFC2_init.SetVirtual()
LFC2_cutout = Box("LFC2_cutout",13.716,125.019,LFC_l+2)
LFC2_cutout.SetVirtual()

LFC2 = SubtractionSolid("LFC2",LFC2_init,LFC2_cutout)
LFC2.SetVirtual()#Virtual; pos/rot 0/0, will be connected to flat thing

#Will connect the conflat w/square hole and the flat part of the vac enclosure with a UnionSolid,
#to make things easier to handle / repeat

LFC2dist = sqrSum([382.262,178.252])+LFC_l/2.0#radial dist of LFC2

LFC2Vac = LFC2.FillVacuum() #POS/ROT = 000/000 at this stage
LFC2Vac.SetVirtual()#don't add to list; will Union with other one


#LVacS1 is the rectangular part of the chamber that connects
#	the conflat flange to the arc part of the chamber

LVacS1_init = Box("LVacS1_init",19.05,130.353,101.6)
LVacS1_init.SetVirtual()

LVacS1_cutout = Box("LVacS1_cutout",13.716,125.019,101.6+2)
LVacS1_cutout.SetVirtual()

LVacS1 = SubtractionSolid("LVacS1",LVacS1_init,LVacS1_cutout)
LVacS1.SetVirtual()

LVacS1Vac = LVacS1.FillVacuum()#Has pos/rot 0/0 at this stage
LVacS1Vac.SetVirtual()#don't add to list!


LVacSP = UnionSolid("LVacSP",LFC2,LVacS1,[0,0,101.6/2.0-12.192+LFC_l/2.0])
#^ Union the conflat 6in and the rectangular enclosure coming out of it
#^ Has pos/rot coordinates of LFC2

LVacSP.SetPosition(PolarPosition(LFC2dist,-lumiAngle,0))
LVacSP.SetRotation([0,-lumiAngle,0])
LVacSP.SetMaterial(Al)
#^OK Done

LVacSVac = UnionSolid("LVacSVac",LFC2Vac,LVacS1Vac,[0,0,101.6/2.0-12.192+LFC_l/2.0])
LVacSVac.SetVirtual()
LVacSVac.SetPosition(LVacSP.GetPosition())
LVacSVac.SetRotation(LVacSP.GetRotation())

LCorner_iRad = 429.647/2.0
LCorner_oRad = 690.353/2.0
LCorner_iiRad = 434.981/2.0
LCorner_ioRad = 685.019/2.0

LVacCorner_init = Tube("LVacCorner_init",LCorner_iRad,LCorner_oRad,19.05,270,90)
LVacCorner_init.SetVirtual()

LVacCorner_cut = Tube("LVacCorner_cut",LCorner_iiRad,LCorner_ioRad,13.716,269,92)
LVacCorner_cut.SetVirtual()

LVacCorner = SubtractionSolid("LVacCorner",LVacCorner_init,LVacCorner_cut)
LVacCorner.SetMaterial(Al)
LVacCorner.SetRotation([0,-lumiAngle+90,0])
LVacCorner.SetPosition(PolarPosition(LFC2dist+101.6-12.192+LFC_l/2.0,-lumiAngle,(LCorner_iRad+LCorner_oRad)/2.0))

LVacCornerVac = LVacCorner.FillVacuum()
LVacCornerVac.SetVirtual()


#Make the rectangular enclosure and the 6in conflat Again for the exit, post-magnet

LVacSPe = CopyObject(LVacSP)
# print LVacSPe.GetRotation()
LVacSPe.SetName("LVacSPe")
LVacSPe.SetRotation(XYZtoZYX([-90,-lumiAngle,0]))#new rotation
LVacSPe.SetPosition(PolarPosition(LFC2dist+101.6-12.192+LFC_l/2.0+(LCorner_iRad+LCorner_oRad)/2.0,-lumiAngle,(LCorner_iRad+LCorner_oRad)/2.0+101.6-12.12+LFC_l/2.0))#new position


LVacSPeVac = CopyObject(LVacSVac)
# print LVacSPe.GetPosition()
LVacSPeVac.SetPosition(LVacSPe.GetPosition())
LVacSPeVac.SetRotation([-90,-25,0])#I don't know why this works but it does (?)
# LVacSPeVac.SetRotation(LVacSPe.GetRotation())

LVacSVac.SetPosition(LVacSP.GetPosition())
LVacSVac.SetRotation(LVacSP.GetRotation())

# print LVacSP.GetRotation(), LVacSP.GetPosition()
# print LVacSVac.GetRotation(), LVacSVac.GetPosition()

# print LVacSPe.GetRotation(), LVacSPe.GetPosition()
# print LVacSPeVac.GetRotation(), LVacSPeVac.GetPosition()

LVacSPeVac.SetVirtual()

#None of the above flange vacuum things are in the right place! >.< Ugh




#Target chamber stuff
Exit_oRad = 19.05

ExitFlangeCutout = Tube("ExitFlangeCutout",0,Exit_oRad,100,80,360)
ExitFlangeCutout.SetVirtual()


TargetChamberUS = SubtractionSolid("TargetChamberUS",TargetChamber0,USBPCutout,[0,TC_oRad,0],[90,0,0])
TargetChamberUS.SetVirtual()

TargetChamberDS = SubtractionSolid("TargetChamberDS",TargetChamberUS,DSBPCutout,[0,-TC_oRad,0],[90,0,0])
TargetChamberDS.SetVirtual()


TargetChamberL = SubtractionSolid("TargetChamberL",TargetChamberDS,LumiBPCutout,[TC_oRad*sin(lumiTheta),-TC_oRad*cos(lumiTheta),0],[90,0,lumiAngle])
TargetChamberL.SetVirtual()

TargetChamber = SubtractionSolid("TargetChamber",TargetChamberL,ExitFlangeCutout,[TC_oRad*sin(-nominalTheta),-TC_oRad*cos(nominalTheta),0],[90,0,-nominalAngle])
TargetChamber.SetMaterial(Al)
TargetChamber.SetRotation([90,0,0])
TargetChamberVacuum = TargetChamber0.FillVacuum()
TargetChamberVacuum.SetVirtual()


TargetChamberVacuum.SetPosition(TargetChamber.GetPosition())
TargetChamberVacuum.SetRotation(TargetChamber.GetRotation())



# ExitFlangeCone = Cone("ExitFlangeCone",69.139,17.399,49.15,19.05,50.8,80,360)
# ExitFlangeCone.SetMaterial(Al)
# ExitFlangeConeDist = sqrSum([-69.3265,0,99.0085])
# ExitFlangeCone.SetPosition(PolarPosition(ExitFlangeConeDist,nominalAngle,0))
# ExitFlangeCone.SetRotation([0,nominalAngle,0])

# ExitFConeVacuum = ExitFlangeCone.FillVacuum()
# ExitFConeVacuum.SetVirtual()


ExitFlangeConflat = Tube("ExitFlangeConflat",49.41,75.819,37.719,80,360)
ExitFlangeConflat.SetVirtual()
ExitFlangeConflat.SetMaterial(Al)
ExitFlangeConflatDist = sqrSum([-100.519,0,143.243])
ExitFlangeConflat.SetPosition(PolarPosition(ExitFlangeConflatDist,nominalAngle,0))
ExitFlangeConflat.SetRotation([0,nominalAngle,0])

ExitFConflatVac = Box("asdf",1,1,1)#ExitFlangeConflat.FillVacuum()
ExitFConflatVac.SetVirtual()

#################################################################
# Pre-Bellows Connectors
#################################################################


#Doing this in 5 pieces: (1) Round exit pipe, (2) x2 conflat flanges,
#	(4) slightly bigger round exit pipe, (5) pipe to bellows disc

#First piece
ExitFlangeConnector = Tube("ExitFlangeConnector",17.399,19.05,8.484,80,360)#17.399,19.05,14.225,80,360)
ExitFlangeConnector.SetMaterial(Al)
ExitFlangeConnectorDist = sqrSum([59.04+6.494/2,0,41.34+4.866/2])#[-45.419,0,64.8655])
ExitFlangeConnector.SetPosition(PolarPosition(ExitFlangeConnectorDist,nominalAngle,0))
ExitFlangeConnector.SetRotation([0,nominalAngle,0])

#ok

ExitFCVacuum = Tube("ExitFlangeConnector_VAC",0,17.399,8.484,80,360)#ExitFlangeConnector.FillVacuum()
ExitFCVacuum.SetPosition(ExitFlangeConnector.GetPosition())
ExitFCVacuum.SetRotation(ExitFlangeConnector.GetRotation())
# ExitFCVacuum = ExitFlangeConnector.FillVacuum()
ExitFCVacuum.SetVirtual()

#Second piece
ExitF_CF = Tube("ExitF_CF",19.05,69.342/2,12.7,90,360)#48.26/2
ExitF_CF.SetMaterial(Al)
ExitF_CF_Dist = sqrSum([71.191002,0,49.848000])
ExitF_CF.SetPosition(PolarPosition(ExitF_CF_Dist,nominalAngle,0))
ExitF_CF.SetRotation([0,nominalAngle,0])

#ok
ExitF_CF_VAC = Tube("ExitF_CF_VAC",0,19.05,13.0,90,360)#ExitF_CF.FillVacuum()
ExitF_CF_VAC.SetPosition(ExitF_CF.GetPosition())
ExitF_CF_VAC.SetRotation(ExitF_CF.GetRotation())
# ExitF_CF_VAC = ExitF_CF.FillVacuum()
ExitF_CF_VAC.SetVirtual()

#Third piece
ExitF2_CF = Tube("ExitF2_CF",44.45/2,69.342/2,12.7,90,360)
ExitF2_CF.SetMaterial(Al)
ExitF2_CF_Dist = sqrSum([81.588997,0,57.125000])
ExitF2_CF.SetPosition(PolarPosition(ExitF2_CF_Dist,nominalAngle,0))
ExitF2_CF.SetRotation([0,nominalAngle,0])

# #ok
ExitF2_CF_VAC = Tube("ExitF2_CF_VAC",0,44.45/2-2,12.7,90,360)#ExitF2_CF.FillVacuum()
ExitF2_CF_VAC.SetPosition(ExitF2_CF.GetPosition())
ExitF2_CF_VAC.SetRotation(ExitF2_CF.GetRotation())
# ExitF2_CF_VAC = ExitF2_CF.FillVacuum()
ExitF2_CF_VAC.SetVirtual()

#Fourth piece
ExitFT = Tube("ExitFT",44.45/2-1.65,44.45/2,6.858,90,360)
ExitFT.SetMaterial(Al)
ExitFT_Dist = sqrSum([86.795+5.618/2,0,60.775+3.933/2])
ExitFT.SetPosition(PolarPosition(ExitFT_Dist,nominalAngle,0))
ExitFT.SetRotation([0,nominalAngle,0])

#ok
ExitFT_VAC = Tube("ExitFT_VAC",0,44.45/2-1.65,7.2,90,360)#ExitFT.FillVacuum()
ExitFT_VAC.SetPosition(ExitFT.GetPosition())
ExitFT_VAC.SetRotation(ExitFT.GetRotation())
# ExitFT_VAC = ExitFT.FillVacuum()
ExitFT_VAC.SetVirtual()

#Fifth piece
ExitFTC = Tube("ExitFTC",44.45/2,88.9/2,1.524,90,360)
ExitFTC.SetMaterial(Al)
ExitFTC_Dist = sqrSum([92.413+1.248/2,0,64.708+0.874/2])
ExitFTC.SetPosition(PolarPosition(ExitFTC_Dist,nominalAngle,0))
ExitFTC.SetRotation([0,nominalAngle,0])

#ok
ExitFTC_VAC = Tube("ExitFTC_VAC",0,44.45/2,2.0,90,360)#ExitFTC.FillVacuum()
ExitFTC_VAC.SetPosition(ExitFTC.GetPosition())
ExitFTC_VAC.SetRotation(ExitFTC.GetRotation())
# ExitFTC_VAC = ExitFTC.FillVacuum()
ExitFTC_VAC.SetVirtual()

ExtraVac = Tube("ExtraVac",0,17.399,50,80,360)
ExtraVac.SetPosition(PolarPosition(ExitF_CF_Dist,nominalAngle,0))
ExtraVac.SetRotation([0,nominalAngle,0])
ExtraVac.SetVirtual()

#################################################################
# Bellows and Collimator
#################################################################
print "Building Bellows and Collimator"

# ConflatToBellows = Cone("ConflatToBellows",18.974,49.15,58.567,50.8,60.325,80,360)
# ConflatToBellows.SetMaterial(Al)
# ConflatToBellowsDist = sqrSum([-117.1055,0,167.2435])
# ConflatToBellows.SetPosition(PolarPosition(ConflatToBellowsDist,nominalAngle,0))
# ConflatToBellows.SetRotation([0,nominalAngle,0])

# CTBVac = ConflatToBellows.FillVacuum()
# CTBVac.SetVirtual()


Bellows_iRad = 86.309/2#58.675
Bellows_oRad = 88.9/2#60.325

center0 = (93.661,65.582)#(175.015,122.547)
bellowsEndDistance = sqrSum([163.051,114.17])#sqrSum([259.261,120.815])
center1 = (bellowsEndDistance*cos(analyzeTheta),bellowsEndDistance*sin(analyzeTheta))
centerDist = sqrSum([i-j for i,j in zip(center1,center0)]) #mm

relativeTheta = nominalTheta-analyzeTheta

thToBeam = -atan((center0[1]-center1[1])/(center0[0]-center1[0]))
print "Theta to Beam:",radToDeg(thToBeam)
angle0 = thToBeam+nominalTheta
print "Starting Angle:",radToDeg(angle0)
angle1 = angle0-relativeTheta
print "Final Angle:",radToDeg(angle1)

scaledBellows_iRad = Bellows_iRad*cos(angle0)
scaledBellows_oRad = Bellows_oRad*cos(angle0)

dx0 = abs(scaledBellows_oRad*tan(angle0))
dx1 = abs(scaledBellows_oRad*tan(angle1))
print "Additional Length: %g and %g" %(dx0,dx1)
cut0 = 2.*dx0*cos(angle0)
cut1 = 2.*dx1*cos(angle1)

totalLength = centerDist + dx0 + dx1
print "total length:",totalLength
dx0vac = abs(scaledBellows_iRad*tan(angle0))
dx1vac = abs(scaledBellows_iRad*tan(angle1))

totalLengthVAC = centerDist + dx0vac + dx1vac


Bellows_Outer = EllipticalTube("Bellows_Outer",scaledBellows_oRad,Bellows_oRad,totalLength/2)
Bellows_Outer.SetVirtual()

Bellows_Inner = EllipticalTube("Bellows_Inner",scaledBellows_iRad,Bellows_iRad,totalLength+1)
Bellows_Inner.SetVirtual()

Bellows_NoCut = SubtractionSolid("Bellows_NoCut",Bellows_Outer,Bellows_Inner)
Bellows_NoCut.SetVirtual()

Cut_Bellows0 = Box("Cut_Bellows0",1000,1000,cut0)
Cut_Bellows0.SetVirtual()

Cut_Bellows1 = Box("Cut_Bellows1",1000,1000,cut1)
Cut_Bellows1.SetVirtual()

Bellows_wCut0 = SubtractionSolid("Bellows_wCut0",Bellows_NoCut,Cut_Bellows0,[0,0,-totalLength/2],[0,-radToDeg(angle0),0])
Bellows_wCut0.SetVirtual()

Bellows_VacInitial = EllipticalTube("Bellows_VacInitial",scaledBellows_iRad,Bellows_iRad,totalLengthVAC/2)
Bellows_VacInitial.SetVirtual()

BellowsVac_wCut0 = SubtractionSolid("BellowsVac_wCut0",Bellows_VacInitial,Cut_Bellows0,[0,0,-totalLengthVAC/2],[0,-radToDeg(angle0),0])
BellowsVac_wCut0.SetVirtual()

Bellows = SubtractionSolid("Bellows",Bellows_wCut0,Cut_Bellows1,[0,0,totalLength/2],[0,-radToDeg(angle1),0])
Bellows.SetMaterial(Al)
Bellows.SetRotation([0,-radToDeg(thToBeam),0])
avgpos = [(i[0]+i[1])/2 for i in zip(center0,center1)]
bCenterDist = sqrSum(avgpos)
bCenterAngle = (analyzeAngle+nominalAngle)/2#radToDeg(atan(avgpos[1]/avgpos[0]))
# print "Bellows Angle:",bCenterAngle
# bCenterDist += 0.5*(dx1-dx0)
bellowsPos = [-avgpos[1],0,avgpos[0]] #PolarPosition(bCenterDist,bCenterAngle,0)
bDeltaPos = PolarPosition(0.5*(dx1-dx0),-radToDeg(thToBeam),0)
# avgpos[0] += Bellows_oRad*cos(nominalTheta) - Bellows_oRad*cos(analyzeTheta)
Bellows.SetPosition([i+j for i,j in zip(bellowsPos,bDeltaPos)])

#ok
BellowsVac = SubtractionSolid("BellowsVac",BellowsVac_wCut0,Cut_Bellows1,[0,0,totalLengthVAC/2],[0,-radToDeg(angle1),0])
BellowsVac.SetVirtual()#SetMaterial(Vac)
BellowsVac.SetRotation(Bellows.GetRotation())
# avgposVac = [(i[0]+i[1])/2 for i in zip(center0,center1)]
# avgposVac[0] += Bellows_iRad*cos(nominalTheta) - Bellows_iRad*cos(analyzeTheta)
bVacCenterDist = bCenterDist# + 0.5*(dx1vac-dx0vac)
bVacDeltaPos = PolarPosition(0.5*(dx1vac-dx0vac),-radToDeg(thToBeam),0)
bellowsVacPos = bellowsPos#PolarPosition(bVacCenterDist,bCenterAngle,0)
BellowsVac.SetPosition([i+j for i,j in zip(bVacDeltaPos,bellowsVacPos)])

flangeThk = 18.974
centers = Centers((-center1[1],center1[0]),-analyzeTheta)
# centers = Centers((-center1[0],center1[1]),-analyzeTheta)

# centers.append(flangeThk)

# BellowsToConflat = CopyObject(ConflatToBellows)
# BellowsToConflat.SetName("BellowsToConflat")
# BellowsToConflat.SetRotation([0,analyzeAngle-180,0])
# BellowsToConflat.SetPosition(centers.lastCenter())

# BTCVac = BellowsToConflat.FillVacuum()
# BTCVac.SetVirtual()


conflatThk = 37.719
centers.append(conflatThk)
BellowsConflat = CopyObject(ExitFlangeConflat)
BellowsConflat.UnsetVirtual()
BellowsConflat.SetName("BellowsConflat")
BellowsConflat.SetPosition(centers.lastCenter())
BellowsConflat.SetRotation([0,analyzeAngle,0])

#ok
BCVac = Tube("BCVacASDF",0,49.41,39,80,360)#BellowsConflat.FillVacuum()
BCVac.SetPosition(BellowsConflat.GetPosition())
BCVac.SetRotation(BellowsConflat.GetRotation())
BCVac.SetVirtual()


colTHK = 32#25.4#*0.75 #mm
roundTHK = 1#23.038-(colTHK-6.35)#23.038
centers.append(roundTHK) #round piece thickness
RoundPiece = Tube("RoundPiece",49.1494,50.927,roundTHK,80,360)
RoundPiece.SetMaterial(Al)
RoundPiece.SetPosition(centers.lastCenter())
RoundPiece.SetRotation([0,analyzeAngle,0])

#ok
RPVac = RoundPiece.FillVacuum()
RPVac.SetVirtual()


#######################################
#Replacing this with the Collimator
rtsTHK = colTHK#6.35
centers.append(rtsTHK) #round to square transition
colLocation = centers.lastCenter()

# RTS0 = Tube("RTS0",0,50.927,rtsTHK,80,360)
# RTS0.SetVirtual()

# RTS_Cut = Box("RTS_Cut",25.4,82.55,8)
# RTS_Cut.SetVirtual()

# RoundToSquare = SubtractionSolid("RoundToSquare",RTS0,RTS_Cut)
# RoundToSquare.SetMaterial(Al)
# RoundToSquare.SetPosition(centers.lastCenter())
# RoundToSquare.SetRotation([0,analyzeAngle,0])
########################################
#The Collimator:

colAcceptance = degToRad(1.0)
colVert = degToRad(1.0)
colCutTHK = colTHK + 5 #mm

distToColCenter = sqrSum(colLocation)
distToCol_Near = distToColCenter - colCutTHK/2.
distToCol_Far = distToColCenter + colCutTHK/2.

colHoriz_Near = distToCol_Near * tan(colAcceptance)
colHoriz_Far = distToCol_Far * tan(colAcceptance)
colVert_Near = distToCol_Near * tan(colVert)
colVert_Far = distToCol_Far	* tan(colVert)

# colSize = 150
Collimator0 = Box("Collimator0",0.888*25.4,4.109*25.4,colTHK)
Collimator0.SetVirtual()

Collimator_cut = Trapezoid("Collimator_cut",colHoriz_Far,colHoriz_Near,colVert_Far,colVert_Near,colCutTHK)
Collimator_cut.SetVirtual()

CollimatorInit = SubtractionSolid("CollimatorInit",Collimator0,Collimator_cut)
CollimatorInit.SetVirtual()

ColVent1_Cut = Tube("ColVent1",0,6.35/2.0,60,80,360)
ColVent1_Cut.SetPosition([0,26.776,0])
ColVent1_Cut.SetRotation(XYZtoZYX([-45,0,0]))
ColVent1_Cut.SetVirtual()

ColVent2_Cut = Tube("ColVent2",0,6.35/2.0,60,80,360)
ColVent2_Cut.SetPosition([0,-26.776,0])
ColVent2_Cut.SetRotation(XYZtoZYX([45,0,0]))
ColVent2_Cut.SetVirtual()

ColVent1Vac = CopyObject(ColVent1_Cut)
ColVent1Vac.SetVirtual()

ColVent2Vac = CopyObject(ColVent2_Cut)
ColVent2Vac.SetVirtual()

CollimatorInit0 = SubtractionSolid("CollimatorInit0",CollimatorInit,ColVent1_Cut)
CollimatorInit0.SetVirtual()

Collimator = SubtractionSolid("Collimator",CollimatorInit,ColVent2_Cut)
Collimator.SetPosition(colLocation)
Collimator.SetRotation([0,analyzeAngle,0])
Collimator.SetMaterial("G4_W")

distToColVac_Near = distToColCenter - colTHK/2.
distToColVac_Far = distToColCenter + colTHK/2.

colHorizVac_Near = distToColVac_Near * tan(colAcceptance)
colHorizVac_Far = distToColVac_Far * tan(colAcceptance)
colVertVac_Near = distToColVac_Near * tan(colVert)
colVertVac_Far = distToColVac_Far	* tan(colVert)

VacInitCol1 = Box("VacInitCol1",1,1,1)
VacInitCol1.SetVirtual()

CollimatorVacInit = Trapezoid("CollimatorVacInit",colHorizVac_Far,colHorizVac_Near,colVertVac_Far,colVertVac_Near,colTHK)
CollimatorVacInit.SetVirtual()#SetMaterial(Vac)
CollimatorVacInit.SetPosition(Collimator.GetPosition())
CollimatorVacInit.SetRotation(Collimator.GetRotation())

CollimatorVac = UnionChain([CollimatorVacInit,ColVent1Vac,ColVent2Vac]).MakeUnionSolids()
CollimatorVac.SetPosition(Collimator.GetPosition())
CollimatorVac.SetRotation(Collimator.GetRotation())
CollimatorVac.SetVirtual()



#################################################################
# Vacuum Chamber and Magnet
#################################################################
print "Building Vacuum Chamber and Magnet"
openingheight=110.713 #outer
wallthk = 2.667
# openinghght = openingheight-2*wallthk
openinghgt = openingheight-2*wallthk

Magnet_iRad = 448.666/2 #187.325 - 5
Magnet_oRad = Magnet_iRad+openingheight#269.875 + 5

flatlength_h = (89.837-25.4-18.288/2.0)#87.7#flat length on the horizontal (entry)
flatlength_v = 74.542#flat length on the vertical (exit)
backlength = 74.6+56.134/2#100.05#length the exit dump protrudes from back

chamberlength = backlength+Magnet_iRad+openingheight+flatlength_h#522.746
centers.append(chamberlength) #chamber position
# print "Chamber_pos \t %g\t 90.4875 \t %g \t mm" % centers.lastCenter()
chamberheight = Magnet_iRad+flatlength_v+openingheight#422.746


SpecChamber0 = Box("SpecChamber0",38.1,chamberheight,chamberlength)
SpecChamber0.SetVirtual()

SpecChamberCutout1 = Tube("SpecChamberCutout1",0,Magnet_iRad,70,180,90)
SpecChamberCutout1.SetVirtual()

SpecChamberCutout2 = Tube("SpecChamberCutout2",Magnet_iRad+wallthk,Magnet_oRad-wallthk,25.4,180,90)
SpecChamberCutout2.SetVirtual()

SpecChamberVac1 = CopyObject(SpecChamberCutout2)
SpecChamberVac1.SetName("SCVac1")
SpecChamberVac1.SetVirtual()

SpecChamberCutout3 = Box("SpecChamberCutout3",25.4,openinghgt,chamberlength+1)
SpecChamberCutout3.SetVirtual()

SpecChamberVac2 = CopyObject(SpecChamberCutout3)
SpecChamberVac2.SetName("SCVac2")
SpecChamberVac2.SetVirtual()



co4hgt = 177.114#(chamberheight-openingheight-flatlength_v)#back cutout height...
co4dpth = backlength+10#196.0/2
SpecChamberCutout4 = Box("SpecChamberCutout4",25.4,co4hgt,co4dpth)
SpecChamberCutout4.SetVirtual()

SpecChamberVac3 = CopyObject(SpecChamberCutout4)
SpecChamberVac3.SetName("SCVac3")
SpecChamberVac3.SetVirtual()

SpecChamberCutout6 = Box("SpecChamberCutout6",25.4,chamberheight-openingheight/2,openinghgt)#vertical vacuum cutout
SpecChamberCutout6.SetVirtual()

SpecChamberCutout5 = Box("SpecChamberCutout5",50,chamberheight-co4hgt-2*wallthk,backlength)#cutout above back extension
SpecChamberCutout5.SetVirtual()

#outside magnet arc, not vac
SpecChamber1 = SubtractionSolid("SpecChamber1",SpecChamber0,SpecChamberCutout1,[0,chamberheight/2-flatlength_v,-chamberlength/2+flatlength_h],[0,90,0])
SpecChamber1.SetVirtual()

#space above front flat length
Block1 = Box("Block1",50,(chamberheight-(openingheight))+1,(flatlength_h+1))
Block1.SetVirtual()

#space in front of upper flat length
Block2 = Box("Block2",50,flatlength_v+1,(Magnet_iRad+flatlength_h+1))
Block2.SetVirtual()

SpecChamber01 = SubtractionSolid("SpecChamber01",SpecChamber1,Block2,[0,chamberheight/2-flatlength_v/2,-chamberlength/2+(Magnet_iRad+flatlength_h)/2],[0,0,0])
SpecChamber01.SetVirtual()

SpecChamber02 = SubtractionSolid("SpecChamber02",SpecChamber01,Block1,[0,chamberheight/2-(chamberheight-openingheight)/2,-chamberlength/2+flatlength_h/2],[0,0,0])
SpecChamber02.SetVirtual()
# #inside magnet arc #VAC
# SpecChamber2 = SubtractionSolid("SpecChamber2",SpecChamber1,SpecChamberCutout2,[0,chamberheight/2-flatlength,-chamberlength/2+flatlength],[0,90,0])
# SpecChamber2.SetVirtual()
SCC2_Pos = [0,chamberheight/2-flatlength_v,-chamberlength/2+flatlength_h]
SCC2_Rot = [0,-90,0]


# #passthrough to back for elastics (part 1) #VAC
# SpecChamber3 = SubtractionSolid("SpecChamber3",SpecChamber2,SpecChamberCutout3,[0,-chamberheight/2+openingheight/2,0],[0,0,0])
# SpecChamber3.SetVirtual()
SCC3_Pos = [0,-chamberheight/2+openingheight/2,0]
SCC3_Rot = [0,0,0]

#vertical cutout/passthrough for mollers
SCC6_Pos = [0,openingheight/2-15,-chamberlength/2+flatlength_h+Magnet_iRad+openingheight/2]
SCC6_Rot = [0,0,0]

# #cutout in back for elastics #VAC
# SpecChamber4 = SubtractionSolid("SpecChamber4",SpecChamber3,SpecChamberCutout4,[0,0,chamberlength/2-co4dpth/2+1],[0,0,0])#-26.988,94.6625],[0,0,0])
# SpecChamber4.SetVirtual()
SCC4_Pos = [0,-chamberheight/2+co4hgt/2+wallthk,chamberlength/2-co4dpth/2+1]
SCC4_Rot = [0,0,0]

SpecChamberCutout2.SetPosition(subtract(SCC2_Pos,SCC3_Pos))
SpecChamberCutout2.SetRotation(subtract(SCC2_Rot,SCC3_Rot))
SpecChamberCutout4.SetPosition(subtract(SCC4_Pos,SCC3_Pos))
SpecChamberCutout4.SetRotation(subtract(SCC4_Rot,SCC3_Rot))
SpecChamberCutout6.SetPosition(subtract(SCC6_Pos,SCC3_Pos))
SpecChamberCutout6.SetRotation(subtract(SCC6_Rot,SCC3_Rot))


SpecChamberVacCutouts = UnionChain([SpecChamberCutout3,SpecChamberCutout2,SpecChamberCutout4,SpecChamberCutout6]).MakeUnionSolids()#,SpecChamberCutout3,SpecChamberCutout4]).MakeUnionSolids()
# SpecChamberVacCutouts = CopyObject(SpecChamberCutout2)

# SpecChamberVacCutouts.SetMaterial(Vac)
SpecChamberVacCutouts.SetVirtual()

SpecChamber00 = SubtractionSolid("SpecChamber00",SpecChamber02,SpecChamberCutout5,[0,chamberheight/2-(SpecChamberCutout5.y())/2+1,1+chamberlength/2-backlength/2],[0,0,0])
SpecChamber00.SetVirtual()

#right-angle thing, not vac
SpecChamber = SubtractionSolid("SpecChamber",SpecChamber00,SpecChamberVacCutouts,[0,-chamberheight/2+openingheight/2,0],[0,0,0])#[0,chamberheight/2-flatlength/2+1,1+chamberlength/2-flatlength/2],[0,0,0])

#Back Exit window flange
ExWinFlange1 = Box("ExWinFlange1",82.55,247.65,15.875*2)
# ExWinFlange1.SetMaterial(Al)
ExWinFlange1.SetRotation([0,analyzeAngle,0])
ExWinFlange1.SetPosition(PolarPosition(sqrSum([437.691,625.087]),analyzeAngle,42.218))
ExWinFlange1.SetVirtual()
ExWinFlangeCutout = Box("ExWinFlangeCutout",24.613,189.814,15.875*3)
ExWinFlangeCutout.SetVirtual()
ExWinFlange = SubtractionSolid("ExWinFlange",ExWinFlange1,ExWinFlangeCutout)
ExWinFlange.SetPosition(ExWinFlange1.GetPosition())
ExWinFlange.SetRotation(ExWinFlange1.GetRotation())
ExWinFlange.SetMaterial(Al)

SpecChamber.SetMaterial(Al)
specPos = centers.lastCenter()
specPos[1] += chamberheight/2 - openingheight/2
SpecChamber.SetPosition(specPos)
SpecChamber.SetRotation([0,analyzeAngle,0])
SCVACALL = SpecChamber.FillVacuum()
# SpecChamberVacCutouts.SetPosition(subtract(SpecChamber.GetPosition(),[0,chamberheight/2-flatlength,-chamberlength/2+flatlength]))
# SpecChamberVacCutouts.SetRotation(subtract(SpecChamber.GetRotation(),[0,90,0]))
# pos1 = [0,139.113,-151]#[0,chamberheight/2-flatlength,-chamberlength/2+flatlength]#
# pos2 = [0,-90.488,0]
# pos3 = [0,-26.988,94.6625]

# pos2m1 = [a-b for a,b in zip(pos2,pos1)]
# Vac0RelPos = [-pos2m1[2],pos2m1[1],-pos2m1[0]]
# SCVac0 = UnionSolid("SCVac0",SpecChamberVac1,SpecChamberVac2,Vac0RelPos,[0,90,0])
# SCVac0.SetVirtual()

# pos3m1 = [a-b for a,b in zip(pos3,pos1)]
# Vac1RelPos = [-pos3m1[2],pos3m1[1],-pos3m1[0]]
# SCVac = UnionSolid("SCVac",SCVac0,SpecChamberVac3,Vac1RelPos,[0,90,0])
# SCVac.SetVirtual()#SetMaterial(Vac)
# pos0 = [a-b for a,b in zip(specPos,pos1)]
# SCVacRot = SpecChamber.GetRotation()
# SCVacRot[1] -= 90
# SCVac.SetRotation(SCVacRot)
# SCVacPos = PolarPosition(sqrSum(centers.lastCenter())-chamberlength/2,analyzeAngle,500+chamberheight/2-flatlength)#276.226/2+90.4875)
# SCVac.SetPosition(SCVacPos)
# SCVac.SetVirtual()

tenthou = 10./1000*25.4 #mm
ExitDumpFoil = Box("ExitDumpFoil",25.4,co4hgt,tenthou)
ExitDumpFoil.SetMaterial("G4_KAPTON")
ExitDumpFoil.SetPosition(PolarPosition(sqrSum(centers.lastCenter())+chamberlength/2+tenthou/2,analyzeAngle,co4hgt/2-openingheight/2+wallthk))
ExitDumpFoil.SetRotation([0,analyzeAngle,0])

############ Magnet Yoke arcs
# MagnetCoil_Width = 30
MagnetDist = sqrSum([specPos[0],specPos[2]]) - chamberlength/2 + flatlength_h


CoilSeparation = 75

coilWidth = (Magnet_oRad - Magnet_iRad)/2
bendRad = (Magnet_oRad + Magnet_iRad)/2

centerPos = PolarPosition(MagnetDist,analyzeAngle,chamberheight-openingheight/2)#specPos[1]+276.226/2)
deltaPos = PolarPosition(CoilSeparation/2,analyzeAngle-90,0)
leftPos = [sum(c) for c in zip(centerPos,deltaPos)]
rightPos = [c[0]-c[1] for c in zip(centerPos,deltaPos)]

# LeftArc = Tube("LeftArc",Magnet_iRad,Magnet_oRad,MagnetCoil_Width,180,90)
# LeftArc.SetPosition(leftPos)
# LeftArc.SetMaterial("G4_Fe")
# LeftArc.SetRotation([0,analyzeAngle-90,0])

# RightArc = Tube("RightArc",Magnet_iRad,Magnet_oRad,MagnetCoil_Width,180,90)
# RightArc.SetPosition(rightPos)
# RightArc.SetMaterial("G4_Fe")
# RightArc.SetRotation([0,analyzeAngle-90,0])

# CenterArc = Tube("CenterArc",Magnet_iRad-MagnetCoil_Width,Magnet_iRad,CoilSeparation+MagnetCoil_Width,180,90)
# CenterArc.SetPosition(centerPos)
# CenterArc.SetMaterial("G4_Fe")
# CenterArc.SetRotation([0,analyzeAngle-90,0])

#################################################################
# Detector
#################################################################
print "Building Detector"

d2img = bendRad*bendRad/MagnetDist
print 'Magnet Dist:',MagnetDist
print 'Bending radius:',bendRad
print 'Image Distance:',d2img
detLen = 150
detWidth = 50
detThk = 1
detCore = "G4_POLYSTYRENE"
detCladding = "G4_PLEXIGLASS"#According to wikipedia, plexiglass is PMMA
detectorPos = [-(MagnetDist+bendRad)*sin(analyzeTheta),bendRad+d2img,(MagnetDist+bendRad)*cos(analyzeTheta)]
vertRot = radToDeg(atan(1+(MagnetDist-bendRad)/(2*bendRad)))
fThk = 0.5 #mm
fWidth = 2.5#mm
fPitchHoriz = fWidth #mm
fPitchVert = fThk #mm
coreFrac = 0.98 #fractional
detThk = 3*fPitchVert+fThk
nX = int(detLen/fWidth)
nY = int(detWidth/fWidth)
print "Array of %i by %i fibers" %(nX,nY)
def fiberPosX(n,layer):
    fDist = (MagnetDist+bendRad) + n*fPitchHoriz*cos(degToRad(vertRot)) - layer*fPitchVert*sin(degToRad(vertRot))
    fDeltY = n*fPitchHoriz*sin(degToRad(vertRot))
    height = bendRad+d2img+layer*fPitchVert*cos(degToRad(vertRot))+fDeltY+fThk/2.
    return  [-(fDist)*sin(analyzeTheta),height+12.5,(fDist)*cos(analyzeTheta)]
#fiberPosX(-10,1)
def fiberPosY(n,layer):
    fDist = (MagnetDist+bendRad) - layer*fPitchVert*sin(degToRad(vertRot))
    xDelt = n*fPitchHoriz*cos(analyzeTheta)
    zDelt = n*fPitchHoriz*sin(analyzeTheta)
    height = bendRad+d2img+layer*fPitchVert*cos(degToRad(vertRot))+fThk/2.
    return  [-(fDist)*sin(analyzeTheta)+xDelt,height+12.5,(fDist)*cos(analyzeTheta)+zDelt]
#fiberPosY(-10,1)


LUMIDetDist = sqrSum([737.778,344.031])

def fiberPosXLUMI(n,layer):
    fDist = LUMIDetDist + n*fPitchHoriz*cos(degToRad(vertRot)) - layer*fPitchVert*sin(degToRad(vertRot))
    fDeltY = n*fPitchHoriz*sin(degToRad(vertRot))
    height = bendRad+d2img+layer*fPitchVert*cos(degToRad(vertRot))+fDeltY+fThk/2.
    return  [-(fDist)*sin(-lumiTheta),height,(fDist)*cos(-lumiTheta)]
#fiberPosX(-10,1)
def fiberPosYLUMI(n,layer):
    fDist = LUMIDetDist - layer*fPitchVert*sin(degToRad(vertRot))
    xDelt = n*fPitchHoriz*cos(-lumiTheta)
    zDelt = n*fPitchHoriz*sin(-lumiTheta)
    height = bendRad+d2img+layer*fPitchVert*cos(degToRad(vertRot))+fThk/2.
    return  [-(fDist)*sin(-lumiTheta)+xDelt,height,(fDist)*cos(-lumiTheta)+zDelt]

def cStr(s):
    if (s > 9) and (s < 100):
        return '0'+str(s)
    elif s < 10:
        return '00'+str(s)
    else:
        return str(s)

if False:#Monolithic detector
    Detector = Box("Detector",detWidth,detThk,detLen)
    Detector.SetPosition(detectorPos)
    Detector.SetMaterial(detCore)
    Detector.SetRotation(XYZtoZYX([vertRot,analyzeAngle,0]))
    Detector.SetSensitive("DetPlane")
if False:#Crossed fiber detector
    if False:#Square
        xFiberOuter = Box("xFiberOuter",fThk,fThk,detWidth)
        xFiberInner = Box("xFiberInner",fThk*coreFrac,fThk*coreFrac,detWidth)
        yFiberOuter = Box("yFiberOuter",fThk,fThk,detLen)
        yFiberInner = Box("yFiberInner",fThk*coreFrac,fThk*coreFrac,detLen)
    if True: #Round
        xFiberOuter = Tube("xFiberOuter",0,fThk/2.,detWidth,0,360)
        xFiberInner = Tube("xFiberInner",0,fThk/2.*coreFrac,detWidth,0,360)
        yFiberOuter = Tube("yFiberOuter",0,fThk/2.,detLen,0,360)
        yFiberInner = Tube("yFiberInner",0,fThk/2.*coreFrac,detLen,0,360)

    xFiberClad = SubtractionSolid("xFiberClad",xFiberOuter,xFiberInner)
    yFiberClad = SubtractionSolid("yFiberClad",yFiberOuter,yFiberInner)
    xFiberClad.SetMaterial(detCladding)
    yFiberClad.SetMaterial(detCladding)
    xFiberInner.SetMaterial(detCore)
    yFiberInner.SetMaterial(detCore)
    xFiberOuter.SetVirtual()
    yFiberOuter.SetVirtual()
    xFiberInner.SetVirtual()
    yFiberInner.SetVirtual()
    xFiberClad.SetVirtual()
    yFiberClad.SetVirtual()
    xF1 = []
    xF2 = []
    yF1 = []
    yF2 = []
    fiberRot = XYZtoZYX([vertRot,analyzeAngle,0])
    fiberRotY = XYZtoZYX([vertRot,analyzeAngle,0])
    fiberRotX = [0,analyzeAngle+90,-vertRot]#XYZtoZYX([0,analyzeAngle,0])
    for i in range(int(nX)):
         xF1.append(CopyObject(xFiberInner))
         xF1[-1].UnsetVirtual()
         xF1[-1].SetMaterial(detCore)
         xF1[-1].SetName("xFiberL0N"+str(i))
         xF1[-1].SetPosition(fiberPosX(i-nX/2,0))
         xF1[-1].SetRotation(fiberRotX)
         xF1[-1].SetSensitive("xFiberL0N"+str(i))
    for i in range(int(nX)):
         xF2.append(CopyObject(xFiberInner))
         xF2[-1].UnsetVirtual()
         xF2[-1].SetMaterial(detCore)
         xF2[-1].SetName("xFiberL2N"+str(i))
         xF2[-1].SetPosition(fiberPosX(i-nX/2-1.0/2,2))
         xF2[-1].SetRotation(fiberRotX)
         xF2[-1].SetSensitive("yFiberL2N"+str(i))
    for i in range(nY):
         yF1.append(CopyObject(yFiberInner))
         yF1[-1].UnsetVirtual()
         yF1[-1].SetMaterial(detCore)
         yF1[-1].SetName("yFiberL1N"+str(i))
         yF1[-1].SetPosition(fiberPosY(i-nY/2,1))
         yF1[-1].SetRotation(fiberRotY)
         yF1[-1].SetSensitive("yFiberL1N"+str(i))
    for i in range(nY):
         yF2.append(CopyObject(yFiberInner))
         yF2[-1].UnsetVirtual()
         yF2[-1].SetMaterial(detCore)
         yF2[-1].SetName("yFiberL3N"+str(i))
         yF2[-1].SetPosition(fiberPosY(i-nY/2-1.0/2,3))
         yF2[-1].SetRotation(fiberRotY)
         yF2[-1].SetSensitive("yFiberL3N"+str(i))
    for i in range(nX):
         xF1.append(CopyObject(xFiberClad))
         xF1[-1].UnsetVirtual()
         xF1[-1].SetMaterial(detCladding)
         xF1[-1].SetName("xFiberCladL0N"+str(i))
         xF1[-1].SetPosition(fiberPosX(i-nX/2,0))
         xF1[-1].SetRotation(fiberRotX)
    for i in range(nX):
         xF2.append(CopyObject(xFiberClad))
         xF2[-1].UnsetVirtual()
         xF2[-1].SetMaterial(detCladding)
         xF2[-1].SetName("xFiberCladL2N"+str(i))
         xF2[-1].SetPosition(fiberPosX(i-nX/2-1.0/2,2))
         xF2[-1].SetRotation(fiberRotX)
    for i in range(nY):
         yF1.append(CopyObject(yFiberClad))
         yF1[-1].UnsetVirtual()
         yF1[-1].SetMaterial(detCladding)
         yF1[-1].SetName("yFiberCladL1N"+str(i))
         yF1[-1].SetPosition(fiberPosY(i-nY/2,1))
         yF1[-1].SetRotation(fiberRotY)
    for i in range(nY):
         yF2.append(CopyObject(yFiberClad))
         yF2[-1].UnsetVirtual()
         yF2[-1].SetMaterial(detCladding)
         yF2[-1].SetName("yFiberCladL3N"+str(i))
         yF2[-1].SetPosition(fiberPosY(i-nY/2-1.0/2,3))
         yF2[-1].SetRotation(fiberRotY)

if True:#Crossed paddle detector

    # fWidth = 2.5#mm

    xPaddle = Box("xPaddle",fWidth,fThk,detWidth)
    yPaddle = Box("yPaddle",fWidth,fThk,detLen)

    # xFiberClad = SubtractionSolid("xFiberClad",xFiberOuter,xFiberInner)
    # yFiberClad = SubtractionSolid("yFiberClad",yFiberOuter,yFiberInner)
    # xFiberClad.SetMaterial(detCladding)
    # yFiberClad.SetMaterial(detCladding)
    xPaddle.SetVirtual()
    yPaddle.SetVirtual()
    # xFiberClad.SetVirtual()
    # yFiberClad.SetVirtual()
    xF1 = []
    yF1 = []
    fiberRot = XYZtoZYX([vertRot,analyzeAngle,0])
    fiberRotY = XYZtoZYX([vertRot,analyzeAngle,0])
    fiberRotX = [0,analyzeAngle+90,-vertRot]#XYZtoZYX([0,analyzeAngle,0])
    for i in range(int(nX)):
         xF1.append(CopyObject(xPaddle))
         xF1[-1].UnsetVirtual()
         xF1[-1].SetMaterial(detCore)
         xF1[-1].SetName("xPaddleN"+str(i))
         xF1[-1].SetPosition(fiberPosX(i-nX/2,0))
         xF1[-1].SetRotation(fiberRotX)
         xF1[-1].SetSensitive("xPaddleN"+str(i))
    for i in range(nY):
         yF1.append(CopyObject(yPaddle))
         yF1[-1].UnsetVirtual()
         yF1[-1].SetMaterial(detCore)
         yF1[-1].SetName("yPaddleN"+str(i))
         yF1[-1].SetPosition(fiberPosY(i-nY/2,1))
         yF1[-1].SetRotation(fiberRotY)
         yF1[-1].SetSensitive("yPaddleN"+str(i))

    fiberRotLUMI = XYZtoZYX([vertRot,-lumiAngle,0])
    fiberRotYLUMI = XYZtoZYX([vertRot,-lumiAngle,0])
    fiberRotXLUMI = [0,-lumiAngle+90,-vertRot]#XYZtoZYX([0,analyzeAngle,0])
    for i in range(int(nX)):
         xF1.append(CopyObject(xPaddle))
         xF1[-1].UnsetVirtual()
         xF1[-1].SetMaterial(detCore)
         xF1[-1].SetName("xPaddleNLUMI"+str(i))
         xF1[-1].SetPosition(fiberPosXLUMI(i-nX/2,0))
         xF1[-1].SetRotation(fiberRotXLUMI)
         xF1[-1].SetSensitive("xPaddleNLUMI"+str(i))
    for i in range(nY):
         yF1.append(CopyObject(yPaddle))
         yF1[-1].UnsetVirtual()
         yF1[-1].SetMaterial(detCore)
         yF1[-1].SetName("yPaddleNLUMI"+str(i))
         yF1[-1].SetPosition(fiberPosYLUMI(i-nY/2,1))
         yF1[-1].SetRotation(fiberRotYLUMI)
         yF1[-1].SetSensitive("yPaddleNLUMI"+str(i))

#################################################################
# Detector connector stuff
#################################################################

strTHK = 1#6.35
STR0 = Tube("STR0",0,50.927,strTHK,80,360)
STR0.SetVirtual()

STR_Cut = Box("STR_Cut",25.4,82.55,8)
STR_Cut.SetVirtual()

RoundToSquare = SubtractionSolid("RoundToSquare",STR0,STR_Cut,[0,0,0],[0,0,-analyzeAngle])
RoundToSquare.SetMaterial(Al)
RoundToSquare.SetPosition([-(MagnetDist+bendRad)*sin(analyzeTheta),centerPos[1]+strTHK/2,(MagnetDist+bendRad)*cos(analyzeTheta)])
RoundToSquare.SetRotation([90,0,0])

RTSVac = Box("RTSVac",25.4,82.55,strTHK)
RTSVac.SetVirtual()#SetMaterial(Vac)
RTSVac.SetRotation([90,analyzeAngle,0])
RTSVac.SetPosition(RoundToSquare.GetPosition())

RoundPiece2 = CopyObject(RoundPiece)
RoundPiece2.SetName("RoundPiece2")
RoundPiece2.SetPosition([-(MagnetDist+bendRad)*sin(analyzeTheta),centerPos[1]+strTHK+roundTHK/2,(MagnetDist+bendRad)*cos(analyzeTheta)])
RoundPiece2.SetRotation([90,0,0])

RP2Vac = RoundPiece2.FillVacuum()
RP2Vac.SetVirtual()

# RP2Vac.SetVirtual()#SetMaterial(Vac)
#RP2Vac.SetPosition(RoundPiece2.GetPosition())
# RP2Vac.SetRotation(RoundPiece2.GetRotation())

FinalConflat = CopyObject(ExitFlangeConflat)
FinalConflat.UnsetVirtual()
FinalConflat.SetName("FinalConflat")
FinalConflat.SetPosition([-(MagnetDist+bendRad)*sin(analyzeTheta),centerPos[1]+strTHK+roundTHK+conflatThk/2,(MagnetDist+bendRad)*cos(analyzeTheta)])
FinalConflat.SetRotation([90,0,0])

FCVac = FinalConflat.FillVacuum()
FCVac.SetVirtual()

# FCVac.SetVirtual()#SetMaterial(Vac)
#FCVac.SetPosition(FinalConflat.GetPosition())
# FCVac.SetRotation(FinalConflat.GetRotation())

fVacOverlap = 0
fvacStart = FinalConflat.GetPosition()
fvacStart[1] += conflatThk/2 #[-(MagnetDist+bendRad)*sin(analyzeTheta),centerPos[1]+strTHK+roundTHK+conflatThk,(MagnetDist+bendRad)*cos(analyzeTheta)]
fvacStart[1] -= fVacOverlap
fVacH = tan(degToRad(vertRot))*82.55/2
fVacHeight = detectorPos[1]-fvacStart[1]+fVacH -1
FinalVacuum0 = Box("FinalVacuum0",25.4,fVacHeight,82.55)
FinalVacuum0.SetVirtual()

# dx0FV = 82.55*sin(degToRad(vertRot))
# cut0FV = 2.*dx0FV*cos(degToRad(vertRot))

cut0FV = 82.55*sin(degToRad(vertRot))

FVac_Cut = Box("FVac_Cut",1000,cut0FV,1000)
FVac_Cut.SetVirtual()

FinalVacuum = SubtractionSolid("FinalVacuum",FinalVacuum0,FVac_Cut,[0,fVacHeight/2,0],[-vertRot,0,0])
FVacPos = fvacStart[::]
FVacPos[1] += fVacHeight/2
FinalVacuum.SetPosition(FVacPos)
FinalVacuum.SetRotation([0,analyzeAngle,0])
FinalVacuum.SetVirtual()#SetMaterial(Vac)

############
# Put stuff here for final vacuum enclosure and window


fVacWinFlgThk = 2*0.625*25.4

fv_width = 1.137*25.4
fv_hLen = 4.359*25.4
fv_vLen = 1.304*25.4

fvT_hLen = (4.359+0.884)*25.4
fvT_vLen = 5.616*25.4

fvU_hLen = 7.683*25.4
fvU_vLen = fVacWinFlgThk/2.0 #0.313*25.4

fvRotAngle = 47.0#Degrees
fvCutRotTheta = atan(0.884/5.616)
fvCutRotAngle = radToDeg(fvCutRotTheta)
fvRotTheta = degToRad(fvRotAngle)


fvPart1 = Box("fvPart1",fv_width,fv_vLen,fv_hLen)
fvPart1.SetVirtual()

# fvPart2_0 = Box("fvPart2_0",fv_width,fvT_vLen,fvT_hLen)
# fvPart2_0.SetVirtual()
#
# fvPart2_Cut0 = Box("fvPart2_Cut0",fv_width+5,fvT_vLen*1.5,100.0)
# fvPart2_Cut0.SetVirtual()
#
# protrudeWidthA = (100.0/2.0)/cos(fvCutRotTheta)
#
# fvPart2_1 = SubtractionSolid("fvPart2_1",fvPart2_0,fvPart2_Cut0,[0,0,fvT_hLen/2.0+protrudeWidthA-(0.884/2.0)*25.4],[fvCutRotAngle,0,0])
# fvPart2_1.SetVirtual()
#
# fvPart2_Cut1 = Box("fvPart2_Cut1",fv_width+5,fvT_vLen*3.0,100)
# fvPart2_Cut1.SetVirtual()

# print "fvRotTheta",fvRotTheta
protrudeWidthF = (100/2.0)/cos(pi/2.0-fvRotTheta)

# print "protrudeWidthF",protrudeWidthF
# fvPart2 = SubtractionSolid("fvPart2",fvPart2_1,fvPart2_Cut1,[0,0,-protrudeWidthF],[(90-fvRotAngle),0,0])
# fvPart2.SetVirtual()
fvPart2 = Wedge("fvPart2",fv_hLen,fvT_vLen,fv_width,90+radToDeg(atan(0.884/5.616)))
fvPart2.SetVirtual()

fvPart3 = Box("fvPart3",fv_width,fvU_hLen,fvU_vLen)
fvPart3.SetVirtual()

#old pos
#[0,fv_vLen/2.0+fvT_vLen/2.0,0.884*25.4/2.0]
fvAll_0 = UnionSolid("fvAll_0",fvPart1,fvPart2,add(fvPart2.GetPosition(),[0,fv_vLen/2.0,-fv_hLen/2.0]),[90,90,0])
fvAll_0.SetVirtual()

protrudeWidthU = (fvU_vLen/2.0)/cos(fvRotTheta)

fvOuter = UnionSolid("fvOuter",fvAll_0,fvPart3,[0,fv_vLen/2.0+fvT_vLen/2.0+protrudeWidthU-(fvU_vLen/2.0)*sin(fvRotTheta),0.884*25.4/2.0-(fvU_vLen/2.0)*cos(fvRotTheta)],[(90-fvRotAngle),0,0])
fvOuter.SetVirtual()
# fvOuter.SetPosition([1000,1000,1000])
# fvOuter.SetMaterial(Al)

wallEncThk = 0.120*25.4

overlapfvI = 0

fvIRotAngle = fvRotAngle#47.0#Degrees
fvICutRotTheta = atan(0.884/5.616)
fvICutRotAngle = radToDeg(fvICutRotTheta)
fvIRotTheta = degToRad(fvIRotAngle)

fvI_width = 1.137*25.4 - 2*wallEncThk
fvI_hLen = 4.359*25.4 - 2*wallEncThk
fvI_vLen = 1.304*25.4 + overlapfvI +2

# fvIT_hLen = (4.359+0.884)*25.4 - 2*wallEncThk
# fvIT_vLen = 5.616*25.4 - wallEncThk/tan(fvICutRotTheta)+ overlapfvI
fvIT_vLen = fvI_hLen*tan(fvRotTheta)/(1.0-tan(fvRotTheta)/tan(pi/2.0-fvCutRotTheta))

fvIU_hLen = 7.683*25.4 - 2*wallEncThk
fvIU_vLen = fVacWinFlgThk/2.0 + wallEncThk*tan(fvRotTheta) +overlapfvI#0.313*25.4 + 4



fvIPart1 = Box("fvIPart1",fvI_width,fvI_vLen,fvI_hLen)
fvIPart1.SetVirtual()

# fvIPart2_0 = Box("fvIPart2_0",fvI_width,fvIT_vLen,fvIT_hLen)
# fvIPart2_0.SetVirtual()
#
# fvIPart2_Cut0 = Box("fvIPart2_Cut0",fvI_width+5,fvIT_vLen*1.5,100.0)
# fvIPart2_Cut0.SetVirtual()
#
# protrudeWidthIA = (100.0/2.0)/cos(fvICutRotTheta)
#
# fvIPart2_1 = SubtractionSolid("fvIPart2_1",fvIPart2_0,fvIPart2_Cut0,[0,0,fvIT_hLen/2.0+protrudeWidthIA-(0.884/2.0)*25.4],[fvICutRotAngle,0,0])
# fvIPart2_1.SetVirtual()
#
# fvIPart2_Cut1 = Box("fvIPart2_Cut1",fvI_width+5,fvIT_vLen*3.0,100)
# fvIPart2_Cut1.SetVirtual()
#
# # print "fvIRotTheta",fvIRotTheta
# protrudeWidthIF = (100/2.0)/cos(pi/2.0-fvIRotTheta)
#
# # print "protrudeWidthIF",protrudeWidthIF
# fvIPart2 = SubtractionSolid("fvIPart2",fvIPart2_1,fvIPart2_Cut1,[0,0,-protrudeWidthIF],[(90-fvIRotAngle),0,0])
# fvIPart2.SetVirtual()
fvIPart2 = Wedge("fvIPart2",fvI_hLen,fvIT_vLen,fvI_width,90+radToDeg(atan(0.884/5.616)))
fvIPart2.SetVirtual()

fvIPart3 = Box("fvIPart3",fvI_width,fvIU_hLen,fvIU_vLen)
fvIPart3.SetVirtual()

# fvIAll_0 = UnionSolid("fvIAll_0",fvIPart1,fvIPart2,[0,fv_vLen/2.0+fvT_vLen/2.0,0.884*25.4/2.0])
# fvIAll_0.SetVirtual()
fvIAll_0 = UnionSolid("fvIAll_0",fvIPart1,fvIPart2,add(fvIPart2.GetPosition(),[0,fvI_vLen/2.0,-fvI_hLen/2.0]),[90,90,0])
fvIAll_0.SetVirtual()


protrudeWidthIU = (fvIU_vLen/2.0)/cos(fvIRotTheta)

fvInner = UnionSolid("fvInner",fvIAll_0,fvIPart3,[0,fvI_vLen/2.0+fvIT_vLen/2.0+protrudeWidthIU-(fvIU_vLen/2.0)*sin(fvIRotTheta),0.884*25.4/2.0-(fvIU_vLen/2.0)*cos(fvIRotTheta)],[(90-fvIRotAngle),0,0])
fvInner.SetVirtual()
# fvInner.SetPosition([500,500,500])
# fvInner.SetMaterial(Al)

#
# TestWedge = Wedge("TestWedge",100,200,50,135)
# TestWedge.SetPosition([300,300,300])
# TestWedge.SetMaterial(Al)
#
# TestBox = Box("TestBox",5,5,5)
# TestBox.SetPosition([300,300,300])
# TestBox.SetMaterial(Al)


fvAll = SubtractionSolid("fvAll",fvOuter,fvInner)
# fvAll.SetPosition([500,500,500])
# fvAll.SetMaterial(Al)
fvAll.SetVirtual()

fvEncVac = fvAll.FillVacuum()
fvEncVac.SetVirtual()

protrudeWidthUF = (fVacWinFlgThk/2.0)/cos(fvIRotTheta)

fvWindowOuter = Box("fvWindowOuter",3.310*25.4,fVacWinFlgThk,9.810*25.4)
fvWindowOuter.SetVirtual()
# fvWindowOuter.SetPosition([1000,1000,1000])
# fvWindowOuter.SetMaterial(Al)

fvWindowInner = Box("fvWindowInner",(2.164-1.146)*25.4,fVacWinFlgThk+4,(8.687-1.124)*25.4)
fvWindowInner.SetVirtual()

# fvWFPos = add(
#     fvAll.GetPosition(),
#     # [0,fv_vLen/2.0+fvT_vLen/2.0+2.0*protrudeWidthU+protrudeWidthUF-(fvU_vLen)*sin(fvRotTheta)-(fVacWinFlgThk/2.0)*sin(fvRotTheta),0.884*25.4/2.0-(fvU_vLen)*cos(fvRotTheta)-(fVacWinFlgThk/2.0)*cos(fvRotTheta)]
# )

fvWindowFlange = SubtractionSolid("fvWindowFlange",fvWindowOuter,fvWindowInner)
fvWindowFlange.SetVirtual()
# fvWindowFlange.SetMaterial(Al)
# fvWindowFlange.SetRotation([(fvRotAngle),0,0])
# fvWindowFlange.SetPosition(fvWFPos)





fvVacEnc = UnionSolid("fvVacEnc",fvAll,fvWindowFlange,[0,fv_vLen/2.0+fvT_vLen/2.0+2.0*protrudeWidthU-(fvU_vLen+3)*sin(fvRotTheta)-4,0.884*25.4/2.0-(fvU_vLen+3)*cos(fvRotTheta)],[-(fvRotAngle),0,0])
# ExitFlangeConflat
# fvVacEnc.SetPosition([500,500,500])
fvVacEnc.SetPosition(add(FinalConflat.GetPosition(),[0,fv_vLen/2.0+conflatThk/2.0+5,0]))#[1000,1000,1000])
fvVacEnc.SetRotation([0,analyzeAngle,0])
fvVacEnc.SetMaterial(Al)
# fvVacEnc.SetVirtual()

# fvAll.UnsetVirtual()
# fvAll.SetPosition([250,250,250])
# fvAll.SetMaterial(Al)



fvEncVac.SetPosition(fvVacEnc.GetPosition())
fvEncVac.SetRotation(fvVacEnc.GetRotation())
fvEncVac.SetVirtual()





# fVacLen = 2*fVacH/sin(degToRad(vertRot))
# FinalWindow = Box("FinalWindow",25.4,tenthou/2,fVacLen)
# FWinPos = detectorPos[::]
# FWinPos[1] -= 1
# FinalWindow.SetPosition(FWinPos)
# FinalWindow.SetRotation(XYZtoZYX([vertRot,analyzeAngle,0]))

FinalWindow = Box("FinalWindow",(2.164-1.146)*25.4,tenthou/2.0,(8.687-1.124)*25.4)
FinalWindow.SetMaterial("G4_KAPTON")
FWPos = add(
    fvVacEnc.GetPosition(),
    [0,fv_vLen/2.0+fvT_vLen/2.0+2.0*protrudeWidthU-(fvU_vLen+3)*sin(fvRotTheta)-4,0.884*25.4/2.0-(fvU_vLen+3)*cos(fvRotTheta)]
)
FinalWindow.SetPosition(add(FWPos,[0,0,0]))
FinalWindow.SetRotation(XYZtoZYX([fvRotAngle,analyzeAngle,0]))

fvAirLen = fvU_vLen-tenthou
fvFlangeAir = Box("fvFlangeAir",(2.164-1.146)*25.4,fvAirLen,(8.687-1.124)*25.4)
fvFlangeAir.SetMaterial("G4_AIR")
fvFlangeAir.SetRotation(FinalWindow.GetRotation())
fvFlangeAir.SetPosition(
    add(
        FinalWindow.GetPosition(),
        [0,fvAirLen*cos(fvRotTheta),0]
    )
)


#################################################################
# Shielding
#################################################################
print "Building Shielding"

#Downstream shielding tube

DS_Shield_len = DS_BP_len+DSBPC_len
DS_Shield_oRad = 88.9/2.0

#
# Box3 = Box("Box3",1,1,1)
# Box3.SetVirtual()

DSBPAS = Tube("DSBPAS",DS_BP_oRad,DS_Shield_oRad,DS_BP_len1,80,360)
DSBPAS.SetVirtual()

DSFLGAS = Tube("DSFLGAS",69.342/2.0,DS_Shield_oRad,25.4,90,360)
DSFLGAS.SetPosition([0,0,DS_BP_len1/2.0 + 25.4/2.0])
DSFLGAS.SetVirtual()

DSBPCAS = Tube("DSBPCAS",DSBPC_oRad,DS_Shield_oRad,DSBPC_len+DS_BP_len2,80,360)
DSBPCAS.SetPosition([0,0,DS_BP_len1/2.0+25.4+(DSBPC_len+DS_BP_len2)/2.0])
DSBPCAS.SetVirtual()

ShieldingBase1 = UnionChain([DSBPAS,DSFLGAS,DSBPCAS]).MakeUnionSolids()
ShieldingBase1.SetVirtual()

# ShieldingBase = Tube("ShieldingBase",0,DS_Shield_oRad,DS_Shield_len-2,80,360)
# ShieldingBase.SetVirtual()

# ShieldingBase1 = SubtractionSolid("ShieldingBase1",ShieldingBase,DS_Apparatus,[0,0,-DS_Shield_len/2.0-TC_oRad])
# ShieldingBase1.SetVirtual()

# DS_Apparatus.UnsetVirtual()
# DS_Apparatus.SetMaterial("G4_Pb")
# DS_Apparatus.SetPosition([500,500,0])

# ShieldingBase1.SetMaterial("G4_Pb")
# ShieldingBase1.SetPosition([500,500,0])
# ShieldingBase1.SetPosition([0,0,TC_oRad+DS_Shield_len/2.0])


ShieldingRZ1 = 193.991
ShieldingRX1 = 29.988

ShieldingRZ2 = 252.334
ShieldingRX2 = 14.287


ShieldingRZ3 = 340.325
ShieldingRX3 = 35.985
ShieldingRZ4 = 363.164

ShldC1 = Box("ShldC1",200,200,ShieldingRZ1+10)
ShldC1.SetVirtual()

ShieldingBase2 = SubtractionSolid("ShieldingBase2",ShieldingBase1,ShldC1,[-100-ShieldingRX1,0,-DS_BP_len1/2.0-TC_oRad+ShieldingRZ1/2.0])
ShieldingBase2.SetVirtual()

ShldC2 = Box("ShldC2",200,200,(ShieldingRZ2-ShieldingRZ1))
ShldC2.SetVirtual()

ShieldingBase3 = SubtractionSolid("ShieldingBase3",ShieldingBase2,ShldC2,[-100-ShieldingRX2,0, -DS_BP_len1/2.0-TC_oRad+ShieldingRZ1+(ShieldingRZ2-ShieldingRZ1)/2.0])
ShieldingBase3.SetVirtual()

ShldC3 = Box("ShldC3",200,200,(ShieldingRZ4-ShieldingRZ3))
ShldC3.SetVirtual()

ShieldingBase4 = SubtractionSolid("ShieldingBase4",ShieldingBase3,ShldC3,[-100-ShieldingRX3,0,-DS_BP_len1/2.0-TC_oRad+ShieldingRZ3+(ShieldingRZ4-ShieldingRZ3)/2.0])
ShieldingBase4.SetVirtual()

ShldCUT1 = Box("ShldCut1",100,100,75)
ShldCUT1.SetVirtual()

Shld1 = SubtractionSolid("Shld1",ShieldingBase4,ShldCUT1,[0,0,-DS_BP_len1/2.0])
# Shld1
Shld1.SetVirtual()
# Shld1.SetMaterial("G4_Pb")
# Shld1.SetPosition([0,0,TC_oRad+DS_BP_len1/2.0])#[500,500,TC_oRad+DS_Shield_len/2.0])

#
#Target chamber half-tube
TC_Shld_len = 367.284
TC_Shld_diff = 83.033
TC_Shld_cutRad = 98.425/2.0

TC_Shld1 = Tube("TC_Shld1",TC_oRad,254/2.0,TC_Shld_len,0,180)
TC_Shld1.SetVirtual()
# TC_Shld1.SetRotation([-90,0,0])

TC_ShldC = Tube("TC_ShldC",0,TC_Shld_cutRad,1000,80,360)
# TC_ShldC.SetRotation([90,0,0])
TC_ShldC.SetVirtual()

TC_Shld2 = SubtractionSolid("TC_Shld2",TC_Shld1,TC_ShldC,[0,0,-TC_Shld_diff],[90,0,nominalAngle])
TC_Shld2.SetVirtual()

TC_ShldC2 = Tube("TC_ShldC2",0,Lumi_BP_oRad,1000,80,360)
TC_ShldC2.SetVirtual()

TC_Shld3 = SubtractionSolid("TC_Shld3",TC_Shld2,TC_ShldC2,[0,0,-TC_Shld_diff],[90,0,-lumiAngle])
TC_Shld3.SetVirtual()

TC_ShldC3 = Tube("TC_ShldC3",0,DS_BP_oRad,1000,80,360)
TC_ShldC3.SetVirtual()

TC_Shld = SubtractionSolid("TC_Shld",TC_Shld3,TC_ShldC3,[0,0,-TC_Shld_diff],[90,0,0])

TC_Shld.SetVirtual()
#
Shielding_Full = UnionSolid("Shielding_Full",TC_Shld,Shld1,[0,TC_oRad+DS_BP_len1/2.0,-TC_Shld_diff],[-90,0,0])
Shielding_Full.SetMaterial("G4_Pb")
Shielding_Full.SetRotation([-90,0,0])
Shielding_Full.SetPosition([0,-TC_Shld_diff,0])

#
# # shieldTHK = 25 #mm
# # shieldTHK = 25.4
# # shieldWidth = 350 #mm
# # shieldHeight = 500 #mm
# # shieldDepth = 350 #mm
#
# if(False):
# 	Shield1 = Box("Shield1",shieldWidth,shieldHeight,shieldTHK)
# 	Shield1.SetVirtual()
#
# 	Shield2 = Box("Shield2",shieldTHK,shieldHeight,shieldDepth)
# 	Shield2.SetVirtual()
#
# 	Shield3 = Box("Shield3",shieldTHK,shieldHeight,shieldDepth)
# 	Shield3.SetVirtual()
#
# 	Shield01 = UnionSolid("Shield01",Shield1,Shield2,[-shieldWidth/2-shieldTHK/2,0,shieldDepth/2-shieldTHK/2])
# 	Shield01.SetVirtual()
#
# 	Shield = UnionSolid("Shield",Shield01,Shield3,[shieldWidth/2+shieldTHK/2,0,shieldDepth/2-shieldTHK/2])
# 	Shield.SetMaterial("G4_Pb")
# 	Shield.SetPosition(PolarPosition(sqrSum(Collimator.GetPosition()),analyzeAngle,colSize/2+shieldHeight/2))
# 	Shield.SetRotation([0,analyzeAngle,0])
#
# if(False):
# 	ShieldLower = CopyObject(Shield)
# 	ShieldLower.SetName("ShieldLower")
# 	lshieldpos = Shield.GetPosition()
# 	lshieldpos[1] *= -1
# 	ShieldLower.SetPosition(lshieldpos)
#
# if(False):
# 	shieldRLen = 250
# 	overlap = conflatThk*1.4
# 	ShieldR = Tube("ShieldR",75.819,85,shieldRLen,0,360)
# 	shieldPos = FinalConflat.GetPosition()
# 	shieldPos[1] += shieldRLen/2 + conflatThk/2 - overlap
#
# 	ShieldR.SetPosition(shieldPos)
# 	ShieldR.SetRotation([90,0,0])
# 	ShieldR.SetMaterial("G4_Pb")

if(True): #Magnets!

	#################################################################
	# Magnet yoke
	#################################################################


	Yoke1 = Tube("Yoke1",  0.  ,785./2.,170.,180.,90.)
	Yoke1.SetVirtual()

	Yoke2 = Tube("Yoke2",335./2.,787./2.,110.,179.,92.)
	Yoke2.SetVirtual()

	YokeBox = Box("YokeBox", 130.815, 130.815, 171.)
	YokeBox.SetVirtual()

	YokeMinus1 =SubtractionSolid("YokeMinus1",Yoke1,Yoke2)
	YokeMinus1.SetVirtual()

	YokeMinus2 =SubtractionSolid("YokeMinus2",YokeMinus1,YokeBox,[0,0,0],[0,0,45])
	YokeMinus2.SetVirtual()#SetMaterial(Al)#.SetVirtual()#SetMaterial(Al)
	#YokeMinus2.SetPosition([-100,0,0])



	#**************************************************************************
	# Left Coil ***************************************************************
	#**************************************************************************


	PoleL = Tube("PoleL",435./2.,685./2.,39.238,180.,90.)
	PoleL.SetVirtual()#.SetMaterial(Al)
	#PoleL.SetPosition([0,0,-(39.238+30.000)/2])

	PoleLBoxMinus1 = Box("PoleLBoxMinus1",131.758,13.415,43.415)
	PoleLBoxMinus1.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()#.SetMaterial(Al)
	#PoleLBoxMinus1.SetPosition([-280,0,-(39.238+30.000)/2])
	#PoleLBoxMinus1.SetRotation([-15,0,0])

	PoleLMinus1 = SubtractionSolid("PoleLMinus1",PoleL,PoleLBoxMinus1,[-280,13.415/2-3.472/2,-39.238/2+14.237+12.958/2.],[15,0,0])
	PoleLMinus1.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleLMinus1.SetPosition([0,0,-(39.238+30.000)/2])

	PoleLBoxMinus2 = Box("PoleLBoxMinus1",138.632,21.4,41.4)
	PoleLBoxMinus2.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()#.SetVirtual()
	#PoleLBoxMinus2.SetPosition([-280,-3.472,-39.238+14.237+12.958+12.841/2-(30.000)/2])
	#PoleLBoxMinus2.SetRotation([-53.127,0,0])

	PoleLMinus2 = SubtractionSolid("PoleLMinus2",PoleLMinus1,PoleLBoxMinus2,[-280,-3.472,-39.238/2+14.237+12.958+12.841/2],[53.127,0,0])
	PoleLMinus2.SetVirtual()#.SetMaterial(Al)
	#PoleLMinus2.SetPosition([0,0,-(39.238+30.000)/2])

	PoleLBoxMinus3 = Box("PoleLBoxMinus3",13.415,131.758,43.415)
	PoleLBoxMinus3.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()#.SetMaterial(Al)
	#PoleLBoxMinus3.SetPosition([13.415/2-3.472/2,-280,-39.238+14.237+12.958/2.-(30.000)/2])
	#PoleLBoxMinus3.SetRotation([0,15,0])

	PoleLMinus3 = SubtractionSolid("PoleLMinus3",PoleLMinus2,PoleLBoxMinus3,[13.415/2.-3.472/2.,-280.,-39.238/2.+14.237+12.958/2.],[0,-15,0])
	PoleLMinus3.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleLMinus3.SetPosition([0,0,-(39.238+30.000)/2])

	PoleLBoxMinus4 = Box("PoleLBoxMinus4",21.4,138.632,41.4)
	PoleLBoxMinus4.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()#.SetVirtual()
	#PoleLBoxMinus4.SetPosition([-3.472,-280,-39.238+14.237+12.958+12.841/2-(30.000)/2])
	#PoleLBoxMinus4.SetRotation([0,53.127,0])

	PoleLMinus4 = SubtractionSolid("PoleLMinus4",PoleLMinus3,PoleLBoxMinus4,[-3.472,-280,-39.238/2+14.237+12.958+12.841/2+5],[0,-53.127,0])
	PoleLMinus4.SetVirtual()#.SetMaterial(Al)
	#PoleLMinus4.SetPosition([0,0,-(39.238+30.000)/2])

	PoleLCutTube1 = Tube("PoleLCutTube1",32./2.,100./2.,39.438,0.,90.) #39.238
	PoleLCutTube1.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleLCutTube1.SetPosition([-435./2.-16.,-16.,-(39.238+30.000)/2])
	PoleLMinus5 = SubtractionSolid("PoleLMinus5",PoleLMinus4,PoleLCutTube1,[-435./2.-15.5,-16.01,0])
	PoleLMinus5.SetVirtual()#.SetMaterial(Al)
	#PoleLMinus5.SetPosition([0,0,-(39.238+30.000)/2]) #-(39.238+30.000)/2


	PoleLCutTube2 = Tube("PoleLCutTube2",32./2.,100./2.,39.438,90.,90.) #39.238
	PoleLCutTube2.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleLCutTube2.SetPosition([-685./2.+16.,-16.,-(39.238+30.000)/2])
	PoleLMinus6 = SubtractionSolid("PoleLMinus6",PoleLMinus5,PoleLCutTube2,[-685./2.+16.5,-16.01,0])
	PoleLMinus6.SetVirtual()#SetMaterial(Al)#SetVirtual()#.SetMaterial(Al)
	#PoleLMinus6.SetPosition([0,0,-(39.238+30.000)/2]) #-(39.238+30.000)/2

	PoleLCutTube3 = Tube("PoleLCutTube3",32./2.,100./2.,39.438,0.,90.) #39.238
	PoleLCutTube3.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleLCutTube3.SetPosition([-16.,-435./2.-16.,-(39.238+30.000)/2])
	PoleLMinus7 = SubtractionSolid("PoleLMinus7",PoleLMinus6,PoleLCutTube3,[-16.01,-435./2.-15.5,0])
	PoleLMinus7.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleLMinus7.SetPosition([0,0,-(39.238+30.000)/2]) #-(39.238+30.000)/2

	PoleLCutTube4 = Tube("PoleLCutTube4",32./2.,100./2.,39.438,270.,90.) #39.238
	PoleLCutTube4.SetVirtual()#.SetMaterial(Al)
	#PoleLCutTube4.SetPosition([-16.,-685./2.+16.,-(39.238+30.000)/2])
	PoleLMinus8 = SubtractionSolid("PoleLMinus8",PoleLMinus7,PoleLCutTube4,[-16.01,-685./2.+16.5,0])
	PoleLMinus8.SetVirtual()#.SetMaterial(Al)#SetVirtual()#.SetMaterial(Al)
	#PoleLMinus8.SetPosition([0,0,-(39.238+30.000)/2]) #-(39.238+30.000)/2

	PoleLCutTube5 = Tube("PoleLCutTube5",447.7/2.,672.3/2.,1.273,180.,90.) #39.238
	PoleLCutTube5.SetVirtual()#.SetMaterial(Al)
	#PoleLCutTube5.SetPosition([0,0,-(1.273+30.000)/2])
	PoleLMinus9 = SubtractionSolid("PoleLMinus9",PoleLMinus8,PoleLCutTube5,[0,0,(39.238-1.273)/2])
	PoleLMinus9.SetVirtual()#.SetMaterial(Al)
	#PoleLMinus9.SetPosition([0,0,-(39.238+30.000)/2])

	#**************************************************************************
	#**************************************************************************




	#**************************************************************************
	# Right Coil ***************************************************************
	#**************************************************************************

	PoleR = Tube("PoleR",435./2.,685./2.,39.238,180.,90.)
	PoleR.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleR.SetPosition([0,0,(39.238+30.000)/2])

	PoleRBoxMinus1 = Box("PoleRBoxMinus1",131.758,13.415,43.415)
	PoleRBoxMinus1.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()#.SetMaterial(Al)
	#PoleRBoxMinus1.SetPosition([-280,0,-(39.238+30.000)/2])
	#PoleRBoxMinus1.SetRotation([-15,0,0])

	PoleRMinus1 = SubtractionSolid("PoleRMinus1",PoleR,PoleRBoxMinus1,[-280,13.415/2-3.472/2,39.238/2-14.237-12.958/2.],[-15,0,0])
	PoleRMinus1.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleRMinus1.SetPosition([0,0,-(39.238+30.000)/2])

	PoleRBoxMinus2 = Box("PoleRBoxMinus1",138.632,21.4,41.4)
	PoleRBoxMinus2.SetVirtual()#.SetVirtual()

	PoleRMinus2 = SubtractionSolid("PoleRMinus2",PoleRMinus1,PoleRBoxMinus2,[-280,-3.472,39.238/2-14.237-12.958-12.841/2],[-53.127,0,0])
	PoleRMinus2.SetVirtual()#.SetMaterial(Al)
	#PoleRMinus2.SetPosition([0,0,(39.238+30.000)/2])

	PoleRBoxMinus3 = Box("PoleRBoxMinus3",13.415,131.758,43.415)
	PoleRBoxMinus3.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()#.SetMaterial(Al)
	#PoleRBoxMinus3.SetPosition([13.415/2-3.472/2,-280,-39.238+14.237+12.958/2.-(30.000)/2])
	#PoleRBoxMinus3.SetRotation([0,15,0])

	PoleRMinus3 = SubtractionSolid("PoleRMinus3",PoleRMinus2,PoleRBoxMinus3,[13.415/2.-3.472/2.,-280.,39.238/2.-14.237-12.958/2.],[0,15,0])
	PoleRMinus3.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleRMinus3.SetPosition([0,0,-(39.238+30.000)/2])

	PoleRBoxMinus4 = Box("PoleRBoxMinus4",21.4,138.632,41.4)
	PoleRBoxMinus4.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()#.SetVirtual()
	#PoleRBoxMinus4.SetPosition([-3.472,-280,-39.238+14.237+12.958+12.841/2-(30.000)/2])
	#PoleRBoxMinus4.SetRotation([0,53.127,0])

	PoleRMinus4 = SubtractionSolid("PoleRMinus4",PoleRMinus3,PoleRBoxMinus4,[-3.472,-280,39.238/2-14.237-12.958-12.841/2-5],[0,53.127,0])
	PoleRMinus4.SetVirtual()#.SetMaterial(Al)
	#PoleRMinus4.SetPosition([0,0,(39.238+30.000)/2])

	PoleRCutTube1 = Tube("PoleRCutTube1",32./2.,100./2.,39.438,0.,90.) #39.238
	PoleRCutTube1.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleRCutTube1.SetPosition([-435./2.-16.,-16.,(39.238+30.000)/2])
	PoleRMinus5 = SubtractionSolid("PoleRMinus5",PoleRMinus4,PoleRCutTube1,[-435./2.-15.5,-16.01,0])
	PoleRMinus5.SetVirtual()#.SetMaterial(Al)
	#PoleRMinus5.SetPosition([0,0,(39.238+30.000)/2]) #(39.238+30.000)/2

	PoleRCutTube2 = Tube("PoleRCutTube2",32./2.,100./2.,39.438,90.,90.) #39.238
	PoleRCutTube2.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleRCutTube2.SetPosition([-685./2.+16.,-16.,(39.238+30.000)/2])
	PoleRMinus6 = SubtractionSolid("PoleRMinus6",PoleRMinus5,PoleRCutTube2,[-685./2.+16.5,-16.01,0])
	PoleRMinus6.SetVirtual()#SetMaterial(Al)#SetVirtual()#.SetMaterial(Al)
	#PoleRMinus6.SetPosition([0,0,(39.238+30.000)/2]) #(39.238+30.000)/2

	PoleRCutTube3 = Tube("PoleRCutTube3",32./2.,100./2.,39.438,0.,90.) #39.238
	PoleRCutTube3.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleRCutTube3.SetPosition([-16.,-435./2.-16.,(39.238+30.000)/2])
	PoleRMinus7 = SubtractionSolid("PoleRMinus7",PoleRMinus6,PoleRCutTube3,[-16.01,-435./2.-15.5,0])
	PoleRMinus7.SetVirtual()#.SetMaterial(Al)#.SetVirtual()#.SetMaterial(Al)
	#PoleRMinus7.SetPosition([0,0,(39.238+30.000)/2]) #(39.238+30.000)/2

	PoleRCutTube4 = Tube("PoleRCutTube4",32./2.,100./2.,39.438,270.,90.) #39.238
	PoleRCutTube4.SetVirtual()#.SetMaterial(Al)
	#PoleRCutTube4.SetPosition([-16.,-685./2.+16.,-(39.238+30.000)/2])
	PoleRMinus8 = SubtractionSolid("PoleRMinus8",PoleRMinus7,PoleRCutTube4,[-16.01,-685./2.+16.5,0])
	PoleRMinus8.SetVirtual()#.SetMaterial(Al)
	#PoleRMinus8.SetPosition([0,0,(39.238+30.000)/2]) #(39.238+30.000)/2

	PoleRCutTube5 = Tube("PoleRCutTube5",447.7/2.,672.3/2.,1.273,180.,90.) #39.238
	PoleRCutTube5.SetVirtual()#.SetMaterial(Al)
	#PoleRCutTube5.SetPosition([0,0,-(1.273+30.000)/2])
	PoleRMinus9 = SubtractionSolid("PoleRMinus9",PoleRMinus8,PoleRCutTube5,[0,0,-(39.238-1.273)/2])
	PoleRMinus9.SetVirtual()#.SetMaterial(Al)
	#PoleRMinus9.SetPosition([0,0,(39.238+30.000)/2])
	#**************************************************************************
	#**************************************************************************





	#**************************************************************************
	# Left Coil ***************************************************************
	#**************************************************************************
	Coil1L = Tube("Coil1L",339./2.,431./2.,36.,180.,90.)
	#Coil1L.SetPosition([0,0,-36.])
	Coil1L.SetVirtual()#.SetMaterial("G4_W")
	Coil2L = Tube("Coil2L",689./2.,780.999/2.,36.,180.,90.)
	#Coil2L.SetPosition([0,0,-36.])
	Coil2L.SetVirtual()#.SetMaterial("G4_W")

	InY = -13.
	Coil3L = Tube("Coil3L",36./2.,128./2.,36.,0.,90.)
	#Coil3L.SetPosition([-232.5,InY,-36.])
	Coil3L.SetVirtual()#.SetMaterial("G4_W")
	Coil4L = Tube("Coil4L",36./2.,128./2.,36.,90.,80.)
	#Coil4L.SetPosition([-327.5,InY,-36.])
	Coil4L.SetVirtual()#.SetMaterial("G4_W")
	Coil5L = Box("Coil5L",96.103,46.,36.) #,93.103,46.,36.)
	#Coil5L.SetPosition([-280,5+46./2.,-36.])
	Coil5L.SetVirtual()#.SetMaterial("G4_W")

	InX = -13.
	Coil6L = Tube("Coil6L",36./2.,128./2.,36.,0.,90.)
	#Coil6L.SetPosition([InX,-232.5,-36.])
	Coil6L.SetVirtual()#.SetMaterial("G4_W")
	Coil7L = Tube("Coil7L",36./2.,128./2.,36.,270.,90.)
	#Coil7L.SetPosition([InX,-327.,-36.])
	Coil7L.SetVirtual()#.SetMaterial("G4_W")
	Coil8L = Box("Coil8L",46.,96.103,36.) #46.,93.103,36.)
	#Coil8L.SetPosition([5+46./2.,-280,-36.])
	Coil8L.SetVirtual()#.SetMaterial("G4_W")


	CoilLSum1 =UnionSolid("CoilLSum1",Coil1L,Coil2L,[0,0,0.])
	CoilLSum1.SetVirtual()#SetMaterial("G4_W")
	CoilLSum2 =UnionSolid("CoilLSum2",CoilLSum1,Coil3L,[-232.5,InY,0.])
	CoilLSum2.SetVirtual()#SetMaterial("G4_W")
	CoilLSum3 =UnionSolid("CoilLSum3",CoilLSum2,Coil4L,[-327.5,InY,0.])
	CoilLSum3.SetVirtual()#SetMaterial("G4_W")
	CoilLSum4 =UnionSolid("CoilLSum4",CoilLSum3,Coil5L,[-280,5+46./2.,0.])
	CoilLSum4.SetVirtual()#.SetMaterial("G4_W")
	CoilLSum5 =UnionSolid("CoilLSum5",CoilLSum4,Coil6L,[InX,-232.5,0.])
	CoilLSum5.SetVirtual()#.SetMaterial("G4_W")
	CoilLSum6 =UnionSolid("CoilLSum6",CoilLSum5,Coil7L,[InX,-327.,0.])
	CoilLSum6.SetVirtual()#.SetMaterial("G4_W")
	CoilLSum7 =UnionSolid("CoilLSum7",CoilLSum6,Coil8L,[5+46./2.,-280,0.])
	CoilLSum7.SetVirtual()#.SetMaterial("G4_W")
	#**************************************************************************
	#**************************************************************************


	#**************************************************************************
	# Right Coil **************************************************************
	#**************************************************************************
	Coil1R = Tube("Coil1R",339./2.,431./2.,36.,180.,90.)
	#Coil1R.SetPosition([0,0,36.])
	Coil1R.SetVirtual()#.SetMaterial("G4_W")
	Coil2R = Tube("Coil2R",689./2.,780.999/2.,36.,180.,90.)
	#Coil2R.SetPosition([0,0,36.])
	Coil2R.SetVirtual()#.SetMaterial("G4_W")

	InY = -13.
	Coil3R = Tube("Coil3R",36./2.,128./2.,36.,0.,90.)
	#Coil3R.SetPosition([-232.5,InY,36.])
	Coil3R.SetVirtual()#.SetMaterial("G4_W")
	Coil4R = Tube("Coil4R",36./2.,128./2.,36.,90.,80.)
	#Coil4R.SetPosition([-327.5,InY,36.])
	Coil4R.SetVirtual()#.SetMaterial("G4_W")
	Coil5R = Box("Coil5R",96.103,46.,36.) #,93.103,46.,36.)
	#Coil5R.SetPosition([-280,5+46./2.,36.])
	Coil5R.SetVirtual()#.SetMaterial("G4_W")

	InX = -13.
	Coil6R = Tube("Coil6R",36./2.,128./2.,36.,0.,90.)
	#Coil6R.SetPosition([InX,-232.5,36.])
	Coil6R.SetVirtual()#.SetMaterial("G4_W")
	Coil7R = Tube("Coil7R",36./2.,128./2.,36.,270.,90.)
	#Coil7R.SetPosition([InX,-327.,36.])
	Coil7R.SetVirtual()#.SetMaterial("G4_W")
	Coil8R = Box("Coil8R",46.,96.103,36.) #46.,93.103,36.)
	#Coil8R.SetPosition([5+46./2.,-280,36.])
	Coil8R.SetVirtual()#.SetMaterial("G4_W")

	CoilRSum1 =UnionSolid("CoilRSum1",Coil1R,Coil2R,[0,0,0.])
	CoilRSum1.SetVirtual()#SetMaterial("G4_W")
	CoilRSum2 =UnionSolid("CoilRSum2",CoilRSum1,Coil3R,[-232.5,InY,0.])
	CoilRSum2.SetVirtual()#SetMaterial("G4_W")
	CoilRSum3 =UnionSolid("CoilRSum3",CoilRSum2,Coil4R,[-327.5,InY,0.])
	CoilRSum3.SetVirtual()#SetMaterial("G4_W")
	CoilRSum4 =UnionSolid("CoilRSum4",CoilRSum3,Coil5R,[-280,5+46./2.,0.])
	CoilRSum4.SetVirtual()#.SetMaterial("G4_W")
	CoilRSum5 =UnionSolid("CoilRSum5",CoilRSum4,Coil6R,[InX,-232.5,0.])
	CoilRSum5.SetVirtual()#.SetMaterial("G4_W")
	CoilRSum6 =UnionSolid("CoilRSum6",CoilRSum5,Coil7R,[InX,-327.,0.])
	CoilRSum6.SetVirtual()#.SetMaterial("G4_W")
	CoilRSum7 =UnionSolid("CoilRSum7",CoilRSum6,Coil8R,[5+46./2.,-280,0.])
	CoilRSum7.SetVirtual()#.SetMaterial("G4_W")
	#**************************************************************************
	#**************************************************************************


	# globalX = -400.
	# globalY =  300.
	# globalZ =  200.

	globalCoilPos = PolarPosition(sqrSum([262.112000,0,183.533005]),analyzeAngle,279.688995)
	[globalX,globalY,globalZ] = globalCoilPos

	PolesFinal = UnionSolid("PolesFinal", PoleLMinus9, PoleRMinus9, [0,0,39.238+30.000])
	PolesFinal.SetVirtual()#.SetMaterial(Al)

	YokePlusPoles = UnionSolid("YokePlusPoles", YokeMinus2, PolesFinal, [0,0,-(39.238+30.000)/2])
	YokePlusPoles.SetMaterial(Al)#.SetVirtual()
	YokePlusPoles.SetPosition([globalX+0.,globalY+0.,globalZ+0.])
	YokePlusPoles.SetRotation([0,-90+analyzeAngle,0])

	Coils = UnionSolid("Coils",CoilLSum7,CoilRSum7,[0,0, 2*36.000])
	Coils.SetMaterial("G4_W")

	offsetDist = 36.0
	offX = offsetDist*cos(analyzeTheta)
	offZ = offsetDist*sin(analyzeTheta)
	Coils.SetPosition([globalX-offX,globalY,globalZ-offZ])
	Coils.SetRotation([0,-90+analyzeAngle,0])




	#################################################################
	# Magnet yoke LUMI spectrometer
	#################################################################

	LUMI_Yoke1 = Tube("LUMI_Yoke1",  0.  ,940./2.,350.,180.,90.)
	LUMI_Yoke1.SetVirtual()
	LUMI_Yoke2 = Tube("LUMI_Yoke2",302./2.,460./2.,236.,179.,92.)
	LUMI_Yoke2.SetVirtual()
	LUMI_Yoke3 = Tube("LUMI_Yoke3",660./2.,818./2.,236.,179.,92.)
	LUMI_Yoke3.SetVirtual()
	LUMI_YokeBox = Box("LUMI_YokeBox",  116.1, 116, 352.)
	LUMI_YokeBox.SetVirtual()

	LUMI_YokeBox1 = Box("LUMI_YokeBox1", 300, 20.785, 20.785)
	LUMI_YokeBox1.SetVirtual()#SetMaterial(Al)#.SetVirtual()
	#LUMI_YokeBox1.SetPosition([0,0,-20.785])
	LUMI_YokeBox2 = Box("LUMI_YokeBox2", 300, 40, 40)
	LUMI_YokeBox2.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	LUMI_YokeBoxMinus1 =SubtractionSolid("LUMI_YokeBoxMinus1",LUMI_YokeBox1,LUMI_YokeBox2, [0,-20*sqrt(2)/2,20*sqrt(2)/2], [45,0,0] )
	LUMI_YokeBoxMinus1.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_YokeBoxMinus1.SetPosition([-116/2*sqrt(2)-20.785*sqrt(2)/2,0,-175+20.745/2]) #-116/2*sqrt(2)
	#LUMI_YokeBoxMinus1.SetRotation([0,0,45])

	LUMI_YokeBox3 = Box("LUMI_YokeBox3", 300, 20.785, 20.785)
	LUMI_YokeBox3.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_YokeBox3.SetPosition([0,0,20.785])
	LUMI_YokeBox4 = Box("LUMI_YokeBox4", 300.1, 40, 40)
	LUMI_YokeBox4.SetVirtual()
	LUMI_YokeBoxMinus2 =SubtractionSolid("LUMI_YokeBoxMinus2",LUMI_YokeBox3,LUMI_YokeBox4, [0,-20*sqrt(2)/2,-20*sqrt(2)/2], [45,0,0] )
	LUMI_YokeBoxMinus2.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_YokeBoxMinus1.SetPosition([-116/2*sqrt(2)-20.785*sqrt(2)/2,0,-175+20.745/2]) #-116/2*sqrt(2)
	#LUMI_YokeBoxMinus1.SetRotation([0,0,45])

	LUMI_YokeBox5 = Box("LUMI_YokeBox5", 28.087, 48.087, 350.1)
	LUMI_YokeBox5.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_YokeBox5.SetPosition([-940./2.,0,0])
	#LUMI_YokeBox5.SetRotation([0,0,45])

	LUMI_YokeBox6 = Box("LUMI_YokeBox6", 28.087, 48.087, 350.1)
	LUMI_YokeBox6.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_YokeBox6.SetPosition([-940./2.,0,0])
	#LUMI_YokeBox6.SetRotation([0,0,45])

	                   # name        z           rmin1  rmin2    rmax1  rmax2  sphi  dphi
	LUMI_Cone1 = Cone("LUMI_Cone1",  470.-400,  400.0  ,  470.  , 500. , 600. , 269. , 92.)
	LUMI_Cone1.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_Cone1.SetPosition([0,0,70./2.+175.-18.*sqrt(2)/2])
	#LUMI_Cone1.SetRotation([0,180,0])
	LUMI_Cone2 = Cone("LUMI_Cone2",  470.-400,  400.0  ,  470.  , 500. , 600. ,179. , 92.)
	LUMI_Cone2.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_Cone2.SetPosition([0,0,500+70./2.+175.-18.*sqrt(2)/2])
	#LUMI_Cone2.SetRotation([0,0,0])

	LUMI_Yoke4 = Tube("LUMI_Yoke4",400./2.,800./2.,20.,179.,92.)
	LUMI_Yoke4.SetVirtual()

	                   # name        z           rmin1  rmin2      rmax1  rmax2  sphi  dphi
	LUMI_Cone3 = Cone("LUMI_Cone3",  342.5-312.5,  312.5  ,  342.5  , 350. , 370. , 269. , 92.)
	LUMI_Cone3.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_Cone3.SetPosition([0,0,-20/2.-7.538*sqrt(2)/2])
	#LUMI_Cone3.SetRotation([0,180,0])
	LUMI_Cone4 = Cone("LUMI_Cone4",  342.5-312.5,  312.5  ,  342.5 , 350. , 370. ,179. , 92.)
	LUMI_Cone4.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_Cone4.SetPosition([0.,0,+20/2.+7.538*sqrt(2)/2])
	#LUMI_Cone4.SetRotation([0,0,0])

	                   # name        z           rmin1  rmin2      rmax1  rmax2  sphi  dphi
	LUMI_Cone5 = Cone("LUMI_Cone5",  247.5-217.5, 190. , 220.,  217.5  ,  247.5  , 179. , 92.)
	LUMI_Cone5.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_Cone5.SetPosition([0,0,-20/2.-7.538*sqrt(2)/2])
	#LUMI_Cone5.SetRotation([0,180,0])
	LUMI_Cone6 = Cone("LUMI_Cone6",   247.5-217.5, 190. , 220.,  217.5  ,  247.5 , 269. , 92.)
	LUMI_Cone6.SetVirtual()#.SetMaterial(Al)#.SetVirtual()
	#LUMI_Cone6.SetPosition([0.,0,+20/2.+7.538*sqrt(2)/2])
	#LUMI_Cone6.SetRotation([0,180,0])

	LUMI_YokeBoxL1 = Box("LUMI_YokeBoxL1", 102.1, 28.868, 28.868)    # 30 deg
	LUMI_YokeBoxL1.SetVirtual()#.SetMaterial(Al)#.SetVirtual()       # 30 deg
	#LUMI_YokeBoxL1.SetPosition([-280.,5.3,-10.-5.2])                # 30 deg
	#LUMI_YokeBoxL1.SetRotation([60,0,0])                            # 30 deg
	LUMI_YokeBoxL2 = Box("LUMI_YokeBoxL2", 102.1, 28.868, 28.868)    # 30 deg
	LUMI_YokeBoxL2.SetVirtual()#.SetMaterial(Al)#.SetVirtual()       # 30 deg
	#LUMI_YokeBoxL2.SetPosition([-280.,5.3,10.+5.2])                 # 30 deg
	#LUMI_YokeBoxL2.SetRotation([-60,0,0])                           # 30 deg
	LUMI_YokeBoxL3 = Box("LUMI_YokeBoxL3", 28.868, 102.1, 28.868)    # 30 deg
	LUMI_YokeBoxL3.SetVirtual()#.SetMaterial(Al)#.SetVirtual()       # 30 deg
	#LUMI_YokeBoxL3.SetPosition([5.3,-280.,-10.-5.2])                # 30 deg
	#LUMI_YokeBoxL3.SetRotation([0,30,0])                            # 30 deg
	LUMI_YokeBoxL4 = Box("LUMI_YokeBoxL4", 28.868, 102.1, 28.868)    # 30 deg
	LUMI_YokeBoxL4.SetVirtual()#.SetMaterial(Al)#.SetVirtual()       # 30 deg
	#LUMI_YokeBoxL4.SetPosition([5.3,-280.,10.+5.2])                 # 30 deg
	#LUMI_YokeBoxL4.SetRotation([0,-30,0])                           # 30 deg

	LUMI_YokeBoxL1a = Box("LUMI_YokeBoxL1a", 102.1, 28.868, 28.868)     # 60 deg
	LUMI_YokeBoxL1a.SetVirtual()#.SetMaterial(Al)#                      # 60 deg
	#LUMI_YokeBoxL1a.SetPosition([-280.,-5.25,-4.7])                    # 60 deg
	#LUMI_YokeBoxL1a.SetRotation([30,0,0])                              # 60 deg
	LUMI_YokeBoxL2a = Box("LUMI_YokeBoxL2a", 102.1,  28.868, 28.868)    # 60 deg
	LUMI_YokeBoxL2a.SetVirtual()#.SetMaterial(Al)#.SetVirtual()         # 60 deg
	#LUMI_YokeBoxL2a.SetPosition([-280.,-5.25,4.7])                     # 60 deg
	#LUMI_YokeBoxL2a.SetRotation([-30,0,0])                             # 60 deg
	LUMI_YokeBoxL3a = Box("LUMI_YokeBoxL3a", 28.868, 102.1, 28.868)     # 60 deg
	LUMI_YokeBoxL3a.SetVirtual()#.SetMaterial(Al)#.SetVirtual()         # 60 deg
	#LUMI_YokeBoxL3a.SetPosition([-5.25,-280.,-4.7])                    # 60 deg
	#LUMI_YokeBoxL3a.SetRotation([0,60,0])                              # 60 deg
	LUMI_YokeBoxL4a = Box("LUMI_YokeBoxL4a", 28.868, 102.1, 28.868)     # 60 deg
	LUMI_YokeBoxL4a.SetVirtual()#.SetMaterial(Al)#.SetVirtual()         # 60 deg
	#LUMI_YokeBoxL4a.SetPosition([-5.25,-280.,4.7])                     # 60 deg
	#LUMI_YokeBoxL4a.SetRotation([0,-60,0])                             # 60 deg


	LUMI_YokeMinus1 =SubtractionSolid("LUMI_YokeMinus1",LUMI_Yoke1,LUMI_Yoke2)
	LUMI_YokeMinus1.SetVirtual()
	LUMI_YokeMinus2 =SubtractionSolid("LUMI_YokeMinus2",LUMI_YokeMinus1,LUMI_Yoke3)
	LUMI_YokeMinus2.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus3 =SubtractionSolid("LUMI_YokeMinus3",LUMI_YokeMinus2,LUMI_YokeBox,[0,0,0],[0,0,45])
	LUMI_YokeMinus3.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus4 =SubtractionSolid("LUMI_YokeMinus4",LUMI_YokeMinus3,LUMI_YokeBoxMinus1,[-116/2*sqrt(2)-20.785*sqrt(2)/2,0,-175+20.745/2],[0,0,-45])
	LUMI_YokeMinus4.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus5 =SubtractionSolid("LUMI_YokeMinus5",LUMI_YokeMinus4,LUMI_YokeBoxMinus2,[-116/2*sqrt(2)-20.785*sqrt(2)/2,0,175-20.745/2],[0,0,-45])
	LUMI_YokeMinus5.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus6 =SubtractionSolid("LUMI_YokeMinus6",LUMI_YokeMinus5,LUMI_YokeBox5,[-940./2.+0.1,0,0],[0,0,-45])
	LUMI_YokeMinus6.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus7 =SubtractionSolid("LUMI_YokeMinus7",LUMI_YokeMinus6,LUMI_YokeBox6,[0,-940./2.+0.1,0],[0,0,-45])
	LUMI_YokeMinus7.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus8 =SubtractionSolid("LUMI_YokeMinus8",LUMI_YokeMinus7,LUMI_Cone1,[0,0,70./2.+175.-18.*sqrt(2)/2],[0,180,0])
	LUMI_YokeMinus8.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus9 =SubtractionSolid("LUMI_YokeMinus9",LUMI_YokeMinus8,LUMI_Cone2,[0,0,-70./2.-175.+18.*sqrt(2)/2],[0,0,0])
	LUMI_YokeMinus9.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus10 =SubtractionSolid("LUMI_YokeMinus10",LUMI_YokeMinus9,LUMI_Yoke4,[0,0,0],[0,0,0])
	LUMI_YokeMinus10.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()



	InitBox = Box("InitBox",0.001,0.001,0.001)
	InitBox.SetVirtual()
	InitBox.SetPosition(LUMI_YokeMinus10.GetPosition())

	LUMI_YokeMinus11 =UnionSolid("LUMI_YokeMinus11",InitBox,LUMI_Cone3,[0.,0,-20/2.-7.538*sqrt(2)/2],[0,180,0])
	LUMI_YokeMinus11.SetVirtual()#SetMaterial("G4_W")#.SetVirtual()

	LUMI_YokeMinus12 =UnionSolid("LUMI_YokeMinus12",LUMI_YokeMinus11,LUMI_Cone4,[0.,0,+20/2.+7.538*sqrt(2)/2],[0,0,0])
	LUMI_YokeMinus12.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus13 =UnionSolid("LUMI_YokeMinus13",LUMI_YokeMinus12,LUMI_Cone5,[0,0,-20/2.-7.538*sqrt(2)/2],[0,0,0])
	LUMI_YokeMinus13.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus14 =UnionSolid("LUMI_YokeMinus14",LUMI_YokeMinus13,LUMI_Cone6,[0-0.,0,+20/2.+7.538*sqrt(2)/2],[0,180,0])
	LUMI_YokeMinus14.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus15 =UnionSolid("LUMI_YokeMinus15",LUMI_YokeMinus14,LUMI_YokeBoxL1,[-280.,5.3,-10.-5.2],[-60,0,0])      # 30 deg
	# LUMI_YokeMinus15 =UnionSolid("LUMI_YokeMinus15",LUMI_YokeMinus14,LUMI_YokeBoxL1a,[-280.,-5.25,-4.7],[-30,0,0])        # 60 deg
	LUMI_YokeMinus15.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus16 =UnionSolid("LUMI_YokeMinus16",LUMI_YokeMinus15,LUMI_YokeBoxL2,[-280.,5.3,10.+5.2],[60,0,0])        # 30 deg
	# LUMI_YokeMinus16 =UnionSolid("LUMI_YokeMinus16",LUMI_YokeMinus15,LUMI_YokeBoxL2a,[-280.,-5.25,4.7],[30,0,0])          # 60 deg
	LUMI_YokeMinus16.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus17 =UnionSolid("LUMI_YokeMinus17",LUMI_YokeMinus16,LUMI_YokeBoxL3,[5.3,-280.,-10.-5.2],[0,-30,0])       # 30 deg
	# LUMI_YokeMinus17 =UnionSolid("LUMI_YokeMinus17",LUMI_YokeMinus16,LUMI_YokeBoxL3a,[-5.25,-280.,-4.7],[0,-60,0])       # 60 deg
	LUMI_YokeMinus17.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
	LUMI_YokeMinus18 =SubtractionSolid("LUMI_YokeMinus18",LUMI_YokeMinus17,LUMI_YokeBoxL4,[5.3,-280.,10.+5.2],[0,30,0])       # 30 deg TEST
	# LUMI_YokeMinus18 =SubtractionSolid("LUMI_YokeAndPoles",LUMI_YokeMinus17,LUMI_YokeBoxL4a,[-5.25,-280.,4.7],[0,60,0])       # 60 deg
	LUMI_YokeMinus18.SetVirtual()


	LUMI_YokeAndPoles =SubtractionSolid("LUMI_YokeAndPoles",LUMI_YokeMinus10,LUMI_YokeMinus18)       # 30 deg TEST
	# LUMI_YokeAndPoles =SubtractionSolid("LUMI_YokeAndPoles",LUMI_YokeMinus17,LUMI_YokeBoxL4a,[-5.25,-280.,4.7],[0,60,0])       # 60 deg
	LUMI_YokeAndPoles.SetMaterial("G4_W")


#BACKUP COPY!!!!
# LUMI_YokeMinus11 =SubtractionSolid("LUMI_YokeMinus11",LUMI_YokeMinus10,LUMI_Cone3,[0.,0,-20/2.-7.538*sqrt(2)/2],[0,180,0])
# LUMI_YokeMinus11.SetVirtual()#SetMaterial("G4_W")#.SetVirtual()
# LUMI_YokeMinus12 =SubtractionSolid("LUMI_YokeMinus12",LUMI_YokeMinus11,LUMI_Cone4,[0.,0,+20/2.+7.538*sqrt(2)/2],[0,0,0])
# LUMI_YokeMinus12.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
# LUMI_YokeMinus13 =SubtractionSolid("LUMI_YokeMinus13",LUMI_YokeMinus12,LUMI_Cone5,[0,0,-20/2.-7.538*sqrt(2)/2],[0,0,0])
# LUMI_YokeMinus13.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
# LUMI_YokeMinus14 =SubtractionSolid("LUMI_YokeMinus14",LUMI_YokeMinus13,LUMI_Cone6,[0-0.,0,+20/2.+7.538*sqrt(2)/2],[0,180,0])
# LUMI_YokeMinus14.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
# LUMI_YokeMinus15 =SubtractionSolid("LUMI_YokeMinus15",LUMI_YokeMinus14,LUMI_YokeBoxL1,[-280.,5.3,-10.-5.2],[-60,0,0])      # 30 deg
# # LUMI_YokeMinus15 =SubtractionSolid("LUMI_YokeMinus15",LUMI_YokeMinus14,LUMI_YokeBoxL1a,[-280.,-5.25,-4.7],[-30,0,0])        # 60 deg
# LUMI_YokeMinus15.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
# LUMI_YokeMinus16 =SubtractionSolid("LUMI_YokeMinus16",LUMI_YokeMinus15,LUMI_YokeBoxL2,[-280.,5.3,10.+5.2],[60,0,0])        # 30 deg
# # LUMI_YokeMinus16 =SubtractionSolid("LUMI_YokeMinus16",LUMI_YokeMinus15,LUMI_YokeBoxL2a,[-280.,-5.25,4.7],[30,0,0])          # 60 deg
# LUMI_YokeMinus16.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()
# LUMI_YokeMinus17 =SubtractionSolid("LUMI_YokeMinus17",LUMI_YokeMinus16,LUMI_YokeBoxL3,[5.3,-280.,-10.-5.2],[0,-30,0])       # 30 deg
# # LUMI_YokeMinus17 =SubtractionSolid("LUMI_YokeMinus17",LUMI_YokeMinus16,LUMI_YokeBoxL3a,[-5.25,-280.,-4.7],[0,-60,0])       # 60 deg
# LUMI_YokeMinus17.SetVirtual()#.SetMaterial("G4_W")#.SetVirtual()





#These make it break for some reason!
	# # LUMI_YokeAndPoles =SubtractionSolid("LUMI_YokeAndPoles",LUMI_YokeMinus17,LUMI_YokeBoxL4,[5.3,-280.,10.+5.2],[0,30,0])       # 30 deg
	# LUMI_YokeAndPoles =SubtractionSolid("LUMI_YokeAndPoles",LUMI_YokeMinus17,LUMI_YokeBoxL4,[5.3,-280.,10.+5.2],[0,30,0])       # 30 deg TEST
	# # LUMI_YokeAndPoles =SubtractionSolid("LUMI_YokeAndPoles",LUMI_YokeMinus17,LUMI_YokeBoxL4a,[-5.25,-280.,4.7],[0,60,0])       # 60 deg
	# LUMI_YokeAndPoles.SetMaterial("G4_W")
	# # LUMI_YokeAndPoles.SetMaterial(Al)

	#Test_Box = Box("Test_Box", 40., 40., 93.566)
	#Test_Box.SetMaterial(Al)#.SetVirtual()#
	#Test_Box.SetPosition([-330.,20.,-175.+57. + 93.566/2.])


	#**************************************************************************
	# Left LUMI_Coil ***************************************************************
	#**************************************************************************
	LUMI_Coil1L = Tube("LUMI_Coil1L",320./2.,440./2.,105.,180.,90.)
	#LUMI_Coil1L.SetPosition([0,0,-36.])
	LUMI_Coil1L.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil2L = Tube("LUMI_Coil2L",680./2.,800./2.,105.,180.,90.)
	#LUMI_Coil2L.SetPosition([0,0,-36.])
	LUMI_Coil2L.SetVirtual()#.SetMaterial("G4_W")

	InY = -5.
	LUMI_Coil3L = Tube("LUMI_Coil3L",20./2.,140./2.,105.,0.,90.)
	#LUMI_Coil3L.SetPosition([-232.5,InY,-36.])
	LUMI_Coil3L.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil4L = Tube("LUMI_Coil4L",20./2.,140./2.,105.,90.,90.)
	#LUMI_Coil4L.SetPosition([-327.5,InY,-36.])
	LUMI_Coil4L.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil5L = Box("LUMI_Coil5L",104.066,60.,105.) #,93.103,60.,105.)
	#LUMI_Coil5L.SetPosition([-280,5+60./2.,-36.])
	LUMI_Coil5L.SetVirtual()#.SetMaterial("G4_W")

	InX = -5.
	LUMI_Coil6L = Tube("LUMI_Coil6L",20./2.,140./2.,105.,0.,90.)
	#LUMI_Coil6L.SetPosition([InX,-232.5,-36.])
	LUMI_Coil6L.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil7L = Tube("LUMI_Coil7L",20./2.,140./2.,105.,270.,90.)
	#LUMI_Coil7L.SetPosition([InX,-327.,-36.])
	LUMI_Coil7L.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil8L = Box("LUMI_Coil8L",60.,104.066,105.) #60.,93.103,105.)
	#LUMI_Coil8L.SetPosition([5+60./2.,-280,-36.])
	LUMI_Coil8L.SetVirtual()#.SetMaterial("G4_W")


	LUMI_CoilLSum1 =UnionSolid("LUMI_CoilLSum1",LUMI_Coil1L,LUMI_Coil2L,[0,0,0.])
	LUMI_CoilLSum1.SetVirtual()#SetMaterial("G4_W")
	LUMI_CoilLSum2 =UnionSolid("LUMI_CoilLSum2",LUMI_CoilLSum1,LUMI_Coil3L,[-229.0,InY,0.])
	LUMI_CoilLSum2.SetVirtual()#SetMaterial("G4_W")
	LUMI_CoilLSum3 =UnionSolid("LUMI_CoilLSum3",LUMI_CoilLSum2,LUMI_Coil4L,[-330.0,InY,0.])
	LUMI_CoilLSum3.SetVirtual()#SetMaterial("G4_W")
	LUMI_CoilLSum4 =UnionSolid("LUMI_CoilLSum4",LUMI_CoilLSum3,LUMI_Coil5L,[-280,5+60./2.,0.])
	LUMI_CoilLSum4.SetVirtual()#.SetMaterial("G4_W")
	LUMI_CoilLSum5 =UnionSolid("LUMI_CoilLSum5",LUMI_CoilLSum4,LUMI_Coil6L,[InX,-229.0,0.])
	LUMI_CoilLSum5.SetVirtual()#.SetMaterial("G4_W")
	LUMI_CoilLSum6 =UnionSolid("LUMI_CoilLSum6",LUMI_CoilLSum5,LUMI_Coil7L,[InX,-330.0,0.])
	LUMI_CoilLSum6.SetVirtual()#.SetMaterial("G4_W")
	LUMI_CoilLSum7 =UnionSolid("LUMI_CoilLSum7",LUMI_CoilLSum6,LUMI_Coil8L,[5+60./2.,-280,0.])
	LUMI_CoilLSum7.SetVirtual()#.SetMaterial("G4_W")
	#**************************************************************************
	#**************************************************************************


	#**************************************************************************
	# Right LUMI_Coil **************************************************************
	#**************************************************************************
	LUMI_Coil1R = Tube("LUMI_Coil1R",320./2.,440./2.,105.,180.,90.)
	#LUMI_Coil1R.SetPosition([0,0,36.])
	LUMI_Coil1R.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil2R = Tube("LUMI_Coil2R",680./2.,800./2.,105.,180.,90.)
	#LUMI_Coil2R.SetPosition([0,0,36.])
	LUMI_Coil2R.SetVirtual()#.SetMaterial("G4_W")

	InY = -5.
	LUMI_Coil3R = Tube("LUMI_Coil3R",20./2.,140./2.,105.,0.,90.)
	#LUMI_Coil3R.SetPosition([-232.5,InY,36.])
	LUMI_Coil3R.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil4R = Tube("LUMI_Coil4R",20./2.,140./2.,105.,90.,90.)
	#LUMI_Coil4R.SetPosition([-327.5,InY,36.])
	LUMI_Coil4R.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil5R = Box("LUMI_Coil5R",104.066,60.,105.) #,93.103,60.,105.)
	#LUMI_Coil5R.SetPosition([-280,5+60./2.,36.])
	LUMI_Coil5R.SetVirtual()#.SetMaterial("G4_W")

	InX = -5.
	LUMI_Coil6R = Tube("LUMI_Coil6R",20./2.,140./2.,105.,0.,90.)
	#LUMI_Coil6R.SetPosition([InX,-232.5,36.])
	LUMI_Coil6R.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil7R = Tube("LUMI_Coil7R",20./2.,140./2.,105.,270.,90.)
	#LUMI_Coil7R.SetPosition([InX,-327.,36.])
	LUMI_Coil7R.SetVirtual()#.SetMaterial("G4_W")
	LUMI_Coil8R = Box("LUMI_Coil8R",60.,104.066,105.) #60.,93.103,105.)
	#LUMI_Coil8R.SetPosition([5+60./2.,-280,36.])
	LUMI_Coil8R.SetVirtual()#.SetMaterial("G4_W")

	LUMI_CoilRSum1 =UnionSolid("LUMI_CoilRSum1",LUMI_Coil1R,LUMI_Coil2R,[0,0,0.])
	LUMI_CoilRSum1.SetVirtual()#SetMaterial("G4_W")
	LUMI_CoilRSum2 =UnionSolid("LUMI_CoilRSum2",LUMI_CoilRSum1,LUMI_Coil3R,[-229.0,InY,0.])
	LUMI_CoilRSum2.SetVirtual()#SetMaterial("G4_W")
	LUMI_CoilRSum3 =UnionSolid("LUMI_CoilRSum3",LUMI_CoilRSum2,LUMI_Coil4R,[-330.0,InY,0.])
	LUMI_CoilRSum3.SetVirtual()#SetMaterial("G4_W")
	LUMI_CoilRSum4 =UnionSolid("LUMI_CoilRSum4",LUMI_CoilRSum3,LUMI_Coil5R,[-280,5+60./2.,0.])
	LUMI_CoilRSum4.SetVirtual()#.SetMaterial("G4_W")
	LUMI_CoilRSum5 =UnionSolid("LUMI_CoilRSum5",LUMI_CoilRSum4,LUMI_Coil6R,[InX,-229.0,0.])
	LUMI_CoilRSum5.SetVirtual()#.SetMaterial("G4_W")
	LUMI_CoilRSum6 =UnionSolid("LUMI_CoilRSum6",LUMI_CoilRSum5,LUMI_Coil7R,[InX,-330.0,0.])
	LUMI_CoilRSum6.SetVirtual()#.SetMaterial("G4_W")
	LUMI_CoilRSum7 =UnionSolid("LUMI_CoilRSum7",LUMI_CoilRSum6,LUMI_Coil8R,[5+60./2.,-280,0.])
	LUMI_CoilRSum7.SetVirtual()#.SetMaterial("G4_W")
	#**************************************************************************
	#**************************************************************************




	LUMI_Coils = UnionSolid("LUMI_Coils",LUMI_CoilLSum7,LUMI_CoilRSum7,[0,0,26+105])
	LUMI_Coils.SetMaterial(Al)#..SetVirtual()
	LUMI_OffsetDist = (26+105)/2
	LUMI_Angle = -90-lumiAngle
	LUMI_Theta = degToRad(LUMI_Angle)
	LUMI_Dist = sqrSum([484.012,225.698])#634.585 #distance to yoke plane in mm
	LUMI_Pos0 = PolarPosition(LUMI_Dist,-lumiAngle,280)

	LUMI_Coils.SetPosition(add(LUMI_Pos0,[LUMI_OffsetDist*sin(LUMI_Theta),0,-LUMI_OffsetDist*cos(LUMI_Theta)]))
	LUMI_Coils.SetRotation([0,LUMI_Angle,0])
	LUMI_YokeAndPoles.SetRotation([0,LUMI_Angle,0])
	LUMI_YokeAndPoles.SetPosition(LUMI_Pos0)
	print LUMI_YokeAndPoles.GetPosition()
#################################################################
# Vacuum Solids
#################################################################

# Turning off internal vacuum for now
print "Building Internal Vacuum"

VacInit = Box("VacInit",1,1,1)
VacInit.SetVirtual()

VacBoundary0 = Box("VacInit",1,1,1)#Tube("VacBoundary0",0,49.41-5,conflatThk+10,0,360)
VacBoundary0.SetPosition(ExitFConflatVac.GetPosition())
VacBoundary0.SetRotation(ExitFConflatVac.GetRotation())
VacBoundary0.SetVirtual()

VacBoundary01 = CopyObject(VacBoundary0)
VacBoundary0.SetPosition(BellowsConflat.GetPosition())
VacBoundary0.SetRotation(BellowsConflat.GetRotation())
VacBoundary01.SetVirtual()

VacBoundary1 = Box("VacInit",1,1,1)#Tube("VacBoundary1",0,49.41-7,1.5*conflatThk,0,360)
VacBoundary1.SetPosition([i-j for i,j in zip(BellowsConflat.GetPosition(),PolarPosition(conflatThk,analyzeAngle,0))])
VacBoundary1.SetRotation(BellowsConflat.GetRotation())
VacBoundary1.SetVirtual()

VacBoundary2 = Box("VacInit",1,1,1)#Tube("VacBoundary2",0,49.41-7,conflatThk,0,360)
VacBoundary2.SetPosition([i+j for i,j in zip(ExitFConflatVac.GetPosition(),PolarPosition(conflatThk,nominalAngle,0))])
VacBoundary2.SetRotation(ExitFConflatVac.GetRotation())
VacBoundary2.SetVirtual()

FCVac = FinalConflat.FillVacuum()
FCVac.SetVirtual()

FinalOverlapVac = Box("VacInit",1,1,1)#Box("FinalOverlapVac",25.4,5,82.55)
FinalOverlapVac.SetPosition([i-j for i,j in zip(FinalVacuum.GetPosition(),[0,fVacHeight/2,0])])
FinalOverlapVac.SetRotation(FinalVacuum.GetRotation())
FinalOverlapVac.SetVirtual()

VacInit2 = Box("VacInit",1,1,1)
VacInit2.SetVirtual()

VacInit3 = Box("VacInit",1,1,1)
VacInit3.SetVirtual()

VInit1 = CopyObject(VacInit)
VInit1.SetName("VInit1")
VAC_OBJECTS1 = [VInit1,TargetChamberVacuum,UpstreamBeampipeVacuum,DownstreamBeampipeVacuum]

VInit2 = CopyObject(VacInit)
VInit2.SetName("VInit2")
VObj1p5 = [VInit2,DS_FLG1Vac,DS_FLG2Vac, DSBPCVac,DSBPConeVac,DSBPLTVac,LumiBeampipeVacuum]

VInit3 = CopyObject(VacInit)
VInit3.SetName("VInit3")
VObj1p75 = [VInit3,LFC1Vac,Lumi_FLG1Vac,Lumi_FLG2Vac,LumiBP2Vac]

VInit4 = CopyObject(VacInit)
VInit4.SetName("VInit4")
VAC_OBJECTS2 = [VInit4,LVacSPeVac,LVacCornerVac]

VInit5 = CopyObject(VacInit)
VInit5.SetName("VInit5")
VAC_OBJECTS2p5 = [VInit5,ExitFCVacuum,ExitFT_VAC,ExitF_CF_VAC,ExitF2_CF_VAC,ExitFTC_VAC,ExtraVac]

VInit6 = CopyObject(VacInit)
VInit6.SetName("VInit6")
VAC_OBJECTS2p75 = [VInit6,BellowsVac,BCVac,RPVac]#

VInit7 = CopyObject(VacInit)
VInit7.SetName("VInit7")
VAC_OBJECTS3 = [VInit7,CollimatorVac,RTSVac,RP2Vac,FCVac]

VInit8 = CopyObject(VacInit)
VInit8.SetName("VInit8")
VAC_OBJECTS4 = [VInit8,VacBoundary0,VacBoundary01,VacBoundary1]

VInit9 = CopyObject(VacInit)
VInit9.SetName("VInit9")
VAC_OBJECTS5 = [VInit9,VacBoundary2,fvEncVac,SCVACALL]##SCVac # FinalOverlapVac,FinalVacuum

VAC1 = UnionChain(VAC_OBJECTS1).MakeUnionSolids()
# VAC1.SetVirtual()
VAC1.SetMaterial(Vac)
VAC1.SetPosition([0,0,0])
VAC1.SetRotation([0,0,0])

VAC1p5 = UnionChain(VObj1p5).MakeUnionSolids()
# VAC1p5.SetVirtual()
VAC1p5.SetMaterial(Vac)
VAC1p5.SetPosition([0,0,0])
VAC1p5.SetRotation([0,0,0])

VAC1p75 = UnionChain(VObj1p75).MakeUnionSolids()
# VAC1p75.SetVirtual()
VAC1p75.SetMaterial(Vac)
VAC1p75.SetPosition([0,0,0])
VAC1p75.SetRotation([0,0,0])

VAC2 = UnionChain(VAC_OBJECTS2).MakeUnionSolids()
# VAC2.SetVirtual()
VAC2.SetMaterial(Vac)
VAC2.SetPosition([0,0,0])
VAC2.SetRotation([0,0,0])

VAC2p5 = UnionChain(VAC_OBJECTS2p5).MakeUnionSolids()
# VAC2p5.SetVirtual()
VAC2p5.SetMaterial(Vac)
VAC2p5.SetPosition([0,0,0])
VAC2p5.SetRotation([0,0,0])

VAC2p75 = UnionChain(VAC_OBJECTS2p75).MakeUnionSolids()
# VAC2p75.SetVirtual()
VAC2p75.SetMaterial(Vac)
VAC2p75.SetPosition([0,0,0])
VAC2p75.SetRotation([0,0,0])

VAC3 = UnionChain(VAC_OBJECTS3).MakeUnionSolids()
# VAC3.SetVirtual()
VAC3.SetMaterial(Vac)
VAC3.SetPosition([0,0,0])
VAC3.SetRotation([0,0,0])

VAC4 = UnionChain(VAC_OBJECTS4).MakeUnionSolids()
# VAC4.SetVirtual()
VAC4.SetMaterial(Vac)
VAC4.SetPosition([0,0,0])
VAC4.SetRotation([0,0,0])

VAC5 = UnionChain(VAC_OBJECTS5).MakeUnionSolids()
# VAC5.SetVirtual()
VAC5.SetMaterial(Vac)
VAC5.SetPosition([0,0,0])
VAC5.SetRotation([0,0,0])






#This makes it break for some reason!?!?!?
# VAC_ALL = UnionChain([VAC1,VAC2,VAC2p5,VAC2p75,VAC3,VAC4,VAC5]).MakeUnionSolids()#CTBVac,BTCVac,ExitFConeVacuum,ExitFConflatVac
# VAC_ALL.SetMaterial(Vac)
# VAC_ALL.SetPosition([0,0,0])
# VAC_ALL.SetRotation([0,0,0])


#################################################################
# Air shields instead of vacuum (maybe this will work better)
#################################################################






print "Done!"
MollerExp.Save()
