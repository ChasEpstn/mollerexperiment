#!/usr/bin/env python
import os
import subprocess,shlex
import argparse
dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path)
parser = argparse.ArgumentParser(description='Set the angle')
parser.add_argument('angle',metavar='ang',type=str,nargs=1,help='specify the angle')
args = parser.parse_args()
print 'The angle is '+args.angle[0]


cmd1 = 'python MollerDetector.py '+args.angle[0]
gDir = '../python_gdml_generator'
cmd2 = 'python ../python_gdml_generator/generateGDML.py'
cmd3 = 'root -l ../Draw.C'

subprocess.call(shlex.split(cmd1))
subprocess.call(shlex.split(cmd2),cwd=gDir)
#subprocess.call(shlex.split(cmd3))
# os.system(cmd1)
# os.chdir(gDir)
# os.system(cmd2)
